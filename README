#GUIA BÀSICA PER TREBALLAR AMB EL PROJECTE PYXEL ALLOTJAT A BITBUCKET
Versió 1.3

Data: 21 de novembre de 2014

Curadors: Irene Gris (Universitat Politecnica de Catalunya i Xavier Giró (Universitat Politecnica de Catalunya)

Antics col.laboradors: Daniel Manchon (Pixable), Amaia Salvador (Universitat Politècnica de Catalunya) i Eva Mohedano (Dublin City University).


CONFIGURACIÓ DE L'ENTORN DE TREBALL AMB GIT

1 - Creeu-vos un compte a https://bitbucket.org. Si sou desenvolupadors oficials de Pyxel (projectistes), doneu el vostre nom d'usuari a l'Eva Mohedano i a en Xavi Giró perquè us hi donin accés.

2 - Si no teniu el programari git, instal.leu-lo ja que s'encarregarà de la comunicació entre la còpia en local del codi i 
la versió que es desa al repositori. 
Podeu comprovar si ja el teniu instal·lat escrivint 'git' directament des del terminal.
Si treballeu amb les màquines del Grup de Processament de la Imatge de la UPC a través del SSH o NX, no cal que l'instal·leu, ja hi és.

3 - Creeu-vos una carpeta en el vostre ordinador local on voleu descarregar-vos el codi del repositori.
S'aconsella que aquesta carpeta sigui [$DIRECTORI_USUARI]/workspace/pyxel.

4 - Descarregeu-vos el repositori al vostre directori local. D'aquesta operació s'anomena "clone", i el mateix bitbucket ja us
dóna la instrucció que cal que executeu. Serà similar al següent:
---> git clone https://emohe@bitbucket.org/emohe/pyxel.git

5 - Ja teniu el repositori en local.


QUÈ SÓN LES BRANQUES I COM M'HI PUC MOURE ?

El repositori pot contenir múltiples versions, que en terminologia 'git' s'anomenen 'branques'.
Hi ha moltes formes de treballar amb branques. Típicament, hi ha una branca 'estable' que anomenen 'master'.
Totes les noves modificacions al codi que estan plenament operatives les trobareu al 'master'.
Mentre treballeu, però, avançareu amb els vostres projectes poc a poc. No és bo doncs que modifiqueu el codi del 'master',
ja que això generaria versions inestables del codi que afectarien a la resta de desenvolupadors.
Per aquest motiu la forma de treballar és crear una branca per cada nova funcionalitat, o per cada usuari.
Típicament, els desenvolupadors comencen una branca personal amb el seu nom, però amb el temps, si desenvolupen diverses funcionalitats alhora
acaben creant una branca per cada nova funcionalitat que vulguin generar. Podeu fer el que preferiu.

Per crear una nova branca cal que feu el següent:

—> git checkout -b [la vostra branca nova] [branca d'origen, típicament, serà la "master"]

El -b serveix per indicar que la branca és nova. Si la branca ja està creada, només cal que feu:

—> git checkout [la vostra branca]

Per exemple, per mirar què hi ha a la branca del master, podeu activar-la amb la següent ordre:

—> git checkout master


COM PUC MODIFICAR LA MEVA BRANCA DE TREBALL

Una vegada tingueu la vostra branca de treball, podeu crear nous fitxers o editar els fitxers els existents.
Si creeu fitxers nous, haureu d'indicar-li al git que voleu que els tingui en compte, amb la següent ordre:

—> git add [nom del fitxer]

Si voleu anar per feina, podeu indicar-li que voleu que afegeixi tots els fitxers que pengen del directori on sigueu amb:

—> git add .

Val a dir que els repositoris de git poden, per defecte, ignorar certs tipus de fitxers perquè no es desin al repositori.
Per exemple, quan treballem amb Python, sovint es creen uns blocs de codi compilats amb l'extensió .pyc que no volem
desar al repositori. Per aquest motiu a l'arrel del projecte Pyxel hi trobareu un fitxer .gitignore que indica les extensions
dels fitxers que s'ignoraran.

Tant si heu creat fitxers nous com si n'heu editat de ja existents, cada vegada que completeu una tasca, és MOLT RECOMANABLE
que en prengueu nota. D'aquesta forma, en el futur, si voleu recuperar versions antigues dels fitxers us serà molt més senzill.
"Prendre nota" en llenguatge git vol dir "fer un commit", i s'executa amb la següent instrucció:

—> git commit -m “[Missatge descriptiu i entenedor del les millores que heu assolit]”

Pots fer tants commits com vulguis però aquestes notes sempre quedaran desades en local, en el mateix ordinador on estiguis treballant.
Així, cap dels teus companys podrà veure-les ja que no estaran el servidor. Per tal de pujar-les al repositori del Bitbucket
cal que executis un "push":

—> git push

Si treballes amb més d'una còpia local del codi com, per exemple, en el teu ordinador personal i en el servidor del GPI, voldràs que
si avances des d'una de les màquines, després puguis seguir treballant amb l'altre entorn on ho havies deixat.
Caldrà que explícitament li diguis que vols que es descarregui la versió del codi que hi ha al repositori amb l'operació contrària,
el "pull":

—> git pull


COM PUC PASSAR LES APORTACIONS QUE HI HA A LA MEVA BRANCA CAP AL MASTER ?

Una vegada hagis arribat a un punt de desenvolupament on el teu codi el donis per estable i interessant per a terceres persones,
haurà arribat el moment de passar-lo cap a la branca del master des d'on tots els desenvolupadors pengen.

Abans de fer-ho, però, t'has d'assegurar que les modificacions que puguin haver-hi hagut al master mentre tu desenvolupaves
no entrin en conflicte amb les modificacions del codi que tu has estat fent a la teva branca. Cal doncs que abans de res importis
la darrera versió del codi del master cap a la teva branca de treball. Aquesta operació s'anomena "rebase" 
(hi ha una operació anomenada "merge" similar, però que no gestiona el registre dels canvis d'una forma tan neta).

—> git rebase master

Recordeu que després dels canvis que us hagin pogut venir des del master, haureu també de desar-los al repositori amb un "push":

—> git push

Finalment, ja estareu preparats per portar el vostre codi cap al master.
Donada la sensibilitat de la branca master tenim aplicada una política de seguretat amb la qual no tots els usuaris poden escriure-hi.
El que haureu de fer és demanar que alguns dels administradors del projecte (Eva, Xavi...) portin cap a master les vostres aportacions.
Per fer-ho, utilitzeu la interfície web del BitBucket i aneu a la secció de "Branches".
Al botó de "..." escolliu l'opció de "Pull Request". 
Demaneu-lo des d'allà i, si veieu que en 24 horesno us l'han resolt, envieu un correu-e a l'Eva i a en Xavi.

Més detalls sobre el rebase: http://git-scm.com/book/en/Git-Branching-Rebasing
Més detalls sobre el pull-request: https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests


DOCUMENTACIÓ ADDICIONAL

Guia basica: http://rogerdudler.github.io/git-guide/index.es.html

Gestió de projectes amb branques: http://nvie.com/posts/a-successful-git-branching-model/

Per afegir canvis de fitxers esborrats:

git rm $(git ls-files --deleted)

# DOCUMENTACIÓ DESFASADA (ignoreu-la si no sabeu exactament a què es refereix)
(look at mediaeval2014 as an example)

- Create the project structure inside tools folder
  mkdir -p tools/socialevent/mediaeval2014
- Make sure each folder in the tree has __init__.py file, we need it to import the modules from the root folder
  cp __init__.py tools/
  cp __init__.py tools/socialevent
  cp __init__.py tools/socialevent/mediaeval2014
- Create the requirements.txt file with the dependencies in the root of the project
- Now create a new virtual enviroment:
  virtualenv venv
- Set the new enviroment:
  source venv/bin/activate
- And install the dependencies (to install numpy and scipy from sources you need to install gfortran libs before):
  pip install -r tools/socialevent/mediaeval2014/requirements.txt
- Now create the app itself, that should have a "run" method
- If you want add a init file with the parameters and paths
- And run your app with (default enviroment is prod):
  python run.py --path tools.socialevent.mediaeval2014
- To exit the virtual enviroment run:
  deactivate