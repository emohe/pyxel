# -*- coding: utf-8 -*-

import numpy
from modules.classification.LabelType import LabelType
from modules.classification.Instance import Instance


class AnnotatedSemanticClass:
    """ This class contains, for a specific semantic class, the imageIDs of
        the positive, neutral and negative labels, organised in three different
        dictionaries. """ 
                      
    def __init__(self):
                               
        # Create a dictionary for each type of Label
        self.dicLabels = {}
        
        # Enums are only defined starting at Python 3.4
        # Declare a list for each type of label
        #for labelType in LabelType:            
        #    self.dicLabels[ labelType ] = []
            
        # Create an empty list to store the instances associated to each label
        self.dicLabels[ LabelType.POSITIVE ] = []
        self.dicLabels[ LabelType.NEUTRAL ] = []
        self.dicLabels[ LabelType.NEGATIVE ] = []

            
        
    def addInstance( self, labelType, docId, score=1.0 ):

        self.list = []
            
        
    def addInstance( self, labelType, docId, score=1.0 ):
        
        # Get the list of instances associated to the provided label
        listLabel = self.dicLabels[ labelType ]  
        
        # Define a new Instance
        instance = Instance( docId, score )
        
        # Add the docId to the list of associated documents
        listLabel.append( instance )
        
    def getNofDocsInClass(self, labelType ):
        
        # Get the list of instances associated to the provided label
        listLabel = self.dicLabels[ labelType ]
        
        # Return the length of that distance
        return len( listLabel )
        
    def getDocIdsInClass( self, labelType ):
        
        # Return the list of doc IDs associated to the provided label
        listDocIds = []
        
        listInstances = self.dicLabels[ labelType ]   
        
        # Get the docId for each instance in the considered list
        for instance in listInstances:
            docId = instance.getDocId()
            listDocIds.append( docId )
        
        return listDocIds
        
    def containsDocId( self, labelType, docId ):
        
        listDocIds = self.getDocIdsInClass( labelType )        
        
        docCounts = listDocIds.count( docId )
        
        if docCounts > 0:
            return True
        else:
            return False

    def addPos(self):

        self.list.append(1)
        #print self.list


    def addNeg(self):

        self.list.append(0)
        #print self.list
        
    def getList(self):

        return self.list

