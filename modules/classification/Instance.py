# -*- coding: utf-8 -*-

class Instance:
    
    def __init__(self, docId, score=1.0):
        
        self.docId = docId
        self.score = score

    def getDocId(self):
        return self.docId
        
    def getScore(self):
        return self.score