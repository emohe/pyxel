# -*- coding: utf-8 -*-
"""
Created on Mon Apr  7 16:03:55 2014

@author: xgiro
"""

import pandas as pd
import nltk
import os
import pickle  

class TextVocabulary:
    """ Creates a visual vocabulary and quantises visual features """
    
    def __init__(self, pathFile=None ):
        
        self.most_freq_tags = []
        
        # If a path file is provided...
        if pathFile !=None:
            # ...read from disk
            self.loadFromDisk( pathFile )
            
    def buildFromTags( self, pathFileTags, numOfTerms ):

        data_csv=pd.read_csv('%s' % (pathFileTags), sep='\t')
        
        
        all_train_tags = data_csv['tag']
        
        # Obtain the tags freq
        all_train_freq_tags = nltk.FreqDist(w.lower() for w in all_train_tags)
        
        # get only the 2000 most freq tags
        self.most_freq_tags = all_train_freq_tags.keys()[:numOfTerms]
            
    def get_most_freq_tags(self):
        return self.most_freq_tags
         
    def loadFromDisk(self, pathFile):
        
        if not os.path.exists( pathFile):
            print "File not found " + pathFile
            return
            
        self.most_freq_tags = pickle.load( open( pathFile, "rb" ) )
        
    def saveToDisk(self, pathFile):
        
        # Save mini batch K-Means to disk using Pickle
        pickle.dump( self.most_freq_tags, open( pathFile, "wb" ) )