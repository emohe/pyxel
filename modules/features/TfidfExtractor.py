import os
import pickle
from sklearn.feature_extraction import DictVectorizer

from collections import defaultdict
from modules.socialevent.classification.Metadata import Metadata
from modules.features.TextVocabulary import TextVocabulary

class TfidfExtractor:
    """ Extracts the TFIDF features from an image or collection of images and, if desired,
    it saves them to disk """
    
    def __init__(self, pathMetadata,pathFileTags, pathFileVocabulary, flagSaveInMemory=False, flagVerbose=False):
        
        # Init the OpenCV class that extracts SIFT descriptors
        self.vec = DictVectorizer()
        # I think that DictVectorizer is used to create a vector from a dict
        self.metadata = Metadata(pathMetadata, pathFileTags)
        
        self.textVocabulary = TextVocabulary(pathFileVocabulary)        
        
        # Array to save descriptors
        self.descriptors = []
        
        # for each tag create an entry in the dict with False value
        self.features_void = {}
        for word in self.textVocabulary.get_most_freq_tags():
            self.features_void[word] = False
        
        self.flagSaveInMemory = flagSaveInMemory
        self.flagVerbose = flagVerbose
        
    
    def feature_extractor( self, imageId):
        document_words = []
        tags = self.metadata.get_tags_by_id(imageId)
        if tags!=None:
            document_words = set(tags)
        
        # now copy the supervector dict ( but not linking both)
        features = defaultdict(lambda:False, self.features_void)

        for word in document_words:
            if(features.has_key(word)):
                features[word] = True
        # return a dictionary
        
        return features        
    
    def processTxtFile( self, pathTxtFile, pathDirTfidfs=None):
        
        if self.flagVerbose:
            print 'Reading images from ' + pathTxtFile + "..."
        
        # Read the file containing the image IDs
        fileDataset = open( pathTxtFile, 'r')
        
        # Read lines from the text file, stripping the end of line character
        imageIds = [line.strip() for line in fileDataset ]
        
        # Close file
        fileDataset.close()
        
        # Process the collection of image IDs
        self.processCollectionFilesData( imageIds, pathDirTfidfs )

        
    def processCollectionFilesData( self, imageIds, pathDirTfidf=None ):
        
        # For each image ID listed in the text file...
        for imageId in imageIds:
            
            # Process the image
            descriptor = self.processData( self.metadata.get_tags_by_id(imageId) )
            
            # If requested, keep in memory
            if( self.flagSaveInMemory == True):
                self.descriptors.append(descriptor)
            
            # If requested, save to disk
            if( pathDirTfidf != None ):
                self.saveToDisk( descriptor, pathDirTfidf, imageId )
                
    def processData( self, imageId):
         # return an array that contains 0 or 1 
        return self.vec.fit_transform(self.feature_extractor(imageId)).toarray().tolist()[0]
                
     
    def saveToDisk(self, descriptors, pathDirTfidfs, imageId):
        
        # Create a file to disk with the descriptor of the current image
        pathTfidf = os.path.join( pathDirTfidfs, imageId + '.p' )

        # Save SIFT descriptor to disk            
        # np.savetxt( pathSift, des ) 
            
        # Save SIFT descriptor to disk using Pickle
        pickle.dump( descriptors, open( pathTfidf, "wb" ) )           
        
    def getDescriptors(self):
        return self.descriptors
    
# Main
if __name__ == "__main__":
    
    pathHome = os.path.expanduser('~')    
    pathWork = os.path.join( pathHome, 'work','mediaeval','2013-sed','classification' )

#    pathDirImages = os.path.join( pathWork, '1_images/train' )
#    pathFileImage = os.path.join(pathDirImages, '320426234853745400_249212839.jpg')
    pathFileMetadata = os.path.join(pathWork,'2_datasets/sed2013_task2_dataset_train.csv')
#    pathFileDatasetTrain = os.path.join(pathWork, '2_datasets/train.txt')
    pathFileTags = os.path.join(pathWork, '2_datasets/sed2013_task2_dataset_train_tags.csv')
    
    pathFileVocabulary = os.path.join(pathWork,'3_vocabulary','text.p')
    
    # Init the SIFT extractor
    tfidfExtractor = TfidfExtractor(pathFileMetadata,pathFileTags,pathFileVocabulary)
    
    tfidfExtractor.processData('320426234853745400_249212839')
    #siftExtractor.processImage('320426234853745400_249212839', pathFileImage, pathDirSifts )
