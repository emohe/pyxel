import matplotlib.pyplot as plt
import pickle
from skimage import color, exposure, io
from skimage.feature import hog

import os

def hogshow( image, hog_image ):
    ''' Generate a figure showing both the source image and the provided 
    HoG image.
    
    Copied from: 
    http://scikit-image.org/docs/dev/auto_examples/plot_hog.html#example-plot-hog-py'''    
    
    plt.axis('off')
    plt.figure(figsize=(8, 4))

    plt.subplot(121).set_axis_off()
    plt.imshow(image, cmap=plt.cm.gray)
    plt.title('Input image')
    
    # Rescale histogram for better display
    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))
    
    plt.subplot(122).set_axis_off()
    plt.imshow(hog_image_rescaled, cmap=plt.cm.gray)
    plt.title('Histogram of Oriented Gradients')
    plt.show()  
    

def processImage( image ):
    ''' Process a single image to compute its HoG feature vector and image'''    
    
     # Convert to grayscale
    imgray = color.rgb2gray( im )
    
    # Extract HoG features
    fd, hog_image = hog(imgray, orientations=8, pixels_per_cell=(16, 16),
                    cells_per_block=(1, 1), visualise=True)
                    
    return fd, hog_image



# Read image from disk
im = io.imread('/Users/xavi/work/ethz/1_images/Applelogos/crystal.jpg')

# Calculate HoG with scikit-image
fd, hog_image = processImage( im )

# Show image and HoG
hogshow( im, hog_image )

# Save features to disk (pickle)
f = open( '/Users/xavi/work/ethz/debug/hog.pkl', 'wb')
pickle.dump( fd, f )
f.close()