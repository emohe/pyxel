from skimage import io, color

import numpy as np
import os


def paint_binary_mask( binaryMask ):
    """ Paint binary mask on a grayscale """
    return io.Image( binaryMask.astype(float) )


def binarize_greyscale( probMap, threshold ):
    """ Generate a boolean array by applying a binarization threshold a 
    probability map. The provided threshold is considered on a scale
    between 0.0 and 1.0"""
    
    return probMap >= threshold
    

def calculate_foreground_ratio( binaryMask ):
    """ Compute the ratio of foreground pixels in the provided binary mask """
    
    return float( binaryMask.sum() ) / float ( np.size( binaryMask ) )
    
def find_binarization_threshold( gtImage, binarization_threshold, desired_ratio ):
    """ Find a binarization threholds that will produce a ratio of foregorund
    pixels slightly below the desired ratio """
    
    # Infinite loop, broken by the internal if
    while True :
        
        # Compute the new binary mask
        binaryMask = binarize_greyscale( gtImage, binarization_threshold )
        
        # Compute the ratio of foregorund pixels
        fgRatio = calculate_foreground_ratio( binaryMask ) 
        
        #print 'binarization_threshold = %s' % (binarization_threshold)
        #print 'Desired FG ratio = %s, Actual FG ratio = %s\n' % (desired_ratio, fgRatio)
            
        # The loop should end if the proportion of foregorund pixels is above
        # the expected one, or if a 0.0 threshold is reached
        if( fgRatio > desired_ratio or binarization_threshold == 0.0 ):
            
            #print 'Binarization threshold locked at %s for a FG ratio of %s (desired=%s)\n' % (binarization_threshold, fgRatio, desired_ratio)
            
            return binaryMask, binarization_threshold, fgRatio
            
        # Reduce the binarization threshold
        binarization_threshold = max( binarization_threshold - 0.0005, 0.0 )
        
            
def generate_binary_masks( gtImage, binFgRatios ):

    # Define a list that will contain touples of foreground ratios and binary masks
    binaryMasks = []
    
    # If the provided ground truth is not binary, generate a binary mask for
    # a set of predefined values...
    binarizationThreshold = 1.0
    for binFgRatio in binFgRatios:
            
        # Find a threshold value which generates a foreground ratio (slightly)
        # higher than the current one
        binaryMask, binarizationThreshold, fgRatio = find_binarization_threshold( gtImage, binarizationThreshold, binFgRatio )
        
        # DEBUG: Print the binarization threshold for the requested foreground ratio
        #print 'binarization_threshold = %s' % (binarizationThreshold)
        #print 'fgRatio = %s\n' % (fgRatio)
            
        # Define a tuple with the ratio of foregorund pixels and the mask
        binaryTuple = ( binFgRatio, binaryMask )
        
        # ...add the tuple to the list of results
        binaryMasks.append( binaryTuple )
        
    return binaryMasks
    
# Main
if __name__ == "__main__":

    # Paths to the data
    pathEeg = '/Users/xavi/work/eeg'
    dirGt = os.path.join(pathEeg, '4_saliency/gbvs')
    pathMaps = os.path.join( pathEeg, '3_maps/averaged')
    
    # Read the image from a file
    gtImage = io.imread( os.path.join( dirGt, 'shape1.png' )  )
    
    # Convert to grayscale (if necessary )
    if color.is_gray( gtImage ) == False:
        gtImage = color.rgb2gray( gtImage )
        
    binFgRatios=[ 0.02, 0.05, 0.1, 0.2 ]

    binaryMasks = generate_binary_masks( gtImage, binFgRatios )
    
    # For each binary mask from the image ground truth...
    #for ( binFgRatio, binaryMask ) in binaryMasks:
    #
    #    print binFgRatio
    
    