from skimage import io, color

import numpy as np
import os
import binarization
import segmentation.maps
import segmentation.plot.jaccard_vs_thrs
import plot.singles


def init_dictionaries( binFgRatios ):
    """ Init a dictionar of dictionaries, where the first level uses as a key
    the percentage of foreground pixels in the GT binary mask, and the second
    level corresponds to the Jaccard indexes obtained for the different
    binarization threholds of the saliency map when compared to the GT binar mask.
    This second dictionary uses as a key the basename of the image file """
    
    # Define a dictionary
    dicBinFgRatios = {}
    
    # For each binarization value of the ground truth saliency map...
    for binFgRatio in binFgRatios:
        
        #print binFgRatio
        
        # Create a dictionary for each probability map to evaluate
        dicBinFgRatios[ binFgRatio ] = {}
        
    return dicBinFgRatios
        
  
def binarize_saliency_maps( dirGt, basename, binFgRatios=[ 0.02, 0.05, 0.1, 0.2 ] ):
    """ This method reads the provided image file and assesses whether itcontains
    a binary mask or a grey-scale map. As a result, it generates one or multiple
    binary mask """
    
    # Read the image from a file
    gtImage = io.imread( os.path.join( dirGt, basename ) + '.png'  )
    
    # Convert to grayscale (if necessary )
    if color.is_gray( gtImage ) == False:
        gtImage = color.rgb2gray( gtImage )
        
    # Normalize between 0.0 and 1.0 (if necessary)
    if gtImage.max() > 1.0 :
        gtImage = gtImage / 255.0
        
    return binarization.generate_binary_masks( gtImage, binFgRatios )


def calculate_jaccards( dirGt, pathMaps, binFgRatios=[ 0.02, 0.05, 0.1, 0.2 ], binThrsMaps=np.linspace(0.0,1.0) ):
    """ Generate a dictionary for each provided probability map. The entries in
    the dictionary correspond to a list of tuple, each of them associating a
    ratio of foregorund pixels to a vector of Jaccard indexes, one for each
    considered binarization threshold  of the saliency maps"""
    
    # Define a dictonary that will store the Jaccard indeces for each binarization
    # threshold of the ground trut saliency map
    dicBinFgRatios = init_dictionaries( binFgRatios )

    # Obtain a list of the probability maps in files
    filesMaps = os.listdir(pathMaps)
    
    # For each probability maps that must be evaluated...
    for pathFile in filesMaps:
            
        # Extract the basename from the filename
        basename = os.path.splitext( pathFile )[0]
        
        # Read probability map
        probMap = io.imread( os.path.join( pathMaps, basename + '.png' )  )
        probMap = color.rgb2gray( probMap )
        
        # Generate ground truth binary masks for each considered binarization
        # thresholds
        binaryMasks = binarize_saliency_maps( dirGt, basename, binFgRatios )
        
        # For each binary mask from the image ground truth...
        for ( binFgRatio, binaryMask ) in binaryMasks:
                        
            # Calculate a vector the Jaccard index for the considered binarization thresholds
            # for the probability map
            jaccards = segmentation.maps.process_image( probMap, binThrsMaps, binaryMask )
            
            print binFgRatio
            print type(binFgRatio)
            
            # Store the Jaccard indexes in the corresponding dictionary entry
            dicBinFgRatio = dicBinFgRatios[ binFgRatio ]
            dicBinFgRatio[ basename ] = jaccards
            
    return dicBinFgRatios, binThrsMaps
    
def average_by_foregorund_ratio( dicBinFgRatios ):
    """ Generates a dictionary with one entry for each foreground ratio.
    Each of the entries contains the averaged Jaccard indices through all the 
    considered images """
    
    # Create a dictionary to contain the averaged Jaccards
    dicAvgdBinFgRatios={}
    
    # For each ratio of foreground pictures...
    for binFgRatio in dicBinFgRatios.keys():
        
        # Get the dictionary associated to the foreground ratio
        dicBinFgRatio = dicBinFgRatios[ binFgRatio ]
        
        # Define a matrix whose rows represent the Jaccard values associated to
        # each considered image
        j = np.array( dicBinFgRatio.values() )
            
        # Average the values obtained for each threshold (by columns)
        averaged_js = np.average( j, 0 )
        
        dicAvgdBinFgRatios[ binFgRatio ] = averaged_js

    return dicAvgdBinFgRatios
    
def show_jaccard_singles( dirGt, pathMaps ):
    
    # Calculate the Jaccard indices for each probability map
    dicBinFgRatios, binThrsMaps = calculate_jaccards( dirGt, pathMaps )
    
    plot.singles.draw_figure( dicBinFgRatios, binThrsMaps )
    
    ## Get the first key, associated to the first image
    #binFgRatio = binFgRatios[1]
    #
    ## Get the Jaccard indices associated to the first image
    #dicBinFgRatio = dicBinFgRatios[ binFgRatio ]
    #
    ## Plot a graph of the Jaccard Index vs binarization threshold
    #segmentation.plot.jaccard_vs_thrs.show( binThrsMaps, dicBinFgRatio )     
    
    
def show_jaccards_averaged( dirGt, pathMaps ):
    
    # Calculate the Jaccard indices for each probability map
    dicBinFgRatios, binThrsMaps = calculate_jaccards( dirGt, pathMaps )
    
    # Average by foregorund ratio
    dicAvgdBinFgRatios = average_by_foregorund_ratio( dicBinFgRatios )
    
    # Plot a graph of the Jaccard Index vs binarization threshold
    segmentation.plot.jaccard_vs_thrs.show( binThrsMaps, dicAvgdBinFgRatios )  
    
    
# Main
if __name__ == "__main__":

    # Paths to the data
    pathEeg = '/Users/xavi/work/eeg'
    dirGt = os.path.join(pathEeg, '4_saliency/gbvs')
    pathMaps = os.path.join( pathEeg, '3_maps/averaged')
    
    
    
#    basename='shape1'
#
#    binaryMasks = read_ground_truth( dirGt, basename, binFgRatios=[ 0.02, 0.05 ] )
#    
#    # Compute the new binary mask
#    #binaryMask = binarize_greyscale( gtImage, binarization_threshold )
#    
#    ( binFgRatio, binaryMask ) = binaryMasks[0]
#    
#    binThrsMaps=np.linspace(0.0,1.0)
#    
#    probMap = io.imread( os.path.join( pathMaps, basename + '.png' )  )
#    if color.is_gray( probMap ) == False:
#        probMap = color.rgb2gray( probMap )
    
    #jaccards = segmentation.maps.process_image( probMap, binThrsMaps, binaryMask )

    show_jaccard_singles( dirGt, pathMaps )

    # show_jaccards_averaged( dirGt, pathMaps )