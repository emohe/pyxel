import numpy as np
import matplotlib.pyplot as plt
import os
import saliency.maps
import saliency.plot.singles

def draw_line( axarr, fgRatio, dicBinFgRatio, thresholds ):
    
    #axarr.set_title( fgRatio, dicBinFgRatio )
    
     # Get the sorted labels of the entries contained in the graph
    labels = sorted( dicBinFgRatio.keys() )
    
    # Color palette to generate the graph plots, one for each label
    colors = plt.get_cmap('jet')(np.linspace(0, 1.0, len(labels)))
    
     # For each label
    for lab, col in zip( labels, colors ):
        
        # Draw a line in a graph for the current label
        axarr.plot( thresholds, dicBinFgRatio[lab], label=lab, color=col )
        
        
    # Set axis between 0.0 a 1.0, as well as autoscale off
    axarr.set_autoscale_on(False)
    
    axarr.set_title( fgRatio )
    axarr.set_xlabel('Binarization threshold')
    axarr.set_xlim(0.0, 1.0)
    axarr.set_ylabel('Jaccard Index')
    axarr.set_ylim(0.0,1.0)
    axarr.legend(loc = 'best')
    

def draw_figure( dicBinFgRatios, binThrsMaps ):
    ''' Generates a figure with a subplot for each foregorund ratio. '''
    
    binFgRatios = dicBinFgRatios.keys()
    
    
    f, axarr = plt.subplots( 2, 2, sharex=True, sharey=True )
    
    fgRatio = binFgRatios[0]
    dicBinFgRatio = dicBinFgRatios[ fgRatio ]
    draw_line( axarr[0, 0], fgRatio, dicBinFgRatio, binThrsMaps )
    
    fgRatio = binFgRatios[1]
    dicBinFgRatio = dicBinFgRatios[ fgRatio ]
    draw_line( axarr[0, 1], fgRatio, dicBinFgRatio, binThrsMaps )
    
    fgRatio = binFgRatios[2]
    dicBinFgRatio = dicBinFgRatios[ fgRatio ]
    draw_line( axarr[1, 0], fgRatio, dicBinFgRatio, binThrsMaps )    
    
    fgRatio = binFgRatios[3]
    dicBinFgRatio = dicBinFgRatios[ fgRatio ]
    draw_line( axarr[1, 1], fgRatio, dicBinFgRatio, binThrsMaps )
    
    # Set a title to the figure
    plt.suptitle('Saliency of Synthetic Images')
    
    # Show figure
    plt.show()
    
    
# Main
if __name__ == "__main__":

    # Paths to the data
    pathEeg = '/Users/xavi/work/eeg'
    dirGt = os.path.join(pathEeg, '4_saliency/gbvs')
    pathMaps = os.path.join( pathEeg, '3_maps/averaged')
    
    # Close all figures
    plt.close('all')
    
    # Calculate data
    dicBinFgRatios, binThrsMaps = saliency.maps.calculate_jaccards( dirGt, pathMaps )
    
    # Draw figures
    draw_figure( dicBinFgRatios, binThrsMaps )
        
                
