from skimage import io, color

import numpy as np
import jaccard
import os
#import segmentation.plot as plt
import plot.jaccard_vs_thrs
import saliency.binarization


def build_binary_collection( probMap, thresholds ):
    """ Generate a collection of binary images by binarizing the given probability
    map for each of the thresholds provided """
    
    # Convert to greyscale, if necessary
    if color.is_gray( probMap ) == False:
        probMap = color.rgb2gray( probMap ) 
        
    # Generate an array of num.thresholds x width x height, one binary mask per threshold
    return np.array( [ saliency.binarization.binarize_greyscale( probMap, t ) for t in thresholds] )
    
 
def process_image( probMap, thresholds, gtMask ):
    """ Generate a vector of the Jaccard indices associated to each of the 
        specified binarization thresholds for the probabilit map"""
        
    # Generate an array of 50 x width x height, one binary mask per threshold
    m = build_binary_collection( probMap, thresholds )
    
    # Compute the Jaccard indexes for each computed binary mask
    return np.array( [ jaccard.calculate( m[n,:,:], gtMask) for n in np.arange(0,m.shape[0]) ] )
    
    
def read_ground_truth( dirGt, basename ):
    
    # Read the image from a file
    gtImage = io.imread( os.path.join( dirGt, basename ) + '.png'  )
    
    # Convert to grayscale (if necessary )
    if color.is_gray( gtImage ) == False:
        gtImage = color.rgb2gray( gtImage )
  
    return gtImage
      

def calculate_jaccards( dirGt, pathMaps, binThresholds=np.linspace(0.0,1.0) ):
    """ Generate a dictionary for each provided probability map. The entries in
    the dictionary correspond to a list of tuple, each of them associating a
    ratio of foregorund pixels to a vector of Jaccard indexes, one for each
    considered binarization threshold """

    # Obtain a list of the probability maps in files
    filesMaps = os.listdir(pathMaps)

    # Define a dictonary that will store the Jaccard index for each image to process
    jaccardsAll = {}
    
    # For each image
    for pathFile in filesMaps:
            
        # Extract the basename from the filename
        basename = os.path.splitext( pathFile )[0]
        
        # Read probability map
        probMap = io.imread( os.path.join( pathMaps, basename + '.png' )  )
        probMap = color.rgb2gray( probMap )
        
        # Read or generate the corresponding ground truth binary mask(s)
        binaryMask = read_ground_truth( dirGt, basename )
        
        # Calculate a vector the Jaccard indexes obtained when comparing the 
        # binary GT mask with the diferent binarizations of the probability mask
        jaccardsImage = process_image( probMap, binThresholds, binaryMask )
                
        # Store the Jaccard indices in a dictionary entry identified by the image basename
        jaccardsAll[ basename ] = jaccardsImage
        
    return jaccardsAll, binThresholds
    
    
def show_averaged_jaccards( dirGt, pathMaps ):
    
    # Compute the jaccard indexes for every image inidividually
    jaccards, binThresholds = calculate_jaccards( dirGt, pathMaps )
    
    # Define a matrix whose rows represent the Jaccard values associated to
    # each considered image
    j = np.array( jaccards.values() )
    
    # Average the values obtained for each threshold (by columns)
    averaged_js = np.average( j, 0 )
    
    # Create a dummy dictionary with the single entry of the averaged values
    averaged={}
    averaged[ 'averaged' ] = averaged_js
    
    # Plot a graph of the Jaccard Index vs binarization threshold
    plot.jaccard_vs_thrs.show( binThresholds, averaged )
    

def show_single_jaccards( dirGt, pathMaps ):
    
    jaccards, binThresholds = calculate_jaccards( dirGt, pathMaps )
    
    # Plot a graph of the Jaccard Index vs binarization threshold
    plot.jaccard_vs_thrs.show( binThresholds, jaccards )
        


def save_single_jaccards( dirGt, pathMaps, pathFigure ):
    
    jaccards, binThresholds = calculate_jaccards( dirGt, pathMaps )
    
    # Save plot to file
    plot.jaccard_vs_thrs.save( binThresholds, jaccards, os.path.join( pathFigure, 'single_jaccard.png') )
    

# Main
if __name__ == "__main__":

    # Paths to the data
    pathEeg = '/Users/xavi/work/eeg'
    dirGt = os.path.join(pathEeg, '2_gt')
    pathMaps = os.path.join( pathEeg, '3_maps/averaged')

    show_single_jaccards( dirGt, pathMaps )

    # show_averaged_jaccards( dirGt, pathMaps )
    
    #pathFigure = os.path.join( pathEeg, 'debug')
    #save_single_jaccards( dirGt, pathMaps, pathFigure )