from skimage import io, color


def verifyMask( mask ):
    """ Binarise input objects, if not arrays of booleans """
    
    if isinstance( mask, bool ) == True:
        return

    if isinstance( mask, io.Image ) == True:
        
        if color.is_gray( mask ) == False:
            mask = color.rgb2gray( mask ) 
        
    return mask > 0;        


def calculate( mask1, mask2 ):
    """ Compute the Jaccard Index betweek two masks. 
    The zero values of the provided masks are considered as background,
    while any other value (1, 255...) is considered as foreground"""
    
    mask1 = verifyMask( mask1 )
    mask2 = verifyMask( mask2 )
    
    # Logical AND for the intersection
    inter = mask1 * mask2
    
    # Logical OR for the union
    union = mask1 + mask2
    
    # Convert to float to allow future division
    interFloat = inter.astype(float)
    unionFloat = union.astype(float)
    
    # Count the amount of intersected and united pixels.
    return interFloat.sum() / unionFloat.sum()
    
    
if __name__ == "__main__":
    """ Module to be run if invoked from the command line"""    
    import sys
    
    # read the two images provided
    mapProb = io.imread( sys.argv[1] )
    maskGT = io.imread( sys.argv[2] )
    
    ## Apply a threshold of the probability map
    mapBin = mapProb > 0.5
    
    j = calculate( mapProb, maskGT )
    
    print j
    
    
## Read probability map
#mapProb = io.imread( '/Users/xavi/work/eeg/3_maps/averaged/shape2.png'  )
#mapProb = color.rgb2gray( mapProb )
#
## Apply a threshold of the probability map
#mapBin = mapProb > 0.8
#
## Read corresponding GT binary mask
#maskGT = io.imread( '/Users/xavi/work/eeg/2_gt/shape2.bmp' )
#maskGT = color.rgb2gray( maskGT )
#
## Compute the Jaccard Index between itself
#j00 = calculate( mapBin, maskGT )
#
#imProb = io.Image( mapBin.astype(float ) )
#
#imGT = io.Image( maskGT )
#
#print j00




