import numpy as np
import matplotlib.pyplot as plt

def draw_figure( thresholds, jaccards ):
    ''' Generates a figure that plots each entry in the input dictionary
    as a line in the graph. '''

    # Get the sorted labels of the entries contained in the graph
    labels = sorted( jaccards.keys() )
    
    ## Get the associated values, using the same keys for sorting
    #values = sorted( jaccards.values(), key=jaccards.keys() )
    
    # Color palette to generate the graph plots, one for each label
    colors = plt.get_cmap('jet')(np.linspace(0, 1.0, len(labels)))
    
    # For each label
    for lab, col in zip( labels, colors ):
        
        # Draw a line in a graph for the current label
        plt.plot( thresholds, jaccards[lab], label=lab, color=col )
        
    #plt.plot( thresholds, values, label=labels )   
    
    # Set labels to axis and legend location
    plt.title('Segmentation of Synthetic Images')
    plt.xlabel('Binarization threshold')
    plt.ylabel('Jaccard Index')
    plt.legend(loc = 'best')
    
    
def show( thresholds, jaccards ):
    
    plt.close('all')
    
    draw_figure( thresholds, jaccards )
    
    plt.show()
          
    
def save( thresholds, jaccards, filename ):

    draw_figure( thresholds, jaccards )
    
    plt.savefig( thresholds, filename, dpi=None )
        