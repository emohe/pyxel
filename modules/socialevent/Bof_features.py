import sys


import cv2
import os
import numpy  as  np 

pathHome = os.path.expanduser('~')
pathDirPython=os.path.join( pathHome, 'workspace/python')
sys.path.append(pathDirPython)

from modules.socialevent.Images import Images

from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.datasets.samples_generator import make_blobs
# define Paths
HOME = os.path.expanduser('~')
#pathTrainImages = HOME+'/work/mediaeval/2013-sed/classification/1_images/train'
pathTrainImages = HOME+'/Desktop/train-0'
path_features = HOME +'/work/mediaeval/2013-sed/classification/3_features/'
# create new Images object type
images = Images(pathTrainImages) # reading all the train photos

# extract descriptor and save images
sift  =  cv2.SIFT()

directory = path_features+'/Sift_images'

if not os.path.exists(directory):
    os.makedirs(directory)

descriptor = images.extract_and_save_descriptors(directory)    
            
#descriptor = {}

#for document_id in images.get_document_ids():    
#    gray = images.get_image_gray_by_id(document_id)
    #kp  =  sift.detect( gray , None ) 
#    kp,des = sift.detectAndCompute(gray, None)
    # save to disc
#    np.savetxt(directory+'/'+document_id+'_sift.txt', (des))
#    descriptor[document_id] = des # create a dict with image and document_id as key
    
# BOF
# train kmeans and quantification
mbk = MiniBatchKMeans(init='k-means++', n_clusters=128,
                      n_init=10, max_no_improvement=10, verbose=0)

arr_descriptor = np.vstack(tuple(descriptor))

mbk.fit( arr_descriptor )

mbk_means_labels = mbk.labels_ 
mbk_means_cluster_centers = mbk.cluster_centers_
mbk_means_labels_unique = np.unique(mbk_means_labels)

# <codecell>

# build the histogram for the images
directory_in = path_features+'/Sift_images'
directory_out = path_features+'/Histo_originals'

if not os.path.exists(directory_out):
        os.makedirs(directory_out)
        
        
list_hist_out = images.load_features_and_save_histograms(directory_in, directory_out,mbk)      
        
#list_hist_o = {}
# for each image

#for document_id in images.get_document_ids():
    #Load descriptor
#    a = np.loadtxt(directory_in+'/'+document_id+'_sift.txt').view(float64)
    
    #Quant descriptor
#    quant = mbk.predict(a)
    
    #Build Histogram
#    histo = np.histogram(quant, bins=128)
    
    #Save histogram to disk
#    np.savetxt(directory_out+'/'+document_id+'_histo.txt', (histo[0]))
    
    #Append Histogram to list
#    list_hist_o[document_id]= histo[0]

print "Hola"

#carregar histogrames

