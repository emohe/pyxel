
import os
import pandas as pd


class GroundTruth:
    """ This class read a MediaEvals 2013 SED Task 1 ground truth CSV file. 
    First column is named 'gt' and corresponds to the event id. 
    Second column is named 'document_id' and corresponds to the photo ID """    
    def __init__(self, pathFile):    
        # Generate a DataFrame by reading the ground truth from a CSV whose
        # fields are separated by \t
        data_csv=pd.read_csv('%s' % (pathFile), sep='\t')
        
        # index ground truth data by the document_id
        #self._data_sorted_by_document_id = data_csv.sort_index(by='document_id')
        
        # Create groups according to the ground truth label ('gt')
        #self._gt_grouped_by_event = self._data_sorted_by_document_id.groupby('gt')
        self._gt_grouped_by_event = data_csv.groupby('gt')
        
        # Store data on a Panda's DataFrame
        #self.dataFrame = pd.DataFrame()
        
        #self.dataFrame = pd.merge(self.dataFrame, data, on='document_id', how='outer')
        
    def get_eventIDs(self):
        """ Obtain a vector containing the different event IDs """
        return self._gt_grouped_by_event.groups.keys()
        
    def get_nof_photos_in_event(self, eventID):
        """ Returns an integer with the amount of photos in the specified event """
        return len(self._gt_grouped_by_event.groups[eventID])
        
    def get_photos_in_event(self, eventID ):
        """ Obtain a vector containing the photo IDs of the event specified """
        return self._gt_grouped_by_event.groups[eventID],1
        
        
# Main
if __name__ == "__main__":

    # Point to the local path where the ground truth and metadata are stored
    HOME = os.path.expanduser('~')
    DATASETS_PATH = HOME+'/work/mediaeval/2013-sed/datasets'
    GROUND_TRUTH_FILE = 'sed2013_dataset_train_gs.csv'

    # Load the metadata
    gtPath = os.path.join(DATASETS_PATH, GROUND_TRUTH_FILE)
    groundTruth = GroundTruth(gtPath)
    