import os

import matplotlib.pyplot as plt


class Histograms:
    """ This class generates histograms to disk """    
    def __init__(self, pathHistogram):
        
          self.pathHistogram = pathHistogram
        
    def save_histogram(self, distances, nof_bins, title, xlabel, filename ):
        """ Generate an histogram and save it to disk """
        
        # Generate histogram
        histogram = plt.hist(distances,nof_bins)
        
        # Add title and labels on axis
        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel('# photos')
        
        # Save histogram to disk
        pathHistogram = os.path.join( self.pathHistogram, filename )
        plt.savefig(pathHistogram )
        
        # Close figure
        plt.close()
        
    