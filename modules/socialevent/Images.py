
import os
import cv2
import numpy as np
from sys import path
from modules.socialevent.classification.GroundTruth import GroundTruth


class Images:
    """ This class read the images contained in a specified path"""
    
    def __init__(self,imagePath):
        # read images name in imagePath
        self.images_document_id = os.listdir(imagePath) 
        self.images_gray = {} # this list will contain the images
        for document_id in self.images_document_id:
            img  =  cv2.imread ( imagePath +'/'+document_id ) # read image
            gray = cv2.cvtColor (img,cv2.COLOR_BGR2GRAY) # convert to gray
            self.images_gray[document_id] = gray # and save in images list
        
        self.descriptor_document_id = self.extract_descriptors()

    def __len__(self):
        """This instance return the number of images reading"""
        return len(self.images_document_id)
    
    def get_document_ids( self ):
        """ Returns a list with all image names"""
        return list(self.images_document_id)
        
    def get_images_in_gray ( self ):
        return self.images_gray
        
    def get_image_gray_by_id(self, document_id):
        return self.images_gray[document_id]
    
    def remove_extension_jpg_in_id(self, document_id):
        """ Function to extract the .jpd extension of document_id image"""
        n = len(document_id) - 4
        return document_id[:n]
        
    def get_label_by_id(self,document_id, gt_file):
        gt = GroundTruth(gt_file)
        
    
    def extract_descriptors(self):
        """ Function to extract and save descriptors in a specified directory
        And returns a dict with document_id as key and the descriptor associated"""
        sift = cv2.SIFT()
        descriptor = {}
        for document_id in self.images_document_id:    
            gray = self.get_image_gray_by_id(document_id)
            #kp  =  sift.detect( gray , None ) 
            kp,des = sift.detectAndCompute(gray, None)
            # save to disc
            #np.savetxt(directory+'/'+document_id+'_sift.txt', (des))
            descriptor[document_id] = des # create a dict with image and document_id as key
        return descriptor
    
    
    def extract_and_save_descriptors(self,directory):
        """ Function to extract and save descriptors in a specified directory
        And return a list with the extraction features"""
        sift = cv2.SIFT()
        descriptor = []
        for document_id in self.images_document_id:    
            gray = self.get_image_gray_by_id(document_id)
            #kp  =  sift.detect( gray , None ) 
            kp,des = sift.detectAndCompute(gray, None)
            # save to disc
            np.savetxt(directory+'/'+document_id+'_sift.txt', (des))
            descriptor.append(des) # create a dict with image and document_id as key
            print document_id
        return descriptor

    def load_features_and_save_histograms(self, directory_in, directory_out,mbk):
        """This function load sift features from a directory, and return a dictionary
        with each histogram associated an id. Also save the histograms in a output directory.
        directory_in: features path; directory_out: path to save histograms; 
        mbk:  MiniBatchKMeans object(from sklearn.cluster import MiniBatchKMeans)"""
        dict_hist_o = {}
        # for each image
        for document_id in self.images_document_id:
            #Load descriptor
            a = np.loadtxt(directory_in+'/'+document_id+'_sift.txt').view(np.float64)
        
            #Quant descriptor
            quant = mbk.predict(a)
        
            #Build Histogram
            histo = np.histogram(quant, bins=128)
        
            #Save histogram to disk
            np.savetxt(directory_out+'/'+document_id+'_histo.txt', (histo[0]))
        
            #Append Histogram to 
            dict_hist_o[document_id]= histo[0]
        return dict_hist_o
        
    
    def load_historams( self, hist_directory ):
        # for each image
        histograms = {}
        for document_id in self.images_document_id:
            #Load histograma
            a = np.loadtxt(hist_directory+'/'+document_id+'_histo.txt').view(np.float64)    
            histograms[document_id] = a
        return histograms

if __name__ == "__main__":

    # Point to the local path where the ground truth and metadata are stored
    HOME = os.path.expanduser('~')
    FOLDER_PATH = HOME+'/Desktop/train-0-reduced'

    # Load the metadata
    images = Images(FOLDER_PATH)