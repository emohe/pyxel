#NLTK
METADATA_FILE_TRAIN = 'sed2013_task2_dataset_train.csv' 
TAG_FILE_TRAIN = 'sed2013_task2_dataset_train_tags.csv'
METADATA_FILE_TEST = 'sed2013_task2_dataset_test.csv'
TAG_FILE_TEST = 'sed2013_task2_dataset_test_tags.csv'
GROUND_TRUTH_FILE = 'sed2013_task2_dataset_train_gs.csv'

# checking the sistem
#METADATA_FILE_TRAIN = 'task2_dataset_train.csv' 
#TAG_FILE_TRAIN = 'task2_dataset_train_tags.csv'
#METADATA_FILE_TEST = 'task2_dataset_test.csv'
#TAG_FILE_TEST = 'task2_dataset_test_tags.csv'
#GROUND_TRUTH_FILE = 'task2_dataset_train_gs.csv'


import os
import pandas as pd
import nltk
from sklearn import svm
from sklearn.svm import LinearSVC
from nltk.classify.scikitlearn import SklearnClassifier
from collections import defaultdict
#returns an object that acts like a list but does not store all the feature sets in memory
#from nltk.classify import apply_features
from modules.socialevent.classification.GroundTruth import GroundTruth
from modules.socialevent.classification.Metadata import Metadata
import pickle
# Pathnames
HOME = os.path.expanduser('~')
DATASETS_PATH = HOME+'/work/mediaeval/2013-sed/classification/2_datasets/'

# Load the metadata
metadataPathTrain = os.path.join(DATASETS_PATH, METADATA_FILE_TRAIN)
tagsPathTrain = os.path.join(DATASETS_PATH, TAG_FILE_TRAIN)
metadataPathTest = os.path.join(DATASETS_PATH, METADATA_FILE_TEST)
tagsPathTest = os.path.join(DATASETS_PATH, TAG_FILE_TEST)
gtPath = os.path.join(DATASETS_PATH, GROUND_TRUTH_FILE)

#  Init a GroundTruth object and a Metadata object
gtr = GroundTruth(gtPath)
train_met = Metadata(metadataPathTrain,tagsPathTrain) 
test_met = Metadata(metadataPathTest, tagsPathTest)


labels = gtr.get_eventIDs()
#labels = ['concert','conference','exhibition','fashion','other','protest','sports','theater_dance','non_event']

# train tags contains a supervector with all the tags appearing in the train set
all_train_tags = train_met.get_supervector()
# order tags by frecuency
all_train_freq_tags = nltk.FreqDist(w.lower() for w in all_train_tags)
# get only the 2000 most freq tags
most_freq_tags = all_train_freq_tags.keys()[:2000]


def save_file ( file_name, to_save ):
    fd = open(str(file_name), "w")
    for i in to_save:
        fd.write(str(i))
        fd.write('\n')
    fd.close()
    
def crea_dict (most_freq_tags):
    dictionary = {}
    for word in most_freq_tags:
        dictionary[word] = False
    return dictionary

dictionary = crea_dict(most_freq_tags)

def feature_extractor(img_tags, dictionary): 
    document_words = set(img_tags)
    features = defaultdict(lambda:False,dictionary)
    for word in document_words:
        features[word] = True
    return features



#train_set = [(list(train_met.get_tags_by_id(doc_id)), label) 
#             for label in labels
#             for doc_id in gtr.get_photos_in_event(str(label))
#             if (train_met.get_tags_by_id(doc_id) != None)]

train_set = [] #init a list to save the train set
for label in labels: # for each label
    for doc_id in gtr.get_photos_in_event(str(label)): # for each foto 
        if (train_met.get_tags_by_id(doc_id) != None): # if have tags
            train_set.append([list(train_met.get_tags_by_id(doc_id)), label])# save tags and label
        else:# added the non tag option
            train_set.append([' ',label])# if it haven't got tags, save only the label without tags
    print label
# save train set
save_file( "chectrain_set.txt",train_set)


# extract train features
# Generate a new train set but now with tags as features
features_train_set = [(feature_extractor(tags,most_freq_tags), label) for (tags,label) in train_set]

# save train features
save_file( "checfeatures_train_set.txt",features_train_set)

#tagged = nltk.pos_tag(train_set[])
#entities = nltk.chunk.ne_chunk(tagged)

#train the Naive Bayes Classifier
classifier = nltk.NaiveBayesClassifier.train(features_train_set)


# train the svm classifier
#classif = SklearnClassifier(LinearSVC())

#rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=3.0, probability = True)
#for (d,c) in train_set:
    #rbf_svc.fit(str(feature_extractor(d,train_tags)), c)
#    classif.train(str(feature_extractor(d,train_tags)), c)
savepath = HOME+'/work/mediaeval/2013-sed/3_features'
output = savepath+'/text_model.model'
pickle.dump(classifier , open(output,'wb'))
output.close()
# -----------------------------------TEST------------------------------------

# obtain the test id
ids = test_met.get_document_id().unique()

# extract test features
#test_set = [(list(test_met.get_tags_by_id(doc_id)), doc_id)
#             for doc_id in ids
#             if (test_met.get_tags_by_id(doc_id) != None)]
test_set=[]
for doc_id in ids:
    if (test_met.get_tags_by_id(doc_id) != None):
        test_set.append(list(test_met.get_tags_by_id(doc_id)), doc_id)
    else: # Added non tag option
        test_set.append(' ',doc_id)           

output = savepath+'/text_test_set.set'
pickle.dump(test_set , open(output,'wb'))


for img_tags,doc_id in test_set:
    feature_test_set = feature_extractor(img_tags, most_freq_tags)

output = savepath+'/text_test_features.features'
pickle.dump(feature_test_set , open(output,'wb'))


def classifyI ( test_set, train_tags ):
    results = open("checclassify.txt", "w")
    for img_tags,doc_id in test_set:
        sol = classifier.classify(feature_extractor(img_tags, most_freq_tags))
        results.write(str(doc_id)+' '+str(sol)+'\n')
    results.close()


#features_test_set = [(feature_extractor(test_tags,train_tags), c) for (test_tags,c) in test_set]
classifyI (test_set, most_freq_tags)

def svm_classify (test_set,train_tags ):
    results = open("svm_classify.txt", "w")
    for img_tags,doc_id in test_set:
        sol = rbf_svc.predict(feature_extractor(img_tags, most_freq_tags))
        results.write(str(doc_id)+' '+str(sol)+'\n')
    results.close()
   
#svm_classify (test_set,train_tags)

  