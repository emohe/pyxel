from sklearn.grid_search import GridSearchCV 
from sklearn.multiclass import OutputCodeClassifier
from sklearn.svm import SVC

def trainSVM(X,Y):
    
    #Define parameters to be tested with SVM
    tuned_parameters = [{'kernel': ['rbf'], 'gamma': [0.001,0.002,0.004,0.006,0.008,0.01,0.02,0.04,0.06,0.08,0.1,0.2,0.4,0.6,0.8,1,5,10], 'C': [1,1.5,2,3,4,5,6,7,8,9,10]}, {'kernel': ['linear'], 'C': [1,1.5,2,3,4,5,6,7,8,9,10]}] 
    #Tune SVM
    clf = GridSearchCV(SVC(),param_grid=tuned_parameters, cv=5,scoring='accuracy')
    clf.fit(X, Y) #Fit classifier to data
    
    #Define parameters for ECOC coding
    tuned_parameters= [{'code_size': [5,10,15,20,25,30,35,40,60,80,100,200]}] 
    #Define the classifier - Error correcting output codes (multiclass) ECOC
    classifier=OutputCodeClassifier(estimator=clf.best_estimator_) 
    #Tune ECOC with the parameters using 5-fold cross validation
    clf = GridSearchCV(classifier,param_grid=tuned_parameters, cv=5,scoring='accuracy') 
    #Fit to data
    clf.fit(X, Y) 
    
    return clf.best_params_, clf.best_score_, clf.best_estimator_ 


#Usage:

#best_params,best_score,classifier=trainSVM(X,Y)
#classifier.fit(X,Y) 
#Save 'classifier' in disk

#At testing time:
#Load classifier
#prediction = classifier.predict(X_test) 
#cm = confusion_matrix(Y_test, prediction) 