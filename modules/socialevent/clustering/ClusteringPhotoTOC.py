import numpy as np
import os
import pandas as pd

from modules.socialevent.Metadata import Metadata

class ClusteringPhotoTOC:
    """ Clusters a collection of photos according to the social event they represent.
        This piece of code implements the PhotoTOC algorithms for clustering. """
 
    def __init__(self, metadata, K, d ):
               
        # Group metadata according to their usernames
        grouped = metadata._data.groupby('username')
        
        # Init the counter of clusters
        cluster_id = 0
        
        # For each username and associated photos
        for name,group in grouped:
            
            print 'clustering user '+name
            
            # Index the current user's photos according to the time stamp of the photo
            group = group.sort_index(by='timestamp_taken',ascending=False)
            
            # Create a new Serie with the time difference between photos
            group['tdelta']=(group['timestamp_taken'].shift()-group['timestamp_taken']).fillna(0)
        
            # Define the threshold function as a lambda function where x is the input parameter
            # More info on lambda functions here: http://www.diveintopython.net/power_of_introspection/lambda_functions.html
            # DANI'S BUG: Should not be 'x+1' thr_func=lambda x: np.log(x+1).mean()+np.log(K)
            thr_func=lambda x: np.log(x).mean()+np.log(K)
            
            # Compute the threshold that should be satisfied to create an event boundary
            # between each pair of photos
            # Apply thr_funct on a window of d elements in the Serie of time differences
            # The window considers the current element and the (d-1) previous elements            
            # More info: http://pandas.pydata.org/pandas-docs/stable/generated/pandas.stats.moments.rolling_apply.html
             
            min_thresholds = pd.stats.moments.rolling_apply(group.tdelta,
                                                        window=2*d+1,
                                                        func=thr_func,
                                                        center=True,
                                                        min_periods=2*d+1) 
            # DANI'S BUG: The window is not considering 2d+1 elements, but d
            # DANI'S BUG: The resulting label is not centered in the middle of the window
            # DANI'S BUG: Considering a single period was not respecting the 2d+1 neighbourhood
            # Threshold=pd.stats.moments.rolling_apply(group.tdelta,
            #                                        window=d,
            #                                        func=thr_func,
            #                                        center=False,
            #                                        min_periods=1)                                                                   
            
            # Generate a series of boolean values by comparing the log(x) with the minimum precomputed threshold
            is_event_boundary = group.tdelta.apply(lambda x: np.log(x)) > min_thresholds
            
            # DANI'S BUG: Should not be 'x+1' but just 'x': Comp = group.tdelta.apply(lambda x: np.log(x+1)) > Threshold
            
            # Define a Series that will contain the cluster_id associated to each document_id
            cluster_ids = pd.Series(np.zeros( metadata.get_nof_photos() ), index = metadata._data.index )
            
            # For each potential event boundary...
            for index,val in is_event_boundary.iteritems():
                
                # If it is an actual event boundary...
                if val == True:
                    # ...increase the counter of clusters
                    cluster_id=cluster_id+1
                
                # The current cluster ID is saved into the corresponding temporal location
                # in the cluster Series
                cluster_ids[index] = cluster_id
                
            # Increase the cluster counter to indicate that a new user is to be processed
            cluster_id=cluster_id+1
            
        # Save the total amount of clusters
        self._num_clusters = cluster_id-1
        
        # Define a DataFrame that will contain the document_id and the associated cluster_id
        self._data = pd.DataFrame()
        
        self._data['document_id'] = metadata._data['document_id'].copy()
        self._data['cluster'] = cluster_ids.copy()
        

# Main
if __name__ == "__main__":

    # Point to the local path where the ground truth and metadata are stored
    HOME = os.path.expanduser('~')
    DATASETS_PATH = HOME+'/work/mediaeval/2013-sed/datasets'
    METADATA_FILE = 'sed2013_dataset_train.csv'

    # Load the metadata
    metadataPath = os.path.join(DATASETS_PATH, METADATA_FILE)
    metadata = Metadata(metadataPath)
    
    # Run the PhotoTOC clustering based on user and temporal data
    ClusteringPhotoTOC( metadata, np.log(17), d=10 )
        