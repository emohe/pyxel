import numpy as np
import os
import pandas as pd

from modules.socialevent.Metadata import Metadata

class ClusteringStats:
    """ Related metadata with a clustering solution to generate stats about it."""
 
    def __init__(self, metadata, clustering ):        

        # Create a new Dataframe combining metadata and cluster IDs.        
        self._data = pd.merge( metadata._data, clustering._data, on='document_id', how='outer')
                
        # Group data according to their associated cluster ID
        grouped = self._data.groupby('cluster')
        cluster=[]
        
        # For each cluster...
        for name,clust in grouped:
            
            print "Generate stats for cluster #"+str(name)
            
            # Build a Series with all words contained in the titles of the clusters
            titles = clust.title.apply(lambda x: set(x.split())).values  
                                              
            # Apply the function make_union cumulatively to the items of titles
            def make_union(x,y):
                return x.union(y) 
            tags = reduce( self.make_union, titles )
            
            descriptions = clust.description.apply(lambda x: set(x.split())).values
            tags  = tags.union(reduce( self.make_union, descriptions ))

            cluster.append({
                    'label':name,
                    'user': clust.username[clust.index[0]],
                    'items':clust.cluster.count(),
                    'images':clust.index,
                    'tags': tags,
                    #'tfidf': calc_sim(tags),
                    'datetaken_mean':clust.timestamp_taken.mean(),
                    'long_mean':clust.longitude.mean(),
                    'lat_mean':clust.latitude.mean(),
                    })
        
        # Store all information in a DataFrame
        self.data = pd.DataFrame(cluster)
        
      

# Main
if __name__ == "__main__":

    # Point to the local path where the ground truth and metadata are stored
    HOME = os.path.expanduser('~')
    DATASETS_PATH = HOME+'/work/mediaeval/2013-sed/datasets'
    METADATA_FILE = 'sed2013_dataset_train.csv'

    # Load the metadata
    metadataPath = os.path.join(DATASETS_PATH, METADATA_FILE)
    metadata = Metadata(metadataPath)
    
    # Run the PhotoTOC clustering based on user and temporal data
    ClusteringPhotoTOC( metadata, np.log(17), d=10 )
        