from modules.socialevent.GroundTruth import GroundTruth
import modules.socialevent.distances as dist
from modules.socialevent.Metadata import Metadata
import numpy as np
import os
import random


class Trainer:
    """ Defines a training dataset with pairs of distances of photos 
    that belong to the same event """
    def __init__(self, groundTruth, metadata, nof_samples):
       
        # Allocate an Numpy array that will contain all distances.
        # Each row will represent the distances for each pair of photos,
        # while each column will be associated to a different feature
        self._data_distances=np.zeros((nof_samples, 4))
        
        # Generate training data with distances of pairs of photos in the same event
        self.train_same_event( nof_samples, groundTruth, metadata, self._data_distances)
 
    def train_same_event(self, nof_samples, groundTruth, metadata, data_distances):
        
        # Get all event IDs in the ground truth
        eventIDs = groundTruth.get_eventIDs()
       
        # Init a counter of the pairs of photos belonging to the same event
        # that have been found so far
        i=0
        
        # While the requested amount of trainig samples has not been reached...
        while i<nof_samples:
            
            # Obtain one event ID by random selection
            eventID = random.sample(eventIDs,1)[0]
            
            # Obtain the number of photos associated to this event
            nof_photos_in_event = groundTruth.get_nof_photos_in_event(eventID)
            
            # If there are more than one photos in the chosen event...
            if nof_photos_in_event > 1:
                
                # Draw randomly two photo indexes
                document_id1=random.sample( groundTruth._gt_grouped_by_event.groups[eventID],1)[0]
                document_id2=random.sample( groundTruth._gt_grouped_by_event.groups[eventID],1)[0]

                # If the same photo index has been chosen twice...
                while(document_id1==document_id2):
                    # ...keep drawing a second photo ID until it is different 
                    # from the first one
                    document_id2=random.sample( groundTruth._gt_grouped_by_event.groups[eventID],1)[0]
                    
                # If the latitude data for the two photos chosen is not available...
                if np.isnan(metadata._data['latitude'].loc[document_id1]) or np.isnan(metadata._data['latitude'].loc[document_id2]):
                    # ...skip this pair of photos
                    pass
                # If the geolocation data for both photos chosen is available...
                else:
                    
                    document1 = metadata.get_document(document_id1)
                    document2 = metadata.get_document(document_id2)
    
                    # Calculate the distances between the two photos
                    data_distances[i,0] = dist.calculate_distance_time(document1, document2)
                    data_distances[i,1] = dist.calculate_distance_geo(document1, document2)
                    data_distances[i,2] = dist.calculate_distance_user(document1, document2 )
                    data_distances[i,3] = dist.calculate_distance_tags_Jaccard(document1, document2 )
                    
                    i=i+1
                    
    def get_distances_time(self):
        return self._data_distances[:,0]   
   
    def get_distances_geo(self):
         
        # Return the elements in the first column of data, which correspond
        # to the geolocation distances
        return self._data_distances[:,1] 
        
    def get_distances_user(self):
        return self._data_distances[:,2]  
        
    def get_distances_tags(self):
        return self._data_distances[:,3] 
        
            
# Main
if __name__ == "__main__":
    
    # Point to the local path where the ground truth and metadata are stored
    HOME = os.path.expanduser('~')
    DATASETS_PATH = HOME+'/work/mediaeval/2013-sed/datasets'
    
    METADATA_FILE = 'sed2013_dataset_train.csv'
    GROUND_TRUTH_FILE = 'sed2013_dataset_train_gs.csv'
    
    # Load the ground truth data from a file
    gtPath = os.path.join(DATASETS_PATH, GROUND_TRUTH_FILE)
    groundTruth = GroundTruth(gtPath)
    
    # Load the metadata
    metadataPath = os.path.join(DATASETS_PATH, METADATA_FILE)
    metadata = Metadata(metadataPath)
    
    # Run the training process of estimating the average and stanard deviation
    # of the distances between pairs of photos in the same event
    trainer = Trainer( groundTruth, metadata, 10)    
    
    # Get the computed distances for geolocation
    distances_geo = trainer.get_distances_geo()
    
    # Print the amount of different distances
    print 'Number of computed distances = ' + repr( len(distances_geo) )
    
            