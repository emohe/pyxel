from math import radians, cos, sin, asin, sqrt
import nltk
import numpy as np

  
def calculate_distances(metadata, document_id1,document_id2):
    """ Calculates all possible distances between the two provided documents """
    document1 = metadata.get_document(document_id1)
    document2 = metadata.get_document(document_id2)

    # Time distance
    time_dist= calculate_distance_time(document1, document2)
    
    # Geolocation distance
    gps_dist = calculate_distance_geo(document1, document2)
    
    # User distance
    same_user=calculate_distance_user(document1, document2 )

    # Jaccard distance for tags
    label_dist=calculate_distance_tags_Jaccard(document1, document2 )
    
    # Return a numpy vector with the distances in a row
    return np.array([time_dist,gps_dist,same_user,label_dist])
    
    
def calculate_distance_time(document1, document2):
    return np.abs(document1['timestamp_taken']-document2['timestamp_taken'])
    
    
def calculate_distance_geo(document1, document2):
    # Geolocation distance
    lat1=document1['latitude']
    lon1=document1['longitude']
    lat2=document2['latitude']
    lon2=document2['longitude']
    
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    return km

def calculate_distance_user(document1, document2 ):
    return (document1['username'] == document2['username'])
    
def calculate_distance_tags_Jaccard(document1, document2 ):
   # Jaccard distance for tags
    strings1=set((document1['title']+" "+document1['description']+document1['tag']).split())
    strings2=set((document2['title']+" "+document2['description']+document2['tag']).split())
    if len(strings1) == 0 or len(strings2) == 0:
        return 1
    else:
        return nltk.distance.jaccard_distance(strings1,strings2)  
    