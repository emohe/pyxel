import argparse

# First read args to determine the project and the config space
parser = argparse.ArgumentParser(description='Run the app.')
parser.add_argument('--path', metavar='path', required=True,
                   help='route to the tool to execute, i.e: tools.xxx.pjt')
parser.add_argument('--env', metavar='env', required=False, default='prod',
                   help='execution enviroment. (dev, test, prod)')

args = parser.parse_args()

# Get the config

# Get the module to run
import_str = "from " + args.path + " import app"
exec import_str


# Run it
app.run(args.env)
