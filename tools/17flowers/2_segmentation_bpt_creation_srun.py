import srun		# You need to have the file srun.py in the same directory
import os		# In order to use the path functions

filenames = open('/imatge/jsanchez/work/17flowers/1_images/files.txt', 'r')	# File with the names of the images
toolpath = '/imatge/jsanchez/workspace/imageplus/tools/bpt_creation/bin/release/bpt_creation' # Tool to execute
inpath = '/imatge/jsanchez/projects/upseek/17flowers/jpg/'			# Directory of the input files
outpath = '/work/jsanchez/17flowers/2_segmentation/0200/'			# Directory to save the output files

imagenames = [line.strip() for line in filenames]				# Delete the \n of the lines in the txt file

for imagename in imagenames:							# For all images
    imagepath = os.path.join(inpath, imagename)					# Extract the image path
    srun.run(toolpath, ["--slic", "-N 200", imagepath, outpath], max_jobs=10)	# Launch tool into the computational service
filenames.close()	# Close the file
