import srun		# You need to have the file srun.py in the same directory
import os		# In order to use the path functions

filenames = open('/imatge/jsanchez/work/17flowers/1_images/files.txt', 'r')	# File with the names of the images
toolpath = '/imatge/jsanchez/workspace/imageplus/tools/bpt_population/bin/release/bpt_population' # Tool to execute
inpath = '/imatge/jsanchez/work/17flowers/2_segmentation/0200/'			# Directory of the input files
outpath = '/imatge/jsanchez/work/17flowers/3_features/0200/'			# Directory to save the output files

imagenames = [line.strip() for line in filenames]				# Delete the \n of the lines in the txt file

for imagename in imagenames:							# For all images
    imagepath = os.path.splitext(os.path.join(inpath, imagename))[0]		# Extract the image path without extension
    bptpath = imagepath + "-bpt.xml"						# Add the extensions

    # Launch tool into the computational service
    srun.run(toolpath, ["--VDSURF=2", bptpath, outpath], max_jobs=20, print_command=False, wait_to_finish = False)

filenames.close()	# Close the file
