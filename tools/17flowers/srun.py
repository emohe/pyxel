import os;
import subprocess;
import time;
import sys;

def run(tool_path, parameters, max_jobs = 10, out_file = sys.stdout, err_file = sys.stderr, wait=3, fast=False, mem=2000, run=True, print_command=False, wait_to_finish = False):
    # Run a given command to the cluster. It does not exit until executed
    
    done = False;
    
    name = os.path.basename(tool_path);
    
    command = 'squeue -o %60j | grep ' + name + ' | wc -l';
    
    parameter_string = " ".join([str(x) for x in parameters]);
    
    if (fast == False):
        srun_command = 'srun';
    else:
        srun_command = 'srun-fast';
        
    mem_string = "--mem=" + str(mem);
            
    if (wait_to_finish):
        async = "";
    else:
        async = "&";                
    
    command_to_run = srun_command + " " + mem_string + " " + tool_path + " " + parameter_string + async;        
    
    if (print_command):
        print command_to_run;
    
    while (done == False):
        proc_num_jobs = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE);
        num_jobs = int(proc_num_jobs.stdout.readline());
        
        if (num_jobs < max_jobs):
            done = True;
            
            
            if (run == True):
                subprocess.call(command_to_run, stdout=out_file, stderr=err_file, shell=True); #, stdout=devnull, stderr=devnull);
                time.sleep(0.1); # wait to create another process
        else:
            time.sleep(wait);
            
    return command_to_run;
    
    
def wait(tool_path):
    # Wait for the all the processes of a given name to finish
    
    name = os.path.basename(tool_path);
    command = 'squeue -o %60j | grep ' + name + ' | wc -l';
    
    terminated = False;
    while (not terminated):
        proc_num_jobs = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE);
        num_jobs = int(proc_num_jobs.stdout.readline());
        if (num_jobs != 0):
            time.sleep(3);
        else:
            terminated = True;
    
