# -*- coding: utf-8 -*-
"""
Created on Mon Feb  2 11:14:02 2015

@author: evamohe
"""

import sys, os
sys.path.append( 'EEG_reader/' )
from UserEEG import *
from EEGmaps import *
from sklearn.ensemble import RandomForestClassifier


'''
This script store the features of each user to disk, it generates a Pickle object.

createFeatureVectors() from 'utils.py' generate the final feature vector by concatenation of the channels selected: 32 if []
'''

dicX = pickle.load(  open( 'data/GT_windows/dictionary_X.bin' , 'rb') )
dicY = pickle.load(  open( 'data/GT_windows/dictionary_Y.bin' , 'rb') )


def storeFeaturesUser( list_users, path_data='../data', oldFashion=False, channels=[]):
    for user in  list_users:
        userSelected =  UserEEG( user, path_data )
        print 'IMAGES: ', userSelected.completedImaID
        
        path_featuresUsers = 'features'
        if not os.path.isdir( path_featuresUsers ):
            os.mkdir( path_featuresUsers )
        
        epochs, windows, images, triggers = userSelected.selectEpochsFromListID( userSelected.completedImaID )
        
        # Generate the features
        X_data = createFeatureVectors( epochs, channels, oldFashion, userSelected.time )
        Y_data = triggers
        Names = windows
        
        print 'Saving pickle object...'
        if oldFashion:
            fileOut = os.path.join( path_featuresUsers, user + '_epochs_allChannels_OLD.bin' )
        elif len(channels)>2:
            fileOut = os.path.join( path_featuresUsers, user + '_epochs_numChannels'+str(len(channels))+'.bin' )
        else:
            fileOut = os.path.join( path_featuresUsers, user + '_epochs_allChannels.bin' )
        
        fid = open( fileOut, 'wb' )
        pickle.dump( X_data, fid )
        pickle.dump( Y_data, fid )
        pickle.dump( Names, fid )
        pickle.dump( userSelected.completedImaID, fid )
        fid.close()

    print 'Stored! ', fileOut  

def readFeaturesUser( user ):
    
    file_to_read = os.path.join( 'features', user + '_epochs_allChannels.bin' )
    
    print 'Loading pickle object for %s...' %  user
    fid = open( file_to_read, 'rb' )
    X_data = pickle.load( fid )
    Y_data = pickle.load( fid )
    Names = pickle.load( fid )
    imagesID = pickle.load( fid )
    fid.close()

    print 'Loaded! \n (n_feats, n_dimensions)', np.shape( X_data )  
    return X_data, Y_data, Names, imagesID
    
def balanceData( X_train, y_train ):
    # get number targets
    num_targ =np.shape(y_train[y_train==1])[0]
    #get number distractors
    num_distractors = np.shape(y_train[y_train==0])[0]
    
    # data targets
    X_targets = X_train[ y_train==1,: ]
    y_targets = y_train[ y_train==1 ]
    
    # data distractors
    X_distractors = X_train[ y_train==0,: ]
    y_distractors = y_train[ y_train==0 ]
    
    # Random select same number of distractors than target
    index_distractors = np.arange( num_distractors )
    # shuffle
    np.random.shuffle( index_distractors )
    # select same num if targets
    index_distractors = index_distractors[:num_targ]
    
    # select data distractors
    X_distractors = X_distractors[ index_distractors, : ]
    y_distractors = y_distractors[ index_distractors ]
    
    print np.shape( X_distractors ), np.shape( X_targets )
    X_train = np.vstack( ( X_targets, X_distractors ) )
    y_train = np.concatenate( ( y_targets, y_distractors ) )
    
    return X_train, y_train
    
def selectImages( list_imageIDs, X_data, Y_data, Names ):
    # indec to image IDs
    imagesID = np.array( [ int(name.split('_')[1].split('ima')[1]) for name in Names ] )
    
    X_validation = []
    y_validation = []
    Names_validation = []
    
    # loop over the images to select the data
    for id_ in list_imageIDs:
        X_validation.extend(X_data[ imagesID==id_, : ])
        y_validation.extend(Y_data[ imagesID==id_ ])
        Names_validation.extend( Names[ imagesID==id_ ])
        
    #convert to numpy array
    X_validation = np.array(X_validation)
    y_validation = np.array(y_validation)
    Names_validation = np.array(Names_validation)
    
    return X_validation, y_validation, Names_validation

    
def evaluatingImages( clf, user, list_imageIDs, X_data, Y_data, Names, path_maps='eeg_maps', saveMaps=True, logFile = 'log.txt'):
    # indec to image IDs
    imagesID = np.array( [ int(name.split('_')[1].split('ima')[1]) for name in Names ] )
    
    results = []
    
    fid = open( logFile, 'a+' )
    fid.write( '\n===========\n' )
    fid.write( 'TestSet: \n' )
    for a in list_imageIDs:
        fid.write( str(a) +'\t' )
    fid.write('\n')
    # loop over the images to select the data
    print "Reporting results for user %s" % user
    for id_ in list_imageIDs:
        prediction = clf.decision_function(X_data[imagesID==id_,:]  )
        results.append( roc_auc_score( Y_data[ imagesID==id_ ], prediction ) )
        print "image %d \t %f" % ( id_, roc_auc_score( Y_data[ imagesID==id_ ], prediction ) )
        fid.write( "image %d \t %f\n" % ( id_, roc_auc_score( Y_data[ imagesID==id_ ], prediction ) ) )
        #Save maps
        buildEEG( user, Names[imagesID==id_], prediction, path_maps , saveMaps )

    fid.close()
    return results

from sklearn import svm
from sklearn import cross_validation
from sklearn.grid_search import GridSearchCV 
from sklearn import preprocessing
from sklearn.metrics import roc_curve, roc_auc_score


#model = 'rf'
model = 'svm'

if __name__ == '__main__':

    list_users = ['eva', 'evaM', 'kevin', 'sergi', 'user1', 'user2', 'user3', 'user4', 'user5']
    
    for user in list_users:
        X_data, Y_data, Names, imagesID = readFeaturesUser( user )
        X_data = preprocessing.scale(X_data)   
        
        set_training = set(imagesID[:])
        set_evaluation = set([1,3,4,5,6])
        set_training = set_training-set_evaluation
        
        print 'EVALUATION: ', set_evaluation
        print 'TRAINING: ', set_training
        print 'COMPLETED: ', imagesID
        
        
        X_train, y_train, Names_train = selectImages( set_training, X_data, Y_data, Names )
        print np.shape( X_train ), ' ', 192*len( set_training )

        X_test, y_test, Names_test = selectImages( set_evaluation, X_data, Y_data, Names )
        print np.shape( X_test ), ' ', 192*len( set_evaluation )        
        
        #X_train, X_test, y_train, y_test = cross_validation.train_test_split(X_data, Y_data, test_size=0.4, random_state=0)
  
        #clf = svm.SVC(kernel='linear', probability=True, class_weight='auto' )
        
        if model == 'svm':
            clf = svm.SVC( kernel='linear', class_weight='auto', probability=True )  
            parameters = { 'C': [1,10,100,1000] }
            clf = GridSearchCV( clf, parameters, cv=5, scoring='roc_auc' )
            clf.fit( X_train, y_train )
            
            print clf.grid_scores_
            print clf.best_estimator_
            #scores = cross_validation.cross_val_score(clf, X_train, y_train, cv=2)
        elif model == 'rf':
            clf = RandomForestClassifier(n_estimators=100, n_jobs=5)
            clf.fit( X_train, y_train )
        print 'DONE!'
        
        #print scores
        #prediction = clf.decision_function(X_test)
        
        # evaluating the set of images
        evaluatingImages( clf, user, set_evaluation,  X_test, y_test, Names_test, path_maps='eeg_maps', saveMaps=True  )

        #Print EEG maps
    
   
