# -*- coding: utf-8 -*-
"""
Created on Mon Jan 26 14:57:27 2015

@author: evamohe
"""

from UserEEG import *
from sklearn.preprocessing import Normalizer
from sklearn.metrics import roc_auc_score, roc_curve
from scipy.misc import imsave


def readScoresFromDisk( fileScores, path_out='' ):
    #Save in output in subdirectory called maps
    path_out = os.path.join( path_out, 'maps' )
    if not os.path.isdir( path_out ):
        os.mkdir( path_out )
    
    dicX = pickle.load(  open( 'data/GT_windows/dictionary_X.bin' , 'rb') )
    dicY = pickle.load(  open( 'data/GT_windows/dictionary_Y.bin' , 'rb') )
    data = np.loadtxt( open(fileScores, 'rb'), dtype='str' )
    
    windows = data[:,0]
    name = windows[0].split('_')[1]
    scale = windows[0].split('_')[-1]
    m = 200
    if scale == 'm1'or 'm1T':
        m=200
    elif scale == 'm2':
        m=400
    
    scores = data[:,1].astype(np.float)
    
    out = np.zeros( (2400,3200) )
    #counts = np.ones( (2400,3200) )
    
    for i in range( len( windows ) ):
        x = dicX[ windows[i] ]
        y = dicY[ windows[i] ]
        
        region = out[x:x+m,y:y+m]

        # Check if there is something in that region
        if np.sum(region)>0:
            #Ok, so.. where there is a value non zero, let's put the average of what we had
            # with the new EEG score
            region[region>0]=(region[region>0]+scores[i])/2
            #region[region>0]+=scores[i]
            # and let's put the new value to the empty region
            region[region==0]=scores[i]
            #counts[ region>0 ]+=1
        else:
            # If not just assign the EEG value
            region = scores[i]
        
        out[x:x+m,y:y+m]=region
    
    #out = out/counts
    
    if path_out != '':
        if not os.path.isdir( path_out ):
            os.mkdir( path_out )
    imsave(  os.path.join( path_out,name+'.png') , out)

def buildEEG( user, window_names, scores, path_out='eeg_maps' , save=True ):
    scores = scores - np.min(scores)
    scores = scores/np.max(scores)    

    ima_name = window_names[0].split('_')[1]
    #Check first level folder        

    if not os.path.isdir( path_out ):
        os.mkdir( path_out )
        
    #Check second lever folder
    path_out = os.path.join( path_out, user )
    if not os.path.isdir( path_out ):
        os.mkdir( path_out )
      
    # SVM scores files to store the scores   
    path_scores = os.path.join( path_out, 'scores' ) 
    if not os.path.isdir( path_scores ):
        os.mkdir( path_scores )
        
    fid = open(  os.path.join(path_scores, ima_name+'.txt'), 'wb') 
    for k, l in zip(window_names, scores):
        fid.write( k+'\t'+str(l)+'\n')
    fid.close()
    
    eegmask = readScoresFromDisk(  os.path.join(path_scores,ima_name+'.txt'), path_out )
    
   
    return eegmask

            

def main(userSelected):
    for id_ in [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22]:   #Put here the images for LOO  
        list_training =  [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22]
        list_training.remove( id_ )


        
        print   list_training      
        
        print "TEST ", id_, "   TRAINING ", list_training
        epochs_test, windows_test, images_test, triggers_test = userSelected.selectEpochsFromListID([id_])
        epochs_train, windows_train, images_train, triggers_train = userSelected.selectEpochsFromListID( list_training )
        
        # Generate the features
        channels = [] # all channels selected
        X_test = createFeatureVectors( epochs_test, channels )
        X_train = createFeatureVectors( epochs_train, channels )
        Y_test = triggers_test
        Y_train = triggers_train
          
        # Normalize the data
        normModel = Normalizer()
        normModel = normModel.fit( np.vstack((X_train,X_test)))
        X_train_ = normModel.transform( X_train )
        X_test_ = normModel.transform( X_test )
        print "Data normalized"
        
        print "Fitting the SVM model..."
        t0 = time.time()
        clf =sklearn.svm.LinearSVC()
        clf.fit(X_train_, Y_train)
        #  print "Best score: ", clf.best_score_
        print "Done!", time.time()-t0 
        print
        
        labels = clf.predict( X_test_ ) # Binary predictions
        decisions = clf.decision_function(X_test_) #EEG scores
        
        print "Building EEGmap"
        eegMap = buildEEG( user, windows_test, decisions )
        
        print np.shape( eegMap )
        
        fpr = []
        tpr = []
        thresholds = []
        evalutation( 'ima'+str(id_), userSelected.user, decisions, labels, Y_test, fpr, tpr, thresholds  ) 
        
def main_OLD(userSelected):
    
    for i in range(5):
        if i == 0:
            list_testing = [1,2,3,4,22]
            list_training = [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]
        elif i==1:
            list_testing = [6,7,8,9,10]
            list_training = [1,2,3,4,5,11,12,13,14,15,16,17,18,19,20,21,22]
        elif i==2:
            list_testing = [11,12,13,14,15]
            list_training = [1,2,3,4,5,6,7,8,9,10,16,17,18,19,20,21,22]

        elif i==3:
            list_testing = [16,17,18,19,20]
            list_training = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,21,22]
        elif i == 4:
            list_testing = [2,5,8,14,21]
            list_training = [1,3,4,6,7,9,10,11,12,13,15,16,17,18,19,20,22]

    
        for id_ in list_testing:
            print "TEST ", id_, "   TRAINING ", list_training
            epochs_test, windows_test, images_test, triggers_test = userSelected.selectEpochsFromListID([id_])
            epochs_train, windows_train, images_train, triggers_train = userSelected.selectEpochsFromListID( list_training )
            
            # Generate the features
            channels = [] # all channels selected
            X_test = createFeatureVectors( epochs_test, channels )
            X_train = createFeatureVectors( epochs_train, channels )
            Y_test = triggers_test
            Y_train = triggers_train
              
            # Normalize the data
            normModel = Normalizer()
            normModel = normModel.fit( np.vstack((X_train,X_test)))
            X_train_ = normModel.transform( X_train )
            X_test_ = normModel.transform( X_test )
            print "Data normalized"
            
            print "Fitting the SVM model..."
            t0 = time.time()
            clf =sklearn.svm.LinearSVC()
            sklearn.linear_model.LogisticRegression            
            clf.fit(X_train_, Y_train)
            #  print "Best score: ", clf.best_score_
            print "Done!", time.time()-t0 
            print
            
            labels = clf.predict( X_test_ ) # Binary predictions
            decisions = clf.decision_function(X_test_) #EEG scores
            
            print "Building EEGmap"
            eegMap = buildEEG( user, windows_test, decisions )
            
            print np.shape( eegMap )
            
            fpr = []
            tpr = []
            thresholds = []
            evalutation( 'ima'+str(id_), userSelected.user, decisions, labels, Y_test, fpr, tpr, thresholds  ) 

def evalutation( ima, user, decisions, labels, Y_test, fpr, tpr, thresholds  ):
    y_test = Y_test
   # print y_test
    y_pred = labels
   # print y_pred
    y_score = decisions
    path_out = os.path.join( 'eeg_out', user )
    if not os.path.isdir( path_out ):
        os.mkdir( path_out )
    fileLog = os.path.join( path_out, 'logAUC.txt' )
    # A better stimation is provided by the AUC value.
    try:
        auc_value = roc_auc_score( y_test, y_score )
        fid = open( fileLog, 'a+' )
        print 'Area Under the Curve: {:.3f}'.format(auc_value)
        fid.write( ima+'\t'+str(auc_value)+'\n' )
        fid.close()

    # Keep all the false positives, true possitive and thresholds vectors to later display the ROC qith all the queires

        fpr_, tpr_, thresholds_ = roc_curve(y_test, y_score)
        fpr.append( fpr_ )
        tpr.append( tpr_ )
        thresholds.append( thresholds_ )
    except:
        print np.unique(y_test), np.unique(y_pred), np.unique(y_score)
        print "OJO!"


if __name__ == '__main__':

    multiUsers = True
    if multiUsers:
        for user in  [ 'kevin','sergi']:
            path_data = '/Users/evamohe/workspace/EEG_journal_extension/data' 
            
            userSelected =  UserEEG( user, path_data )
            
            main( userSelected )  
    else:
        user = 'kevin'
        path_data = '/Users/evamohe/workspace/EEG_journal_extension/data' 
            
        userSelected =  UserEEG( user, path_data )
        #print userSelected.completedImaID
        main( userSelected )          
   
