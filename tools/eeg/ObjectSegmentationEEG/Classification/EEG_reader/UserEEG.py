# -*- coding: utf-8 -*-
from utils import *

import codecs, os
import pickle 
class UserEEG:
    """User class to whole EEG and Presentation recordings"""
    def __init__(self, user, path_data, load_epochs = True, load_eprime = True):
        
        self.user = user
        self.user_epochs = os.path.join( path_data, user, 'filter_1_70', 'epochs.txt' )
        self.user_eprime = os.path.join( path_data, user, 'filter_1_70', 'eprime_reduced.txt' )
        
        self.time = None
        
        #Load Image info
        
        try:
            print 'Loading dicctionaries...'
            self.dicTriggers = pickle.load(  open( 'data/GT_windows/dictionary_triggers.bin' , 'rb') )

        except:
            print 'Unable to load files!'
            print 'tmp/triggers.bin ', os.path.isdir( 'data/GT_windows/dictionary_triggers.bin' )
        
        
        # Save as pickle... it will load quicker
        if load_epochs:
            try:
                print 'loading from binary...'
                self.epochs = pickle.load(  open( os.path.join( path_data, user, user+'_epochs.bin' ), 'rb') )
            except:
                print 'Loading old school...'
                self.loadEpochs()
                pickle.dump( self.epochs, open(os.path.join( path_data, user, user+'_epochs.bin' ), 'wb')   )
            
        # Load epochs and IDs
        
        if load_eprime:
            try:
                self.loadEprime()
            except:
                self.load_reduced()
        
        self.user_details()
        
       
    def user_details(self):
        print "User name is %s" % self.user
        print "Path epochs is %s" % self.user_epochs
        print "Path eprime is %s" % self.user_eprime
    
    # Functions for EPOCHS--------
    def loadEpochs(self):
        print "Loading epochs...",  os.path.basename( self.user_epochs )
        epochs = load_epochs( self.user_epochs )
        self.epochs = epochs[0]
        self.time = epochs[1]   
        self.n_channels = np.shape(self.epochs)[2]
        
    def getNumberChannels(self):
        print "Number of channels %"
        return self.n_channels
        
    def getEpochs(self):
        return self.epochs
    
    def loadEprime(self):
        # Open Eprime file - ORDER OF THE IMAGES
    
        print "Loading eprime file... " ,  os.path.basename( self.user_eprime )
        
        if 'user' in self.user:
            parseString = 'fname:' 
            data = np.loadtxt( open(self.user_eprime, 'r'), dtype='str' )
            self.windows_name = data[:,0]        
            self.triggers = np.array( [ self.dicTriggers[ wind ] for wind in self.windows_name ] )
            
        else:
             # lists with names and triggers
            list_names = []
            list_triggers = []
            
            parseString = 'imageWindows:'
            f = codecs.open( self.user_eprime, "r", "utf-16" )
            file_content =f.read()
            f.close()
        
            # Create tmp folder if it doesn't exists
            if not os.path.isdir('tmp'):
                os.mkdir( 'tmp' )
        
            # Create temporal file to store the events list
            tmpFile = 'tmp/events.txt'
            a = file_content.encode('ascii')    
            f = open(  tmpFile , 'w' )
            f.write(a)
            f.close()
            
            # parse the relevant parameters imageWindows and Triggers
            for line in open( tmpFile, 'r'):
                line = line.split('\r\n')[0]
                # Get image names            
                if str(line).find( parseString  )>-1:
                    lineP = line.split( parseString )[1].split( '\\' )
                    list_names.append( lineP[-1] )  
                    
                    try:
                        list_triggers.append( self.dicTriggers[lineP[-1]])
                    except:
                        print "Error in find trigger for image ", lineP[-1]
                        print type(lineP[-1])
                        print "Check if it is key: ", lineP[-1] in self.dicTriggers.keys()
            
            if type( list_names )==list:    
                self.windows_name = np.array(list_names)    
            self.triggers = np.array(list_triggers)

        self.images_name =  np.array([int(ima.split('_')[1].split('ima')[1]) for ima in self.windows_name ])
        completedImaID = []
        for ID in np.unique( self.images_name ):
            numWind = 192
            if self.user == 'evaM':
                numWind = 405
            if np.shape(  self.triggers[ self.images_name==ID ] )[0] == numWind:
                completedImaID.append( ID )
        # ID of completed images    
        self.completedImaID = completedImaID
            
    def load_reduced( self ):
        data = np.loadtxt( userSelected.user_eprime, dtype='str' )
    
        # lists with names and triggers
        list_names = []
        list_triggers = []
        for name in data[:,0]:
            list_names.append( name )
            list_triggers.append( dicTriggers[ names ] )

        self.windows_name = np.array( list_names )
        self.triggers = np.array( list_triggers )
        self.images_name =  np.array([int(ima.split('_')[1].split('ima')[1]) for ima in self.windows_name ])

        completedImaID = []
        for ID in np.unique( self.images_name ):
            numWind = 192
            if self.user == 'evaM':
                numWind = 405
            if np.shape(  self.triggers[ self.images_name==ID ] )[0] == numWind:
                completedImaID.append( ID )
        # ID of completed images
        self.completedImaID = completedImaID



    def selectEpochsOneImage(self, ID):
        """ Selects the data relative to ONE image ID """
        if ID in self.completedImaID:
            return self.epochs[ self.images_name == ID,: ], self.windows_name[self.images_name == ID], self.images_name[self.images_name == ID], self.triggers[ self.images_name == ID ]
        else:
            print ID, " Not included in the completed images."
            print "Completed images are ", self.completedImaID
            
    def selectEpochsFromListID( self, ID ):
        """ Selects the data relative to one/a list of image IDs """
        if len(ID) >0:
            if len(ID)==1:
                return self.selectEpochsOneImage( ID )
            else:
                for id_ in range(len(ID)):
                    if id_==0:
                        #Load data for the first image
                        x, w, i, t = self.selectEpochsOneImage( ID[id_] )
                    else:
                        #Load new data
                        xx, ww, ii, tt = self.selectEpochsOneImage( ID[id_] )
                        
                        # append to the data load before
                        x = np.vstack( ( x,xx ) )
                        w = np.concatenate( ( w,ww ) )
                        i = np.concatenate( ( i,ii ) )
                        t = np.concatenate( ( t,tt ) )
                return x,w,i,t
        else:
            print "The function is specting a list of ID"
            print "You have called the function with ", ID