# -*- coding: utf-8 -*-
"""
Created on Mon Jan 26 14:58:08 2015

@author: evamohe
"""

import numpy as np
import time
import sklearn

import sklearn.grid_search
import sklearn.svm
from scipy.interpolate import interp1d

def load_epochs( epoch_target_data_file ):
    try:
        data_relevant = np.loadtxt( epoch_target_data_file )
    except:
        print 'Skiping first row...'
        data_relevant = np.loadtxt( epoch_target_data_file, skiprows=1 )

    time_ = np.unique( data_relevant[:,0] )
    N = np.shape(time_)[0]
    
    epochs = []
    for i in np.arange(np.shape(data_relevant)[0]/N):
        epochs.append(data_relevant[i*N:(i+1)*N,1:])
    epochs = (np.array(epochs))
    return epochs, time_

def computeMeanChannel( channel, epochs ):
    return np.average( epochs[:,:,channel], axis=0 )

def plotEEG( time_, vector, time_0, time_1 ):
    plot( time_[fm_original*(1+time_0):fm_original*(1+time_1)], vector[fm_original*(1+time_0):fm_original*(1+time_1)] )

def cuttime_Segment( vector, fm, time_0, time_1 ):
    return vector[fm*(1+time_0):fm*(1+time_1)]


def changeSampleRate( vector, fm_original, fm ):
    return vector[0:-1:fm_original/fm]
    
def changeSampleRate2( vector, fm_original, fm ):
    size = int( fm_original/fm )
    num_points = int( len(vector)/size )   

    new_vector = []    
    for i in range( num_points ):
        if i == 0:
            new_vector.append( np.average( vector[:(i+1)*size] ) )
        elif i == num_points-1:
            new_vector.append( np.average( vector[i*size:] ) )
        else:
            new_vector.append( np.average( vector[(i-1)*size:(i+2)*size] ) )
    return np.array( new_vector )

def createFeatureVectors( epochs, list_channels=[], oldFashion=False, timeEpochs = None ):
    n_epochs = np.shape(epochs)[0]
    n_channels = np.shape(epochs)[2]
    
    if len(list_channels)>0:
        list_chan = list_channels
    else:
        list_chan = range( n_channels )
    data = []
    for n in range( n_epochs ):
        concatened_signal = []
        for c in list_chan:
            
            if oldFashion:
                signal = old_featureMaker(  epochs[n, :, c], timeEpochs )
            else:
                signal = cuttime_Segment( epochs[n, :, c], 250, 0.2,1.0)
                signal = changeSampleRate2( signal, 250, 20 )
            
            concatened_signal.extend( signal )
        data.append( np.array(concatened_signal) )
    
    return np.array( data )


def load_targets( list_path ):
    print 'reading ', len(list_path), ' files...'
    allData_t = []
    allData_file = []
    
    for file_epoch in list_path:
        figure_epochs_target, time_ = load_epochs( file_epoch )
        allData_t.append( figure_epochs_target )
        allData_file.append( file_epoch.split('/')[-1])
    
    return time_, allData_t, allData_file

def old_featureMaker(epoch_vector, epoch_time, sampling_rate=20, timerange=(200,1000)):
    f=interp1d(epoch_time, epoch_vector)
    n_sampling = np.linspace(timerange[0], timerange[1], sampling_rate)
        
    vals = f(n_sampling).tolist()
    return vals



