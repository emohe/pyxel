
%% INIT PATHS
% path to the data
path_croped = 'C:\Users\Eva\Desktop\EEG_Images\Cropped_Images\'
path_images = 'C:\Users\Eva\Desktop\EEG_Images\Entire_Images\'

%% Selection of the image
path_image = uigetfile( strcat(path_images,'*.jpg') );
[pathstr,name,ext] = fileparts(path_image);

% READ File label images
path_wind_imageSelected = sprintf('%s\\%s_m1\\', path_croped, name )

% Read data
fid = fopen( sprintf('%s%s_m1.txt',path_wind_imageSelected, name) )
data = textscan( fid, '%d%d%d%d%d' )
fclose(fid)

% Percentage objects
pObj = data{4};
label = ones( length(pObj),1 );

% Label for the image
label( find( pObj == 0 ) ) = 0;



%Generate random labels
% Prob = 80% accuracy
p = 0.9;

[ labelSimulated ] = generateSimulatedLabels( p, label );


% Generate mask

ima = imread( [pathstr,name,ext] );

rows = size( ima, 1 );
cols = size( ima, 2 );

mask = zeros( rows, cols );

% Size of the windows
windSize = 200;

xPos = data{2};
yPos = data{3};


%Pint windows
for i = 1:length( labelSimulated )
    mask( xPos(i):xPos(i)+windSize-1, yPos(i):yPos(i)+windSize  ) = labelSimulated(i);
    
end

maskClassLevel = mask;
% figure('name', 'Simulated Mask')
% subplot(1,2,1), imshow( ima )
% subplot(1,2,2), imshow( mask )

% Generate SVM score
close all
% EASY CASE: Uniform dist of 1  between 0.5 and 0.9
nSamples = length( labelSimulated );
indSamples = randperm( nSamples );

% Normal Distribution 
mu = mean(p1)
sigma = std(p1)
probV = (randn(1000, 1))*sigma + 0.5;
probV = probV/max(probV);

% Distribution 
mu = mean(p1)
sigma = std(p1)
probV = (randn(1000, 1))*sigma/2 + 0.9;
probV = probV/max(probV);


SVM_score = probV(indSamples);
SVM_score( find( SVM_score <0.5) ) = 1 - SVM_score( find( SVM_score <0.5) );


% Pint Windows
for i = 1:length( labelSimulated )
    
    if labelSimulated(i) == 1
        mask( xPos(i):xPos(i)+windSize-1, yPos(i):yPos(i)+windSize  ) = SVM_score(i);
        
    else
        mask( xPos(i):xPos(i)+windSize-1, yPos(i):yPos(i)+windSize  ) = abs( SVM_score(i)-1 );
    end
    
end


%Print windows
figure('name', 'Simulated Mask SVM')
subplot(1,3,1), imshow( ima )
xlabel( 'Image' )
subplot(1,3,2), imshow( maskClassLevel )
xlabel( 'Simulated Classification' )
subplot(1,3,3), imshow( mask )
xlabel( 'Simulated EEGmap' )