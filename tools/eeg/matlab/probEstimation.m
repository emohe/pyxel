pathData= 'C:\Users\Eva\Desktop\SVM_data\FirstTest_EvasData\'

%% Read Predictions SVM for the test data
fid = fopen( [pathData, 'o.pred.txt'] );

header = fgets(fid)

data = textscan(fid, '%d%f%f'  );
label = data{1};
p1 = data{2};
p0 = data{3};

%% Read the GT labels for the test data classified
fid = fopen( [pathData, 'test'] );

i = 1;
dline = 1;
while (  dline ~= -1)
    dline = fgets(fid);
    
    if isempty(dline) || ~ischar(dline), break, end

    labelTruth(i) = str2num( dline(1:2) );
    i=1+i;
end

fclose( fid );

labelTruth = labelTruth';


%% Get Accuracy for each different class

wellClass = label( label == labelTruth );
pWellClass = p1( label == labelTruth );
figure( 'name', 'Well Classified' )
hist( pWellClass )
[h, n]=hist( pWellClass );
xlabel( 'Prob Well Classified' )
ylabel( 'Number of instances' )
legend( num2str(sum(h)) )


%%

wellClass = label( label == labelTruth & label == 1);
pWellClass_1 = p1( label == labelTruth & label == 1 );
pWellClass_0 = p1( label == labelTruth & label == -1 );

figure( 'name', 'Well1Classified' )
subplot( 2,1,1 ), hist( pWellClass_1, 19 )
subplot( 2,1,2 ), hist( 1-pWellClass_0, 10 )



%%
badClass = label( label ~= labelTruth );
pBadClass = p1( label ~= labelTruth );
figure( 'name', 'Bad Classified' )
hist( pBadClass )
[h, n]=hist( pBadClass );
xlabel( 'Prob Bad Classified' )
ylabel( 'Number of instances' )
legend( num2str(sum(h)) )


%%
badClass = label( label ~= labelTruth & label == 1);
pBadClass_1 = p1( label ~= labelTruth & label == 1 );
pBadClass_0 = p1( label ~= labelTruth & label == -1 );

figure( 'name', 'Bad1Classified' )
subplot( 1,2,1 ), hist( pBadClass_1, 5 )
subplot( 1,2,2 ), hist( pBadClass_0, 5 )


%%
nGTOnes = length( find( labelTruth == 1 ) );
nGTZeros = length( find( labelTruth == -1 ) );

% Suposed Ones in the final predicction

supsOnes = label( 1:nGTOnes );
display( sprintf( '** Class Ones %d/%d',  length( find(supsOnes == 1)), nGTOnes ) );
accOnes = length( find(supsOnes == 1) )/nGTOnes

% Suposed Zeros in the final predicction
supsZeros = label( nGTOnes+1:end );
display( sprintf( '** Class Zeros %d/%d',  length( find(supsZeros == -1)), nGTZeros ) );
accZeros = length( find(supsZeros == -1) )/nGTZeros


%% Get prob to Ones
close all
% Well classified
indexRealOnes = find(supsOnes == 1 )
pRealOnes = p1( indexRealOnes );

figure( 'name', 'ONES GT' )
[h, n] = hist( pRealOnes , 20);
%y = h/sum(h);
histfit( pRealOnes, 10, 'kernel')
xlabel('Probability value')
ylabel( '# of predicted 1' )
legend( num2str( sum(h)) )


% Bad Classified
indexFalseOnes = find(supsZeros == 1 )
pFalseOnes = p1( nGTOnes+1:end );

figure( 'name', 'ONES False GT' )
[h, n] = hist( pFalseOnes( indexFalseOnes ) , 20);
%y = h/sum(h);
histfit( pFalseOnes( indexFalseOnes ), 10, 'kernel')
xlabel('Probability value')
ylabel( '# of predicted 1' )
legend( num2str( sum(h)) )



%%
close all

figure('name', 'hist p1')
%histfit( p1( find(label==1) ), 100 ,'kernel')
[h, n] = hist( p1( find(label==1) ), 20);
y = h/sum(h);
histfit( p1( find(label==1) ), 10, 'kernel')
xlabel('Probability value')
ylabel( '# of predicted 1' )
legend( sum(h) )

figure('name', 'hist p0')
[h, n] = hist( p0( find(label==-1) ), 20);
histfit( p0( find( label==-1 ) ), 10, 'kernel' )
xlabel('Probability value')
ylabel( '# of predicted 0' )
legend( sum(h) )


fclose( fid )

close all
figure('name', 'all P1')
hist( p1 )

%% Generate the distribution
close all
hist(p1)
mu = mean(p1)
sigma = std(p1)

a = (randn(1000, 1))*sigma/2 + 0.9;
a = a/max(a)

figure('name', 'generated dist' )
hist(a)

%%
close all
% Create a normally distributed (mu: 5, sigma: 3) random data set
x = p1( find(label==1) );

hist(x)
% Compute and plot results. The results are sorted by "Bayesian information
% criterion".
[D, PD] = allfitdist(x, 'PDF');
