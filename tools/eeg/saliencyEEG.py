from skimage import io, color

import os
import saliency.maps
import matplotlib.pyplot as plt

def show_maps( dirMaps, title=None ):
    
    # Set a title to the figure
    plt.suptitle( title )

    # Obtain a list of the probability maps in files
    filesMaps = os.listdir(dirMaps)
    
    for i in range(0,4):
    
        pathMap = os.path.join( dirMaps, filesMaps[i] )
        #print pathMap
        probMap = io.imread( pathMap )
        probMap = color.rgb2gray( probMap )
        
        plt.subplot(220 + i)
        frame = plt.gca()
        frame.axes.get_xaxis().set_visible(False)
        frame.axes.get_yaxis().set_visible(False)
        
        plt.imshow( probMap )
        
    plt.show() 


def show_saliency_binarizations(dirSaliency):
    
    plt.suptitle( 'Binarization of the saliency maps' )
     
    # Obtain a list of the probability maps in files
    filesMaps = os.listdir(dirSaliency)
    
    # For saliency maps that is to be considered...
    for i in range(0,4):
        
        pathFile = filesMaps[i]
            
        # Extract the basename from the filename
        basename = os.path.splitext( pathFile )[0]
        
        # Generate a ground truth binary mask for each considered binarization
        # thresholds
        binaryMasks = saliency.maps.binarize_saliency_maps( dirSaliency, basename )
        
        # For each binary mask generate from the saliency map...
        for j in range(0,4):
        
            ( binFgRatio, binaryMask ) = binaryMasks[i]
        
            plt.subplot(440 + i*4 + j)
            frame = plt.gca()
            frame.axes.set_title( '%s %.2f'% (basename, binFgRatio) )
            frame.axes.get_xaxis().set_visible(False)
            frame.axes.get_yaxis().set_visible(False)
            
            plt.imshow( binaryMask )
            


# Main
if __name__ == "__main__":

    # Paths to the data
    pathEeg = '/Users/xavi/work/eeg'
    dirSaliency = os.path.join(pathEeg, '4_saliency/gbvs')
    dirMaps = os.path.join( pathEeg, '3_maps/averaged')
    
    plt.close('all')
    
    ## Generate a figure with the four images to process
    plt.figure(1)
    show_maps( dirMaps, 'Probability maps' )
    #
    plt.figure(2)
    show_maps( dirSaliency, 'Saliency maps' )    

    # Plot binary maps for each input image and FG ratio
    plt.figure(3)
    show_saliency_binarizations(dirSaliency)

    # Plot Jaccard indices
    plt.figure(4)
    saliency.maps.show_jaccard_singles( dirSaliency, dirMaps )
    
    plt.figure(5)
    saliency.maps.show_jaccards_averaged( dirSaliency, dirMaps )
    
    ## Save figure in the specified directory
    #segmentation.maps.save_single_jaccards( pathGt, pathMaps, pathEeg )