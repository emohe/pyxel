
import os
import segmentation.maps
import matplotlib.pyplot as plt


# Paths to the data
pathEeg = '/Users/xavi/work/eeg'
pathGt = os.path.join(pathEeg, '2_gt')
pathMaps = os.path.join( pathEeg, '3_maps/averaged')

# Generate a figure
#plt.figure(1)
#segmentation.maps.show_single_jaccards( pathGt, pathMaps )
#
#plt.figure(2)
segmentation.maps.show_averaged_jaccards( pathGt, pathMaps )

## Save figure in the specified directory
#segmentation.maps.save_single_jaccards( pathGt, pathMaps, pathEeg )

    