
import os

pathEthz = '/Users/xavi/work/ethz'

pathImages = os.path.join(pathEthz, '1_images')
pathDatasets = os.path.join( pathEthz, '2_datasets')

# Define the filename to contain the list of images to process
filenameOut = os.path.join( pathDatasets, 'train.txt')
outfile = open( filenameOut, 'w')

# For each class
for category in os.listdir(pathImages):

    # For each JPG image
    dirClass = os.path.join( pathImages, category )
    for fileImage in os.listdir( dirClass ): 
        
        if fileImage.endswith('.jpg'):
            
            # Extract the basename from the filename
            basename = os.path.splitext( fileImage )[0]

            # Add the filename to the output list of files
            labelOut = os.path.join( category, basename )
            outfile.write ( labelOut + '\n' )
            
            # DEBUG: display filename in console
            print labelOut
            
# Close output file
outfile.close()