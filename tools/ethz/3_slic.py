import os

from modules.segmentation.SegmentorSlic import SegmentorSlic

# Read the dataset file
pathHome = os.path.expanduser('~')

pathWork = os.path.join( pathHome, 'work','ethz' )

pathDirImages = os.path.join( pathWork, '1_images' )
pathDirSlic = os.path.join( pathWork, '3_segmentation','slic' )

# Read the dataset file
pathDataset = os.path.join( pathWork, '2_datasets','train.txt' )

# Read lines from the text file, stripping the end of line character
basenames = [line.strip() for line in open( pathDataset, 'r' )]

# Init the segmentator engine based on SLIC
segmentorSlic = SegmentorSlic(True)

# For each image in the list
for basename in basenames:
    
    # Build the path to the image file
    pathImage = os.path.join( pathDirImages, basename + '.jpg' )
    
    # If necessary, create a category directory where segmentation is to be stored
    category, label = os.path.split( basename )
    pathDirSlicCategory = os.path.join( pathDirSlic, category )
    if os.path.exists( pathDirSlicCategory) == False:
        os.makedirs( pathDirSlicCategory )
    
    # Segment the image and generate a new image with the overlaid contours
    segmentorSlic.processImage( pathImage, None, pathDirSlicCategory )
    
print 'SLIC segmentation... completed.'
