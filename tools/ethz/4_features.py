from PIL import Image
from pylab import *
from skimage import io

from skimage.feature import hog

import os


# Read the dataset file
pathEthz = '/Users/xavi/work/ethz'

pathDataset = os.path.join( pathEthz, '2_datasets/train.txt' )

fileDataset = open( pathDataset, 'r')
fileDataset.close()

# Load the collection of images in the list of files
io.im_read_collection( fileDataset, False )

#For every image
for pathImage in fileDataset:
    
    # Read an image from disk
    im = io.imread( pathImage )
    
    # Extract HOG features
    fd, hog_image = hogtools.processImage( im )
    
    # Save HoG features to disk
    f = open( '/Users/xavi/work/ethz/debug/hog.pkl', 'w')
    pickle.dump( fd, f )
    f.close()

# Close