from skimage import io

import os
import pickle
import hogtools

# Read the dataset file
pathEthz = '/Users/xavi/work/ethz'

pathDirImages = os.path.join( pathEthz, '1_images' )
pathDirHog = os.path.join( pathEthz, '3_features/hog' )

# Read the dataset file
pathDataset = os.path.join( pathEthz, '2_datasets/train.txt' )

# Read lines from the text file, stripping the end of line character
basenames = [line.strip() for line in open( pathDataset, 'r' )]

for basename in basenames:
    
    print 'Extracting HoG features from ' + basename + '...'

    pathImage = os.path.join( pathDirImages, basename + '.jpg' )
    
    # Read an image from disk
    im = io.imread( pathImage )
    
    # Extract HoG features
    fd, hog_image = hogtools.processImage( im )
    
    # If necessary, create a categoryd irectory where features are to be stored
    category, label = os.path.split( basename )
    pathDirHogCategory = os.path.join( pathDirHog, category )
    if os.path.exists( pathDirHogCategory) == False:
        os.makedirs( pathDirHogCategory )
    
    # Save HoG features to disk
    pathHog = os.path.join( pathDirHogCategory, label + '.pkl' )
    f = open( pathHog, 'wb')
    pickle.dump( fd, f )
    f.close()

print 'HoG features extraction... completed.'
