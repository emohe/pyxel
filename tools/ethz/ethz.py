from PIL import Image
import os

pathEthz = '/Users/xavi/work/ethz'

pathImages = os.path.join(pathEthz, '1_images')
pathDatasets = os.path.join( pathEthz, '2_datasets')

# Define the filename to contain the list of images to process
filenameOut = os.path.join( pathDatasets, 'train.txt')
outfile = open( filenameOut, 'w')

# For each class
for category in os.listdir(pathImages):

    # For each JPG image
    dirClass = os.path.join( pathImages, category )
    for fileImage in os.listdir( dirClass ): 
        
        if fileImage.endswith('.jpg'):

            # Add the filename to the output list of files
            labelOut = os.path.join( category, fileImage )
            outfile.write ( labelOut + '\n' )
            
            # DDEBUG: isplay filename in console
            print labelOut
        
            # Read an image from disk
            #pil_im = Image.open(os.path.join(path_images,'/Users/xavi/work/ethz/1_images/Applelogos/white.jpg')
    
            # Generate a thumbnail
            #thumb = pil_im.thumbnail( 128, 128 )
    
            # Save image to disk
            
# Close output file
outfile.close()