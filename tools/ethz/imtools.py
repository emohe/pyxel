# -*- coding: utf-8 -*-
'''
Created on 14/08/2013

Functions copied from the book "Programming Computer Vision with Python" 
by Jan Erik Solem



@author: xavi
'''

import os

def get_imlist(path):
    """ Returns a list of filenames for
    all jpg images in a directory. """
    return [os.path.join(path,f) for f in os.listdir(path) if f.endswith('.jpg')]