#!/bin/sh

# ${1} directory of files to parse. e.g. /home/sergi/Desktop/MSc
# ${2} extension

# mkdir ${1}/Frames

BIN=$HOME/bin/bpt_creation_32

WORK_DIR=$HOME/work
VIDEOS_DIR=$WORK_DIR/0_videos
FRAMES_DIR=$WORK_DIR/1_frames
for i in `ls $VIDEOS_DIR/*.mp4`
do
	
	rootname=$(basename "$i" .mp4)
	echo 'rootname	: ' $rootname
	# Create the directory name where the frames of the video will be saved to
	FRAMES_VIDEO_DIR=$FRAMES_DIR/${rootname}"pp"
	mkdir $FRAMES_VIDEO_DIR

	echo $i
	#ffmpeg -i ${1}/${filename}.${2} -r 30 -f image2 ${1}/Frames/${filename}/image$filename-%3d.jpeg 
	ffmpeg -i $i -f image2 $FRAMES_VIDEO_DIR/$rootname-%1d.jpeg
done
