#!/bin/sh

# This script uses the B_PERCEPTUAL developed by Xavier Giro
# Written by Xavier Giro
# 15th January 2007

BIN=$HOME/bin/bpt_creation_32

CONFIG=./2_bpt_creation.cfg

WORK_DIR=$HOME/work
FRAMES_DIR=$WORK_DIR/1_frames
SEGMENTATION_DIR=$WORK_DIR/2_segmentation

for VIDEO_DIR in $FRAMES_DIR/*; 	
do	
	if [ -d $VIDEO_DIR ]
	then 		
		VIDEO=`basename $VIDEO_DIR`
		echo 'VIDEO: ' $VIDEO

		# If necessary, create the VIDEO directory in the partition/bpt dir
		RESULTS_VIDEO_DIR=$SEGMENTATION_DIR/$VIDEO
		#echo 'RESULTS_VIDEO_DIR: ' $RESULTS_VIDEO_DIR
		mkdir -p $RESULTS_VIDEO_DIR

		for FRAME in $VIDEO_DIR/*; do

			# Check if BPT was already generated for this FRAME
			BPT=$RESULTS_VIDEO_DIR/`basename $FRAME .jpg`-bpt.xml

			if [ -f $BPT ]; then
				# Do nothing if features were already generated
				echo 'BPT previously generated in '$BPT' ***'	
			else				
				echo 'Calculating BPT from '$FRAME' ...'
				# echo $BIN '--config '$CONFIG' -t '$THUMBNAILS_VIDEO_DIR $FRAME $RESULTS_VIDEO_DIR
				$BIN -t $RESULTS_VIDEO_DIR $FRAME $RESULTS_VIDEO_DIR
			fi
		done
	fi
done








