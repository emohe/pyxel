import os
import sys

from modules.features.VisualVocabulary import VisualVocabulary

def usage():
    print "This script will generate a visual vocabulary."


def main(argv):
    
    # Define the default values for the options
    pathHome = os.path.expanduser('~')
    pathWork = os.path.join( pathHome, 'work')  
    pathDirImages = os.path.join( pathWork, '1_frames', 'train' )
    pathFileDatasetTrain = os.path.join(pathWork, '2_datasets', 'train.txt')
    pathVocabulary = os.path.join(pathWork, '4_vocabulary', 'vocabulary.p')

    _flagVerbose=False
    
    vocabularySize=256                  # Amount of visual words
    maxNumImages=10                     # Maximum amount of images to consider

    # Init the Visual Vocabulary
    visualVocabulary = VisualVocabulary(flagVerbose=_flagVerbose)
    
    
    visualVocabulary.buildFromImageCollection( pathFileDatasetTrain, 
                                              pathDirImages, 
                                              'jpeg', 
                                              vocabularySize, 
                                              maxNumImages )
    
    visualVocabulary.saveToDisk( pathVocabulary )


if __name__ == "__main__":
    main(sys.argv[1:])
