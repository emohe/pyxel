# tool to build the textual and visual models

import os
import time

from modules.socialevent.classification.Trainer import Trainer


if __name__ == "__main__":
    
    pathHome = os.path.expanduser('~')
    pathWork = os.path.join( pathHome, 'work', 'mediaeval','2013-sed','classification' )    
    pathDirModels = os.path.join( pathWork, '5_models')
    
    # If necessary, create a directory to save the models
    if not os.path.exists(pathDirModels):
        print 'New directory created at ' + pathDirModels
        os.makedirs(pathDirModels)
    
    HOME = os.path.expanduser('~')
    pathDirDatasets = os.path.join( pathWork,'2_datasets')
    pathFileFeaturesImage = os.path.join(pathWork,'4_bof','train')
    pathFileFeaturesText = os.path.join(pathWork,'4_tfidf','train')
    basenameGroundTruth = 'sed2013_task2_dataset_train_gs.csv'

    
    # Init trainers
    pathFileGroundTruth = os.path.join( pathDirDatasets, basenameGroundTruth )
    visual_trainer = Trainer(  pathFileFeaturesImage, pathFileGroundTruth )
    textual_trainer = Trainer( pathFileFeaturesText, pathFileGroundTruth)
    
    # Run trainers
    pathFileDataset = os.path.join( pathDirDatasets, 'train.txt')
    
    t_ref = time.time()
#    print 'Training with visual features...'
    
#    visual_trainer.run( pathFileDataset )
    
#    visual_time = time.time() - t_ref
#    print 'Visual classifier trained in ' , visual_time, ' seconds'

    print 'Training with textual features...'
    t_ref = time.time()
    textual_trainer.run( pathFileDataset )
    
    textual_time = time.time() - t_ref
    print 'Textual classifier trained in ',textual_time, ' seconds'
    

    # Save models to disk
    textual_trainer.save_model_to_disk( pathDirModels, 'textual_model_svm.p')
#    visual_trainer.save_model_to_disk( pathDirModels, 'visual_model_svm.p')
    
    
   