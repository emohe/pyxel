#!/bin/sh

# ${1} directory of files to parse. e.g. /home/sergi/Desktop/MSc
# ${2} extension

BIN=$HOME/bin/bpt_creation_32

CONFIG=./bpt_creation.cfg

WORK_DIR=$HOME/work
FRAMES_DIR=$WORK_DIR/1_frames
SEGMENTATION_DIR=$WORK_DIR/2_segmentation

mkdir ${1}/Frames

for i in `ls ${1}/*.${2}`
do
	if [ -d ${1}/${i} ]
	then
    		echo "${i} is a directory"
	else
		filename=$(basename "$i" .${2})
		mkdir ${1}/Frames/${filename}
		echo $filename	
		#ffmpeg -i ${1}/${filename}.${2} -r 30 -f image2 ${1}/Frames/${filename}/image$filename-%3d.jpeg 
		ffmpeg -i ${1}/${filename}.${2} -f image2 ${1}/Frames/${filename}/%1d.jpg
 	
	fi
done
