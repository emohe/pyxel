import os
import sys
import pickle

from modules.features.Annotation import Annotation
from modules.features.AnnotatedSemanticClass import AnnotatedSemanticClass
from modules.features.Ontology import Ontology
from modules.features.LabelType import LabelType

def usage():
    print "This script will generate a ground truth."


def main(argv):
    
	# Define the default values for the options
	ontology = Ontology()
	#pathDirAnns='/home/sergi/Desktop/MSc/ann/'
	pathHome = os.path.expanduser('~')
	pathWork = os.path.join( pathHome, 'work')  
	pathDirAnns = os.path.join( pathWork, '3_annotation' ) 
 
	for product in os.listdir(pathDirAnns):

	    proName=os.path.splitext(product)[0]
	    ontology.addSemanticClass(proName)	
            
        
        
	ann = Annotation( ontology )
     
	for product in os.listdir(pathDirAnns):
	    
	   
    	    pathProduct = os.path.join(pathDirAnns, product)
            fileP=open(pathProduct, 'r')

	    proName=os.path.splitext(product)[0]
            data = fileP.readlines()
            for  line  in  data:

            	line=line.rstrip()
            	dataWords = line.split()
            	dID=dataWords[0]
                lt=dataWords[1]
       
        	if lt=='1':
            		ann.addInstance( proName, LabelType.POSITIVE, dID)  
        	if lt=='-1':   
            		ann.addInstance(proName,LabelType.NEGATIVE,dID)
            
    	    fileP.close()
	print ann.getDocIdsInClass( proName, LabelType.POSITIVE )
	HOME = os.path.expanduser('~')
        DATASETS_PATH = os.path.join(HOME,'work')
        gtf = 'eyetracker_gt.p'
	gtPath = os.path.join(DATASETS_PATH, gtf )
	pickle.dump( ann, open( gtPath, "wb" ) )
	

if __name__ == "__main__":
    main(sys.argv[1:])
