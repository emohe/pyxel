import os
import time

from modules.socialevent.classification.Detector import Detector


    
if __name__ == "__main__":
    
    pathHome = os.path.expanduser('~')
    pathWork = os.path.join( pathHome, 'work', 'mediaeval','2013-sed','classification' )    
    # load models from path:
    pathDirModels = os.path.join( pathWork, '5_models')
    # load fueatures from path:
    pathDirDatasets = os.path.join( pathWork,'2_datasets')
    pathDirFeaturesImage = os.path.join(pathWork,'4_bof','test')
    pathDirFeaturesText = os.path.join(pathWork,'4_tfidf','test')
    
    pathFileVisualModel = os.path.join(pathDirModels,'visual_model_svm.p')
    pathFileTextualModel = os.path.join(pathDirModels,'textual_model_svm.p')
    
    # Init detectors
    visual_detector = Detector(pathFileVisualModel )
    textual_detector = Detector(pathFileTextualModel)
    
    # file with test images ids
    pathFileDataset = os.path.join( pathDirDatasets, 'test.txt')
    
    # Run detectors
    t_ref = time.time()
    print 'Detecting visual test images...'
    
    visual_voting = visual_detector.get_voting_collection( pathFileDataset, pathDirFeaturesImage )
    
    visual_time = time.time() - t_ref
    print 'Visual classifier detected in ' , visual_time, ' seconds'

    print 'Detecting textual text images...'
    t_ref = time.time()
    textual_voting = textual_detector.get_voting_collection( pathFileDataset,pathDirFeaturesText )
    
    textual_time = time.time() - t_ref
    print 'Textual classifier detected in ',textual_time, ' seconds'
    
    #path to save results
    pathDirResults = os.path.join( pathWork,'5_results')
    #file to save results
    pathFileVisualResults = os.path.join( pathDirResults,'visual_results.txt')
    pathFileTextualResults = os.path.join( pathDirResults,'textual_results.txt')
    
    #combine vots
    combine_vots,labels = textual_detector.combine_voting_in_added_with(visual_voting)
    
    results = textual_detector.predict_class_from_voting(combine_vots)    
    
    # Save resuts to file
    visual_detector.save_to_file(pathFileVisualResults)
    textual_detector.save_to_file(pathFileTextualResults)
    
    