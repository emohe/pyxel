# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import cv2
import numpy as np,sys
import os
import sklearn

# <codecell>

#Path with the original files
path_folders = '/Users/evamohe/Desktop/CheckVentory' 
path_originals = path_folders+'/tom'
path_recaptured = path_folders+'/Tom_recaptured'
path_recaptured_renamed = path_folders+'/tom_recaptured_renamed'

# <codecell>

list_recapt = []
list_original = []
[list_recapt.append(name) for name in os.listdir(path_recaptured) if '.JPG' in name]
[list_original.append(name) for name in os.listdir(path_originals) if '.JPG' in name]

print list_recapt[5], list_original[5]

# <codecell>

img = cv2.imread( path_recaptured+'/'+list_recapt[10] )
img2 = cv2.imread( path_originals+'/'+list_original[10] )
subplot(121),imshow(img)
subplot(122),imshow(img2)
show

# <codecell>

desc_original = []
sift = cv2.SIFT()

directory = path_folders+'/Sift_originals'

if not os.path.exists(directory):
    os.makedirs(directory)
    
for i in range(len(list_original)):
    img = cv2.imread( path_originals+'/'+list_original[i] )
    gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    kp, des = sift.detectAndCompute(gray,None)
    
    np.savetxt(directory+'/'+list_original[i]+'_sift.txt', (des))
    desc_original.append(des)
    
    print "%d / %d" % (i, len(list_original))




# <codecell>

desc_recaptured = []
sift = cv2.SIFT()

directory = path_folders+'/Sift_recaptured'

if not os.path.exists(directory):
    os.makedirs(directory)
    
for i in range(len(list_recapt)):
    img = cv2.imread( path_recaptured+'/'+list_recapt[i] )
    gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    kp, des = sift.detectAndCompute(gray,None)
    
    np.savetxt(directory+'/'+list_recapt[i]+'_sift.txt', (des))
    desc_recaptured.append(des)
    
    print "%d / %d" % (i, len(list_recapt))



# <codecell>

arr_desc_original = vstack(tuple(desc_original))
arr_desc_recaptured = vstack(tuple(desc_recaptured))

tot = vstack([arr_desc_original,arr_desc_recaptured ])
print shape(tot)

# <codecell>

from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.datasets.samples_generator import make_blobs
import time

# <codecell>

##############################################################################
# Compute clustering with MiniBatchKMeans

X = tot

mbk = MiniBatchKMeans(init='k-means++', n_clusters=128,
                      n_init=10, max_no_improvement=10, verbose=0)
t0 = time.time()
mbk.fit(X)
t_mini_batch = time.time() - t0
mbk_means_labels = mbk.labels_
mbk_means_cluster_centers = mbk.cluster_centers_
mbk_means_labels_unique = np.unique(mbk_means_labels)

# <codecell>

# build the histogram for the images
directory_in = path_folders+'/Sift_originals'
directory_out = path_folders+'/Histo_originals'
if not os.path.exists(directory_out):
        os.makedirs(directory_out)
        
list_hist_o = []
# for each image
for i in range(len(list_original)):
    #Load descriptor
    a = loadtxt(directory_in+'/'+list_original[i]+'_sift.txt').view(float64)
    
    #Quant descriptor
    quant = mbk.predict(a)
    
    #Build Histogram
    histo = np.histogram(quant, bins=128)
    
    #Save histogram to disk
    np.savetxt(directory_out+'/'+list_original[i]+'_histo.txt', (histo[0]))
    
    #Append Histogram to list
    list_hist_o.append( histo[0] )

# <codecell>

# build the histogram for the images
directory_in = path_folders+'/Sift_recaptured'
directory_out = path_folders+'/Histo_recap'
if not os.path.exists(directory_out):
        os.makedirs(directory_out)
        
list_hist_r = []
# for each image
for i in range(len(list_recapt)):
    #Load descriptor
    a = loadtxt(directory_in+'/'+list_recapt[i]+'_sift.txt').view(float64)
    
    #Quant descriptor
    quant = mbk.predict(a)
    
    #Build Histogram
    histo = np.histogram(quant, bins=128)
    
    #Save histogram to disk
    np.savetxt(directory_out+'/'+list_recapt[i]+'_histo.txt', (histo[0]))
    
    #Append Histogram to list
    list_hist_r.append( histo[0] )

# <codecell>

iO = 10
dist = 99999
for iR in range(len(list_recapt)):
    nameO = list_original[iO]
    nameR = list_recapt[iR]
    
    a = cv2.imread(path_originals+'/'+nameO)
    b = cv2.imread(path_recaptured+'/'+nameR)
    
    subplot(121),imshow(a)
    subplot(122),imshow(b)
    show()
    
    h1 = list_hist_o[iO]/float(sum(list_hist_o[iO]))
    h2 = list_hist_o[iR]/float(sum(list_hist_o[iR]))
    d = euclidean_distances(h1, h2)
    print d
    
    if d < dist:
        dist = d
        name = list_hist_o[iR]
        index = iR

# <codecell>

nameO = list_original[iO]
nameR = list_recapt[index]

a = cv2.imread(path_originals+'/'+nameO)
b = cv2.imread(path_recaptured+'/'+nameR)

subplot(121),imshow(a)
subplot(122),imshow(b)
show()

# <codecell>


