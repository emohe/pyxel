print " hola "
from modules.socialevent.classification.Trainer import Trainer
import os
print " acabo de importar les llibreries "
HOME = os.path.expanduser('~')
pathTrainImages = HOME+'/Desktop/train-0'

DATASETS_PATH = HOME+'/work/mediaeval/2013-sed/classification/2_datasets'
gtfile = 'sed2013_task2_dataset_train_gs.csv'

gtPath = os.path.join(DATASETS_PATH, gtfile)

path_features = HOME +'/work/mediaeval/2013-sed/classification/3_features'
directory_h = path_features+'/Histo_originals'
print directory_h

directory_hist = os.path.join(path_features, directory_h)

print " ara entro al trainer"
train = Trainer(gtPath,pathTrainImages)
print "he sortit del trainer"
params,score,classificator =  train.trainSVM(directory_hist)
print params, score, classificator