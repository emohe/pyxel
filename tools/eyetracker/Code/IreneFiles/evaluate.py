from sklearn.svm import SVC
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import precision_recall_fscore_support

define evaluate(annGT,annRes):

	pathHome = os.path.expanduser('~')
    	pathWork = os.path.join( pathHome, 'work')  
    	pathDirAnns = os.path.join( pathWork, '3_annotation' )

	gtCN=annRes.getSemanticClassNames()
	lth=len(gtCN)-1 
	
	for x in range(0, lth):
		labelType=1
		nam=gtCN[x]
		vecGTpos=[]
		vecGTpos=annGT.getDocIdsInClass( nam, labelType )
		vecRespos=[]
		vecRespos=annRes.getDocIdsInClass( nam, labelType )

		gt = []
		res = []
	
		for item in vecRespos:

			res.append(1)
  			
			
			var=0
                        
			for item2 in vecGTpos:
				
				if item2==item:
					
					var=1
					return
				
			gt.append(var)
							
		
		precision_recall_fscore_support(gt, res, average='weighted')
