import os
import pandas as pd


class GroundTruth:
    """ Class to read the ground truth files of each product. Class to be used by Half """    
    def __init__(self, pathDirProducts):    
        
        self._gt = {}
        for product in os.listdir(pathDirProducts):
            ProductDir = os.path.join(pathDirProducts, product)
            gt = pd.read_csv('%s' % (ProductDir), sep=' ')
            self._gt[product[:-4]]=gt
       
     
    def get_eventIDs(self):
        """ Obtain a vector containing the different event IDs """
        return self._gt.keys()
        
    def get_nof_products(self):
        """ Returns an integer with the amount of products """
        return len(self._gt.keys())
        
    def get_photos_in_product(self, product ): # works!
        """ Obtain a vector containing the photo IDs of the event specified """
        return self._gt[product]

      


                     
# Main
if __name__ == "__main__":

    # Point to the local path where the ground truth and metadata are stored
    HOME = os.path.expanduser('~')
    DATASETS_PATH = os.path.join(HOME,'Desktop','annotation')


    # Load the metadata
    #gtPath = os.path.join(DATASETS_PATH, GROUND_TRUTH_FILE)
    groundTruth = GroundTruth(DATASETS_PATH)
    
