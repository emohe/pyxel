from __future__ import division 
'''
Created on Nov 18, 2012
@author: meskevich

input format:
python scriptName runFile windowSize

* windowSize = 10, 30, 60 (seconds)

output format:
MRR mGAP MASP

default:
granularity = 10 seconds

'''

import sys
import re 

def _read_runFile(myFileName):
    lineTemplate = re.compile("item_(?P<QueryID>[0-9]+)\sQ0\s(?P<FileName>.+)(\.flv\.ogv)?\s(?P<StartTimeMin>[0-9]+)\.(?P<StartTimeSec>[0-9]+)\s(?P<EndTimeMin>[0-9]+)\.(?P<EndTimeSec>[0-9]+)\s(?P<JumpinPointMin>[0-9]+)\.(?P<JumpinPointSec>[0-9]+)\s(?P<Rank>[0-9]+)\s(?P<Score>[\.0-9A-Za-z]+)\s(?P<RunName>.+)")
    f = open(myFileName); lines = f.readlines(); f.close()
    resultData = {}
    for line in lines:
        lineTemplateCheck = lineTemplate.search(line)
        if lineTemplateCheck:
            query = int(lineTemplateCheck.group('QueryID'))
            if query not in resultData.keys():
                resultData[query] = {}
            rank, fileName = int(lineTemplateCheck.group('Rank')), lineTemplateCheck.group('FileName') 
            startTimeMin, startTimeSec = int(lineTemplateCheck.group('StartTimeMin')), int(lineTemplateCheck.group('StartTimeSec'))
            endTimeMin, endTimeSec = int(lineTemplateCheck.group('EndTimeMin')), int(lineTemplateCheck.group('EndTimeSec'))
            jumpinPointMin, jumpinPointSec = int(lineTemplateCheck.group('JumpinPointMin')), int(lineTemplateCheck.group('JumpinPointSec'))
            resultData[query][rank] = [fileName, startTimeMin*60 + startTimeSec, endTimeMin*60 + endTimeSec, jumpinPointMin*60 + jumpinPointSec]
    return resultData

def _read_qrelFile(myFileName):
    lineTemplate = re.compile('(?P<QueryID>[0-9]+)\s(?P<FileNames>.+)\s(?P<StartTimeMin>[0-9]+)\.(?P<StartTimeSec>[0-9]+)\s(?P<EndTimeMin>[0-9]+)\.(?P<EndTimeSec>[0-9]+)')
    f = open(myFileName); lines = f.readlines(); f.close()
    resultData = {}
    for line in lines:
        lineTemplateCheck = lineTemplate.search(line)
        if lineTemplateCheck:
            query = int(lineTemplateCheck.group('QueryID'))
            if query not in resultData.keys():
                resultData[query] = {}
            fileNames = lineTemplateCheck.group('FileNames').split('/')
            startTimeMin, startTimeSec = int(lineTemplateCheck.group('StartTimeMin')), int(lineTemplateCheck.group('StartTimeSec'))
            endTimeMin, endTimeSec = int(lineTemplateCheck.group('EndTimeMin')), int(lineTemplateCheck.group('EndTimeSec'))
            resultData[query] = [fileNames, startTimeMin*60 + startTimeSec, endTimeMin*60 + endTimeSec]
    return resultData
    
def _calculate_MRR_mGAP_MASP(myQrelData, myRunData, myWindowSize):
    granularity = 10
    rr, gap, asp = [], [], []
    for query in myRunData.keys():
        fileNames = myQrelData[query][0]
        qrel_start, qrel_end = myQrelData[query][1], myQrelData[query][2]
        totalLen = 0
        relFlag = 0
        ranks = myRunData[query].keys(); ranks.sort()
        for rank in ranks:
            totalLen = totalLen + (myRunData[query][rank][2] - myRunData[query][rank][1])

            if myRunData[query][rank][0] in fileNames:
                run_start, run_end, run_jp = myRunData[query][rank][1], myRunData[query][rank][2], myRunData[query][rank][3]
                # check whether the jump-in point is within the window distance to the actual start of the relevant content
                if qrel_start - myWindowSize <= run_jp and run_jp <= qrel_start + myWindowSize:
                    relFlag = 1
                    # count Reciprocal Rank
                    rr.append(1/rank)
                    # count GAP
                    penalty = 0
                    if run_jp <= qrel_start:
                        while run_jp < qrel_start:
                            run_jp = run_jp + granularity
                            penalty = penalty + 1
                    else:
                        while qrel_start < run_jp:
                            run_jp = run_jp - granularity
                            penalty = penalty + 1
                    gap.append((1 - penalty*granularity/myWindowSize)/rank)
                    # count ASP
                    relLen = 0
                    if qrel_start <= run_start and run_end <= qrel_end:
                        relLen = run_end - run_start
                    elif run_start < qrel_start and qrel_end < run_end:
                        relLen = qrel_end - qrel_start
                    elif run_start < qrel_start and qrel_start < run_end and run_end < qrel_end:
                        relLen = run_end - qrel_start
                    elif qrel_start < run_start and run_start < qrel_end and qrel_end <run_end:
                        relLen = qrel_end - run_start
                    if relLen <> 0.0:
                        asp.append(relLen/totalLen)
                    else:
                        asp.append(0.0)
                    break
        if relFlag == 0:
            rr.append(0.0); gap.append(0.0); asp.append(0.0)
        print 'item_' + str(query), rr[-1], gap[-1], asp[-1]
    return sum(rr)/len(myQrelData.keys()), sum(gap)/len(myQrelData.keys()), sum(asp)/len(myQrelData.keys())     
    
def _main():
    try:
        runFile, qrelFile, windowSize = sys.argv[1], sys.argv[2], int(sys.argv[3]) 
    except:
        print "Usage", sys.argv[0], "Input data missing\n\
                1.    run File\n\
                2.    qrel File\n\
                3.    window size\n", sys.exit(1)
    runData = _read_runFile(runFile)
    qrelData = _read_qrelFile(qrelFile)
    MRR, mGAP, MASP = _calculate_MRR_mGAP_MASP(qrelData, runData, windowSize)
    print "\nMRR,mGAP, MASP:\n", MRR, mGAP, MASP
_main()
