'''
Created on 13 Sep 2013

@author: meskevich
Usage: need to change the path to the local directories with transcripts!
python me_sh13_MTurkHITEvaluation_v2.py ~/Dropbox/SearchAndHyperlinkingOrganizers/HyperlinkingEvaluation/HIT_output/Batch_1263166_batch_results.csv ~/Databases/MediaEval2013/releaseData/LIMSI/ ~/Databases/MediaEval2013/releaseData/LIUM_1-best/ ~/Databases/MediaEval2013/releaseData/Subtitles_xml/ output.csv

After the running of the program, remove the quotes (") in the first line after the HITId, otherwise the MTurk gives an error about the uploaded file.

Reading files:
    1. MTurk csv file:
        - anchor_ID
        - target_ID
        - Yes/No answer (1 field)
        - Comment Field
        - Anchor transcript words
        - Target transcript words
    2-4. Directories with transcripts (LIUM, LIMSI, Subtitles)
    5. OutputFilename 
    
Processing:
    1. Check whether all the fields are empty. 
        If yes, then rejection with message "The questions in the HIT were not answered correctly (empty fields)."  
    2. If there is Yes or No answer:
        If yes, check whether all 3 fields have content (Comment Field, Anchor transcript words, Target transcript words):
            If yes, then check the transcript words.
                If the transcript words are found in the transcript files, then accept the HIT.
                If not, these are to be checked manually.


Manual check:
If there are both Yes and No answers, we keep track of this information in the file HyperlinkingEvaluation/HIT_comments/StatisticsAboutDoubleYesNoDecision.txt and APPROVE the HIT
    
If there are no Yes/No answers, but other fields have reasonable content that allows us to know whether it is Yes or No, we APPROVE the HIT and keep track of the decision in HyperlinkingEvaluation/HIT_comments/StatisticsAboutAbsentYesNoDecision.txt
    
If one of the fields is missing, but the decision is there, and the description is reasonable, we APPROVE the HIT
    
If the workers report that one of the videos was not loading, and the manual check shows that this is true, than we APPROVE the HIT and save the file name in /SearchAndHyperlinkingOrganizers/HyperlinkingEvaluation/HIT_comments/BrokenVideosList.txt

'''
import sys
import os
import re

def _get_list_of_xml_files(myDirName):

    listOfFiles = {}
    for root, dirs, files in os.walk(myDirName):
        for name in files:
            nickName = re.sub("\.xml", '', name)
            if not re.search("\.directory", name):
                listOfFiles[nickName] = os.path.join(root, name)
    return listOfFiles

def _get_list_of_ctm_files(myDirName):

    listOfFiles = {}
    for root, dirs, files in os.walk(myDirName):
        for name in files:
            nickName = re.sub("\.ctm", '', name)
            if not re.search("\.directory", name):
                listOfFiles[nickName] = os.path.join(root, name)
    return listOfFiles


def _cut_fileName_startTime_endTime(myFileName):
    fileNameTemplate = re.compile("(?P<FileNick>.+)_(?P<StartTime_min>[0-9]+)\.(?P<StartTime_sec>[0-9]+)_(?P<EndTime_min>[0-9]+)\.(?P<EndTime_sec>[0-9]+)\.webm")
    fileNameTemplateCheck = fileNameTemplate.search(myFileName)
    if fileNameTemplateCheck:
        fileNick = fileNameTemplateCheck.group('FileNick')
        startTime = float(int(fileNameTemplateCheck.group('StartTime_min'))*60 + int(fileNameTemplateCheck.group('StartTime_sec')))
        endTime = float(int(fileNameTemplateCheck.group('EndTime_min'))*60 + int(fileNameTemplateCheck.group('EndTime_sec')))
    return fileNick, startTime, endTime

def _read_LIMSI_transcript_file(myFileName, mySegmentStartTime, mySegmentEndTime):
    words = []
    f = open(myFileName)
    lines = f.readlines()
    f.close()
    lineTemplate = re.compile('<Word stime="(?P<StartTime>[0-9\.]+)" dur="(?P<Duration>[0-9\.]+)" conf="[0-9\.]+"> (?P<Word>.+) </Word>')
    for line in lines:
        lineTemplateCheck = lineTemplate.search(line)
        if lineTemplateCheck:
            startTime = float(lineTemplateCheck.group('StartTime'))
            if mySegmentStartTime <= startTime and startTime <= mySegmentEndTime:
                words.append(lineTemplateCheck.group('Word').lower())
            elif mySegmentEndTime <= startTime:
                break
    return words

def _read_LIUM_transcript_file(myFileName, mySegmentStartTime, mySegmentEndTime):
    words = []
    f = open(myFileName)
    lines = f.readlines()
    f.close()
    lineTemplate = re.compile('[^ ]+ 1 (?P<StartTime>[0-9\.]+) 0.02 (?P<Word>.+) [0-9\.]+')
    for line in lines:
        lineTemplateCheck = lineTemplate.search(line)
        if lineTemplateCheck:
            startTime = float(lineTemplateCheck.group('StartTime'))
            if mySegmentStartTime <= startTime and startTime <= mySegmentEndTime:
                words.append(lineTemplateCheck.group('Word').lower())
            elif mySegmentEndTime <= startTime:
                break
    return words

def _read_Subtitles_transcript_file(myFileName, mySegmentStartTime, mySegmentEndTime):
    words = []
    f = open(myFileName)
    lines = f.readlines()
    f.close()
    lineTemplate = re.compile('<p begin="(?P<StartTime_hour>[0-9]{2}):(?P<StartTime_min>[0-9]{2}):(?P<StartTime_secAndMilSec>[0-9\.]+)" end="(?P<EndTime_hour>[0-9]{2}):(?P<EndTime_min>[0-9]{2}):(?P<SEndTime_secAndMilSec>[0-9\.]+)"><span tts:color="\#[0-9a-z]{6}" tts:textAlign="center"> (?P<Sentence>[^<]+)</span><br/></p>')
    for line in lines:
        lineTemplateCheck = lineTemplate.search(line)
        if lineTemplateCheck:
            startTime = float(int(lineTemplateCheck.group('StartTime_hour'))*360 + int(lineTemplateCheck.group('StartTime_min'))*60 + float(lineTemplateCheck.group('StartTime_secAndMilSec')))
            if mySegmentStartTime <= startTime and startTime <= mySegmentEndTime:
                allWordsInSentence = re.sub('(\?|,|\!|\.)',' ', lineTemplateCheck.group('Sentence'))
                wordsInSentence = allWordsInSentence.split(' ')
                for word in wordsInSentence:
                    if word <> '':
                        words.append(word.lower())
            elif mySegmentEndTime <= startTime:
                break
    return words

def _compare_inputWords_with_transcript(myInutWords, myTranscriptWords):
    pass 

def _read_and_process_mturkOutput_file(myFileName, myDirWithTranscript_LIMSI, myDirWithTranscript_LIUM, myDirWithTranscript_Subtitles, myListOfFiles_LIMSI, myListOfFiles_LIUM, myListOfFiles_Subtitles):
    print "read mturkOutput file"
    f = open(myFileName)
    lines = f.readlines()
    f.close()
#"HITId","HITTypeId","Title","Description","Keywords","Reward","CreationTime","MaxAssignments","RequesterAnnotation","AssignmentDurationInSeconds","AutoApprovalDelayInSeconds","Expiration","NumberOfSimilarHITs","LifetimeInSeconds","AssignmentId","WorkerId","AssignmentStatus","AcceptTime","SubmitTime","AutoApprovalTime","ApprovalTime","RejectionTime","RequesterFeedback","WorkTimeInSeconds","LifetimeApprovalRate","Last30DaysApprovalRate","Last7DaysApprovalRate","Input.AnchorId","Input.VideoAnchor","Input.AnchorStart","Input.AnchorEnd","Input.VideoTarget","Input.TargetStart","Input.TargetEnd","Input.AnchorDescription","Answer.DetailsKeyWordsAnchor","Answer.DetailsKeyWordsTarget","Answer.ExplanationField","Answer.OptionNo","Answer.OptionYes","Approve","Reject"
#"21MF0OHOVR14IWAKYJ02NECYW6K54U","2WC51Y7QEOZFYPIX8QKMUFXRD79DGU","Watch 2 video segments and say whether they are related according to the given description","The HIT contains a video served over http: your browser (use Chrome, Firefox, or Opera!!!) must be set to display http within https. We are carrying out non-profit research at a university about the videos that can be related.","video search retrieval watch user user-study research university youtube","$0.15","Tue Sep 10 19:05:32 GMT 2013","1","BatchId:1263166;","3000","1209600","Tue Sep 24 19:05:32 GMT 2013","","","2M8J2D21O3W5XGQFLNJI8KYJ18JE8H","A2O5PN17VNYXI0","Submitted","Wed Sep 11 08:41:28 GMT 2013","Wed Sep 11 08:44:25 GMT 2013","Wed Sep 25 01:44:25 PDT 2013","","","","177","89% (2743/3068)","96% (1286/1338)","100% (4/4)","anchor_32","20080510_191500_bbcthree_top_gear_11.20_12.33.webm","11.2","12.33","20080511_180000_bbcthree_radio_1_s_big_weekend_5.07_6.38.webm","5.07","6.38","I want to see more videos explaining more about this new concept car, when it will be available, technical spec and more information.","stuff coming drive","know do go","Because the first video talks about a new car where as the second one is a live music show","2",""
#"222H1Q6SVQ8H87CMQ4C5UBVE41FM3H","2WC51Y7QEOZFYPIX8QKMUFXRD79DGU","Watch 2 video segments and say whether they are related according to the given description","The HIT contains a video served over http: your browser (use Chrome, Firefox, or Opera!!!) must be set to display http within https. We are carrying out non-profit research at a university about the videos that can be related.","video search retrieval watch user user-study research university youtube","$0.15","Tue Sep 10 19:05:36 GMT 2013","1","BatchId:1263166;","3000","1209600","Tue Sep 24 19:05:36 GMT 2013","","","23EOFXRDS4IC1DO9JVAFS11FE1PXUK","A3MJTCFEAVBOSM","Submitted","Tue Sep 10 20:10:50 GMT 2013","Tue Sep 10 20:15:51 GMT 2013","Tue Sep 24 13:15:51 PDT 2013","","","","301","0% (0/0)","0% (0/0)","0% (0/0)","anchor_4","20080508_005500_bbcone_meet_the_immigrants_3.31_4.27.webm","3.31","4.27","20080401_200000_bbcone_hotel_babylon_20.12_21.54.webm","20.12","21.54","I want to see more videos about (1) learning the hotel trade, (2) immigrants having their on the job training, (3) what became easier for EU Immigrants to work in the UK.","million, immigrants, hotel, life, free","boyfriend, police, sorry, anyone, plate","the video on the right is a TV series doe not have anything to do with (1) learning the hotel trade, (2) immigrants having their on the job training, (3) what became easier for EU Immigrants to work in the UK.","2",""
    counter_AssignmentStatus = 0 #AssignmentStatus: Approved or Rejected
    counter_Input_VideoAnchor = 0
    counter_Input_VideoTarget = 0
    counter_Answer_DecisionOption = 0
    counter_Answer_ExplanationField = 0
    counter_Answer_DetailsKeyWordsAnchor, counter_Answer_DetailsKeyWordsTarget = 0, 0
 
    namings = lines[0].split('","')
    for counter in range(len(namings)):
        if namings[counter] == 'Input.VideoAnchor':
            counter_Input_VideoAnchor = counter
        elif namings[counter] == 'Input.VideoTarget':
            counter_Input_VideoTarget = counter
        elif namings[counter] == 'Answer.DecisionOption':
            counter_Answer_DecisionOption = counter
        elif namings[counter] == 'Answer.ExplanationField':
            counter_Answer_ExplanationField = counter        
        elif namings[counter] == 'Answer.DetailsKeyWordsAnchor':
            counter_Answer_DetailsKeyWordsAnchor = counter
        elif namings[counter] == 'Answer.DetailsKeyWordsTarget':
            counter_Answer_DetailsKeyWordsTarget = counter
        elif namings[counter] == 'AssignmentStatus':
            counter_AssignmentStatus = counter    

    newCSVFile = []  
    for line in lines:
        line = re.sub('\n', '', line); line = re.sub('\r', '', line); line = line.lstrip('"')#line = line.rstrip('"'); 
        lineEntries = line.split('","')
        if len(lineEntries) == len(namings) - 2: # Approve; Reject
            lineEntries[counter_Answer_DecisionOption] = re.sub('"', '', lineEntries[counter_Answer_DecisionOption])
            assignmentStatus = lineEntries[counter_AssignmentStatus]
            if assignmentStatus == 'Submitted':
#               1. Check whether all the fields are empty. 
#                If yes, then rejection with message "The questions in the HIT were not answered correctly (empty fields)."  
                if len(lineEntries[counter_Answer_ExplanationField]) == 0 and \
                    len(lineEntries[counter_Answer_DetailsKeyWordsAnchor]) == 0 and \
                    len(lineEntries[counter_Answer_DetailsKeyWordsTarget]) == 0:
                    lineEntries.append('')
                    lineEntries.append('The questions in the HIT were not answered properly (empty fields).')
                    print "condition 1", lineEntries[counter_Input_VideoTarget]
                    
#               2. Check whether there is only Yes or No answer (Note: cases when there are both yes and no, or neither, are to be checked manually).
#                    If yes, check whether all 3 fields have content (Comment Field, Anchor transcript words, Target transcript words):
#                        If yes, then check the transcript words.
#                            If the transcript words are found in the transcript files, then accept the HIT.
                lineEntries[counter_Answer_ExplanationField] = re.sub('"', '', lineEntries[counter_Answer_ExplanationField])                
                if lineEntries[counter_Answer_DecisionOption] == '1' or lineEntries[counter_Answer_DecisionOption] == '2':
                    if len(lineEntries[counter_Answer_ExplanationField]) <> 0 and \
                        len(lineEntries[counter_Answer_DetailsKeyWordsAnchor]) <> 0 and \
                        len(lineEntries[counter_Answer_DetailsKeyWordsTarget]) <> 0:
#                       prepare bag of anchor transcript words
                            anchorFileNick, segmentStartTime, segmentEndTime = _cut_fileName_startTime_endTime(lineEntries[counter_Input_VideoAnchor])
                            anchorTranscriptWords_LIMSI, anchorTranscriptWords_LIUM, anchorTranscriptWords_Subtitles = [], [], []
                            if anchorFileNick in myListOfFiles_LIMSI:
                                anchorTranscriptWords_LIMSI = _read_LIMSI_transcript_file(myDirWithTranscript_LIMSI + anchorFileNick + '.xml', segmentStartTime, segmentEndTime)
                            if anchorFileNick in myListOfFiles_LIUM:
                                anchorTranscriptWords_LIUM = _read_LIUM_transcript_file(myDirWithTranscript_LIUM + anchorFileNick + '.ctm', segmentStartTime, segmentEndTime)
                            if anchorFileNick in myListOfFiles_Subtitles:
                                anchorTranscriptWords_Subtitles = _read_Subtitles_transcript_file(myDirWithTranscript_Subtitles + anchorFileNick + '.xml', segmentStartTime, segmentEndTime)
#                       check anchor words
                            lineEntries[counter_Answer_DetailsKeyWordsAnchor] = re.sub('(\?|,|\!|\.)',' ', lineEntries[counter_Answer_DetailsKeyWordsAnchor])
                            anchorWords = lineEntries[counter_Answer_DetailsKeyWordsAnchor].split(' ')
                            anchorWordsCorrectness = False

                            counterOfCorrectAnchorWords = 0
                            for anchorWord in anchorWords:
                                if anchorWord in anchorTranscriptWords_LIMSI \
                                    or anchorWord in anchorTranscriptWords_LIUM \
                                    or anchorWord in anchorTranscriptWords_Subtitles:
                                    counterOfCorrectAnchorWords = counterOfCorrectAnchorWords + 1
                            if counterOfCorrectAnchorWords >= 2:
                                anchorWordsCorrectness = True

#                       prepare bag of target transcript words
                            targetFileNick, segmentStartTime, segmentEndTime = _cut_fileName_startTime_endTime(lineEntries[counter_Input_VideoTarget])
                            targetTranscriptWords_LIMSI, targetTranscriptWords_LIUM, targetTranscriptWords_Subtitles = [], [], []
                            if targetFileNick in myListOfFiles_LIMSI:
                                targetTranscriptWords_LIMSI = _read_LIMSI_transcript_file(myDirWithTranscript_LIMSI + targetFileNick + '.xml', segmentStartTime, segmentEndTime)
                            if targetFileNick in myListOfFiles_LIUM:
                                targetTranscriptWords_LIUM = _read_LIUM_transcript_file(myDirWithTranscript_LIUM + targetFileNick + '.ctm', segmentStartTime, segmentEndTime)
                            if targetFileNick in myListOfFiles_Subtitles:
                                targetTranscriptWords_Subtitles = _read_Subtitles_transcript_file(myDirWithTranscript_Subtitles + targetFileNick + '.xml', segmentStartTime, segmentEndTime)
#                       check target words
                            lineEntries[counter_Answer_DetailsKeyWordsTarget] = re.sub('(\?|,|\!|\.)',' ', lineEntries[counter_Answer_DetailsKeyWordsTarget])
                            targetWords = lineEntries[counter_Answer_DetailsKeyWordsTarget].split(' ')
                            targetWordsCorrectness = False
                            
                            counterOfCorrectTargetWords = 0
                            for targetWord in targetWords:
                                if targetWord in targetTranscriptWords_LIMSI \
                                    or targetWord in targetTranscriptWords_LIUM \
                                    or targetWord in targetTranscriptWords_Subtitles:
                                    counterOfCorrectTargetWords = counterOfCorrectTargetWords + 1
                            if counterOfCorrectTargetWords >= 2:
                                targetWordsCorrectness = True                            
                            print "condition 2", targetWordsCorrectness, anchorWordsCorrectness, lineEntries[counter_Input_VideoTarget]
                            if anchorWordsCorrectness == True and targetWordsCorrectness == True:
                                lineEntries.append('x')
                newLine = ''
                for entry in lineEntries:
                    newLine = newLine + '"' + entry + '",'
                newCSVFile.append(newLine)
            else:
                newCSVFile.append(line)
        else:
            newCSVFile.append(line)#('"' + line)
    return newCSVFile

def _write_into_file(myFileName, myNewCSVFile):
    f = open(myFileName, 'w')
    for line in myNewCSVFile:
        f.write(line + '\n')
    f.close()
    
def _main():
    try:
        mturkOutputFile = sys.argv[1]
        dirWithTranscript_LIMSI, dirWithTranscript_LIUM, dirWithTranscript_Subtitles = sys.argv[2], sys.argv[3], sys.argv[4]
        outputFile = sys.argv[5]
    except:
        print "Usage:", sys.argv[0], "the input info is missing:\n\
            1. MTurk csv file\n\
            2. Directory with LIMSI transcript files\n\
            3. Directory with LIUM transcript files\n\
            4. Directory with Subtitles transcript files\n\
            5. Output filename\n", sys.exit(1)
            
    listOfFiles_LIMSI = _get_list_of_xml_files(dirWithTranscript_LIMSI)
    listOfFiles_LIUM = _get_list_of_ctm_files(dirWithTranscript_LIUM)
    listOfFiles_Subtitles = _get_list_of_xml_files(dirWithTranscript_Subtitles)
    newCSVFile = _read_and_process_mturkOutput_file(mturkOutputFile, dirWithTranscript_LIMSI, dirWithTranscript_LIUM, dirWithTranscript_Subtitles, listOfFiles_LIMSI, listOfFiles_LIUM, listOfFiles_Subtitles)

    _write_into_file(outputFile, newCSVFile)
_main()
