from math import *
import re

anchors2use = [u'anchor_12', u'anchor_21', u'anchor_23', u'anchor_27', u'anchor_31', u'anchor_32', u'anchor_39', u'anchor_4', u'anchor_43', u'anchor_45', u'anchor_47', u'anchor_51', u'anchor_56', u'anchor_57', u'anchor_60', u'anchor_63', u'anchor_70', u'anchor_71', u'anchor_72', u'anchor_74', u'anchor_78', u'anchor_80', u'anchor_81', u'anchor_84', u'anchor_85', u'anchor_87', u'anchor_88', u'anchor_91', u'anchor_93', u'anchor_95']

videoFiles = [ 
[ "20080401_002000_bbcthree_pulling", "00:29:47.28" ], [ "20080401_003500_bbcone_springwatch_weatherview", "00:07:26.32" ], [
"20080401_004000_bbcone_miracle_on_the_estate", "01:00:12.40" ], [ "20080401_010000_bbcfour_the_book_quiz", "00:29:51.64" ], [
"20080401_013000_bbcfour_legends_marty_feldman_six_degrees_of", "00:59:59.68" ], [ "20080401_014000_bbcone_the_shogun",
"01:00:05.12" ], [ "20080401_015000_bbcthree_dog_borstal", "00:56:58.28" ], [ "20080401_023000_bbcfour_the_book_quiz",
"00:28:02.56" ], [ "20080401_024000_bbcone_to_buy_or_not_to_buy", "00:44:50.16" ], [
"20080401_030000_bbctwo_key_stage_3_bitesize_english_1", "01:59:20.40" ], [ "20080401_050000_bbctwo_tikkabilla", "00:29:33.32" ],
[ "20080401_063000_bbctwo_mama_mirabelle_s_home_movies", "00:12:31.36" ], [ "20080401_064500_bbctwo_tommy_zoom", "00:16:49.20" ],
[ "20080401_070000_bbctwo_arthur", "00:25:24.24" ], [ "20080401_074500_bbctwo_chucklevision", "00:15:11.36" ], [
"20080401_080000_bbctwo_tracy_beaker", "00:28:28.40" ], [ "20080401_090000_bbctwo_stupid", "00:27:59.36" ], [
"20080401_100000_bbcone_to_buy_or_not_to_buy", "00:44:49.24" ], [ "20080401_110000_bbctwo_the_daily_politics", "00:29:37.40" ], [
"20080401_111500_bbcone_bargain_hunt", "00:44:17.32" ], [ "20080401_113000_bbctwo_working_lunch", "00:29:58.32" ], [
"20080401_120000_bbcone_bbc_news", "00:31:41.28" ], [ "20080401_124500_bbcone_doctors", "00:30:52.44" ], [
"20080401_140500_bbcone_space_pirates", "00:28:47.36" ], [ "20080401_143500_bbcone_raven", "00:29:10.40" ], [
"20080401_144500_bbctwo_flog_it", "00:44:52.32" ], [ "20080401_160000_bbcone_newsround", "00:11:18.28" ], [
"20080401_161500_bbcone_the_weakest_link", "00:45:40.28" ], [ "20080401_170000_bbcone_bbc_news", "00:29:02.04" ], [
"20080401_170000_bbctwo_eggheads", "00:29:29.84" ], [ "20080401_173000_bbcone_bbc_london_news", "00:28:34.56" ], [
"20080401_173000_bbctwo_great_british_menu", "00:30:11.32" ], [ "20080401_180000_bbcfour_world_news_today", "00:29:53.32" ], [
"20080401_180000_bbcone_the_one_show", "00:30:07.36" ], [ "20080401_180000_bbcthree_dog_borstal", "00:57:20.32" ], [
"20080401_180000_bbctwo_around_the_world_in_80_gardens", "01:01:04.28" ], [ "20080401_183000_bbcfour_pop_goes_the_sixties",
"00:05:39.16" ], [ "20080401_190000_bbcfour_tv_s_believe_it_or_not", "00:58:59.36" ], [ "20080401_190000_bbcone_holby_city",
"00:58:18.36" ], [ "20080401_190000_bbcthree_doctor_who", "01:01:02.32" ], [
"20080401_190000_bbctwo_university_challenge_the_professionals", "00:29:30.32" ], [
"20080401_193000_bbctwo_johnny_s_new_kingdom", "00:29:55.20" ], [ "20080401_200000_bbcfour_tv_s_believe_it_or_not", "01:00:06.16"
], [ "20080401_200000_bbcone_hotel_babylon", "00:58:17.32" ], [ "20080401_210000_bbcfour_the_hard_sell", "00:30:07.24" ], [
"20080401_210000_bbcone_bbc_ten_o_clock_news", "00:26:21.24" ], [ "20080401_210000_bbcthree_eastenders", "00:33:36.36" ], [
"20080401_210000_bbctwo_later_live_with_jools_holland", "00:31:16.32" ], [ "20080401_213000_bbcfour_a_year_in_tibet",
"01:00:02.32" ], [ "20080401_213000_bbcthree_lily_allen_and_friends", "00:45:57.48" ], [ "20080401_213000_bbctwo_newsnight",
"00:49:46.16" ], [ "20080401_213500_bbcone_one_life", "00:44:20.40" ], [ "20080401_223000_bbcfour_the_book_quiz", "00:30:03.40"
], [ "20080401_230000_bbcfour_proms_on_four", "02:15:42.16" ], [ "20080401_233500_bbcthree_dog_borstal", "00:57:20.16" ], [
"20080402_001000_bbcone_weatherview", "00:04:35.32" ], [ "20080402_001500_bbcone_see_hear", "00:30:21.20" ], [
"20080402_003000_bbcthree_lily_allen_and_friends", "00:45:22.36" ], [ "20080402_004500_bbcone_panorama", "00:37:46.36" ], [
"20080402_011500_bbcfour_tv_s_believe_it_or_not", "00:59:18.52" ], [ "20080402_012000_bbcone_life_in_cold_blood", "00:59:19.32"
], [ "20080402_021500_bbcfour_tv_s_believe_it_or_not", "00:59:31.12" ], [ "20080402_022000_bbcone_to_buy_or_not_to_buy",
"00:45:20.28" ], [ "20080402_030000_bbctwo_key_stage_3_bitesize_maths_1", "01:59:33.20" ], [ "20080402_050000_bbcone_breakfast",
"03:14:56.00" ], [ "20080402_050000_bbctwo_tikkabilla", "00:29:01.88" ], [ "20080402_063000_bbctwo_mama_mirabelle_s_home_movies",
"00:12:28.40" ], [ "20080402_064500_bbctwo_tommy_zoom", "00:10:11.28" ], [ "20080402_065500_bbctwo_take_a_bow", "00:06:33.32" ],
[ "20080402_070000_bbctwo_arthur", "00:25:48.32" ], [ "20080402_074500_bbctwo_chucklevision", "00:15:01.32" ], [
"20080402_080000_bbctwo_tracy_beaker", "00:28:03.40" ], [ "20080402_090000_bbcone_homes_under_the_hammer", "01:00:58.20" ], [
"20080402_090000_bbctwo_stupid", "00:28:07.28" ], [ "20080402_093000_bbctwo_what_the_ancients_did_for_us", "01:00:13.44" ], [
"20080402_100000_bbcone_to_buy_or_not_to_buy", "00:44:52.20" ], [ "20080402_103000_bbctwo_the_daily_politics", "01:30:21.28" ], [
"20080402_111500_bbcone_bargain_hunt", "00:44:03.24" ], [ "20080402_120000_bbcone_bbc_news", "00:31:43.32" ], [
"20080402_120000_bbctwo_see_hear", "00:29:33.16" ], [ "20080402_123000_bbctwo_working_lunch", "00:30:10.20" ], [
"20080402_124500_bbcone_doctors", "00:28:58.36" ], [ "20080402_140500_bbcone_space_pirates", "00:28:59.24" ], [
"20080402_144500_bbctwo_flog_it", "00:44:23.20" ], [ "20080402_150500_bbcone_young_dracula", "00:29:02.04" ], [
"20080402_153500_bbcone_blue_peter", "00:24:44.40" ], [ "20080402_160000_bbcone_newsround", "00:12:10.40" ], [
"20080402_161500_bbcone_the_weakest_link", "00:45:28.36" ], [ "20080402_161500_bbctwo_escape_to_the_country", "00:44:56.28" ], [
"20080402_170000_bbcone_bbc_news", "00:28:37.84" ], [ "20080402_170000_bbctwo_eggheads", "00:29:54.72" ], [
"20080402_173000_bbcone_bbc_london_news", "00:29:21.24" ], [ "20080402_173000_bbctwo_great_british_menu", "00:30:45.12" ], [
"20080402_180000_bbcfour_world_news_today", "00:30:08.56" ], [ "20080402_180000_bbcone_the_one_show", "00:30:00.28" ], [
"20080402_180000_bbcthree_dog_borstal", "00:58:37.20" ], [ "20080402_180000_bbctwo_coast_the_journey_continues", "00:59:54.24" ],
[ "20080402_183000_bbcfour_pop_goes_the_sixties", "00:04:29.56" ], [ "20080402_183000_bbcone_street_doctor", "00:31:49.48" ], [
"20080402_190000_bbcfour_around_the_world_in_80_treasures", "01:00:47.44" ], [ "20080402_190000_bbcone_traffic_cops",
"00:59:59.32" ], [ "20080402_190000_bbcthree_doctor_who", "01:01:21.36" ], [ "20080402_190000_bbctwo_natural_world",
"00:49:30.28" ], [ "20080402_195000_bbctwo_wild", "00:09:51.44" ], [ "20080402_200000_bbcfour_hughie_green_most_sincerely",
"01:19:51.52" ], [ "20080402_200000_bbcone_the_apprentice", "00:59:55.36" ], [
"20080402_200000_bbctwo_dan_cruickshank_s_adventures", "01:01:01.20" ], [ "20080402_210000_bbcone_bbc_ten_o_clock_news",
"00:27:11.28" ], [ "20080402_210000_bbctwo_the_apprentice_you_re_fired", "00:32:11.40" ], [ "20080402_212000_bbcfour_up_pompeii",
"00:30:32.44" ], [ "20080402_213000_bbctwo_newsnight", "00:50:57.12" ], [ "20080402_214000_bbcone_one_foot_in_the_grave",
"00:29:57.36" ], [ "20080402_215000_bbcfour_mark_lawson_talks_to_barry_cryer", "01:00:18.52" ], [
"20080402_222000_bbctwo_desi_dna", "00:29:24.32" ], [ "20080402_225000_bbcfour_hughie_green_most_sincerely", "01:19:51.28" ], [
"20080403_000500_bbcone_weatherview", "00:05:51.36" ], [ "20080403_001000_bbcfour_a_year_in_tibet", "00:59:38.44" ], [
"20080403_001000_bbcone_antiques_roadshow", "00:59:50.32" ], [ "20080403_005000_bbcthree_dog_borstal", "00:58:13.44" ], [
"20080403_011000_bbcfour_play_it_again_the_panel_game", "00:30:21.28" ], [ "20080403_011000_bbcone_the_choir", "01:00:31.36" ], [
"20080403_014000_bbcfour_mark_lawson_talks_to_barry_cryer", "01:00:14.08" ], [ "20080403_021000_bbcone_an_island_parish",
"00:30:21.24" ], [ "20080403_024000_bbcone_to_buy_or_not_to_buy", "00:44:55.40" ], [
"20080403_030000_bbctwo_key_stage_3_bitesize_science_1", "02:00:08.12" ], [ "20080403_050000_bbcone_breakfast", "03:15:07.32" ],
[ "20080403_050000_bbctwo_tikkabilla", "00:29:27.32" ], [ "20080403_063000_bbctwo_mama_mirabelle_s_home_movies", "00:12:37.24" ],
[ "20080403_064500_bbctwo_tommy_zoom", "00:09:50.24" ], [ "20080403_065500_bbctwo_take_a_bow", "00:07:02.12" ], [
"20080403_070000_bbctwo_arthur", "00:25:46.40" ], [ "20080403_074500_bbctwo_chucklevision", "00:15:12.24" ], [
"20080403_080000_bbctwo_tracy_beaker", "00:28:23.28" ], [ "20080403_090000_bbcone_homes_under_the_hammer", "01:00:32.24" ], [
"20080403_090000_bbctwo_stupid", "00:29:44.36" ], [ "20080403_100000_bbcone_to_buy_or_not_to_buy", "00:44:29.20" ], [
"20080403_105500_bbctwo_coast", "00:05:32.04" ], [ "20080403_110000_bbctwo_the_daily_politics", "00:29:30.64" ], [
"20080403_111500_bbcone_bargain_hunt", "00:44:19.24" ], [ "20080403_113000_bbctwo_working_lunch", "00:30:42.12" ], [
"20080403_120000_bbcone_bbc_news", "00:31:58.40" ], [ "20080403_124500_bbcone_doctors", "00:29:28.36" ], [
"20080403_124500_bbctwo_racing_from_aintree", "02:59:42.32" ], [ "20080403_140500_bbcone_space_pirates", "00:28:45.32" ], [
"20080403_150500_bbcone_stake_out", "00:28:49.28" ], [ "20080403_153500_bbcone_beat_the_boss", "00:28:15.32" ], [
"20080403_154500_bbctwo_flog_it", "00:29:46.32" ], [ "20080403_160000_bbcone_newsround", "00:11:44.40" ], [
"20080403_161500_bbcone_the_weakest_link", "00:45:16.28" ], [ "20080403_170000_bbcone_bbc_news", "00:29:04.80" ], [
"20080403_170000_bbctwo_eggheads", "00:00:53.32" ], [ "20080403_173000_bbcone_bbc_london_news", "00:28:28.44" ], [
"20080403_180000_bbcfour_world_news_today", "00:29:28.24" ], [ "20080403_180000_bbcone_the_one_show", "00:31:05.44" ], [
"20080403_180000_bbctwo_torchwood", "00:50:05.36" ], [ "20080403_183000_bbcfour_up_pompeii", "00:29:48.28" ], [
"20080403_183000_bbcone_eastenders", "00:30:55.20" ], [ "20080403_185000_bbctwo_torchwood_declassified", "00:11:40.32" ], [
"20080403_190000_bbcone_holby_blue", "00:59:52.36" ], [ "20080403_190000_bbcthree_doctor_who", "01:14:51.40" ], [
"20080403_190000_bbctwo_10_things_you_didn_t_know_about", "01:00:25.28" ], [ "20080403_200000_bbcfour_a_year_in_tibet",
"01:00:15.40" ], [ "20080403_200000_bbctwo_the_apprentice_the_worst_decisions_ever", "00:59:39.44" ], [
"20080403_201500_bbcthree_doctor_who_confidential_kylie_special", "00:44:41.24" ], [ "20080403_210000_bbcfour_ashes_to_ashes",
"01:00:01.24" ], [ "20080403_210000_bbcone_bbc_ten_o_clock_news", "00:26:51.72" ], [ "20080403_210000_bbcthree_eastenders",
"00:30:33.36" ], [ "20080403_210000_bbctwo_empty", "00:30:36.36" ], [ "20080403_213000_bbctwo_newsnight", "00:50:05.16" ], [
"20080403_222000_bbctwo_ideal", "00:29:30.20" ], [ "20080403_224500_bbcfour_david_ogilvy_the_first_mad_man", "00:59:36.16" ], [
"20080403_231500_bbcthree_pulling", "00:30:06.36" ], [ "20080403_232000_bbcone_holiday_weather", "00:06:18.36" ], [
"20080403_232500_bbcone_panorama", "00:30:51.28" ], [ "20080403_234500_bbcthree_pulling", "00:29:36.32" ], [
"20080403_235500_bbcone_johnny_s_new_kingdom", "00:30:08.28" ], [ "20080404_002500_bbcone_the_great_war_in_colour_the_wonderful",
"00:29:55.48" ], [ "20080404_004500_bbcfour_the_lost_world_of_tibet", "00:59:52.60" ], [
"20080404_011000_bbcthree_doctor_who_confidential_kylie_special", "00:43:26.08" ], [ "20080404_014500_bbcfour_mapping_everest",
"00:29:12.28" ], [ "20080404_015500_bbcone_bill_oddie_s_wild_side", "00:30:08.32" ], [ "20080404_021500_bbcfour_a_year_in_tibet",
"00:58:56.20" ], [ "20080404_022500_bbcone_to_buy_or_not_to_buy", "00:45:24.40" ], [ "20080404_022500_bbcthree_pulling",
"00:29:42.20" ], [ "20080404_030000_bbctwo_gcse_bitesize_revision_french_1", "01:59:46.08" ], [
"20080404_050000_bbcone_breakfast", "03:15:10.28" ], [ "20080404_050000_bbctwo_tikkabilla", "00:29:29.36" ], [
"20080404_063000_bbctwo_mama_mirabelle_s_home_movies", "00:12:33.36" ], [ "20080404_064500_bbctwo_tommy_zoom", "00:10:29.40" ], [
"20080404_065500_bbctwo_take_a_bow", "00:06:41.44" ], [ "20080404_070000_bbctwo_arthur", "00:25:32.28" ], [
"20080404_074500_bbctwo_chucklevision", "00:15:12.48" ], [ "20080404_080000_bbctwo_tracy_beaker", "00:28:04.44" ], [
"20080404_090000_bbctwo_stupid", "00:29:45.24" ], [ "20080404_100000_bbcone_to_buy_or_not_to_buy", "00:44:33.36" ], [
"20080404_110000_bbctwo_the_daily_politics", "00:30:14.24" ], [ "20080404_111500_bbcone_bargain_hunt", "00:44:21.28" ], [
"20080404_113000_bbctwo_working_lunch", "01:00:25.32" ], [ "20080404_120000_bbcone_bbc_news", "00:32:09.36" ], [
"20080404_123000_bbctwo_coast", "00:15:00.36" ], [ "20080404_124500_bbcone_doctors", "00:29:56.40" ], [
"20080404_124500_bbctwo_racing_from_aintree", "02:59:30.20" ], [ "20080404_140500_bbcone_space_pirates", "00:28:47.44" ], [
"20080404_153500_bbcone_the_slammer", "00:28:17.44" ], [ "20080404_154500_bbctwo_flog_it", "00:29:56.36" ], [
"20080404_160000_bbcone_newsround", "00:10:58.32" ], [ "20080404_161500_bbcone_the_weakest_link", "00:45:23.40" ], [
"20080404_170000_bbcone_bbc_news", "00:28:34.20" ], [ "20080404_170000_bbctwo_eggheads", "00:29:09.32" ], [
"20080404_173000_bbcone_bbc_london_news", "00:29:21.36" ], [ "20080404_180000_bbcfour_world_news_today", "00:30:25.36" ], [
"20080404_180000_bbcone_the_one_show", "00:29:33.16" ], [ "20080404_180000_bbcthree_top_gear", "01:00:56.20" ], [
"20080404_180000_bbctwo_grand_national_preview", "00:29:58.16" ], [ "20080404_183000_bbcfour_transatlantic_sessions",
"00:30:42.28" ], [ "20080404_183000_bbcone_inside_out", "00:31:45.28" ], [ "20080404_190000_bbcfour_sacred_music", "01:00:00.52"
], [ "20080404_190000_bbcone_eastenders", "00:30:19.36" ], [ "20080404_190000_bbctwo_gardeners_world", "00:59:53.40" ], [
"20080404_193000_bbcone_a_question_of_sport", "00:29:58.44" ], [ "20080404_193000_bbcthree_eastenders_ricky_and_bianca",
"00:58:20.36" ], [ "20080404_200000_bbctwo_torchwood", "00:49:57.32" ], [ "20080404_203000_bbcthree_lily_allen_and_friends",
"00:45:55.36" ], [ "20080404_205000_bbctwo_torchwood_declassified", "00:12:38.28" ], [
"20080404_210000_bbcfour_hughie_green_most_sincerely", "01:19:43.24" ], [ "20080404_210000_bbcone_bbc_ten_o_clock_news",
"00:25:33.24" ], [ "20080404_211500_bbcthree_eastenders", "00:30:37.44" ], [ "20080404_213000_bbctwo_newsnight", "00:30:59.28" ],
[ "20080404_214500_bbcthree_eastenders_ricky_and_bianca", "00:59:28.32" ], [ "20080404_222000_bbcfour_tv_s_believe_it_or_not",
"00:58:13.96" ], [ "20080404_223500_bbctwo_later_with_jools_holland", "01:01:23.40" ], [
"20080404_231500_bbcfour_tv_s_believe_it_or_not", "01:00:21.28" ], [ "20080404_233000_bbcthree_gavin_and_stacey", "00:29:29.44"
], [ "20080405_000000_bbcthree_gavin_and_stacey", "00:30:26.28" ], [ "20080405_003000_bbcthree_gavin_and_stacey", "00:29:55.32"
], [ "20080405_005000_bbcone_horizon", "00:50:22.28" ], [ "20080405_010000_bbcthree_gavin_and_stacey", "00:30:13.24" ], [
"20080405_012000_bbcfour_sacred_music", "00:59:53.56" ], [ "20080405_013000_bbcthree_lily_allen_and_friends", "00:44:49.28" ], [
"20080405_014000_bbcone_shroud_of_turin", "01:00:04.28" ], [ "20080405_022000_bbcfour_transatlantic_sessions", "00:30:38.08" ], [
"20080405_024000_bbcone_natural_world", "00:50:48.24" ], [ "20080405_031000_bbcthree_gavin_and_stacey", "00:30:03.36" ], [
"20080405_050000_bbcone_breakfast", "03:50:02.36" ], [ "20080405_052000_bbctwo_tikkabilla", "00:33:03.12" ], [
"20080405_090000_bbctwo_hedz", "00:29:16.32" ], [ "20080405_093000_bbctwo_the_slammer", "00:29:42.32" ], [
"20080405_105000_bbcone_bbc_news", "00:10:21.32" ], [ "20080405_110000_bbcone_match_of_the_day_live_fa_cup_semi_final",
"02:29:27.84" ], [ "20080405_110000_bbctwo_sound", "00:28:06.56" ], [ "20080405_113000_bbctwo_the_surgery", "00:20:52.20" ], [
"20080405_115000_bbctwo_them", "00:09:46.28" ], [ "20080405_120000_bbctwo_the_grand_national", "01:30:04.24" ], [
"20080405_133000_bbcone_grand_national", "02:29:51.24" ], [ "20080405_153500_bbctwo_final_score", "00:41:16.32" ], [
"20080405_160000_bbcone_bbc_news", "00:10:51.28" ], [ "20080405_161500_bbcone_bbc_london_news", "00:08:20.32" ], [
"20080405_161500_bbctwo_wild", "00:30:04.28" ], [ "20080405_162000_bbcone_the_weakest_link", "01:00:12.16" ], [
"20080405_164500_bbctwo_watching_the_wild", "00:10:24.36" ], [ "20080405_172000_bbcone_doctor_who", "00:49:39.24" ], [
"20080405_180000_bbcfour_sounds_of_the_sixties", "00:07:17.20" ], [ "20080405_180000_bbcthree_football_gaffes_galore",
"00:08:59.20" ], [ "20080405_181000_bbcfour_the_naked_civil_servant", "01:21:50.32" ], [
"20080405_181000_bbcone_i_d_do_anything", "01:10:23.20" ], [ "20080405_181000_bbcthree_doctor_who_confidential", "00:46:45.28" ],
[ "20080405_182500_bbctwo_dad_s_army", "00:36:19.40" ], [ "20080405_185500_bbcthree_top_gear", "01:03:09.24" ], [
"20080405_190000_bbctwo_china_s_terracotta_army", "01:00:26.40" ], [ "20080405_193000_bbcfour_doctor_who_the_daleks",
"00:25:29.80" ], [ "20080405_193000_bbcone_casualty", "00:49:47.24" ], [ "20080405_195500_bbcfour_doctor_who_the_daleks",
"00:26:22.36" ], [ "20080405_200000_bbcthree_two_pints_of_lager_outtakes", "00:19:44.16" ], [
"20080405_202000_bbcfour_doctor_who_the_daleks", "00:27:32.32" ], [ "20080405_202000_bbcone_love_soup", "00:30:26.32" ], [
"20080405_205000_bbcfour_verity_lambert_drama_queen", "01:00:08.32" ], [ "20080405_205000_bbcone_bbc_news", "00:20:24.24" ], [
"20080405_211000_bbcone_match_of_the_day", "01:30:48.24" ], [ "20080405_221500_bbcthree_gavin_and_stacey", "00:28:52.20" ], [
"20080405_221500_bbctwo_the_apprentice", "01:00:02.28" ], [ "20080405_224000_bbcfour_jonathan_creek", "00:50:09.40" ], [
"20080405_224000_bbcone_grand_national_highlights", "00:30:39.24" ], [ "20080405_224500_bbcthree_gavin_and_stacey", "00:30:21.16"
], [ "20080405_231500_bbcthree_gavin_and_stacey", "00:29:43.32" ], [ "20080405_233000_bbcfour_mark_lawson_talks_to_george_cole",
"01:00:09.64" ], [ "20080405_234500_bbcthree_gavin_and_stacey", "00:29:59.32" ], [ "20080406_005000_bbctwo_space_race",
"00:59:37.32" ], [ "20080406_010000_bbcthree_lily_allen_and_friends", "00:44:42.32" ], [
"20080406_012500_bbcfour_verity_lambert_drama_queen", "01:00:04.60" ], [ "20080406_014500_bbcthree_eastenders_ricky_and_bianca",
"00:58:31.24" ], [ "20080406_022500_bbcfour_the_hard_sell", "00:29:28.24" ], [
"20080406_024500_bbcthree_two_pints_of_lager_outtakes", "00:18:26.36" ], [ "20080406_030000_bbcthree_doctor_who_confidential",
"00:44:09.24" ], [ "20080406_050000_bbcone_breakfast", "01:26:19.44" ], [ "20080406_052000_bbctwo_tikkabilla", "00:32:28.16" ], [
"20080406_062500_bbcone_match_of_the_day", "01:33:23.32" ], [ "20080406_070000_bbctwo_trapped", "00:28:53.24" ], [
"20080406_073000_bbctwo_raven_the_secret_temple", "00:28:43.20" ], [ "20080406_080000_bbcone_the_andrew_marr_show", "00:59:53.24"
], [ "20080406_080000_bbctwo_hider_in_the_house", "00:59:08.24" ], [ "20080406_100000_bbcone_countryfile", "01:00:20.32" ], [
"20080406_110000_bbcone_bbc_news", "00:07:17.20" ], [ "20080406_115000_bbcone_allo_allo", "00:30:09.28" ], [
"20080406_122000_bbcone_eastenders", "01:58:55.36" ], [ "20080406_143000_bbctwo_women_s_european_gymnastics_championship",
"01:35:00.24" ], [ "20080406_160000_bbcone_songs_of_praise", "00:35:51.40" ], [ "20080406_160500_bbctwo_natural_world",
"00:50:09.36" ], [ "20080406_165500_bbctwo_delia", "00:30:50.24" ], [ "20080406_170000_bbcone_keeping_up_appearances",
"00:29:36.16" ], [ "20080406_172500_bbctwo_millions", "01:32:28.12" ], [ "20080406_173000_bbcone_seaside_rescue", "00:29:57.16"
], [ "20080406_180000_bbcfour_sacred_music", "01:00:30.64" ], [ "20080406_180000_bbcone_bbc_news", "00:21:33.84" ], [
"20080406_180000_bbcthree_eastenders_ricky_and_bianca", "00:58:47.32" ], [ "20080406_182000_bbcone_bbc_london_news",
"00:07:17.40" ], [ "20080406_183000_bbcone_i_d_do_anything_results", "00:30:06.40" ], [
"20080406_190000_bbcfour_mozart_sacred_music", "00:58:39.16" ], [ "20080406_190000_bbcone_tiger_spy_in_the_jungle", "01:00:26.32"
], [ "20080406_190000_bbcthree_doctor_who", "00:49:12.28" ], [ "20080406_190000_bbctwo_top_gear", "01:02:15.36" ], [
"20080406_195000_bbcthree_doctor_who_confidential", "00:10:01.36" ], [ "20080406_200000_bbcfour_dear_television", "00:10:12.28"
], [ "20080406_200000_bbcthree_gavin_and_stacey", "00:29:05.20" ], [
"20080406_200000_bbctwo_louis_theroux_s_african_hunting_holiday", "00:59:16.16" ], [ "20080406_201000_bbcfour_washes_whiter",
"00:50:38.56" ], [ "20080406_203000_bbcthree_pulling", "00:30:37.32" ], [ "20080406_210000_bbcone_bbc_news", "00:22:02.28" ], [
"20080406_210000_bbctwo_match_of_the_day_2", "01:11:10.24" ], [ "20080406_214500_bbcfour_hughie_green_most_sincerely",
"01:19:44.32" ], [ "20080406_221000_bbctwo_last_man_standing", "00:57:18.32" ], [ "20080406_224500_bbcthree_gavin_and_stacey",
"00:30:18.28" ], [ "20080406_230500_bbcfour_legends_marty_feldman_six_degrees_of", "00:59:45.52" ], [
"20080406_231500_bbcone_the_sky_at_night", "00:17:20.28" ], [ "20080406_231500_bbcthree_pulling", "00:30:00.36" ], [
"20080406_233500_bbcone_weatherview", "00:07:26.44" ], [ "20080406_234000_bbcone_around_the_world_in_80_gardens", "01:00:19.36"
], [ "20080407_000500_bbcfour_mozart_sacred_music", "00:59:07.48" ], [ "20080407_004000_bbcone_holby_city", "00:58:50.28" ], [
"20080407_004000_bbcthree_eastenders_ricky_and_bianca", "00:58:17.36" ], [ "20080407_010500_bbcfour_sacred_music", "01:00:02.80"
], [ "20080407_014000_bbcone_watchdog", "00:29:37.24" ], [ "20080407_020500_bbcfour_legends_marty_feldman_six_degrees_of",
"00:59:34.24" ], [ "20080407_021000_bbcone_to_buy_or_not_to_buy", "00:46:33.28" ], [
"20080407_030500_bbcthree_doctor_who_confidential", "00:12:46.36" ], [ "20080407_034000_bbctwo_inside_sport", "00:42:14.20" ], [
"20080407_050000_bbcone_breakfast", "03:14:38.00" ], [ "20080407_050000_bbctwo_tikkabilla", "00:30:33.24" ], [
"20080407_063000_bbctwo_mama_mirabelle_s_home_movies", "00:12:36.32" ], [ "20080407_064500_bbctwo_tommy_zoom", "00:17:29.24" ], [
"20080407_074500_bbctwo_chucklevision", "00:14:51.36" ], [ "20080407_080000_bbctwo_tracy_beaker", "00:28:13.24" ], [
"20080407_081500_bbcone_to_buy_or_not_to_buy", "00:45:13.44" ], [ "20080407_090000_bbctwo_stupid", "00:28:58.40" ], [
"20080407_095000_bbctwo_schools_look_and_read", "00:21:16.28" ], [ "20080407_101000_bbctwo_schools_razzledazzle", "00:19:36.24"
], [ "20080407_111500_bbcone_bargain_hunt", "00:44:28.40" ], [ "20080407_113000_bbctwo_working_lunch", "00:29:48.36" ], [
"20080407_120000_bbcone_bbc_news", "00:31:26.04" ], [ "20080407_120000_bbctwo_schools_science_clip_investigates", "00:10:49.28"
], [ "20080407_121000_bbctwo_schools_science_clip_investigates", "00:09:20.16" ], [
"20080407_122000_bbctwo_schools_primary_geography", "00:09:56.36" ], [ "20080407_123000_bbcone_bbc_london_news", "00:13:51.32" ],
[ "20080407_124500_bbcone_doctors", "00:28:29.40" ], [ "20080407_140500_bbcone_space_pirates", "00:28:56.28" ], [
"20080407_143500_bbcone_small_talk_diaries", "00:15:06.24" ], [ "20080407_144500_bbctwo_flog_it", "00:44:49.28" ], [
"20080407_150500_bbcone_the_revenge_files_of_alistair_fury", "00:27:55.24" ], [ "20080407_153000_bbctwo_ready_steady_cook",
"00:44:37.28" ], [ "20080407_153500_bbcone_mi_high", "00:28:53.28" ], [ "20080407_160000_bbcone_newsround", "00:11:04.24" ], [
"20080407_161500_bbcone_the_weakest_link", "00:45:30.28" ], [ "20080407_170000_bbcone_bbc_news", "00:29:37.20" ], [
"20080407_170000_bbctwo_eggheads", "00:30:06.28" ], [ "20080407_173000_bbcone_bbc_london_news", "00:23:03.24" ], [
"20080407_180000_bbcfour_world_news_today", "00:30:24.40" ], [ "20080407_180000_bbcone_the_one_show", "00:30:22.16" ], [
"20080407_180000_bbcthree_two_pints_of_lager_outtakes", "00:18:43.24" ], [ "20080407_180000_bbctwo_the_undercover_diplomat",
"00:59:39.28" ], [ "20080407_183000_bbcfour_pop_goes_the_sixties", "00:04:48.40" ], [ "20080407_183000_bbcone_watchdog",
"00:32:07.48" ], [ "20080407_183500_bbcfour_doctor_who_the_daleks", "00:25:32.68" ], [
"20080407_190000_bbcfour_the_sky_at_night", "00:30:26.32" ], [ "20080407_190000_bbcone_eastenders", "00:29:51.24" ], [
"20080407_190000_bbctwo_university_challenge_the_professionals", "00:30:10.36" ], [ "20080407_193000_bbcfour_the_book_quiz",
"00:30:32.32" ], [ "20080407_193000_bbcone_panorama", "00:29:44.32" ], [ "20080407_193000_bbctwo_delia", "00:30:25.32" ], [
"20080407_200000_bbcfour_verity_lambert_drama_queen", "01:00:44.68" ], [ "20080407_200000_bbctwo_clowns", "01:00:16.16" ], [
"20080407_210000_bbcfour_shoulder_to_shoulder_annie_kenney", "01:17:24.64" ], [ "20080407_210000_bbcone_bbc_ten_o_clock_news",
"00:27:08.40" ], [ "20080407_210000_bbcthree_eastenders", "00:29:08.28" ], [ "20080407_212500_bbcone_bbc_london_news",
"00:10:15.24" ], [ "20080407_213000_bbcthree_gavin_and_stacey", "00:30:19.24" ], [ "20080407_213000_bbctwo_newsnight",
"00:49:29.28" ], [ "20080407_220000_bbcthree_pulling", "00:29:48.24" ], [
"20080407_222000_bbcfour_the_cult_of_adam_adamant_lives", "00:30:30.52" ], [
"20080407_222000_bbctwo_archaeology_digging_the_past", "01:00:14.24" ], [ "20080407_222500_bbcone_inside_sport", "00:40:41.32" ],
[ "20080407_225000_bbcfour_adam_adamant_lives", "00:45:39.28" ], [ "20080407_233500_bbcfour_the_sky_at_night", "00:30:26.32" ], [
"20080408_000500_bbcfour_the_book_quiz", "00:29:51.40" ], [ "20080408_001500_bbcthree_gavin_and_stacey", "00:29:04.32" ], [
"20080408_002000_bbcone_weatherview", "00:06:09.28" ], [ "20080408_002500_bbcone_gardener_s_world", "01:00:01.32" ], [
"20080408_003500_bbcfour_verity_lambert_drama_queen", "01:00:08.32" ], [ "20080408_004500_bbcthree_pulling", "00:30:02.32" ], [
"20080408_012500_bbcone_unknown_africa_the_comoros_islands", "00:29:33.24" ], [ "20080408_013500_bbcfour_the_book_quiz",
"00:29:53.32" ], [ "20080408_014500_bbcthree_two_pints_of_lager_outtakes", "00:18:55.32" ], [
"20080408_015500_bbcone_to_buy_or_not_to_buy", "00:44:34.20" ], [ "20080408_020500_bbcfour_verity_lambert_drama_queen",
"00:58:32.32" ], [ "20080408_030000_bbctwo_key_stage_three_bitesize", "01:59:40.32" ], [ "20080408_050000_bbcone_breakfast",
"03:15:15.36" ], [ "20080408_050000_bbctwo_tikkabilla", "00:30:21.20" ], [ "20080408_063000_bbctwo_mama_mirabelle_s_home_movies",
"00:12:32.40" ], [ "20080408_064500_bbctwo_tommy_zoom", "00:10:41.28" ], [ "20080408_065500_bbctwo_take_a_bow", "00:06:52.32" ],
[ "20080408_070000_bbctwo_arthur", "00:29:46.40" ], [ "20080408_074500_bbctwo_chucklevision", "00:15:02.40" ], [
"20080408_080000_bbctwo_tracy_beaker", "00:27:49.32" ], [ "20080408_081500_bbcone_to_buy_or_not_to_buy", "00:44:30.40" ], [
"20080408_090000_bbctwo_stupid", "00:29:10.40" ], [ "20080408_093000_bbctwo_schools_words_and_pictures", "00:09:02.36" ], [
"20080408_094000_bbctwo_schools_words_and_pictures", "00:10:54.32" ], [ "20080408_101000_bbctwo_coast", "00:06:41.32" ], [
"20080408_111500_bbcone_bargain_hunt", "00:44:24.36" ], [ "20080408_113000_bbctwo_working_lunch", "00:29:58.32" ], [
"20080408_120000_bbcone_bbc_news", "00:31:47.40" ], [ "20080408_120000_bbctwo_schools_the_maths_channel", "00:09:21.24" ], [
"20080408_121000_bbctwo_schools_primary_geography", "00:19:35.28" ], [ "20080408_123000_bbcone_bbc_london_news", "00:13:15.28" ],
[ "20080408_124500_bbcone_doctors", "00:29:36.40" ], [ "20080408_140500_bbcone_space_pirates", "00:29:26.20" ], [
"20080408_143500_bbcone_small_talk_diaries", "00:14:51.40" ], [ "20080408_144500_bbctwo_flog_it", "00:44:55.20" ], [
"20080408_153000_bbctwo_ready_steady_cook", "00:44:59.28" ], [ "20080408_153500_bbcone_blue_peter", "00:24:46.48" ], [
"20080408_160000_bbcone_newsround", "00:11:34.36" ], [ "20080408_161500_bbcone_the_weakest_link", "00:45:45.28" ], [
"20080408_170000_bbcone_bbc_news", "00:29:10.20" ], [ "20080408_170000_bbctwo_eggheads", "00:29:30.28" ], [
"20080408_173000_bbcone_bbc_london_news", "00:24:01.28" ], [ "20080408_175500_bbcone_party_election_broadcast_by_the",
"00:06:02.36" ], [ "20080408_180000_bbcfour_world_news_today", "00:30:39.40" ], [ "20080408_180000_bbcone_the_one_show",
"00:30:54.44" ], [ "20080408_180000_bbctwo_how_diana_died_a_conspiracy", "01:00:27.24" ], [
"20080408_183000_bbcfour_pop_goes_the_sixties", "00:04:28.24" ], [ "20080408_183000_bbcone_eastenders", "00:31:58.24" ], [
"20080408_183000_bbcthree_dog_borstal", "00:30:39.36" ], [ "20080408_183500_bbcfour_doctor_who_the_daleks", "00:25:38.32" ], [
"20080408_190000_bbcfour_life_in_cold_blood", "00:59:21.40" ], [ "20080408_190000_bbcone_holby_city", "00:57:19.24" ], [
"20080408_190000_bbcthree_dawn_gets_a_baby", "00:57:39.40" ], [ "20080408_190000_bbctwo_university_challenge_the_professionals",
"00:30:34.32" ], [ "20080408_193000_bbctwo_johnny_s_new_kingdom", "00:30:04.40" ], [ "20080408_200000_bbcfour_chinese_school",
"01:00:22.48" ], [ "20080408_200000_bbcone_hotel_babylon", "01:00:38.40" ], [ "20080408_200000_bbctwo_massacre_at_virginia_tech",
"01:00:00.24" ], [ "20080408_210000_bbcfour_a_year_in_tibet", "01:00:10.48" ], [ "20080408_210000_bbcone_bbc_ten_o_clock_news",
"00:26:42.24" ], [ "20080408_210000_bbcthree_eastenders", "00:29:23.32" ], [
"20080408_210000_bbctwo_later_live_with_jools_holland", "00:30:53.28" ], [ "20080408_212500_bbcone_bbc_london_news",
"00:10:19.28" ], [ "20080408_213000_bbcthree_little_britain_abroad", "00:31:27.16" ], [ "20080408_213000_bbctwo_newsnight",
"00:48:58.40" ], [ "20080408_213500_bbcone_the_killing_of_sally_anne_bowman", "00:40:18.28" ], [
"20080408_220000_bbcfour_the_book_quiz", "00:30:32.44" ], [ "20080408_220000_bbcthree_the_wall", "00:44:01.28" ], [
"20080408_223000_bbcfour_chinese_school", "00:59:54.28" ], [ "20080408_233000_bbcfour_proms_on_four", "02:16:24.64" ], [
"20080408_235000_bbcone_weatherview", "00:05:44.44" ], [ "20080408_235500_bbcone_see_hear", "00:29:58.32" ], [
"20080409_002500_bbcone_unknown_africa_central_african_republic", "00:29:49.20" ], [ "20080409_005500_bbcone_life_in_cold_blood",
"00:58:25.32" ], [ "20080409_005500_bbcthree_dawn_gets_a_baby", "00:57:24.28" ], [ "20080409_014500_bbcfour_chinese_school",
"00:59:39.40" ], [ "20080409_015500_bbcone_to_buy_or_not_to_buy", "00:44:21.28" ], [ "20080409_022500_bbcthree_dog_borstal",
"00:29:11.28" ], [ "20080409_024500_bbcfour_the_book_quiz", "00:27:18.28" ], [ "20080409_030000_bbctwo_key_stage_three_bitesize",
"01:43:33.32" ], [ "20080409_050000_bbcone_breakfast", "03:14:44.44" ], [ "20080409_050000_bbctwo_tikkabilla", "00:29:56.28" ], [
"20080409_063000_bbctwo_mama_mirabelle_s_home_movies", "00:12:34.28" ], [ "20080409_064500_bbctwo_tommy_zoom", "00:10:13.40" ], [
"20080409_065500_bbctwo_take_a_bow", "00:06:35.36" ], [ "20080409_074500_bbctwo_chucklevision", "00:15:16.40" ], [
"20080409_080000_bbctwo_tracy_beaker", "00:28:19.32" ], [ "20080409_081500_bbcone_to_buy_or_not_to_buy", "00:44:37.24" ], [
"20080409_090000_bbctwo_stupid", "00:29:22.32" ], [ "20080409_104500_bbctwo_coast", "00:05:15.32" ], [
"20080409_111500_bbcone_bargain_hunt", "00:44:14.28" ], [ "20080409_120000_bbcone_bbc_news", "00:31:58.32" ], [
"20080409_120000_bbctwo_see_hear", "00:30:14.28" ], [ "20080409_123000_bbcone_bbc_london_news", "00:13:42.44" ], [
"20080409_123000_bbctwo_working_lunch", "00:30:37.24" ], [ "20080409_124500_bbcone_doctors", "00:30:26.20" ], [
"20080409_130000_bbctwo_world_swimming_championships", "00:58:37.28" ], [ "20080409_140500_bbcone_space_pirates", "00:29:05.28"
], [ "20080409_143500_bbcone_small_talk_diaries", "00:14:45.20" ], [ "20080409_144500_bbctwo_flog_it", "00:44:40.20" ], [
"20080409_150500_bbcone_young_dracula", "00:29:17.28" ], [ "20080409_153000_bbctwo_ready_steady_cook", "00:44:46.24" ], [
"20080409_153500_bbcone_blue_peter", "00:24:32.44" ], [ "20080409_160000_bbcone_newsround", "00:12:07.36" ], [
"20080409_161500_bbcone_the_weakest_link", "00:45:19.40" ], [ "20080409_170000_bbcone_bbc_news", "00:28:16.16" ], [
"20080409_170000_bbctwo_eggheads", "00:29:30.20" ], [ "20080409_173000_bbcone_bbc_london_news", "00:23:59.36" ], [
"20080409_175500_bbcone_party_election_broadcast_by_the", "00:05:23.36" ], [ "20080409_180000_bbcfour_world_news_today",
"00:30:28.36" ], [ "20080409_180000_bbcone_the_one_show", "00:30:06.40" ], [
"20080409_180000_bbctwo_world_swimming_championships", "00:59:49.24" ], [ "20080409_183000_bbcfour_doctor_who_the_daleks",
"00:26:45.52" ], [ "20080409_183000_bbcone_street_doctor", "00:32:04.24" ], [ "20080409_183000_bbcthree_dog_borstal",
"00:30:29.32" ], [ "20080409_185500_bbcfour_doctor_who_the_daleks", "00:24:01.36" ], [ "20080409_190000_bbcone_traffic_cops",
"01:00:00.40" ], [ "20080409_190000_bbctwo_natural_world", "00:49:47.24" ], [
"20080409_192000_bbcfour_live_on_the_night_time_shift", "00:40:41.08" ], [ "20080409_195000_bbctwo_badlands_raging_bulls",
"00:10:29.32" ], [ "20080409_200000_bbcfour_frankie_howerd_rather_you_than_me", "00:58:44.32" ], [
"20080409_200000_bbcone_the_apprentice", "01:00:07.28" ], [ "20080409_200000_bbctwo_dan_cruickshank_s_adventures", "01:00:25.16"
], [ "20080409_210000_bbcfour_arena_oooh_er_missus_the_frankie", "00:58:56.44" ], [
"20080409_210000_bbcone_bbc_ten_o_clock_news", "00:25:31.32" ], [ "20080409_210000_bbctwo_the_apprentice_you_re_fired",
"00:32:15.32" ], [ "20080409_212500_bbcone_bbc_london_news", "00:10:35.36" ], [ "20080409_213000_bbctwo_newsnight", "00:49:37.32"
], [ "20080409_220000_bbcfour_up_pompeii", "00:30:20.56" ], [ "20080409_222000_bbctwo_golf_us_masters", "01:13:41.08" ], [
"20080409_223000_bbcfour_mark_lawson_talks_to_david_renwick", "00:59:32.32" ], [ "20080409_231500_bbcthree_the_wall",
"00:45:17.20" ], [ "20080409_233000_bbcfour_frankie_howerd_rather_you_than_me", "00:58:43.60" ], [
"20080410_002500_bbcfour_demob_happy_how_tv_conquered", "00:59:46.60" ], [ "20080410_003000_bbcone_weatherview", "00:05:19.20" ],
[ "20080410_003500_bbcone_unknown_africa_angola", "00:30:28.24" ], [
"20080410_012500_bbcfour_mark_lawson_talks_to_david_renwick", "00:59:10.24" ], [
"20080410_022500_bbcfour_frankie_howerd_rather_you_than_me", "00:57:37.24" ], [ "20080410_023500_bbcone_to_buy_or_not_to_buy",
"00:44:41.48" ], [ "20080410_025000_bbcthree_dog_borstal", "00:29:33.24" ], [ "20080410_030000_bbctwo_key_stage_three_bitesize",
"01:59:26.48" ], [ "20080410_050000_bbctwo_tikkabilla", "00:30:00.32" ], [ "20080410_063000_bbctwo_mama_mirabelle_s_home_movies",
"00:12:42.16" ], [ "20080410_064500_bbctwo_tommy_zoom", "00:16:38.36" ], [ "20080410_070000_bbctwo_arthur", "00:32:29.36" ], [
"20080410_074500_bbctwo_chucklevision", "00:14:43.32" ], [ "20080410_080000_bbctwo_tracy_beaker", "00:27:12.32" ], [
"20080410_081500_bbcone_to_buy_or_not_to_buy", "00:45:34.32" ], [ "20080410_090000_bbctwo_stupid", "00:28:40.20" ], [
"20080410_093000_bbctwo_schools_primary_history", "00:19:25.24" ], [ "20080410_101000_bbctwo_schools_primary_geography",
"00:19:48.32" ], [ "20080410_103000_bbctwo_schools_science_clips", "00:09:12.44" ], [
"20080410_104000_bbctwo_schools_science_clips", "00:09:23.96" ], [ "20080410_105000_bbctwo_schools_hands_up", "00:10:53.24" ], [
"20080410_110000_bbctwo_open_gardens", "00:29:44.24" ], [ "20080410_111500_bbcone_bargain_hunt", "00:44:48.40" ], [
"20080410_113000_bbctwo_working_lunch", "00:30:59.20" ], [ "20080410_120000_bbcone_bbc_news", "00:31:37.36" ], [
"20080410_120000_bbctwo_world_swimming_championships", "01:58:49.32" ], [ "20080410_123000_bbcone_bbc_london_news", "00:13:15.24"
], [ "20080410_124500_bbcone_doctors", "00:29:56.40" ], [ "20080410_140500_bbcone_space_pirates", "00:28:38.28" ], [
"20080410_143500_bbcone_small_talk_diaries", "00:15:21.28" ], [ "20080410_144500_bbctwo_flog_it", "00:44:39.56" ], [
"20080410_150500_bbcone_stake_out", "00:28:36.28" ], [ "20080410_153500_bbcone_beat_the_boss", "00:28:24.32" ], [
"20080410_160000_bbcone_newsround", "00:10:46.28" ], [ "20080410_161500_bbcone_the_weakest_link", "00:45:20.36" ], [
"20080410_170000_bbcone_bbc_news", "00:28:25.96" ], [ "20080410_170000_bbctwo_eggheads", "00:29:39.08" ], [
"20080410_173000_bbcone_bbc_london_news", "00:29:54.48" ], [ "20080410_180000_bbcfour_world_news_today", "00:30:04.72" ], [
"20080410_180000_bbcone_the_one_show", "00:30:55.28" ], [ "20080410_180000_bbctwo_world_swimming_championships", "01:01:02.24" ],
[ "20080410_183000_bbcfour_in_search_of_medieval_britain", "00:30:02.32" ], [ "20080410_183000_bbcone_eastenders", "00:31:29.32"
], [ "20080410_190000_bbcone_holby_blue", "00:58:41.32" ], [ "20080410_190000_bbctwo_coast", "00:58:53.48" ], [
"20080410_200000_bbctwo_golf_us_masters", "01:33:05.08" ], [ "20080410_210000_bbcfour_chinese_school", "01:00:12.28" ], [
"20080410_210000_bbcone_bbc_ten_o_clock_news", "00:26:20.28" ], [ "20080410_210000_bbcthree_eastenders", "00:30:03.28" ], [
"20080410_212500_bbcone_bbc_london_news", "00:09:56.24" ], [ "20080410_213000_bbcthree_lily_allen_my_favourite_bits",
"00:59:24.08" ], [ "20080410_213500_bbcone_golf_us_masters", "02:09:35.28" ], [ "20080410_222000_bbctwo_ideal", "00:29:29.20" ],
[ "20080410_224500_bbcfour_in_search_of_medieval_britain", "00:30:00.28" ], [ "20080410_231000_bbcthree_pulling", "00:29:36.24"
], [ "20080410_231500_bbcfour_the_sky_at_night", "00:30:34.24" ], [ "20080410_235000_bbcone_panorama", "00:29:17.24" ], [
"20080411_004000_bbcthree_lily_allen_my_favourite_bits", "00:58:00.32" ], [ "20080411_004500_bbcfour_chinese_school",
"00:59:53.44" ], [ "20080411_005000_bbcone_the_twenties_in_colour_the_wonderful", "00:30:05.28" ], [
"20080411_012000_bbcone_bill_oddie_s_wild_side", "00:30:50.32" ], [ "20080411_014000_bbcthree_pulling", "00:29:45.44" ], [
"20080411_014500_bbcfour_in_search_of_medieval_britain", "00:30:40.48" ], [ "20080411_030000_bbctwo_gcse_bitesize", "01:59:10.28"
], [ "20080411_050000_bbcone_breakfast", "03:16:19.28" ], [ "20080411_050000_bbctwo_tikkabilla", "00:29:37.28" ], [
"20080411_063000_bbctwo_mama_mirabelle_s_home_movies", "00:12:38.28" ], [ "20080411_064500_bbctwo_tommy_zoom", "00:10:10.32" ], [
"20080411_065500_bbctwo_take_a_bow", "00:06:18.24" ], [ "20080411_070000_bbctwo_arthur", "00:24:43.28" ], [
"20080411_074500_bbctwo_chucklevision", "00:14:41.36" ], [ "20080411_080000_bbctwo_tracy_beaker", "00:27:14.20" ], [
"20080411_081500_bbcone_to_buy_or_not_to_buy", "00:44:31.24" ], [ "20080411_090000_bbctwo_stupid", "00:28:59.40" ], [
"20080411_094500_bbctwo_schools_the_way_things_work", "00:15:12.20" ], [ "20080411_100000_bbctwo_schools_the_way_things_work",
"00:15:23.32" ], [ "20080411_103000_bbctwo_schools_watch", "00:14:25.16" ], [ "20080411_104500_bbctwo_schools_something_special",
"00:14:25.28" ], [ "20080411_111500_bbcone_bargain_hunt", "00:43:56.64" ], [ "20080411_113000_bbctwo_working_lunch",
"01:00:16.92" ], [ "20080411_123000_bbcone_bbc_london_news", "00:13:32.32" ], [
"20080411_123000_bbctwo_world_swimming_championships", "01:29:44.32" ], [ "20080411_140500_bbcone_space_pirates", "00:28:58.20"
], [ "20080411_143500_bbcone_small_talk_diaries", "00:15:19.32" ], [ "20080411_144500_bbctwo_flog_it", "00:44:57.28" ], [
"20080411_151000_bbcone_basil_brush", "00:25:21.16" ], [ "20080411_153000_bbctwo_ready_steady_cook", "00:44:52.32" ], [
"20080411_153500_bbcone_the_slammer", "00:28:35.28" ], [ "20080411_160000_bbcone_newsround", "00:09:41.24" ], [
"20080411_161500_bbcone_the_weakest_link", "00:45:35.20" ], [ "20080411_170000_bbcone_bbc_news", "00:28:22.32" ], [
"20080411_170000_bbctwo_eggheads", "00:29:54.32" ], [ "20080411_173000_bbcone_bbc_london_news", "00:30:08.36" ], [
"20080411_180000_bbcone_the_one_show", "00:29:07.28" ], [ "20080411_180000_bbcthree_top_gear", "01:00:18.36" ], [
"20080411_180000_bbctwo_world_swimming_championships", "01:00:01.32" ], [ "20080411_183000_bbcfour_transatlantic_sessions",
"00:30:12.40" ], [ "20080411_183000_bbcone_inside_out", "00:32:23.28" ], [ "20080411_190000_bbcfour_sacred_music", "01:00:30.40"
], [ "20080411_190000_bbcone_eastenders", "00:30:08.28" ], [ "20080411_190000_bbctwo_gardeners_world", "01:00:20.28" ], [
"20080411_193000_bbcone_a_question_of_sport", "00:30:07.36" ], [ "20080411_200000_bbctwo_golf_world_us_masters", "01:32:39.44" ],
[ "20080411_202000_bbcthree_doctor_who_confidential", "00:45:56.16" ], [
"20080411_210000_bbcfour_frankie_howerd_rather_you_than_me", "00:59:22.36" ], [ "20080411_210000_bbcone_bbc_ten_o_clock_news",
"00:26:54.12" ], [ "20080411_210000_bbcthree_eastenders", "00:30:09.20" ], [ "20080411_212500_bbcone_bbc_london_news",
"00:09:57.28" ], [ "20080411_213000_bbctwo_newsnight", "00:31:01.20" ], [ "20080411_224000_bbctwo_later_with_jools_holland",
"01:01:48.20" ], [ "20080411_231500_bbcthree_gavin_and_stacey", "00:29:22.20" ], [
"20080411_234500_bbcthree_lily_allen_my_favourite_bits", "00:58:18.32" ], [ "20080412_000500_bbcfour_sacred_music", "00:59:48.16"
], [ "20080412_010500_bbcfour_frankie_howerd_rather_you_than_me", "00:58:40.36" ], [ "20080412_014000_bbcthree_gavin_and_stacey",
"00:28:17.40" ], [ "20080412_015000_bbcone_weatherview", "00:15:39.12" ], [
"20080412_015500_bbcone_dan_cruickshank_s_adventures", "01:00:25.32" ], [
"20080412_024000_bbcthree_lily_allen_my_favourite_bits", "00:57:38.16" ], [ "20080412_025500_bbcone_natural_world", "00:50:18.48"
], [ "20080412_050000_bbcone_breakfast", "03:59:45.32" ], [ "20080412_052000_bbctwo_tikkabilla", "00:32:56.28" ], [
"20080412_090000_bbctwo_hedz", "00:29:10.24" ], [ "20080412_093000_bbctwo_the_slammer", "00:29:31.20" ], [
"20080412_104500_bbctwo_sportsround", "00:15:37.40" ], [ "20080412_111000_bbcone_football_focus", "00:49:44.08" ], [
"20080412_114500_bbctwo_the_surgery", "00:20:21.16" ], [ "20080412_120000_bbcone_swimming_world_championships", "01:10:24.36" ],
[ "20080412_120500_bbctwo_sound", "00:31:30.24" ], [ "20080412_123500_bbctwo_the_sky_at_night", "00:20:31.28" ], [
"20080412_131000_bbcone_rugby_union_anglo_welsh_cup_final", "02:20:01.32" ], [
"20080412_152500_bbctwo_world_swimming_championships", "01:06:35.28" ], [ "20080412_153000_bbcone_final_score", "00:49:48.48" ],
[ "20080412_162000_bbcone_bbc_news", "00:08:33.36" ], [ "20080412_162500_bbcone_bbc_london_news", "00:07:35.28" ], [
"20080412_163500_bbcone_outtake_tv", "00:29:22.20" ], [ "20080412_170500_bbcone_the_kids_are_all_right", "00:41:12.20" ], [
"20080412_174500_bbcone_doctor_who", "00:49:40.16" ], [ "20080412_180000_bbcfour_meetings_with_remarkable_trees", "00:09:35.32"
], [ "20080412_180000_bbcthree_dog_borstal", "00:33:19.08" ], [ "20080412_180000_bbctwo_dad_s_army", "00:29:48.20" ], [
"20080412_181000_bbcfour_in_search_of_medieval_britain", "00:30:28.60" ], [ "20080412_183000_bbctwo_the_lost_world_of_tibet",
"01:00:20.36" ], [ "20080412_183500_bbcone_i_d_do_anything", "01:05:22.20" ], [
"20080412_183500_bbcthree_doctor_who_confidential", "00:46:14.28" ], [ "20080412_184000_bbcfour_the_book_quiz", "00:30:23.32" ],
[ "20080412_192000_bbcthree_top_gear", "01:00:25.28" ], [ "20080412_193000_bbctwo_golf_us_masters", "04:14:02.20" ], [
"20080412_195000_bbcone_casualty", "00:50:43.32" ], [ "20080412_201000_bbcfour_pompeii_the_last_day", "01:00:06.40" ], [
"20080412_204000_bbcone_love_soup", "00:30:06.36" ], [ "20080412_211000_bbcone_bbc_news", "00:21:07.36" ], [
"20080412_213000_bbcone_match_of_the_day", "01:20:24.36" ], [ "20080412_222000_bbcthree_gavin_and_stacey", "00:29:55.36" ], [
"20080412_234500_bbctwo_the_apprentice", "01:00:56.96" ], [ "20080413_002500_bbcone_weatherview", "00:07:30.40" ], [
"20080413_003000_bbcthree_lily_allen_my_favourite_bits", "00:57:15.36" ], [ "20080413_004500_bbctwo_space_race", "01:00:39.40" ],
[ "20080413_005500_bbcfour_10_things_you_didn_t_know_about", "00:59:23.68" ], [ "20080413_013000_bbcthree_gavin_and_stacey",
"00:29:25.36" ], [ "20080413_015500_bbcfour_the_book_quiz", "00:29:53.68" ], [ "20080413_020000_bbcthree_the_wall", "00:45:21.16"
], [ "20080413_050000_bbcone_breakfast", "01:06:24.24" ], [ "20080413_050000_bbctwo_fimbles", "00:20:39.32" ], [
"20080413_052000_bbctwo_tikkabilla", "00:41:54.32" ], [ "20080413_060500_bbcone_match_of_the_day", "01:23:18.40" ], [
"20080413_070000_bbctwo_trapped", "00:29:21.20" ], [ "20080413_073000_bbcone_london_marathon", "05:29:49.40" ], [
"20080413_073000_bbctwo_raven_the_secret_temple", "00:28:38.40" ], [ "20080413_113000_bbctwo_moto_gp", "01:30:28.28" ], [
"20080413_130000_bbcone_the_politics_show", "01:00:15.24" ], [ "20080413_130000_bbctwo_premiership_rugby", "00:43:53.16" ], [
"20080413_134500_bbctwo_world_swimming_championships", "03:05:02.32" ], [ "20080413_140500_bbcone_eastenders", "01:54:49.20" ], [
"20080413_163000_bbcone_songs_of_praise", "00:35:23.36" ], [ "20080413_170500_bbcone_bbc_news", "00:16:49.32" ], [
"20080413_172000_bbcone_bbc_london_news", "00:07:16.32" ], [ "20080413_174000_bbctwo_london_marathon_highlights", "00:50:46.40"
], [ "20080413_180000_bbcfour_sacred_music", "01:28:05.68" ], [ "20080413_180000_bbcthree_sound", "00:29:35.28" ], [
"20080413_183000_bbcone_i_d_do_anything_results", "00:30:55.20" ], [ "20080413_183000_bbctwo_golf_us_masters", "05:13:54.28" ], [
"20080413_190000_bbcfour_proms_on_four", "00:02:22.60" ], [ "20080413_190000_bbcone_tiger_spy_in_the_jungle", "01:00:27.28" ], [
"20080413_195000_bbcthree_doctor_who_confidential", "00:10:43.36" ], [ "20080413_200000_bbcfour_dear_television", "00:10:16.00"
], [ "20080413_200000_bbcthree_gavin_and_stacey", "00:28:55.32" ], [ "20080413_203000_bbcthree_pulling", "00:30:50.28" ], [
"20080413_210000_bbcone_bbc_ten_o_clock_news", "00:20:17.44" ], [ "20080413_210000_bbcthree_lily_allen_my_favourite_bits",
"00:58:22.32" ], [ "20080413_214500_bbcfour_frankie_howerd_rather_you_than_me", "00:59:23.80" ], [
"20080413_224500_bbcthree_gavin_and_stacey", "00:30:22.44" ], [ "20080413_231500_bbcthree_pulling", "00:30:19.32" ], [
"20080413_234500_bbcfour_proms_on_four", "00:55:31.36" ], [ "20080413_234500_bbcthree_lily_allen_my_favourite_bits",
"00:56:51.28" ], [ "20080413_234500_bbctwo_last_man_standing", "00:57:05.28" ], [ "20080414_000000_bbcone_weatherview",
"00:05:48.24" ], [ "20080414_000500_bbcone_around_the_world_in_80_gardens", "01:00:08.36" ], [
"20080414_004000_bbcfour_sacred_music", "01:00:32.56" ], [ "20080414_010500_bbcone_holby_city", "00:57:43.24" ], [
"20080414_014000_bbcfour_frankie_howerd_rather_you_than_me", "00:56:47.24" ], [ "20080414_020500_bbcone_watchdog", "00:29:38.40"
], [ "20080414_023500_bbcone_to_buy_or_not_to_buy", "00:28:44.20" ], [ "20080414_030500_bbcthree_dog_borstal", "00:28:18.36" ], [
"20080414_033000_bbctwo_inside_sport", "00:39:39.36" ], [ "20080414_033500_bbcthree_doctor_who_confidential", "00:10:31.28" ], [
"20080414_041000_bbctwo_london_marathon_highlights", "00:49:29.64" ], [ "20080414_050000_bbcone_breakfast", "03:14:33.20" ], [
"20080414_050000_bbctwo_tikkabilla", "00:30:31.32" ], [ "20080414_062500_bbctwo_newsround", "00:05:13.28" ], [
"20080414_063000_bbctwo_hider_in_the_house", "01:01:52.28" ], [ "20080414_073000_bbctwo_jackanory_junior", "00:14:45.24" ], [
"20080414_080500_bbctwo_boogie_beebies", "00:15:24.20" ], [ "20080414_081500_bbcone_to_buy_or_not_to_buy", "00:44:19.24" ], [
"20080414_083500_bbctwo_something_special", "00:21:19.20" ], [ "20080414_095000_bbctwo_schools_look_and_read", "00:19:37.44" ], [
"20080414_103000_bbcone_cash_in_the_attic", "00:46:08.08" ], [ "20080414_110000_bbctwo_open_gardens", "00:29:42.88" ], [
"20080414_111500_bbcone_bargain_hunt", "00:44:35.40" ], [ "20080414_120000_bbcone_bbc_news", "00:31:17.12" ], [
"20080414_120000_bbctwo_schools_science_clip_investigates", "00:09:16.32" ], [
"20080414_121000_bbctwo_schools_science_clip_investigates", "00:09:20.32" ], [
"20080414_122000_bbctwo_schools_primary_geography", "00:10:56.16" ], [ "20080414_124500_bbcone_doctors", "00:30:16.24" ], [
"20080414_140500_bbcone_space_pirates", "00:29:27.20" ], [ "20080414_143500_bbcone_small_talk_diaries", "00:15:06.28" ], [
"20080414_144500_bbctwo_flog_it", "00:44:41.40" ], [ "20080414_151000_bbcone_roar", "00:28:22.32" ], [
"20080414_153000_bbctwo_ready_steady_cook", "00:45:10.28" ], [ "20080414_153500_bbcone_grange_hill", "00:24:53.28" ], [
"20080414_160000_bbcone_newsround", "00:10:32.32" ], [ "20080414_161500_bbcone_the_weakest_link", "00:45:37.20" ], [
"20080414_170000_bbcone_bbc_news", "00:27:33.24" ], [ "20080414_170000_bbctwo_eggheads", "00:30:07.20" ], [
"20080414_173000_bbcone_bbc_london_news", "00:25:00.32" ], [ "20080414_173000_bbctwo_great_british_menu", "00:30:48.32" ], [
"20080414_180000_bbcone_the_one_show", "00:30:10.32" ], [ "20080414_180000_bbcthree_young_mums_mansion_friends_and_family",
"00:29:24.36" ], [ "20080414_183000_bbcone_watchdog", "00:31:32.36" ], [ "20080414_183000_bbcthree_dog_borstal", "00:29:57.24" ],
[ "20080414_185000_bbctwo_new_forest_ponies", "00:11:01.24" ], [ "20080414_190000_bbcone_eastenders", "00:30:29.44" ], [
"20080414_190000_bbctwo_university_challenge_the_professionals", "00:30:22.28" ], [ "20080414_193000_bbcfour_the_book_quiz",
"00:30:46.36" ], [ "20080414_193000_bbcone_panorama", "00:29:47.16" ], [ "20080414_193000_bbctwo_delia", "00:30:09.36" ], [
"20080414_200000_bbcone_waking_the_dead", "00:59:26.28" ], [ "20080414_210000_bbcone_bbc_ten_o_clock_news", "00:26:51.24" ], [
"20080414_210000_bbcthree_eastenders", "00:29:44.24" ], [ "20080414_210000_bbctwo_grumpy_guide_to_politics", "00:30:42.44" ], [
"20080414_213000_bbcthree_gavin_and_stacey", "00:30:17.20" ], [ "20080414_213000_bbctwo_newsnight", "00:50:33.36" ], [
"20080414_213500_bbcone_meet_the_immigrants", "00:29:59.40" ], [ "20080414_220000_bbcthree_pulling", "00:29:41.36" ], [
"20080414_220500_bbcone_inside_sport", "00:41:39.36" ], [ "20080414_222000_bbctwo_court_on_camera", "00:41:29.40" ], [
"20080414_230000_bbcfour_arena_saints", "01:11:11.20" ], [ "20080415_001500_bbcthree_gavin_and_stacey", "00:28:50.40" ], [
"20080415_002500_bbcone_weatherview", "00:07:05.24" ], [ "20080415_003000_bbcone_louis_theroux_s_african_hunting_holiday",
"00:59:22.32" ], [ "20080415_004000_bbcthree_pulling", "00:29:41.20" ], [ "20080415_011500_bbcfour_the_book_quiz", "00:30:41.56"
], [ "20080415_013000_bbcone_gardeners_world", "01:00:11.36" ], [ "20080415_023000_bbcone_blackpool_medics", "00:29:37.20" ], [
"20080415_024500_bbcfour_arena_saints", "01:08:33.40" ], [ "20080415_030000_bbcone_to_buy_or_not_to_buy", "00:28:51.36" ], [
"20080415_030000_bbctwo_history_file_20th_century_world", "01:59:33.80" ], [
"20080415_030500_bbcthree_young_mums_mansion_friends_and_family", "00:26:19.20" ], [ "20080415_050000_bbcone_breakfast",
"03:14:28.20" ], [ "20080415_050000_bbctwo_tikkabilla", "00:30:51.20" ], [ "20080415_062500_bbctwo_newsround", "00:05:16.20" ], [
"20080415_063000_bbctwo_hider_in_the_house", "01:02:14.08" ], [ "20080415_073000_bbctwo_jackanory_junior", "00:14:42.36" ], [
"20080415_080500_bbctwo_boogie_beebies", "00:15:12.24" ], [ "20080415_081500_bbcone_to_buy_or_not_to_buy", "00:44:51.16" ], [
"20080415_083500_bbctwo_something_special", "00:21:24.20" ], [ "20080415_093000_bbctwo_schools_words_and_pictures", "00:09:18.32"
], [ "20080415_094000_bbctwo_schools_words_and_pictures", "00:10:16.32" ], [ "20080415_101000_bbctwo_timewatch", "00:58:52.80" ],
[ "20080415_111000_bbctwo_coast", "00:20:51.20" ], [ "20080415_111500_bbcone_bargain_hunt", "00:44:27.32" ], [
"20080415_113000_bbctwo_working_lunch", "00:29:35.16" ], [ "20080415_120000_bbctwo_schools_the_maths_channel", "00:09:26.36" ], [
"20080415_121000_bbctwo_schools_primary_geography", "00:20:19.32" ], [ "20080415_123000_bbctwo_coast", "00:05:23.36" ], [
"20080415_124500_bbcone_doctors", "00:29:33.48" ], [ "20080415_140500_bbcone_space_pirates", "00:29:00.36" ], [
"20080415_143500_bbcone_small_talk_diaries", "00:15:08.28" ], [ "20080415_144500_bbctwo_flog_it", "00:44:47.28" ], [
"20080415_153500_bbcone_blue_peter", "00:24:56.28" ], [ "20080415_161500_bbcone_the_weakest_link", "00:45:13.20" ], [
"20080415_161500_bbctwo_escape_to_the_country", "00:45:13.40" ], [ "20080415_170000_bbcone_bbc_news", "00:28:38.16" ], [
"20080415_170000_bbctwo_eggheads", "00:29:46.28" ], [ "20080415_173000_bbcone_bbc_london_news", "00:24:44.32" ], [
"20080415_173000_bbctwo_great_british_menu", "00:29:42.40" ], [ "20080415_180000_bbcfour_world_news_today", "00:30:29.68" ], [
"20080415_180000_bbcone_the_one_show", "00:31:03.28" ], [ "20080415_180000_bbctwo_torchwood", "00:49:04.32" ], [
"20080415_183000_bbcfour_pop_goes_the_sixties", "00:05:21.04" ], [ "20080415_183000_bbcone_eastenders", "00:31:50.20" ], [
"20080415_183000_bbcthree_dog_borstal", "00:29:26.32" ], [ "20080415_185000_bbctwo_torchwood_declassified", "00:11:40.32" ], [
"20080415_190000_bbcfour_life_in_cold_blood", "00:59:43.60" ], [ "20080415_190000_bbcone_holby_city", "00:58:35.24" ], [
"20080415_190000_bbctwo_university_challenge_the_professionals", "00:29:41.36" ], [ "20080415_200000_bbcfour_chinese_school",
"01:00:28.60" ], [ "20080415_200000_bbcone_waking_the_dead", "01:00:20.32" ], [ "20080415_200000_bbcthree_young_mums_mansion",
"00:59:58.24" ], [ "20080415_200000_bbctwo_age_of_terror", "01:00:47.36" ], [ "20080415_210000_bbcfour_goodness_gracious_me",
"00:27:59.32" ], [ "20080415_210000_bbcone_bbc_ten_o_clock_news", "00:27:21.24" ], [ "20080415_210000_bbcthree_eastenders",
"00:29:59.40" ], [ "20080415_210000_bbctwo_later_live_with_jools_holland", "00:31:19.28" ], [
"20080415_213000_bbcthree_little_britain_abroad", "00:31:04.20" ], [ "20080415_213000_bbctwo_newsnight", "00:49:19.44" ], [
"20080415_220000_bbcthree_the_wall", "00:43:29.40" ], [ "20080415_232500_bbcthree_young_mums_mansion", "00:57:37.32" ], [
"20080415_234500_bbcone_weatherview", "00:05:42.32" ], [ "20080415_235000_bbcfour_chinese_school", "00:59:58.60" ], [
"20080416_002000_bbcone_blackpool_medics", "00:30:33.40" ], [ "20080416_005000_bbcfour_proms_on_four", "01:31:51.52" ], [
"20080416_005000_bbcone_tiger_spy_in_the_jungle", "00:59:35.28" ], [ "20080416_012000_bbcthree_dog_borstal", "00:29:44.20" ], [
"20080416_015000_bbcthree_dog_borstal", "00:29:25.44" ], [ "20080416_022500_bbcfour_chinese_school", "00:59:48.28" ], [
"20080416_030000_bbctwo_key_stage_three_bitesize", "01:44:20.36" ], [ "20080416_044500_bbctwo_talk_german", "00:15:24.40" ], [
"20080416_062500_bbctwo_newsround", "00:05:06.04" ], [ "20080416_063000_bbctwo_hider_in_the_house", "01:01:54.20" ], [
"20080416_073000_bbctwo_jackanory_junior", "00:14:47.24" ], [ "20080416_080500_bbctwo_boogie_beebies", "00:15:23.28" ], [
"20080416_081500_bbcone_to_buy_or_not_to_buy", "00:44:38.32" ], [ "20080416_083500_bbctwo_something_special", "00:21:23.24" ], [
"20080416_100000_bbcone_open_house", "00:29:10.36" ], [ "20080416_111500_bbcone_bargain_hunt", "00:59:58.36" ], [
"20080416_113000_bbctwo_open_gardens", "00:00:44.24" ], [ "20080416_120000_bbcone_bbc_news", "00:15:21.32" ], [
"20080416_120000_bbctwo_see_hear", "00:20:34.20" ], [ "20080416_123000_bbctwo_working_lunch", "00:31:08.20" ], [
"20080416_124500_bbcone_doctors", "00:29:14.28" ], [ "20080416_140500_bbcone_space_pirates", "00:28:35.24" ], [
"20080416_143500_bbcone_small_talk_diaries", "00:15:18.24" ], [ "20080416_144500_bbctwo_flog_it", "00:44:35.16" ], [
"20080416_151000_bbcone_young_dracula", "00:28:58.20" ], [ "20080416_153000_bbctwo_ready_steady_cook", "00:45:00.36" ], [
"20080416_153500_bbcone_blue_peter", "00:24:47.40" ], [ "20080416_160000_bbcone_newsround", "00:10:35.28" ], [
"20080416_161500_bbcone_the_weakest_link", "00:45:39.32" ], [ "20080416_170000_bbcone_bbc_news", "00:27:56.16" ], [
"20080416_170000_bbctwo_eggheads", "00:30:17.36" ], [ "20080416_173000_bbcone_bbc_london_news", "00:23:40.16" ], [
"20080416_180000_bbcfour_world_news_today", "00:30:32.32" ], [ "20080416_180000_bbcone_the_one_show", "00:30:03.48" ], [
"20080416_183000_bbcfour_pop_goes_the_sixties", "00:04:04.24" ], [ "20080416_185000_bbctwo_coast", "00:10:38.36" ], [
"20080416_190000_bbcfour_a_history_of_britain_by_simon_schama", "01:00:51.40" ], [ "20080416_190000_bbcone_traffic_cops",
"01:00:27.32" ], [ "20080416_190000_bbctwo_natural_world", "00:49:41.28" ], [ "20080416_195000_bbctwo_watching_the_wild",
"00:10:08.20" ], [ "20080416_200000_bbcfour_the_saint_and_the_hanged_man", "00:58:55.36" ], [
"20080416_200000_bbcone_the_apprentice", "01:00:04.28" ], [ "20080416_200000_bbctwo_dan_cruickshank_s_adventures", "01:00:47.28"
], [ "20080416_210000_bbcfour_fantabulosa_kenneth_williams", "01:20:56.32" ], [ "20080416_210000_bbcone_bbc_ten_o_clock_news",
"00:26:33.24" ], [ "20080416_210000_bbctwo_the_apprentice_you_re_fired", "00:32:05.16" ], [ "20080416_213000_bbctwo_newsnight",
"00:49:56.24" ], [ "20080416_222000_bbcfour_kenneth_williams_in_his_own_words", "00:29:27.40" ], [
"20080416_222000_bbctwo_desi_dna", "00:30:47.16" ], [ "20080416_231500_bbcthree_the_wall", "00:44:23.24" ], [
"20080416_235000_bbcfour_the_saint_and_the_hanged_man", "00:58:27.28" ], [ "20080417_001000_bbcone_weatherview", "00:05:38.28" ],
[ "20080417_001500_bbcone_blackpool_medics", "00:30:32.40" ], [ "20080417_005000_bbcfour_fantabulosa_kenneth_williams",
"01:20:21.40" ], [ "20080417_014500_bbcone_an_island_parish", "00:30:11.16" ], [
"20080417_021000_bbcfour_the_saint_and_the_hanged_man", "00:59:02.20" ], [ "20080417_021500_bbcone_to_buy_or_not_to_buy",
"00:29:10.32" ], [ "20080417_024000_bbcthree_the_wall", "00:43:45.32" ], [ "20080417_030000_bbctwo_key_stage_three_bitesize",
"02:00:04.20" ], [ "20080417_050000_bbcone_breakfast", "03:14:26.00" ], [ "20080417_050000_bbctwo_tikkabilla", "00:30:29.92" ], [
"20080417_063000_bbctwo_hider_in_the_house", "01:01:34.28" ], [ "20080417_073000_bbctwo_jackanory_junior", "00:14:42.20" ], [
"20080417_080500_bbctwo_boogie_beebies", "00:15:16.20" ], [ "20080417_081500_bbcone_to_buy_or_not_to_buy", "00:44:35.20" ], [
"20080417_083500_bbctwo_something_special", "00:21:16.36" ], [ "20080417_093000_bbctwo_schools_primary_history", "00:19:28.28" ],
[ "20080417_095000_bbctwo_schools_megamaths", "00:21:19.32" ], [ "20080417_101000_bbctwo_schools_landmarks", "00:19:16.16" ], [
"20080417_103000_bbctwo_schools_science_clips", "00:09:23.36" ], [ "20080417_104000_bbctwo_schools_science_clips", "00:10:37.20"
], [ "20080417_105000_bbctwo_schools_hands_up", "00:10:03.28" ], [ "20080417_110000_bbctwo_open_gardens", "00:29:55.92" ], [
"20080417_111500_bbcone_bargain_hunt", "00:44:21.00" ], [ "20080417_113000_bbctwo_working_lunch", "00:30:13.32" ], [
"20080417_120000_bbcone_bbc_news", "00:31:51.24" ], [ "20080417_120000_bbctwo_coast", "00:06:18.24" ], [
"20080417_124500_bbcone_doctors", "00:30:11.40" ], [ "20080417_140500_bbcone_space_pirates", "00:28:55.24" ], [
"20080417_143500_bbcone_small_talk_diaries", "00:15:27.36" ], [ "20080417_144500_bbctwo_flog_it", "00:45:00.32" ], [
"20080417_150500_bbcone_stake_out", "00:28:21.28" ], [ "20080417_153000_bbctwo_ready_steady_cook", "00:44:16.20" ], [
"20080417_153500_bbcone_beat_the_boss", "00:28:29.32" ], [ "20080417_160000_bbcone_newsround", "00:10:34.32" ], [
"20080417_161500_bbcone_the_weakest_link", "00:45:51.32" ], [ "20080417_170000_bbcone_bbc_news", "00:28:27.24" ], [
"20080417_170000_bbctwo_eggheads", "00:29:58.32" ], [ "20080417_173000_bbcone_bbc_london_news", "00:24:44.20" ], [
"20080417_173000_bbctwo_great_british_menu", "00:31:01.32" ], [ "20080417_180000_bbcfour_world_news_today", "00:30:56.32" ], [
"20080417_180000_bbcone_the_one_show", "00:30:38.16" ], [ "20080417_180000_bbctwo_delia", "00:28:22.24" ], [
"20080417_183000_bbcfour_in_search_of_medieval_britain", "00:29:45.28" ], [ "20080417_183000_bbcone_eastenders", "00:31:13.44" ],
[ "20080417_190000_bbcone_holby_blue", "00:59:48.28" ], [ "20080417_190000_bbcthree_dawn_gets_naked", "00:58:45.36" ], [
"20080417_190000_bbctwo_coast", "00:58:57.28" ], [ "20080417_200000_bbcfour_inside_the_medieval_mind", "00:58:59.56" ], [
"20080417_204500_bbctwo_heroes_return", "00:17:00.28" ], [ "20080417_210000_bbcfour_crusades", "00:49:51.28" ], [
"20080417_210000_bbcone_bbc_ten_o_clock_news", "00:25:50.28" ], [ "20080417_210000_bbcthree_eastenders", "00:29:05.24" ], [
"20080417_213000_bbcthree_sex_with_mum_and_dad", "00:58:03.12" ], [ "20080417_213000_bbctwo_newsnight", "00:51:22.32" ], [
"20080417_222000_bbctwo_ideal", "00:29:20.20" ], [ "20080417_223500_bbcfour_chinese_school", "01:00:16.48" ], [
"20080417_232000_bbcone_holiday_weather", "00:05:20.20" ], [ "20080417_232500_bbcone_panorama", "00:29:45.32" ], [
"20080417_233500_bbcfour_in_search_of_medieval_britain", "00:30:08.32" ], [ "20080417_235500_bbcone_the_undercover_diplomat",
"00:59:36.28" ], [ "20080418_000500_bbcfour_inside_the_medieval_mind", "00:59:59.56" ], [
"20080418_004000_bbcthree_dawn_gets_naked", "00:57:32.40" ], [ "20080418_005500_bbcone_johnny_s_new_kingdom", "00:30:04.36" ], [
"20080418_010500_bbcfour_the_book_quiz", "00:30:14.56" ], [ "20080418_012500_bbcone_the_twenties_in_colour_the_wonderful",
"00:30:02.40" ], [ "20080418_013500_bbcfour_in_search_of_medieval_britain", "00:30:06.44" ], [
"20080418_013500_bbcthree_pulling", "00:28:56.16" ], [ "20080418_015500_bbcone_bill_oddie_s_wild_side", "00:29:48.48" ], [
"20080418_020500_bbcfour_inside_the_medieval_mind", "00:59:14.32" ], [ "20080418_020500_bbcthree_sex_with_mum_and_dad",
"00:57:12.28" ], [ "20080418_022500_bbcone_to_buy_or_not_to_buy", "00:29:18.96" ], [ "20080418_030000_bbctwo_gcse_bitesize",
"01:59:54.48" ], [ "20080418_050000_bbcone_breakfast", "03:15:01.28" ], [ "20080418_050000_bbctwo_tikkabilla", "00:31:02.24" ], [
"20080418_073000_bbctwo_jackanory_junior", "00:14:50.16" ], [ "20080418_080500_bbctwo_boogie_beebies", "00:15:21.36" ], [
"20080418_081500_bbcone_to_buy_or_not_to_buy", "00:44:53.16" ], [ "20080418_083500_bbctwo_something_special", "00:20:50.24" ], [
"20080418_094500_bbctwo_schools_the_way_things_work", "00:15:12.24" ], [ "20080418_100000_bbcone_open_house", "00:28:35.28" ], [
"20080418_100000_bbctwo_schools_the_way_things_work", "00:15:26.28" ], [ "20080418_103000_bbctwo_schools_watch", "00:14:38.24" ],
[ "20080418_104500_bbctwo_schools_something_special", "00:14:56.24" ], [ "20080418_110000_bbctwo_open_gardens", "00:30:35.20" ],
[ "20080418_111500_bbcone_bargain_hunt", "00:44:08.44" ], [ "20080418_113000_bbctwo_working_lunch", "01:00:22.44" ], [
"20080418_120000_bbcone_bbc_news", "00:31:31.32" ], [ "20080418_124500_bbcone_doctors", "00:29:23.16" ], [
"20080418_140500_bbcone_space_pirates", "00:28:55.20" ], [ "20080418_143500_bbcone_small_talk_diaries", "00:15:10.32" ], [
"20080418_144500_bbctwo_flog_it", "00:44:52.28" ], [ "20080418_153000_bbctwo_ready_steady_cook", "00:45:12.36" ], [
"20080418_153500_bbcone_the_slammer", "00:28:19.28" ], [ "20080418_160000_bbcone_newsround", "00:10:38.32" ], [
"20080418_161500_bbcone_the_weakest_link", "00:45:31.24" ], [ "20080418_170000_bbcone_bbc_news", "00:28:23.28" ], [
"20080418_170000_bbctwo_eggheads", "00:29:48.32" ], [ "20080418_173000_bbcone_bbc_london_news", "00:25:07.20" ], [
"20080418_180000_bbcfour_world_news_today", "00:30:12.28" ], [ "20080418_180000_bbcone_the_one_show", "00:29:39.36" ], [
"20080418_180000_bbcthree_top_gear", "01:00:12.24" ], [ "20080418_183000_bbcfour_chopin_etudes", "00:07:05.44" ], [
"20080418_183000_bbcone_inside_out", "00:32:11.28" ], [ "20080418_183000_bbctwo_delia", "00:29:50.28" ], [
"20080418_183500_bbcfour_the_creation_by_haydn_at_the_barbican", "01:53:09.24" ], [ "20080418_190000_bbcone_eastenders",
"00:30:05.36" ], [ "20080418_190000_bbctwo_gardeners_world", "01:00:39.12" ], [ "20080418_193000_bbcone_a_question_of_sport",
"00:30:20.40" ], [ "20080418_200000_bbcthree_doctor_who", "00:49:29.32" ], [
"20080418_200000_bbctwo_the_apprentice_motor_mouths", "00:58:11.20" ], [
"20080418_203000_bbcfour_amazing_journey_the_story_of_the_who", "01:59:59.44" ], [
"20080418_205000_bbcthree_doctor_who_confidential", "00:11:50.32" ], [ "20080418_210000_bbcone_bbc_ten_o_clock_news",
"00:27:03.36" ], [ "20080418_210000_bbcthree_eastenders", "00:30:10.40" ], [ "20080418_213000_bbctwo_newsnight", "00:31:46.32" ],
[ "20080418_220000_bbctwo_newsnight_review", "00:32:41.24" ], [ "20080418_223500_bbctwo_later_with_jools_holland", "01:01:50.20"
], [ "20080418_231500_bbcthree_gavin_and_stacey", "00:28:51.20" ], [ "20080418_232000_bbcfour_the_who_at_the_electric_proms",
"00:49:55.36" ], [ "20080419_001000_bbcfour_pop_britannia", "00:59:09.28" ], [ "20080419_001000_bbcone_weatherview",
"00:05:20.28" ], [ "20080419_001500_bbcone_dan_cruickshank_s_adventures", "01:00:43.28" ], [
"20080419_011000_bbcfour_the_creation_by_haydn_at_the_barbican", "01:54:17.08" ], [ "20080419_011500_bbcone_natural_world",
"00:49:56.24" ], [ "20080419_014000_bbcthree_gavin_and_stacey", "00:28:55.36" ], [ "20080419_020500_bbcone_to_buy_or_not_to_buy",
"00:45:07.32" ], [ "20080419_021000_bbcthree_eastenders_ricky_and_bianca", "00:58:25.20" ], [ "20080419_050000_bbcone_breakfast",
"03:59:27.36" ], [ "20080419_050000_bbctwo_fimbles", "00:19:46.36" ], [ "20080419_052000_bbctwo_tikkabilla", "00:33:15.24" ], [
"20080419_070000_bbctwo_sorcerer_s_apprentice", "00:30:07.20" ], [ "20080419_090000_bbctwo_hedz", "00:29:12.32" ], [
"20080419_093000_bbctwo_the_slammer", "00:28:09.32" ], [ "20080419_104500_bbctwo_sportsround", "00:15:40.28" ], [
"20080419_110000_bbcone_bbc_news", "00:09:38.68" ], [ "20080419_114500_bbctwo_the_surgery", "00:20:14.16" ], [
"20080419_120000_bbcone_snooker_world_championship", "03:29:58.40" ], [ "20080419_120500_bbctwo_sound", "00:29:41.32" ], [
"20080419_153000_bbcone_final_score", "00:50:06.32" ], [ "20080419_153000_bbctwo_snooker_world_championship", "00:44:55.28" ], [
"20080419_161500_bbctwo_rugby_league_challenge_cup", "02:15:09.36" ], [ "20080419_162000_bbcone_bbc_news", "00:10:24.24" ], [
"20080419_163000_bbcone_bbc_london_news", "00:08:05.28" ], [ "20080419_164000_bbcone_the_kids_are_all_right", "00:40:36.28" ], [
"20080419_172000_bbcone_doctor_who", "00:44:44.44" ], [ "20080419_180000_bbcthree_gaffes_galore_outtakes", "00:05:54.28" ], [
"20080419_180500_bbcone_i_d_do_anything", "01:01:30.28" ], [ "20080419_180500_bbcthree_doctor_who_confidential", "00:44:54.36" ],
[ "20080419_183000_bbcfour_in_search_of_medieval_britain", "00:29:44.32" ], [
"20080419_183000_bbctwo_snooker_world_championship", "01:05:35.16" ], [ "20080419_185000_bbcthree_top_gear", "01:02:51.40" ], [
"20080419_190500_bbcfour_the_book_quiz", "00:30:35.44" ], [ "20080419_193500_bbcfour_mrs_brown", "01:40:11.20" ], [
"20080419_193500_bbctwo_dad_s_army", "00:29:54.12" ], [ "20080419_195500_bbcone_casualty", "00:48:57.32" ], [
"20080419_200500_bbctwo_have_i_got_a_bit_more_news_for_you", "00:40:41.32" ], [ "20080419_204500_bbcone_love_soup", "00:30:26.32"
], [ "20080419_204500_bbctwo_comedy_map_of_britain", "01:00:05.28" ], [ "20080419_211500_bbcone_bbc_news", "00:17:44.28" ], [
"20080419_213000_bbcone_match_of_the_day", "01:20:31.36" ], [ "20080419_214500_bbctwo_the_apprentice", "01:00:31.24" ], [
"20080419_221000_bbcthree_gavin_and_stacey", "00:28:55.36" ], [ "20080419_225000_bbcfour_fantabulosa_kenneth_williams",
"01:20:39.64" ], [ "20080420_000500_bbctwo_snooker_world_championship_highlights", "00:50:25.16" ], [
"20080420_001000_bbcfour_the_saint_and_the_hanged_man", "00:59:16.36" ], [ "20080420_002000_bbcthree_gavin_and_stacey",
"00:29:28.20" ], [ "20080420_005000_bbcthree_the_wall", "00:43:52.32" ], [ "20080420_005500_bbctwo_snooker_extra", "01:40:23.24"
], [ "20080420_013500_bbcone_weatherview", "00:05:47.44" ], [ "20080420_021000_bbcfour_fantabulosa_kenneth_williams",
"01:18:44.80" ], [ "20080420_032000_bbcthree_gaffes_galore_outtakes", "00:05:46.44" ], [ "20080420_050000_bbcone_breakfast",
"01:36:36.36" ], [ "20080420_052000_bbctwo_tikkabilla", "00:33:32.36" ], [ "20080420_063500_bbcone_match_of_the_day",
"01:23:08.28" ], [ "20080420_070000_bbctwo_trapped", "00:29:23.20" ], [ "20080420_073000_bbctwo_raven_the_secret_temple",
"00:29:05.16" ], [ "20080420_080000_bbcone_the_andrew_marr_show", "00:59:41.24" ], [ "20080420_080000_bbctwo_hider_in_the_house",
"00:59:32.32" ], [ "20080420_100000_bbcone_countryfile", "00:56:27.32" ], [ "20080420_110000_bbcone_the_politics_show",
"01:00:30.68" ], [ "20080420_120000_bbcone_allo_allo", "00:25:25.24" ], [ "20080420_120000_bbctwo_snooker_world_championship",
"03:44:11.32" ], [ "20080420_131000_bbcone_eastenders", "01:54:38.36" ], [ "20080420_150500_bbcone_tiger_spy_in_the_jungle",
"01:00:08.44" ], [ "20080420_154500_bbctwo_premiership_rugby", "00:30:36.36" ], [ "20080420_160500_bbcone_lifeline",
"00:09:27.40" ], [ "20080420_161500_bbcone_points_of_view", "00:15:35.80" ], [
"20080420_161500_bbctwo_rugby_league_challenge_cup", "02:14:31.24" ], [ "20080420_163000_bbcone_songs_of_praise", "00:34:46.32"
], [ "20080420_170500_bbcone_bbc_news", "00:17:11.16" ], [ "20080420_172000_bbcone_bbc_london_news", "00:06:59.44" ], [
"20080420_173000_bbcone_seaside_rescue", "00:30:31.20" ], [ "20080420_180000_bbcfour_inside_the_medieval_mind", "01:00:50.20" ],
[ "20080420_180000_bbcthree_sound", "00:28:41.20" ], [ "20080420_183000_bbcone_i_d_do_anything_results", "00:30:37.20" ], [
"20080420_183000_bbctwo_snooker_world_championship", "00:30:30.32" ], [
"20080420_190000_bbcone_the_british_academy_television_awards", "02:01:08.20" ], [
"20080420_194500_bbcthree_doctor_who_confidential", "00:15:57.40" ], [ "20080420_200000_bbcfour_dear_television", "00:10:27.20"
], [ "20080420_200000_bbcthree_gavin_and_stacey", "00:29:56.40" ], [ "20080420_201000_bbcfour_washes_whiter", "00:51:34.24" ], [
"20080420_203000_bbcthree_pulling", "00:30:35.28" ], [ "20080420_210000_bbcone_bbc_ten_o_clock_news", "00:20:14.28" ], [
"20080420_210000_bbcthree_amy_my_body_for_bucks", "00:58:47.12" ], [ "20080420_214000_bbctwo_graham_norton_uncut", "00:44:41.16"
], [ "20080420_215000_bbcfour_amazing_journey_the_story_of_the_who", "02:00:29.12" ], [
"20080420_222500_bbctwo_last_man_standing", "00:56:43.36" ], [ "20080420_224500_bbcthree_gavin_and_stacey", "00:30:02.32" ], [
"20080420_231500_bbcthree_pulling", "00:31:05.28" ], [ "20080420_232000_bbctwo_snooker_world_championship_highlights",
"00:51:12.36" ], [ "20080420_234000_bbcone_weatherview", "00:05:47.28" ], [
"20080420_234500_bbcone_around_the_world_in_80_gardens", "01:00:23.40" ], [ "20080420_234500_bbcthree_amy_my_body_for_bucks",
"00:58:21.36" ], [ "20080420_235000_bbcfour_the_who_at_the_electric_prom", "00:50:10.36" ], [
"20080421_001000_bbctwo_snooker_extra", "01:50:07.32" ], [ "20080421_004500_bbcone_holby_city", "00:57:56.28" ], [
"20080421_014000_bbcfour_inside_the_medieval_mind", "00:59:00.28" ], [ "20080421_014500_bbcone_age_of_terror", "01:00:02.32" ], [
"20080421_021000_bbcthree_dog_borstal", "00:29:40.44" ], [ "20080421_024000_bbcthree_dog_borstal", "00:29:39.40" ], [
"20080421_024500_bbcone_watchdog", "00:30:17.28" ], [ "20080421_031000_bbcthree_doctor_who_confidential", "00:13:25.20" ], [
"20080421_031000_bbctwo_inside_sport", "00:39:34.24" ], [ "20080421_050000_bbcone_breakfast", "03:14:39.36" ], [
"20080421_050000_bbctwo_tikkabilla", "00:30:49.40" ], [ "20080421_062500_bbctwo_newsround", "00:05:26.36" ], [
"20080421_063000_bbctwo_hider_in_the_house", "01:02:13.32" ], [ "20080421_073000_bbctwo_jackanory_junior", "00:14:57.24" ], [
"20080421_080000_bbctwo_boogie_beebies", "00:15:25.20" ], [ "20080421_081500_bbcone_missing_live", "00:45:15.28" ], [
"20080421_084000_bbctwo_something_special", "00:21:07.32" ], [ "20080421_095000_bbctwo_schools_look_and_read", "00:21:21.28" ], [
"20080421_100000_bbcone_to_buy_or_not_to_buy", "00:44:31.32" ], [ "20080421_101000_bbctwo_schools_razzledazzle", "00:19:41.16" ],
[ "20080421_110000_bbctwo_the_daily_politics", "00:30:12.16" ], [ "20080421_111500_bbcone_bargain_hunt", "00:44:35.32" ], [
"20080421_113000_bbctwo_working_lunch", "00:30:20.24" ], [ "20080421_120000_bbcone_bbc_news", "00:31:19.12" ], [
"20080421_120000_bbctwo_schools_science_clips_investigates", "00:09:14.16" ], [
"20080421_121000_bbctwo_schools_science_clips_investigates", "00:09:25.32" ], [
"20080421_122000_bbctwo_schools_primary_geography", "00:10:37.36" ], [ "20080421_123000_bbctwo_snooker_world_championship",
"03:47:55.28" ], [ "20080421_124500_bbcone_doctors", "00:29:37.32" ], [ "20080421_140500_bbcone_space_pirates", "00:29:18.32" ],
[ "20080421_143500_bbcone_chucklevision", "00:15:08.40" ], [ "20080421_151000_bbcone_roar", "00:28:07.36" ], [
"20080421_153500_bbcone_grange_hill", "00:24:57.28" ], [ "20080421_160000_bbcone_newsround", "00:11:07.32" ], [
"20080421_161500_bbcone_the_weakest_link", "00:45:17.24" ], [ "20080421_170000_bbctwo_eggheads", "00:30:02.20" ], [
"20080421_173000_bbcone_bbc_london_news", "00:26:55.40" ], [ "20080421_180000_bbcfour_world_news_today", "00:30:38.44" ], [
"20080421_180000_bbcone_the_one_show", "00:30:11.24" ], [ "20080421_180000_bbcthree_dragons_den", "00:59:59.40" ], [
"20080421_180000_bbctwo_snooker_world_championship", "01:01:06.28" ], [ "20080421_183000_bbcfour_abroad_again_in_britain",
"00:58:59.44" ], [ "20080421_183000_bbcone_watchdog", "00:31:39.28" ], [ "20080421_190000_bbcone_eastenders", "00:29:59.16" ], [
"20080421_190000_bbctwo_university_challenge_the_professionals", "00:30:05.16" ], [ "20080421_193000_bbcfour_the_book_quiz",
"00:30:27.28" ], [ "20080421_193000_bbcone_panorama", "00:29:34.36" ], [ "20080421_193000_bbcthree_glamour_girls", "00:29:57.36"
], [ "20080421_193000_bbctwo_the_hairy_bikers_come_home", "00:29:08.20" ], [ "20080421_200000_bbcfour_how_to_build_a_cathedral",
"00:59:16.96" ], [ "20080421_200000_bbcone_waking_the_dead", "00:59:37.48" ], [
"20080421_200000_bbcthree_natalie_cassidy_s_diet_secrets", "00:59:15.00" ], [ "20080421_210000_bbcone_bbc_news_at_ten",
"00:26:23.28" ], [ "20080421_210000_bbcthree_eastenders", "00:29:51.20" ], [ "20080421_213000_bbcthree_gavin_and_stacey",
"00:30:55.32" ], [ "20080421_213000_bbctwo_newsnight", "00:49:07.16" ], [ "20080421_213500_bbcone_meet_the_immigrants",
"00:30:27.36" ], [ "20080421_220000_bbcthree_pulling", "00:29:25.36" ], [ "20080421_220500_bbcone_inside_sport", "00:41:05.32" ],
[ "20080421_222000_bbctwo_a_study_in_sherlock", "00:40:27.16" ], [ "20080421_225000_bbcfour_britain_s_best_buildings",
"00:49:37.12" ], [ "20080421_230000_bbctwo_snooker_world_championship_highlights", "00:51:10.36" ], [
"20080421_231500_bbcthree_glamour_girls", "00:29:13.28" ], [ "20080421_234000_bbcfour_how_to_build_a_cathedral", "00:58:54.28" ],
[ "20080421_234500_bbcthree_natalie_cassidy_s_diet_secrets", "00:58:13.28" ], [ "20080421_235000_bbctwo_snooker_extra",
"01:39:48.28" ], [ "20080422_004000_bbcfour_abroad_again_in_britain", "00:59:13.28" ], [
"20080422_004000_bbcthree_gavin_and_stacey", "00:29:57.24" ], [ "20080422_004500_bbcone_weatherview", "00:05:46.20" ], [
"20080422_005000_bbcone_clowns", "00:59:34.32" ], [ "20080422_011000_bbcthree_pulling", "00:29:27.20" ], [
"20080422_014000_bbcfour_the_book_quiz", "00:30:06.28" ], [ "20080422_021000_bbcfour_how_to_build_a_cathedral", "00:59:26.20" ],
[ "20080422_025000_bbcone_to_buy_or_not_to_buy", "00:44:55.24" ], [ "20080422_030000_bbctwo_gcse_bitesize", "01:59:40.72" ], [
"20080422_031000_bbcthree_dog_borstal", "00:30:27.36" ], [ "20080422_050000_bbcone_breakfast", "03:15:06.76" ], [
"20080422_050000_bbctwo_tikkabilla", "00:30:44.36" ], [ "20080422_062500_bbctwo_newsround", "00:05:43.24" ], [
"20080422_073000_bbctwo_jackanory_junior", "00:14:51.36" ], [ "20080422_080000_bbctwo_boogie_beebies", "00:15:21.24" ], [
"20080422_081500_bbcone_missing", "00:44:36.20" ], [ "20080422_084000_bbctwo_something_special", "00:21:02.20" ], [
"20080422_090000_bbcone_homes_under_the_hammer", "01:00:36.28" ], [ "20080422_093000_bbctwo_schools_fun_with_phonics",
"00:09:20.40" ], [ "20080422_094000_bbctwo_schools_fun_with_phonics", "00:10:11.28" ], [
"20080422_100000_bbcone_to_buy_or_not_to_buy", "00:44:30.32" ], [ "20080422_101000_bbctwo_timewatch", "00:49:52.40" ], [
"20080422_110000_bbctwo_the_daily_politics", "00:29:59.92" ], [ "20080422_111500_bbcone_bargain_hunt", "00:44:43.36" ], [
"20080422_113000_bbctwo_working_lunch", "00:30:05.28" ], [ "20080422_120000_bbcone_bbc_news_at_one", "00:31:53.28" ], [
"20080422_120000_bbctwo_schools_the_maths_channel", "00:09:25.24" ], [ "20080422_121000_bbctwo_schools_primary_geography",
"00:20:26.24" ], [ "20080422_123000_bbctwo_snooker_world_championship", "04:30:09.20" ], [ "20080422_124500_bbcone_doctors",
"00:29:37.36" ], [ "20080422_140500_bbcone_space_pirates", "00:29:22.40" ], [ "20080422_143500_bbcone_chucklevision",
"00:14:56.32" ], [ "20080422_153500_bbcone_blue_peter", "00:24:57.40" ], [ "20080422_160000_bbcone_newsround", "00:11:35.44" ], [
"20080422_161500_bbcone_the_weakest_link", "00:45:22.36" ], [ "20080422_170000_bbcone_bbc_news_at_six", "00:27:50.28" ], [
"20080422_170000_bbctwo_eggheads", "00:29:54.28" ], [ "20080422_173000_bbcone_bbc_london_news", "00:30:09.40" ], [
"20080422_180000_bbcfour_world_news_today", "00:30:29.44" ], [ "20080422_180000_bbctwo_snooker_world_championship", "01:00:23.36"
], [ "20080422_183000_bbcfour_pop_goes_the_sixties", "00:04:31.72" ], [ "20080422_183000_bbcone_eastenders", "00:31:01.32" ], [
"20080422_190000_bbcfour_life_in_cold_blood", "00:59:10.40" ], [ "20080422_190000_bbcone_holby_city", "00:59:55.16" ], [
"20080422_190000_bbctwo_university_challenge_the_professionals", "00:29:58.28" ], [
"20080422_193000_bbctwo_the_hairy_bikers_come_home", "00:30:58.04" ], [ "20080422_200000_bbcfour_chinese_school", "01:00:26.44"
], [ "20080422_200000_bbcone_waking_the_dead", "00:59:57.28" ], [ "20080422_200000_bbctwo_age_of_terror", "00:59:46.32" ], [
"20080422_210000_bbcfour_goodness_gracious_me", "00:29:28.24" ], [ "20080422_210000_bbcone_bbc_news_at_ten", "00:26:12.24" ], [
"20080422_210000_bbcthree_eastenders", "00:29:13.32" ], [ "20080422_210000_bbctwo_later_live_with_jools_holland", "00:31:37.20"
], [ "20080422_213000_bbcthree_little_britain", "00:30:26.28" ], [ "20080422_213000_bbctwo_newsnight", "00:50:07.20" ], [
"20080422_213500_bbcone_ex_forces_and_homeless", "00:40:16.40" ], [ "20080422_220000_bbcthree_the_wall", "00:43:38.40" ], [
"20080422_225000_bbcfour_chinese_school", "00:59:58.60" ], [ "20080422_231000_bbctwo_snooker_world_championship_highlights",
"00:50:34.36" ], [ "20080422_235000_bbcfour_proms_on_four", "01:54:42.28" ], [ "20080422_235000_bbcone_weatherview",
"00:05:13.24" ], [ "20080422_235500_bbcone_see_hear", "00:29:49.36" ], [ "20080423_000000_bbctwo_snooker_extra", "01:30:28.32" ],
[ "20080423_002500_bbcone_tiger_spy_in_the_jungle", "00:59:52.24" ], [ "20080423_003000_bbcthree_dawn_gets_naked", "00:56:43.32"
], [ "20080423_012500_bbcone_to_buy_or_not_to_buy", "00:45:12.32" ], [ "20080423_012500_bbcthree_natalie_cassidy_s_diet_secrets",
"00:58:37.40" ], [ "20080423_014500_bbcfour_chinese_school", "00:59:46.24" ], [ "20080423_021000_bbcone_to_buy_or_not_to_buy",
"00:46:53.32" ], [ "20080423_030000_bbctwo_gcse_bitesize_revision", "01:58:29.36" ], [ "20080423_050000_bbcone_breakfast",
"03:15:10.28" ], [ "20080423_050000_bbctwo_tikkabilla", "00:30:37.36" ], [ "20080423_062500_bbctwo_newsround", "00:05:46.36" ], [
"20080423_063000_bbctwo_hider_in_the_house", "01:01:26.28" ], [ "20080423_073000_bbctwo_jackanory_junior", "00:14:47.20" ], [
"20080423_080000_bbctwo_boogie_beebies", "00:15:22.32" ], [ "20080423_081500_bbcone_missing_live", "00:44:42.40" ], [
"20080423_084000_bbctwo_something_special", "00:20:58.16" ], [ "20080423_090000_bbcone_homes_under_the_hammer", "01:01:17.28" ],
[ "20080423_093000_bbctwo_what_the_ancients_did_for_us", "01:00:04.16" ], [ "20080423_100000_bbcone_to_buy_or_not_to_buy",
"00:44:14.44" ], [ "20080423_103000_bbctwo_the_daily_politics", "01:30:46.24" ], [ "20080423_104500_bbcone_cash_in_the_attic",
"00:29:55.28" ], [ "20080423_111500_bbcone_bargain_hunt", "00:44:49.32" ], [ "20080423_120000_bbcone_bbc_news_at_one",
"00:31:50.28" ], [ "20080423_120000_bbctwo_stand_by_your_man", "00:29:42.32" ], [ "20080423_123000_bbctwo_working_lunch",
"00:30:24.36" ], [ "20080423_124500_bbcone_doctors", "00:29:15.24" ], [ "20080423_130000_bbctwo_lifeline", "00:09:55.32" ], [
"20080423_131000_bbctwo_snooker_world_championship", "03:50:18.40" ], [ "20080423_140500_bbcone_space_pirates", "00:28:47.16" ],
[ "20080423_143500_bbcone_chucklevision", "00:14:50.16" ], [ "20080423_151000_bbcone_young_dracula", "00:28:46.20" ], [
"20080423_153500_bbcone_blue_peter", "00:24:47.32" ], [ "20080423_160000_bbcone_newsround", "00:11:05.32" ], [
"20080423_161500_bbcone_the_weakest_link", "00:45:38.20" ], [ "20080423_170000_bbcone_bbc_news_at_six", "00:27:51.24" ], [
"20080423_170000_bbctwo_eggheads", "00:30:19.24" ], [ "20080423_173000_bbcone_bbc_london_news", "00:29:42.28" ], [
"20080423_180000_bbcfour_world_news_today", "00:31:32.32" ], [ "20080423_180000_bbcone_the_one_show", "00:31:16.40" ], [
"20080423_180000_bbcthree_holby_city", "01:00:39.20" ], [ "20080423_180000_bbctwo_snooker_world_championship", "00:59:45.36" ], [
"20080423_183000_bbcfour_pop_goes_the_sixties", "00:05:06.88" ], [ "20080423_183000_bbcone_street_doctor", "00:31:18.24" ], [
"20080423_190000_bbcfour_a_history_of_britain_by_simon_schama", "00:58:26.32" ], [ "20080423_190000_bbcone_traffic_cops",
"00:59:43.28" ], [ "20080423_190000_bbcthree_glamour_girls", "00:29:04.32" ], [ "20080423_190000_bbctwo_natural_world",
"00:49:52.24" ], [ "20080423_195000_bbctwo_watching_the_wild", "00:10:02.40" ], [ "20080423_200000_bbcone_the_apprentice",
"01:00:08.32" ], [ "20080423_200000_bbcthree_amy_my_body_for_bucks", "00:59:04.24" ], [
"20080423_200000_bbctwo_dan_cruickshank_s_adventures", "01:00:25.28" ], [ "20080423_210000_bbcone_bbc_news_at_ten", "00:25:39.24"
], [ "20080423_210000_bbctwo_the_apprentice_you_re_fired", "00:32:02.24" ], [ "20080423_214000_bbcone_made_in_england",
"00:30:46.24" ], [ "20080423_221000_bbcone_aliyah_the_journey_home", "00:31:58.32" ], [ "20080423_222000_bbctwo_desi_dna",
"00:30:04.32" ], [ "20080423_225000_bbctwo_snooker_world_championship_highlights", "00:50:33.28" ], [
"20080423_225500_bbcfour_the_last_duel", "01:00:46.12" ], [ "20080423_232000_bbcthree_the_wall", "00:43:40.32" ], [
"20080424_003000_bbcone_weatherview", "00:05:06.40" ], [ "20080424_003500_bbcone_seaside_rescue", "00:30:06.36" ], [
"20080424_003500_bbcthree_glamour_girls", "00:29:09.32" ], [ "20080424_010500_bbcfour_how_to_build_a_cathedral", "00:59:34.72" ],
[ "20080424_010500_bbcone_meet_the_immigrants", "00:29:47.44" ], [ "20080424_010500_bbcthree_amy_my_body_for_bucks",
"00:57:39.40" ], [ "20080424_020500_bbcfour_the_last_duel", "00:59:19.48" ], [ "20080424_020500_bbcone_to_buy_or_not_to_buy",
"00:44:39.32" ], [ "20080424_023000_bbcthree_the_wall", "00:44:07.44" ], [ "20080424_030000_bbctwo_gcse_bitesize_revision",
"01:58:23.96" ], [ "20080424_031500_bbcthree_dog_borstal", "00:29:29.28" ], [ "20080424_050000_bbcone_breakfast", "03:14:59.44"
], [ "20080424_050000_bbctwo_tikkabilla", "00:30:39.36" ], [ "20080424_062500_bbctwo_newsround", "00:05:35.24" ], [
"20080424_063000_bbctwo_hider_in_the_house", "01:02:08.24" ], [ "20080424_073000_bbctwo_jackanory_junior", "00:14:54.24" ], [
"20080424_080000_bbctwo_boogie_beebies", "00:15:18.20" ], [ "20080424_084000_bbctwo_something_special", "00:14:44.36" ], [
"20080424_093000_bbctwo_schools_primary_history", "00:19:24.32" ], [ "20080424_095000_bbctwo_schools_megamaths", "00:19:02.24" ],
[ "20080424_100000_bbcone_to_buy_or_not_to_buy", "00:44:29.16" ], [ "20080424_100500_bbctwo_schools_last_chance_to_see",
"00:02:11.36" ], [ "20080424_101000_bbctwo_schools_primary_geography", "00:19:33.20" ], [
"20080424_103000_bbctwo_schools_key_stage_1_science_clips", "00:09:19.24" ], [
"20080424_104000_bbctwo_schools_key_stage_1_science_clips", "00:10:23.16" ], [ "20080424_104500_bbcone_cash_in_the_attic",
"00:30:07.20" ], [ "20080424_105000_bbctwo_schools_hands_up", "00:10:16.28" ], [ "20080424_110000_bbctwo_the_daily_politics",
"00:30:43.20" ], [ "20080424_111500_bbcone_bargain_hunt", "00:44:46.44" ], [ "20080424_113000_bbctwo_working_lunch",
"00:29:55.32" ], [ "20080424_120000_bbcone_bbc_news_at_one", "00:31:08.28" ], [ "20080424_120000_bbctwo_open_gardens",
"00:30:05.32" ], [ "20080424_123000_bbctwo_snooker_world_championship", "04:30:07.28" ], [ "20080424_124500_bbcone_doctors",
"00:28:56.32" ], [ "20080424_140500_bbcone_space_pirates", "00:28:31.32" ], [ "20080424_143500_bbcone_chucklevision",
"00:15:29.32" ], [ "20080424_150500_bbcone_stake_out", "00:28:56.24" ], [ "20080424_153500_bbcone_beat_the_boss", "00:28:14.20"
], [ "20080424_160000_bbcone_newsround", "00:10:35.44" ], [ "20080424_161500_bbcone_the_weakest_link", "00:45:33.28" ], [
"20080424_170000_bbcone_bbc_news_at_six", "00:29:07.32" ], [ "20080424_170000_bbctwo_eggheads", "00:29:52.28" ], [
"20080424_173000_bbcone_bbc_london_news", "00:29:07.28" ], [ "20080424_180000_bbcfour_world_news_today", "00:30:28.24" ], [
"20080424_180000_bbcone_the_one_show", "00:30:19.60" ], [ "20080424_180000_bbctwo_snooker_world_championship", "01:01:04.16" ], [
"20080424_183000_bbcone_eastenders", "00:32:06.24" ], [ "20080424_190000_bbcfour_the_voices_of_morebath", "00:09:49.44" ], [
"20080424_190000_bbcone_holby_blue", "00:59:19.84" ], [ "20080424_190000_bbctwo_top_gear", "00:59:10.56" ], [
"20080424_191000_bbcfour_a_history_of_british_art_dreams_and", "00:50:25.24" ], [
"20080424_200000_bbcfour_inside_the_medieval_mind", "00:59:12.64" ], [ "20080424_200000_bbcthree_child_stars", "01:00:30.40" ], [
"20080424_205000_bbctwo_heroes_unmasked", "00:09:45.24" ], [ "20080424_210000_bbcfour_crusades", "00:49:35.36" ], [
"20080424_210000_bbcone_bbc_news_at_ten", "00:26:32.40" ], [ "20080424_210000_bbcthree_eastenders", "00:29:35.32" ], [
"20080424_213000_bbctwo_newsnight", "00:47:49.28" ], [ "20080424_222000_bbctwo_ideal", "00:29:22.28" ], [
"20080424_223500_bbcfour_chinese_school", "01:00:06.20" ], [ "20080424_224500_bbcone_this_week", "00:44:37.20" ], [
"20080424_225000_bbctwo_snooker_world_championship_highlights", "00:50:11.24" ], [ "20080424_230000_bbcthree_pulling",
"00:29:51.36" ], [ "20080424_233000_bbcone_weatherview", "00:06:58.44" ], [ "20080424_233000_bbcthree_child_stars", "00:58:26.40"
], [ "20080424_233500_bbcfour_in_search_of_medieval_britain", "00:29:54.28" ], [ "20080424_233500_bbcone_panorama", "00:30:01.20"
], [ "20080424_234000_bbctwo_snooker_extra", "01:50:09.16" ], [ "20080425_000500_bbcfour_inside_the_medieval_mind", "00:59:04.24"
], [ "20080425_000500_bbcone_johnny_s_new_kingdom", "00:30:00.24" ], [
"20080425_003500_bbcone_the_twenties_in_colour_the_wonderful", "00:29:43.20" ], [ "20080425_010500_bbcfour_the_book_quiz",
"00:30:07.60" ], [ "20080425_010500_bbcone_bill_oddie_s_wild_side", "00:29:40.36" ], [
"20080425_013500_bbcfour_in_search_of_medieval_britain", "00:29:47.20" ], [ "20080425_013500_bbcone_to_buy_or_not_to_buy",
"00:44:48.16" ], [ "20080425_015500_bbcthree_pulling", "00:29:28.20" ], [ "20080425_020500_bbcfour_inside_the_medieval_mind",
"01:00:12.16" ], [ "20080425_030000_bbctwo_gcse_bitesize_revision", "01:58:44.84" ], [ "20080425_050000_bbcone_breakfast",
"03:14:57.16" ], [ "20080425_050000_bbctwo_tikkabilla", "00:30:10.40" ], [ "20080425_062500_bbctwo_newsround", "00:05:18.24" ], [
"20080425_073000_bbctwo_jackanory_junior", "00:14:53.36" ], [ "20080425_080000_bbctwo_boogie_beebies", "00:15:18.20" ], [
"20080425_081500_bbcone_missing_live", "00:45:22.32" ], [ "20080425_084000_bbctwo_something_special", "00:20:59.32" ], [
"20080425_090000_bbcone_homes_under_the_hammer", "01:00:44.24" ], [ "20080425_100000_bbcone_to_buy_or_not_to_buy", "00:44:20.20"
], [ "20080425_100000_bbctwo_schools_the_way_things_work", "00:13:23.40" ], [ "20080425_103000_bbctwo_schools_watch",
"00:14:43.40" ], [ "20080425_104500_bbcone_cash_in_the_attic", "00:29:53.44" ], [
"20080425_104500_bbctwo_schools_something_special", "00:15:31.24" ], [ "20080425_110000_bbctwo_the_daily_politics", "00:30:47.88"
], [ "20080425_111500_bbcone_bargain_hunt", "00:44:30.40" ], [ "20080425_113000_bbctwo_working_lunch", "01:00:03.32" ], [
"20080425_123000_bbctwo_snooker_world_championship", "04:29:53.28" ], [ "20080425_124500_bbcone_doctors", "00:29:10.20" ], [
"20080425_140500_bbcone_space_pirates", "00:28:44.12" ], [ "20080425_143500_bbcone_chucklevision", "00:15:05.32" ], [
"20080425_153500_bbcone_the_slammer", "00:28:34.32" ], [ "20080425_160000_bbcone_newsround", "00:10:37.28" ], [
"20080425_161500_bbcone_the_weakest_link", "00:45:31.36" ], [ "20080425_170000_bbcone_bbc_news_at_six", "00:28:15.36" ], [
"20080425_170000_bbctwo_eggheads", "00:29:54.40" ], [ "20080425_173000_bbcone_bbc_london_news", "00:25:16.28" ], [
"20080425_180000_bbcfour_world_news_today", "00:33:06.52" ], [ "20080425_180000_bbcone_the_one_show", "00:30:32.24" ], [
"20080425_180000_bbcthree_top_gear", "01:01:31.36" ], [ "20080425_180000_bbctwo_snooker_world_championship", "01:00:00.28" ], [
"20080425_183000_bbcfour_swan_lake", "01:26:15.20" ], [ "20080425_190000_bbcone_eastenders", "00:30:21.40" ], [
"20080425_190000_bbcthree_almost_famous", "00:57:28.44" ], [ "20080425_190000_bbctwo_gardeners_world", "00:59:49.36" ], [
"20080425_193000_bbcone_a_question_of_sport", "00:30:02.12" ], [ "20080425_200000_bbcfour_soul_britannia", "01:00:22.60" ], [
"20080425_200000_bbcthree_doctor_who", "00:44:57.28" ], [ "20080425_210000_bbcfour_bbc_four_sessions_van_morrison", "01:00:26.56"
], [ "20080425_210000_bbcone_bbc_news_at_ten", "00:25:21.24" ], [ "20080425_210000_bbcthree_eastenders", "00:30:06.20" ], [
"20080425_213000_bbctwo_newsnight", "00:32:12.40" ], [ "20080425_220000_bbcfour_van_morrison_on_later", "00:40:14.32" ], [
"20080425_220000_bbctwo_newsnight_review", "00:31:28.28" ], [ "20080425_223500_bbctwo_later_with_jools_holland", "01:03:13.28" ],
[ "20080425_230500_bbcthree_gavin_and_stacey", "00:29:39.96" ], [ "20080425_233500_bbcthree_almost_famous", "00:56:59.24" ], [
"20080425_233500_bbctwo_snooker_world_championship_highlights", "00:49:14.36" ], [
"20080426_001000_bbcfour_bbc_four_sessions_van_morrison", "01:01:11.32" ], [ "20080426_002000_bbcone_weatherview", "00:04:36.20"
], [ "20080426_002500_bbcone_dan_cruickshank_s_adventures", "01:00:14.32" ], [ "20080426_002500_bbctwo_snooker_extra",
"01:36:21.24" ], [ "20080426_003500_bbcthree_child_stars", "00:57:47.32" ], [ "20080426_011000_bbcfour_soul_britannia",
"01:00:15.32" ], [ "20080426_012500_bbcone_natural_world", "00:49:57.24" ], [ "20080426_021000_bbcfour_van_morrison_on_later",
"00:28:17.32" ], [ "20080426_023000_bbcthree_gavin_and_stacey", "00:29:43.20" ], [ "20080426_052000_bbctwo_tikkabilla",
"00:33:32.24" ], [ "20080426_070000_bbctwo_sorcerer_s_apprentice", "00:33:48.24" ], [ "20080426_090000_bbcone_saturday_kitchen",
"01:30:05.12" ], [ "20080426_090000_bbctwo_hedz", "00:31:08.28" ], [ "20080426_093000_bbctwo_the_slammer", "00:30:11.24" ], [
"20080426_104500_bbctwo_sportsround", "00:15:09.24" ], [ "20080426_110000_bbcone_bbc_news", "00:10:14.16" ], [
"20080426_111000_bbcone_football_focus", "00:50:01.32" ], [ "20080426_120000_bbcone_snooker_world_championship", "03:29:47.28" ],
[ "20080426_123000_bbctwo_the_surgery", "00:19:50.20" ], [ "20080426_125000_bbctwo_sound", "00:32:52.28" ], [
"20080426_153000_bbcone_final_score", "00:49:43.32" ], [ "20080426_154500_bbctwo_snooker_world_championship", "01:46:34.20" ], [
"20080426_162000_bbcone_bbc_news", "00:10:26.24" ], [ "20080426_163000_bbcone_bbc_london_news", "00:08:30.32" ], [
"20080426_164000_bbcone_the_kids_are_all_right", "00:40:18.36" ], [ "20080426_172000_bbcone_doctor_who", "00:45:45.32" ], [
"20080426_173000_bbctwo_dad_s_army", "00:29:51.36" ], [ "20080426_180000_bbcthree_two_pints_of_lager_outtakes", "00:04:57.28" ],
[ "20080426_180000_bbctwo_snooker_world_championship", "02:04:41.28" ], [ "20080426_180500_bbcone_i_d_do_anything", "01:01:35.20"
], [ "20080426_180500_bbcthree_doctor_who_confidential", "00:45:10.24" ], [
"20080426_181000_bbcfour_in_search_of_medieval_britain", "00:30:02.68" ], [ "20080426_184000_bbcfour_the_book_quiz",
"00:30:20.20" ], [ "20080426_185000_bbcthree_top_gear", "01:04:34.40" ], [ "20080426_191000_bbcfour_a_perfect_spy", "00:56:02.32"
], [ "20080426_195500_bbcone_casualty", "00:46:54.40" ], [ "20080426_200500_bbcfour_a_perfect_spy", "00:55:05.56" ], [
"20080426_200500_bbctwo_have_i_got_a_bit_more_news_for_you", "00:40:36.20" ], [ "20080426_204500_bbcone_love_soup", "00:30:35.28"
], [ "20080426_204500_bbctwo_comedy_map_of_britain", "00:59:37.40" ], [ "20080426_211500_bbcone_bbc_news", "00:19:18.36" ], [
"20080426_213000_bbcone_match_of_the_day", "01:19:32.32" ], [ "20080426_214500_bbctwo_the_apprentice", "01:00:44.48" ], [
"20080426_220000_bbcthree_gavin_and_stacey", "00:30:06.24" ], [ "20080426_224500_bbctwo_last_man_standing", "00:56:57.20" ], [
"20080426_231500_bbcthree_glamour_girls", "00:31:04.32" ], [ "20080426_234000_bbctwo_snooker_world_championship_highlights",
"00:49:34.24" ], [ "20080426_234500_bbcthree_child_stars", "00:59:00.36" ], [
"20080427_001500_bbcfour_selling_power_the_admen_and_no_10", "00:59:29.20" ], [ "20080427_003000_bbctwo_snooker_extra",
"01:35:03.36" ], [ "20080427_004500_bbcthree_gavin_and_stacey", "00:30:19.24" ], [
"20080427_011500_bbcfour_in_search_of_medieval_britain", "00:29:36.52" ], [ "20080427_011500_bbcone_weatherview", "00:06:00.16"
], [ "20080427_011500_bbcthree_the_wall", "00:44:44.28" ], [ "20080427_014500_bbcfour_the_book_quiz", "00:29:43.24" ], [
"20080427_020000_bbcthree_doctor_who_confidential", "00:44:58.20" ], [
"20080427_021500_bbcfour_selling_power_the_admen_and_no_10", "01:01:06.16" ], [ "20080427_024500_bbcthree_child_stars",
"00:59:10.36" ], [ "20080427_050000_bbcone_breakfast", "01:36:32.40" ], [ "20080427_052000_bbctwo_tikkabilla", "00:41:57.32" ], [
"20080427_063500_bbcone_match_of_the_day", "01:23:08.24" ], [ "20080427_070000_bbctwo_trapped", "00:29:42.24" ], [
"20080427_073000_bbctwo_raven_the_secret_temple", "00:28:59.32" ], [ "20080427_080000_bbcone_the_andrew_marr_show", "00:59:55.36"
], [ "20080427_080000_bbctwo_hider_in_the_house", "00:59:30.36" ], [ "20080427_090000_bbcone_sunday_life", "00:59:57.36" ], [
"20080427_100000_bbcone_countryfile", "01:00:19.08" ], [ "20080427_110000_bbcone_the_politics_show", "00:59:08.56" ], [
"20080427_110000_bbctwo_boxing", "01:00:03.28" ], [ "20080427_120000_bbcone_eastenders", "01:55:12.36" ], [
"20080427_120000_bbctwo_snooker_world_championship", "04:29:55.20" ], [ "20080427_154500_bbcone_keeping_up_appearances",
"00:30:22.28" ], [ "20080427_161500_bbcone_points_of_view", "00:15:18.28" ], [ "20080427_163000_bbcone_songs_of_praise",
"00:35:01.08" ], [ "20080427_163000_bbctwo_lemurs_of_madagascar", "00:40:12.20" ], [ "20080427_170500_bbcone_bbc_news",
"00:17:57.36" ], [ "20080427_171000_bbctwo_natural_world", "00:49:45.40" ], [ "20080427_172000_bbcone_bbc_london_news",
"00:08:41.16" ], [ "20080427_173000_bbcone_seaside_rescue", "00:30:15.36" ], [
"20080427_180000_bbcfour_inside_the_medieval_mind", "01:00:00.28" ], [ "20080427_180000_bbcthree_sound", "00:30:14.32" ], [
"20080427_180000_bbctwo_snooker_world_championship", "02:00:18.36" ], [ "20080427_183000_bbcone_i_d_do_anything_results",
"00:30:35.40" ], [ "20080427_190000_bbcfour_how_to_build_a_cathedral", "00:59:36.28" ], [
"20080427_190000_bbcone_miss_austen_regrets", "01:26:10.44" ], [ "20080427_190000_bbcthree_doctor_who", "00:45:19.36" ], [
"20080427_194500_bbcthree_doctor_who_confidential", "00:15:23.40" ], [ "20080427_200000_bbcthree_pulling", "00:29:52.12" ], [
"20080427_201000_bbcfour_washes_whiter", "00:51:23.20" ], [ "20080427_203000_bbcone_the_vicar_of_dibley", "00:31:08.28" ], [
"20080427_203000_bbcthree_little_britain", "00:29:39.40" ], [ "20080427_205000_bbctwo_coast", "00:10:01.20" ], [
"20080427_210000_bbcone_bbc_news_at_ten", "00:19:24.32" ], [ "20080427_210000_bbctwo_match_of_the_day_2", "00:50:52.32" ], [
"20080427_212000_bbcone_panorama", "01:00:59.32" ], [ "20080427_215000_bbcfour_bbc_four_session_van_morrison", "01:01:04.24" ], [
"20080427_215000_bbctwo_graham_norton_uncut", "00:44:52.16" ], [ "20080427_222000_bbcone_celine_dion_in_las_vegas_a_new_day",
"01:01:01.48" ], [ "20080427_224500_bbcthree_pulling", "00:30:24.24" ], [ "20080427_225000_bbcfour_van_morrison_at_the_bbc",
"00:59:19.12" ], [ "20080427_231500_bbcthree_page_three_teens", "00:58:14.28" ], [
"20080427_232500_bbctwo_snooker_world_championship_highlights", "00:50:19.28" ], [ "20080427_235000_bbcfour_soul_britannia",
"01:00:20.20" ], [ "20080428_001500_bbctwo_snooker_extra", "01:44:22.20" ], [ "20080428_004500_bbcone_weatherview", "00:04:20.32"
], [ "20080428_004500_bbcthree_natalie_cassidy_s_diet_secrets", "00:59:06.24" ], [
"20080428_005000_bbcfour_how_to_build_a_cathedral", "01:00:05.08" ], [ "20080428_005000_bbcone_watchdog", "00:30:03.36" ], [
"20080428_012000_bbcone_holby_city", "00:59:39.28" ], [ "20080428_015000_bbcfour_inside_the_medieval_mind", "00:59:16.36" ], [
"20080428_022000_bbcone_age_of_terror", "01:00:03.40" ], [ "20080428_023000_bbctwo_inside_sport", "00:40:34.32" ], [
"20080428_024000_bbcthree_dog_borstal", "00:30:06.28" ], [ "20080428_031000_bbcthree_doctor_who_confidential", "00:14:53.28" ], [
"20080428_050000_bbcone_breakfast", "03:14:56.20" ], [ "20080428_050000_bbctwo_tikkabilla", "00:31:06.12" ], [
"20080428_062500_bbctwo_newsround", "00:05:33.32" ], [ "20080428_063000_bbctwo_hider_in_the_house", "01:01:54.36" ], [
"20080428_073000_bbctwo_jackanory_junior", "00:15:32.24" ], [ "20080428_080000_bbctwo_boogie_beebies", "00:15:48.32" ], [
"20080428_081500_bbcone_missing_live", "00:45:10.44" ], [ "20080428_084000_bbctwo_something_special", "00:21:07.28" ], [
"20080428_093000_bbctwo_schools_focus", "00:19:19.32" ], [ "20080428_095000_bbctwo_schools_look_and_read", "00:21:05.16" ], [
"20080428_100000_bbcone_to_buy_or_not_to_buy", "00:44:25.44" ], [ "20080428_101000_bbctwo_schools_razzledazzle", "00:19:29.28" ],
[ "20080428_103000_bbctwo_schools_barnaby_bear", "00:14:19.16" ], [ "20080428_110000_bbctwo_the_daily_politics", "00:31:01.88" ],
[ "20080428_111500_bbcone_bargain_hunt", "00:44:09.32" ], [ "20080428_113000_bbctwo_working_lunch", "00:30:09.32" ], [
"20080428_120000_bbcone_bbc_news_at_one", "00:31:24.20" ], [ "20080428_120000_bbctwo_science_clips_investigates_habitats",
"00:09:15.44" ], [ "20080428_121000_bbctwo_science_clips_investigates_friction", "00:09:26.28" ], [
"20080428_122000_bbctwo_kenya_shorts_kenya_and_world", "00:10:22.32" ], [ "20080428_123000_bbctwo_snooker_world_championship",
"04:29:36.32" ], [ "20080428_124500_bbcone_doctors", "00:28:04.28" ], [ "20080428_140500_bbcone_space_pirates", "00:29:01.28" ],
[ "20080428_143500_bbcone_chucklevision", "00:15:12.40" ], [ "20080428_151000_bbcone_roar", "00:28:21.04" ], [
"20080428_153500_bbcone_grange_hill", "00:25:08.40" ], [ "20080428_160000_bbcone_newsround", "00:10:22.36" ], [
"20080428_161500_bbcone_the_weakest_link", "00:45:55.24" ], [ "20080428_170000_bbcone_bbc_news_at_six", "00:47:43.20" ], [
"20080428_170000_bbctwo_eggheads", "00:30:12.32" ], [ "20080428_173000_bbcone_bbc_london_news", "00:06:33.28" ], [
"20080428_180000_bbcfour_world_news_today", "00:30:41.44" ], [ "20080428_180000_bbcthree_dragons_den", "01:00:34.32" ], [
"20080428_180000_bbctwo_snooker_world_championship", "02:02:14.20" ], [ "20080428_183000_bbcfour_art_of_eternity", "00:58:39.76"
], [ "20080428_183000_bbcone_watchdog", "00:31:34.36" ], [ "20080428_190000_bbcone_eastenders", "00:30:35.32" ], [
"20080428_193000_bbcfour_the_book_quiz", "00:30:07.24" ], [ "20080428_193000_bbcone_panorama", "00:30:12.28" ], [
"20080428_193000_bbcthree_glamour_girls", "00:30:33.28" ], [ "20080428_200000_bbcfour_a_history_of_britain_by_simon_schama",
"01:00:58.24" ], [ "20080428_200000_bbcone_waking_the_dead", "00:59:09.20" ], [ "20080428_210000_bbcthree_eastenders",
"00:30:11.28" ], [ "20080428_213000_bbcthree_ideal", "00:31:25.40" ], [ "20080428_213000_bbctwo_newsnight", "00:50:00.24" ], [
"20080428_213500_bbcone_meet_the_immigrants", "00:30:35.32" ], [ "20080428_220000_bbcthree_pulling", "00:29:53.28" ], [
"20080428_221000_bbcfour_the_hour_of_the_pig", "01:48:54.64" ], [ "20080428_222000_bbctwo_snooker_world_championship_highlights",
"01:08:11.28" ], [ "20080428_231500_bbcthree_glamour_girls", "00:29:48.32" ], [ "20080429_000000_bbcfour_the_book_quiz",
"00:30:02.12" ], [ "20080429_003500_bbcone_springwatch_weatherview", "00:04:50.04" ], [ "20080429_004500_bbcthree_ideal",
"00:30:02.28" ], [ "20080429_011500_bbcthree_pulling", "00:30:34.20" ], [ "20080429_014000_bbcfour_the_book_quiz", "00:30:01.36"
], [ "20080429_021000_bbcfour_art_of_eternity", "00:57:24.28" ], [ "20080429_021000_bbcone_to_buy_or_not_to_buy", "00:46:08.28"
], [ "20080429_030000_bbctwo_gcse_bitesize_revision", "01:59:53.16" ], [ "20080429_031500_bbcthree_dog_borstal", "00:29:33.36" ],
[ "20080429_050000_bbcone_breakfast", "03:15:16.24" ], [ "20080429_050000_bbctwo_tikkabilla", "00:30:40.20" ], [
"20080429_063000_bbctwo_hider_in_the_house", "01:02:12.28" ], [ "20080429_073000_bbctwo_jackanory_junior", "00:15:14.28" ], [
"20080429_080000_bbctwo_boogie_beebies", "00:15:34.28" ], [ "20080429_081500_bbcone_missing_live", "00:44:48.40" ], [
"20080429_084000_bbctwo_something_special", "00:21:10.28" ], [ "20080429_093000_bbctwo_schools_let_s_write_poetry", "00:19:26.40"
], [ "20080429_100000_bbcone_to_buy_or_not_to_buy", "00:44:38.24" ], [ "20080429_104500_bbcone_cash_in_the_attic", "00:29:53.52"
], [ "20080429_110000_bbctwo_the_daily_politics", "00:29:53.12" ], [ "20080429_111500_bbcone_bargain_hunt", "00:44:02.36" ], [
"20080429_113000_bbctwo_working_lunch", "00:29:59.24" ], [ "20080429_120000_bbcone_bbc_news_at_one", "00:31:16.20" ], [
"20080429_120000_bbctwo_schools_maths_channel", "00:09:22.28" ], [ "20080429_121000_bbctwo_schools_primary_geography",
"00:20:46.40" ], [ "20080429_123000_bbctwo_snooker_world_championship", "04:30:29.16" ], [ "20080429_124000_bbcone_doctors",
"00:29:08.24" ], [ "20080429_143500_bbcone_chucklevision", "00:15:37.24" ], [ "20080429_153500_bbcone_blue_peter", "00:24:49.32"
], [ "20080429_160000_bbcone_newsround", "00:11:06.24" ], [ "20080429_161500_bbcone_the_weakest_link", "00:45:54.32" ], [
"20080429_170000_bbcone_bbc_news_at_six", "00:28:56.32" ], [ "20080429_170000_bbctwo_eggheads", "00:30:12.24" ], [
"20080429_173000_bbcone_bbc_london_news", "00:25:08.36" ], [ "20080429_173000_bbctwo_great_british_menu", "00:30:11.16" ], [
"20080429_175500_bbcone_party_election_broadcast", "00:05:14.16" ], [ "20080429_180000_bbcfour_world_news_today", "00:30:07.36"
], [ "20080429_180000_bbcone_the_one_show", "00:30:08.40" ], [ "20080429_180000_bbcthree_dog_borstal", "00:58:30.40" ], [
"20080429_183000_bbcfour_pop_goes_the_sixties", "00:05:08.20" ], [ "20080429_183000_bbcone_eastenders", "00:32:13.16" ], [
"20080429_190000_bbcfour_life_in_cold_blood", "00:58:57.36" ], [ "20080429_190000_bbcone_holby_city", "00:57:44.16" ], [
"20080429_190000_bbcthree_natalie_cassidy_s_diet_secrets", "00:59:33.72" ], [ "20080429_200000_bbcfour_chinese_school",
"00:59:47.56" ], [ "20080429_200000_bbcone_waking_the_dead", "00:59:41.24" ], [ "20080429_200000_bbctwo_age_of_terror",
"00:59:05.24" ], [ "20080429_210000_bbcfour_goodness_gracious_me", "00:29:57.52" ], [ "20080429_210000_bbcone_bbc_news_at_ten",
"00:27:08.28" ], [ "20080429_210000_bbcthree_eastenders", "00:29:58.28" ], [
"20080429_210000_bbctwo_later_live_with_jools_holland", "00:31:15.16" ], [ "20080429_213000_bbcthree_scallywagga", "00:29:56.32"
], [ "20080429_213000_bbctwo_newsnight", "00:50:46.24" ], [ "20080429_220000_bbcthree_the_wall", "00:45:14.32" ], [
"20080429_224000_bbcfour_chinese_school", "00:59:38.20" ], [ "20080429_231000_bbctwo_snooker_world_championship_highlights",
"00:50:11.40" ], [ "20080429_232500_bbcone_weatherview", "00:04:36.48" ], [ "20080429_233000_bbcone_stand_by_your_man",
"00:29:33.24" ], [ "20080429_234000_bbcfour_proms_on_four", "02:16:23.56" ], [ "20080430_000000_bbcone_tiger_spy_in_the_jungle",
"00:59:54.44" ], [ "20080430_000000_bbctwo_snooker_extra", "01:30:38.20" ], [ "20080430_003000_bbcthree_scallywagga",
"00:28:43.32" ], [ "20080430_005500_bbcthree_the_wall", "00:44:04.36" ], [ "20080430_013000_bbcone_to_buy_or_not_to_buy",
"00:44:25.24" ], [ "20080430_014000_bbcthree_natalie_cassidy_s_diet_secrets", "00:58:16.32" ], [
"20080430_015500_bbcfour_chinese_school", "00:59:31.16" ], [ "20080430_021500_bbcone_to_buy_or_not_to_buy", "00:45:23.32" ], [
"20080430_030000_bbctwo_gcse_bitesize", "01:58:31.88" ], [ "20080430_050000_bbcone_breakfast", "03:15:05.16" ], [
"20080430_050000_bbctwo_tikkabilla", "00:30:59.28" ], [ "20080430_063000_bbctwo_hider_in_the_house", "01:01:56.24" ], [
"20080430_073000_bbctwo_jackanory_junior", "00:15:24.16" ], [ "20080430_080000_bbctwo_boogie_beebies", "00:15:30.24" ], [
"20080430_081500_bbcone_missing_live", "00:44:49.32" ], [ "20080430_084000_bbctwo_something_special", "00:21:29.36" ], [
"20080430_093000_bbctwo_a_picture_of_britain", "00:59:50.40" ], [ "20080430_100000_bbcone_to_buy_or_not_to_buy", "00:44:31.36" ],
[ "20080430_103000_bbctwo_the_daily_politics", "01:30:37.12" ], [ "20080430_104500_bbcone_cash_in_the_attic", "00:29:54.56" ], [
"20080430_111500_bbcone_bargain_hunt", "00:44:22.32" ], [ "20080430_120000_bbcone_bbc_news_at_one", "00:30:58.32" ], [
"20080430_123000_bbcone_bbc_london_news", "00:12:16.32" ], [ "20080430_123000_bbctwo_working_lunch", "00:30:59.32" ], [
"20080430_124000_bbcone_doctors", "00:28:46.32" ], [ "20080430_130000_bbctwo_snooker_world_championship", "02:29:29.16" ], [
"20080430_140500_bbcone_space_pirates", "00:28:55.28" ], [ "20080430_143500_bbcone_chucklevision", "00:15:15.16" ], [
"20080430_151000_bbcone_young_dracula", "00:28:57.24" ], [ "20080430_153500_bbcone_blue_peter", "00:24:20.40" ], [
"20080430_160000_bbcone_newsround", "00:11:08.20" ], [ "20080430_161500_bbcone_the_weakest_link", "00:45:58.24" ], [
"20080430_170000_bbcone_bbc_news_at_six", "00:28:46.36" ], [ "20080430_170000_bbctwo_eggheads", "00:29:49.32" ], [
"20080430_173000_bbcone_bbc_london_news", "00:29:01.36" ], [ "20080430_180000_bbcfour_world_news_today", "00:30:29.56" ], [
"20080430_180000_bbcone_the_one_show", "00:30:29.24" ], [ "20080430_180000_bbcthree_holby_city", "00:58:32.52" ], [
"20080430_180000_bbctwo_snooker_world_championship", "01:59:36.68" ], [ "20080430_183000_bbcfour_pop_goes_the_sixties",
"00:04:47.72" ], [ "20080430_183000_bbcone_street_doctor", "00:31:31.44" ], [ "20080430_190000_bbcfour_life_in_cold_blood",
"00:58:54.28" ], [ "20080430_190000_bbcone_traffic_cops", "00:59:56.60" ], [ "20080430_190000_bbcthree_glamour_girls",
"00:29:10.32" ], [ "20080430_200000_bbcfour_a_history_of_britain_by_simon_schama", "00:59:39.52" ], [
"20080430_200000_bbcone_the_apprentice", "00:59:51.36" ], [ "20080430_200000_bbcthree_two_pints_of_lager_and", "00:30:18.36" ], [
"20080430_203000_bbcthree_ideal", "00:30:33.16" ], [ "20080430_210000_bbcone_bbc_news_at_ten", "00:26:57.24" ], [
"20080430_210000_bbctwo_the_apprentice_you_re_fired", "00:32:57.28" ], [ "20080430_213000_bbctwo_newsnight", "00:47:38.28" ], [
"20080430_222000_bbctwo_snooker_world_championship_highlights", "00:51:37.44" ], [ "20080430_231000_bbctwo_snooker_extra",
"01:50:57.08" ], [ "20080430_233500_bbcthree_two_pints_of_lager_and", "00:30:17.32" ], [
"20080430_235000_bbcfour_selling_the_sixties", "00:59:42.20" ], [ "20080430_235000_bbcone_weatherview", "00:05:47.20" ], [
"20080430_235500_bbcone_seaside_rescue", "00:29:23.32" ], [ "20080501_000500_bbcthree_ideal", "00:29:47.28" ], [
"20080501_002500_bbcone_meet_the_immigrants", "00:30:46.32" ], [ "20080501_005000_bbcfour_david_ogilvy_original_mad_man",
"00:59:39.52" ], [ "20080501_010500_bbcthree_the_wall", "00:44:42.40" ], [ "20080501_015000_bbcthree_glamour_girls",
"00:29:54.36" ], [ "20080501_015500_bbcone_to_buy_or_not_to_buy", "00:44:37.24" ], [ "20080501_022000_bbcthree_page_three_teens",
"00:58:13.28" ], [ "20080501_030000_bbctwo_gcse_bitesize", "01:58:16.80" ], [ "20080501_050000_bbcone_breakfast", "03:14:57.28"
], [ "20080501_050000_bbctwo_tikkabilla", "00:31:09.32" ], [ "20080501_063000_bbctwo_hider_in_the_house", "01:01:53.12" ], [
"20080501_073000_bbctwo_jackanory_junior", "00:15:25.32" ], [ "20080501_080000_bbctwo_boogie_beebies", "00:15:34.20" ], [
"20080501_081500_bbcone_missing_live", "00:45:14.32" ], [ "20080501_084000_bbctwo_something_special", "00:21:23.12" ], [
"20080501_093000_bbctwo_schools_primary_history", "00:19:22.20" ], [ "20080501_095000_bbctwo_schools_megamaths", "00:20:53.16" ],
[ "20080501_100000_bbcone_to_buy_or_not_to_buy", "00:44:20.28" ], [ "20080501_101000_bbctwo_schools_primary_geography",
"00:19:12.28" ], [ "20080501_103000_bbctwo_schools_ks1_science_clips", "00:11:19.24" ], [
"20080501_104000_bbctwo_schools_ks1_science_clips", "00:09:24.32" ], [ "20080501_104500_bbcone_cash_in_the_attic", "00:30:01.96"
], [ "20080501_105000_bbctwo_schools_hands_up", "00:09:18.24" ], [ "20080501_110000_bbctwo_the_daily_politics", "00:30:45.28" ],
[ "20080501_111500_bbcone_bargain_hunt", "00:44:44.20" ], [ "20080501_113000_bbctwo_working_lunch", "00:29:57.44" ], [
"20080501_120000_bbcone_bbc_news_at_one", "00:30:40.20" ], [ "20080501_123000_bbctwo_snooker_world_championship", "04:29:09.80"
], [ "20080501_124000_bbcone_doctors", "00:29:35.16" ], [ "20080501_140500_bbcone_space_pirates", "00:28:40.28" ], [
"20080501_143500_bbcone_chucklevision", "00:15:41.28" ], [ "20080501_150500_bbcone_stake_out", "00:28:49.20" ], [
"20080501_153500_bbcone_beat_the_boss", "00:28:34.32" ], [ "20080501_160000_bbcone_newsround", "00:10:43.28" ], [
"20080501_161500_bbcone_the_weakest_link", "00:45:20.32" ], [ "20080501_170000_bbcone_bbc_news_at_six", "00:29:16.24" ], [
"20080501_170000_bbctwo_eggheads", "00:29:50.56" ], [ "20080501_173000_bbcone_bbc_london_news", "00:29:11.28" ], [
"20080501_180000_bbcone_the_one_show", "00:31:58.08" ], [ "20080501_180000_bbctwo_snooker_world_championship", "02:00:37.20" ], [
"20080501_183000_bbcfour_in_search_of_medieval_britain", "00:29:26.32" ], [ "20080501_190000_bbcfour_sacred_music", "00:59:51.28"
], [ "20080501_190000_bbcone_holby_blue", "00:59:42.48" ], [ "20080501_200000_bbcfour_inside_the_medieval_mind", "00:57:02.36" ],
[ "20080501_200000_bbcone_the_invisibles", "00:57:19.28" ], [ "20080501_200000_bbcthree_should_i_smoke_dope", "00:56:45.16" ], [
"20080501_204500_bbctwo_heroes_unmasked", "00:18:10.32" ], [ "20080501_210000_bbcfour_crusades", "00:50:42.16" ], [
"20080501_210000_bbcone_bbc_news_at_ten", "00:25:30.36" ], [ "20080501_210000_bbcthree_eastenders", "00:29:27.36" ], [
"20080501_213000_bbctwo_newsnight", "00:48:51.20" ], [ "20080501_222000_bbctwo_snooker_world_championship_highlights",
"00:50:24.32" ], [ "20080501_223500_bbcone_election_night_2008", "06:23:46.64" ], [ "20080501_224000_bbcfour_chinese_school",
"01:00:59.68" ], [ "20080501_225500_bbcthree_pulling", "00:30:20.32" ], [ "20080501_231000_bbctwo_panorama", "00:29:51.32" ], [
"20080501_232500_bbcthree_should_i_smoke_dope", "00:54:42.36" ], [ "20080501_234000_bbcfour_in_search_of_medieval_britain",
"00:30:40.48" ], [ "20080502_001000_bbctwo_the_twenties_in_colour_the_wonderful", "00:29:45.60" ], [
"20080502_004000_bbctwo_bill_oddie_s_wild_side", "00:29:42.16" ], [ "20080502_011000_bbcfour_the_book_quiz", "00:31:04.48" ], [
"20080502_012000_bbcthree_pulling", "00:30:46.32" ], [ "20080502_014000_bbcfour_in_search_of_medieval_britain", "00:29:49.28" ],
[ "20080502_014000_bbctwo_to_buy_or_not_to_buy", "01:08:04.20" ], [ "20080502_021000_bbcfour_inside_the_medieval_mind",
"01:00:14.20" ], [ "20080502_030000_bbctwo_gcse_bitesize", "01:59:31.64" ], [ "20080502_050000_bbctwo_tikkabilla", "00:31:15.20"
], [ "20080502_062500_bbctwo_newsround", "00:05:38.32" ], [ "20080502_063000_bbctwo_hider_in_the_house", "01:01:07.36" ], [
"20080502_080000_bbctwo_boogie_beebies", "00:15:44.28" ], [ "20080502_081500_bbcone_missing_live", "00:44:47.28" ], [
"20080502_084000_bbctwo_something_special", "00:21:17.40" ], [ "20080502_094500_bbctwo_schools_the_way_things_work",
"00:16:16.12" ], [ "20080502_100000_bbcone_to_buy_or_not_to_buy", "00:44:55.12" ], [
"20080502_100000_bbctwo_schools_the_way_things_work", "00:13:27.32" ], [ "20080502_103000_bbctwo_schools_watch", "00:14:38.36" ],
[ "20080502_104500_bbcone_cash_in_the_attic", "00:30:09.00" ], [ "20080502_104500_bbctwo_schools_something_special",
"00:14:41.24" ], [ "20080502_110000_bbctwo_the_daily_politics", "00:29:14.52" ], [ "20080502_111500_bbcone_bargain_hunt",
"00:44:01.20" ], [ "20080502_113000_bbctwo_working_lunch", "01:00:28.28" ], [ "20080502_120000_bbcone_bbc_news_at_one",
"00:30:20.28" ], [ "20080502_123000_bbctwo_snooker_world_championship", "04:30:52.16" ], [ "20080502_124000_bbcone_doctors",
"00:28:28.32" ], [ "20080502_140500_bbcone_space_pirates", "00:28:47.40" ], [ "20080502_143500_bbcone_chucklevision",
"00:15:17.28" ], [ "20080502_153500_bbcone_the_slammer", "00:28:35.56" ], [ "20080502_160000_bbcone_newsround", "00:10:50.32" ],
[ "20080502_161500_bbcone_the_weakest_link", "00:45:27.36" ], [ "20080502_170000_bbcone_bbc_news_at_six", "00:29:17.28" ], [
"20080502_170000_bbctwo_eggheads", "00:29:49.28" ], [ "20080502_173000_bbcone_bbc_london_news", "00:29:10.36" ], [
"20080502_180000_bbcfour_world_news_today", "00:30:36.40" ], [ "20080502_180000_bbcthree_top_gear", "01:01:08.28" ], [
"20080502_180000_bbctwo_snooker_world_championship", "02:01:04.16" ], [ "20080502_183000_bbcfour_transatlantic_sessions",
"00:30:27.28" ], [ "20080502_183000_bbcone_inside_out", "00:31:32.40" ], [
"20080502_190000_bbcfour_darcey_bussell_s_ten_best_ballet_moments", "00:59:33.84" ], [ "20080502_190000_bbcone_eastenders",
"00:29:26.32" ], [ "20080502_190000_bbcthree_last_man_standing", "00:57:25.64" ], [ "20080502_193000_bbcone_a_question_of_sport",
"00:30:40.12" ], [ "20080502_200000_bbcfour_james_taylor_one_man_band", "01:04:03.28" ], [ "20080502_200000_bbcthree_doctor_who",
"00:45:22.20" ], [ "20080502_200000_bbctwo_living_the_dream_revisited", "00:59:49.28" ], [
"20080502_204500_bbcthree_doctor_who_confidential", "00:14:44.16" ], [ "20080502_210000_bbcone_bbc_news_at_ten", "00:26:01.40" ],
[ "20080502_210500_bbcfour_hotel_california_from_the_byrds_to", "01:34:03.40" ], [ "20080502_213000_bbctwo_newsnight",
"00:37:01.28" ], [ "20080502_220000_bbcthree_two_pints_of_lager_and", "00:30:17.16" ], [
"20080502_221000_bbctwo_newsnight_review", "00:32:34.28" ], [ "20080502_224000_bbcfour_in_concert_james_taylor", "00:43:19.24" ],
[ "20080502_224000_bbctwo_later_with_jools_holland", "01:01:58.64" ], [
"20080502_234000_bbctwo_snooker_world_championship_highlights", "00:49:20.36" ], [ "20080502_234500_bbcthree_ideal",
"00:29:38.20" ], [ "20080503_001000_bbcfour_james_taylor_one_man_band", "00:58:42.28" ], [
"20080503_001500_bbcthree_two_pints_of_lager_and", "00:29:46.36" ], [ "20080503_002500_bbcone_weatherview", "00:07:04.36" ], [
"20080503_003000_bbcone_dan_cruickshank_s_adventures", "00:59:47.20" ], [ "20080503_004500_bbcthree_two_pints_of_lager_and",
"00:30:07.28" ], [ "20080503_011000_bbcfour_darcey_bussell_s_ten_best_ballet_moments", "00:59:52.24" ], [
"20080503_011500_bbcthree_glamour_girls", "00:29:35.28" ], [ "20080503_013000_bbcone_natural_world", "00:48:56.40" ], [
"20080503_014500_bbcthree_scallywagga", "00:28:55.32" ], [ "20080503_021000_bbcfour_transatlantic_sessions", "00:29:09.28" ], [
"20080503_021000_bbcthree_ideal", "00:29:17.32" ], [ "20080503_050000_bbcone_breakfast", "03:59:43.36" ], [
"20080503_050000_bbctwo_fimbles", "00:20:59.28" ], [ "20080503_052000_bbctwo_tikkabilla", "00:33:29.16" ], [
"20080503_070000_bbctwo_sorcerer_s_apprentice", "00:33:25.40" ], [ "20080503_090000_bbcone_saturday_kitchen", "01:30:12.24" ], [
"20080503_090000_bbctwo_hedz", "00:29:27.36" ], [ "20080503_093000_bbctwo_the_slammer", "00:29:07.32" ], [
"20080503_104500_bbctwo_sportsround", "00:15:37.32" ], [ "20080503_110000_bbcone_bbc_news", "00:10:13.28" ], [
"20080503_111000_bbcone_football_focus", "00:49:53.16" ], [ "20080503_120000_bbcone_snooker_world_championship", "03:14:53.24" ],
[ "20080503_125000_bbctwo_sound", "00:31:29.16" ], [ "20080503_153000_bbctwo_flog_it", "01:00:35.28" ], [
"20080503_162000_bbcone_bbc_news", "00:10:58.40" ], [ "20080503_163000_bbcone_bbc_london_news", "00:07:59.32" ], [
"20080503_163000_bbctwo_coast", "00:59:14.12" ], [ "20080503_164000_bbcone_the_kids_are_all_right", "00:39:49.88" ], [
"20080503_172000_bbcone_doctor_who", "00:45:44.44" ], [ "20080503_173000_bbctwo_dad_s_army", "00:30:45.32" ], [
"20080503_180000_bbcfour_meetings_with_remarkable_trees", "00:11:19.48" ], [ "20080503_180000_bbcthree_three_s_outtakes",
"00:05:14.36" ], [ "20080503_180000_bbctwo_snooker_world_championship", "03:03:59.32" ], [
"20080503_180500_bbcone_i_d_do_anything", "01:01:02.28" ], [ "20080503_180500_bbcthree_doctor_who_confidential", "00:43:40.20" ],
[ "20080503_181000_bbcfour_in_search_of_medieval_britain", "00:30:04.24" ], [ "20080503_190500_bbcthree_top_gear", "01:03:35.28"
], [ "20080503_191000_bbcfour_a_perfect_spy", "00:55:54.16" ], [ "20080503_195500_bbcone_casualty", "00:48:15.36" ], [
"20080503_200500_bbcfour_a_perfect_spy", "00:54:45.28" ], [ "20080503_204500_bbcone_love_soup", "00:30:29.40" ], [
"20080503_210000_bbctwo_have_i_got_a_bit_more_news_for_you", "00:42:17.32" ], [ "20080503_211500_bbcone_bbc_news", "00:17:41.36"
], [ "20080503_213000_bbcone_match_of_the_day", "01:20:35.40" ], [ "20080503_214500_bbctwo_comedy_map_of_britain", "01:00:38.28"
], [ "20080503_215500_bbcthree_scallywagga", "00:28:19.24" ], [ "20080503_234500_bbctwo_the_apprentice_you_re_fired",
"00:32:30.08" ], [ "20080504_001500_bbcthree_glamour_girls", "00:29:51.48" ], [ "20080504_004500_bbcthree_page_three_teens",
"00:59:11.28" ], [ "20080504_005500_bbcfour_david_ogilvy_original_mad_man", "01:00:10.36" ], [
"20080504_014500_bbcthree_the_wall", "00:44:32.20" ], [ "20080504_015500_bbcfour_in_search_of_medieval_britain", "00:30:25.28" ],
[ "20080504_022500_bbcfour_the_book_quiz", "00:29:19.72" ], [ "20080504_023000_bbcthree_scallywagga", "00:27:58.36" ], [
"20080504_050000_bbcone_breakfast", "01:44:45.24" ], [ "20080504_052000_bbctwo_tikkabilla", "00:41:38.24" ], [
"20080504_064500_bbcone_moto_gp_shanghai", "01:15:07.36" ], [ "20080504_070000_bbctwo_trapped", "00:28:25.16" ], [
"20080504_073000_bbctwo_hider_in_the_house", "01:05:08.24" ], [ "20080504_080000_bbcone_the_andrew_marr_show", "01:00:05.40" ], [
"20080504_083500_bbctwo_match_of_the_day", "01:23:01.16" ], [ "20080504_090000_bbcone_sunday_life", "00:59:57.40" ], [
"20080504_100000_bbcone_countryfile", "00:59:56.20" ], [ "20080504_110000_bbcone_the_politics_show", "00:59:14.32" ], [
"20080504_113000_bbctwo_premiership_rugby", "00:30:26.40" ], [ "20080504_120000_bbcone_eastenders", "01:54:00.28" ], [
"20080504_120000_bbctwo_badminton_horse_trials", "01:59:57.20" ], [ "20080504_140000_bbctwo_snooker_world_championship",
"02:59:16.76" ], [ "20080504_152500_bbcone_allo_allo", "00:24:45.20" ], [ "20080504_155000_bbcone_keeping_up_appearances",
"00:28:53.96" ], [ "20080504_162000_bbcone_points_of_view", "00:14:58.28" ], [ "20080504_163500_bbcone_songs_of_praise",
"00:35:22.24" ], [ "20080504_170000_bbctwo_bbc_young_musician_of_the_year", "01:00:05.48" ], [ "20080504_171000_bbcone_bbc_news",
"00:16:25.36" ], [ "20080504_172500_bbcone_bbc_london_news", "00:08:51.28" ], [
"20080504_180000_bbcfour_inside_the_medieval_mind", "00:57:40.28" ], [ "20080504_180500_bbcone_i_d_do_anything_results",
"00:30:39.24" ], [ "20080504_190000_bbcthree_doctor_who", "00:44:58.24" ], [ "20080504_190000_bbctwo_snooker_world_championship",
"02:51:14.36" ], [ "20080504_200000_bbcfour_dear_television", "00:09:44.80" ], [ "20080504_201000_bbcfour_washes_whiter",
"00:51:08.32" ], [ "20080504_203000_bbcthree_little_britain", "00:29:03.40" ], [ "20080504_210000_bbcone_bbc_news_at_ten",
"00:19:19.40" ], [ "20080504_212000_bbcone_match_of_the_day_2", "00:51:33.28" ], [
"20080504_214500_bbcfour_james_taylor_one_man_band", "01:02:52.24" ], [ "20080504_214500_bbcthree_scallywagga", "00:29:19.16" ],
[ "20080504_215000_bbctwo_coast", "00:11:27.32" ], [ "20080504_221500_bbcthree_two_pints_of_lager_and", "00:30:18.36" ], [
"20080504_224500_bbcthree_two_pints_of_lager_and", "00:31:21.28" ], [
"20080504_225000_bbcfour_hotel_california_from_the_byrds_to", "01:32:52.12" ], [ "20080504_231500_bbcthree_child_stars",
"00:58:48.28" ], [ "20080504_234500_bbcone_the_sky_at_night", "00:19:49.12" ], [ "20080504_234500_bbctwo_graham_norton_uncut",
"00:45:22.36" ], [ "20080505_000500_bbcone_weatherview", "00:05:21.28" ], [ "20080505_001000_bbcone_watchdog", "00:29:57.36" ], [
"20080505_002500_bbcfour_in_concert_james_taylor", "00:41:28.16" ], [ "20080505_004000_bbcone_holby_city", "00:58:11.12" ], [
"20080505_004500_bbcthree_scallywagga", "00:28:18.20" ], [ "20080505_011000_bbcthree_two_pints_of_lager_and", "00:31:04.20" ], [
"20080505_014000_bbcthree_two_pints_of_lager_and", "00:30:56.24" ], [ "20080505_021000_bbcfour_inside_the_medieval_mind",
"00:58:36.16" ], [ "20080505_031000_bbctwo_inside_sport", "00:55:22.12" ], [ "20080505_050000_bbcone_breakfast", "02:59:54.36" ],
[ "20080505_050000_bbctwo_tikkabilla", "00:31:04.40" ], [ "20080505_062500_bbctwo_newsround", "00:04:04.28" ], [
"20080505_063000_bbctwo_hider_in_the_house", "01:03:50.16" ], [ "20080505_073000_bbctwo_jackanory_junior", "00:15:22.24" ], [
"20080505_080000_bbcone_missing_live", "00:45:05.24" ], [ "20080505_080000_bbctwo_boogie_beebies", "00:15:39.20" ], [
"20080505_084000_bbctwo_something_special", "00:21:32.24" ], [ "20080505_094500_bbcone_to_buy_or_not_to_buy", "00:45:23.28" ], [
"20080505_111500_bbcone_bbc_news", "00:14:51.36" ], [ "20080505_114000_bbcone_match_of_the_day_live", "02:32:02.32" ], [
"20080505_133000_bbctwo_snooker_world_championship", "02:46:12.24" ], [ "20080505_170000_bbctwo_eggheads", "00:30:58.24" ], [
"20080505_173500_bbcone_bbc_news", "00:14:18.36" ], [ "20080505_175000_bbcone_bbc_london_news", "00:09:04.24" ], [
"20080505_180000_bbcfour_the_book_quiz", "00:30:48.52" ], [ "20080505_180000_bbcone_the_one_show", "00:30:12.44" ], [
"20080505_180000_bbcthree_dragon_s_den", "04:02:47.12" ], [ "20080505_180000_bbctwo_val_doonican_rocks", "00:59:20.28" ], [
"20080505_183000_bbcfour_the_sky_at_night", "00:30:08.92" ], [ "20080505_183000_bbcone_watchdog", "00:30:52.32" ], [
"20080505_190000_bbcfour_bbc_young_musician_of_the_year_2008", "01:00:38.20" ], [ "20080505_190000_bbcone_eastenders",
"00:30:39.08" ], [ "20080505_190000_bbctwo_snooker_world_championship", "00:49:40.16" ], [ "20080505_193000_bbcone_panorama",
"00:30:04.40" ], [ "20080505_193000_bbcthree_glamour_girls", "00:29:59.32" ], [
"20080505_200000_bbcfour_christina_a_medieval_life", "01:00:48.28" ], [ "20080505_200000_bbcone_waking_the_dead", "00:59:35.28"
], [ "20080505_210000_bbcone_bbc_news_at_ten", "00:15:07.32" ], [ "20080505_210000_bbcthree_eastenders", "00:29:51.40" ], [
"20080505_212000_bbcone_meet_the_immigrants", "00:29:50.40" ], [ "20080505_213000_bbcthree_ideal", "00:30:47.28" ], [
"20080505_215000_bbcone_inside_sport", "00:40:58.36" ], [ "20080505_220000_bbcthree_placebo", "00:29:02.44" ], [
"20080505_225000_bbcfour_christina_a_medieval_life", "00:59:48.64" ], [ "20080505_231500_bbcthree_glamour_girls", "00:29:26.32"
], [ "20080505_235000_bbcfour_bbc_young_musician_of_the_year_2008", "00:59:43.40" ], [ "20080506_001500_bbcone_weatherview",
"00:05:35.28" ], [ "20080506_004500_bbcthree_ideal", "00:29:43.32" ], [ "20080506_011500_bbcthree_placebo", "00:30:08.32" ], [
"20080506_012000_bbcfour_the_book_quiz", "00:30:44.20" ], [ "20080506_015000_bbcfour_christina_a_medieval_life", "00:59:38.56" ],
[ "20080506_025000_bbcfour_the_book_quiz", "00:29:48.68" ], [ "20080506_030000_bbctwo_shakespeare_the_animated_tales",
"01:59:17.32" ], [ "20080506_050000_bbcone_breakfast", "03:15:08.28" ], [ "20080506_050000_bbctwo_tikkabilla", "00:30:53.36" ], [
"20080506_062500_bbctwo_newsround", "00:04:11.24" ], [ "20080506_063000_bbctwo_hider_in_the_house", "01:03:35.28" ], [
"20080506_073000_bbctwo_jackanory_junior", "00:15:00.32" ], [ "20080506_081500_bbcone_missing_live", "00:44:41.24" ], [
"20080506_084000_bbctwo_something_special", "00:21:20.28" ], [ "20080506_093000_bbctwo_schools_let_s_write_poetry", "00:19:30.32"
], [ "20080506_100000_bbcone_to_buy_or_not_to_buy", "00:44:47.24" ], [ "20080506_101000_bbctwo_timewatch", "00:49:57.08" ], [
"20080506_104500_bbcone_cash_in_the_attic", "00:29:52.68" ], [ "20080506_110000_bbctwo_the_daily_politics", "00:30:11.44" ], [
"20080506_111500_bbcone_bargain_hunt", "00:44:47.28" ], [ "20080506_113000_bbctwo_working_lunch", "00:29:47.28" ], [
"20080506_120000_bbcone_bbc_news_at_one", "00:30:04.20" ], [ "20080506_120000_bbctwo_schools_maths_channel", "00:09:25.32" ], [
"20080506_121000_bbctwo_schools_primary_geography", "00:20:33.36" ], [ "20080506_140500_bbcone_space_pirates", "00:28:50.20" ], [
"20080506_141500_bbctwo_through_the_keyhole", "00:30:19.32" ], [ "20080506_143500_bbcone_chucklevision", "00:15:47.32" ], [
"20080506_144500_bbctwo_flog_it", "00:45:28.16" ], [ "20080506_153000_bbctwo_ready_steady_cook", "00:44:42.64" ], [
"20080506_153500_bbcone_blue_peter", "00:25:34.32" ], [ "20080506_160000_bbcone_newsround", "00:11:11.84" ], [
"20080506_161500_bbcone_the_weakest_link", "00:45:27.24" ], [ "20080506_161500_bbctwo_escape_to_the_country", "00:44:29.28" ], [
"20080506_170000_bbcone_bbc_news_at_six", "00:29:23.28" ], [ "20080506_170000_bbctwo_eggheads", "00:29:40.40" ], [
"20080506_173000_bbcone_bbc_london_news", "00:29:24.24" ], [ "20080506_173000_bbctwo_great_british_menu", "00:30:19.20" ], [
"20080506_180000_bbcone_the_one_show", "00:32:07.32" ], [ "20080506_180000_bbcthree_dog_borstal", "00:58:41.32" ], [
"20080506_183000_bbcfour_pop_goes_the_sixties", "00:04:21.28" ], [ "20080506_183000_bbcone_eastenders", "00:30:12.28" ], [
"20080506_190000_bbcfour_bbc_young_musician_of_the_year_2008", "00:59:17.32" ], [ "20080506_190000_bbcone_holby_city",
"00:59:02.24" ], [ "20080506_190000_bbctwo_natural_world", "00:49:58.28" ], [ "20080506_195000_bbctwo_watching_the_wild",
"00:10:06.40" ], [ "20080506_200000_bbcfour_chinese_school", "01:00:54.40" ], [ "20080506_200000_bbctwo_age_of_terror",
"00:59:41.12" ], [ "20080506_210000_bbcone_bbc_news_at_ten", "00:28:43.20" ], [ "20080506_210000_bbcthree_eastenders",
"00:27:13.20" ], [ "20080506_210000_bbctwo_later_live_with_jools_holland", "00:31:42.20" ], [
"20080506_213000_bbcthree_scallywagga", "00:30:43.20" ], [ "20080506_213000_bbctwo_newsnight", "00:50:11.28" ], [
"20080506_220000_bbcthree_the_wall", "00:44:56.24" ], [ "20080506_233000_bbcone_weatherview", "00:05:03.24" ], [
"20080506_233500_bbcfour_chinese_school", "01:00:22.48" ], [ "20080506_233500_bbcone_don_t_leave_me_this_way", "00:28:31.36" ], [
"20080507_000500_bbcone_the_primary", "01:00:54.24" ], [ "20080507_003000_bbcthree_scallywagga", "00:28:58.28" ], [
"20080507_003500_bbcfour_bbc_young_musician_of_the_year_2008", "00:59:47.44" ], [ "20080507_005500_bbcthree_the_wall",
"00:44:36.28" ], [ "20080507_013500_bbcfour_the_book_quiz", "00:30:20.60" ], [ "20080507_020500_bbcfour_chinese_school",
"00:59:54.16" ], [ "20080507_024000_bbcthree_dog_borstal", "00:56:42.28" ], [
"20080507_030000_bbctwo_shakespeare_the_animated_tales", "01:58:43.88" ], [ "20080507_050000_bbcone_breakfast", "03:14:59.28" ],
[ "20080507_050000_bbctwo_tikkabilla", "00:30:53.28" ], [ "20080507_062500_bbctwo_newsround", "00:04:04.28" ], [
"20080507_063000_bbctwo_hider_in_the_house", "01:03:36.24" ], [ "20080507_073000_bbctwo_jackanory_junior", "00:15:06.24" ], [
"20080507_080000_bbctwo_boogie_beebies", "00:15:42.24" ], [ "20080507_081500_bbcone_missing_live", "00:45:11.32" ], [
"20080507_093000_bbctwo_a_picture_of_britain", "00:59:56.32" ], [ "20080507_100000_bbcone_to_buy_or_not_to_buy", "00:44:53.32" ],
[ "20080507_103000_bbctwo_the_daily_politics", "01:30:12.32" ], [ "20080507_104500_bbcone_cash_in_the_attic", "00:29:33.48" ], [
"20080507_111500_bbcone_bargain_hunt", "00:44:10.24" ], [ "20080507_120000_bbcone_bbc_news_at_one", "00:31:13.32" ], [
"20080507_120000_bbctwo_see_hear", "00:30:11.24" ], [ "20080507_123000_bbctwo_working_lunch", "00:29:56.24" ], [
"20080507_124000_bbcone_doctors", "00:28:26.40" ], [ "20080507_130000_bbctwo_animal_park", "00:29:46.32" ], [
"20080507_140500_bbcone_space_pirates", "00:28:46.60" ], [ "20080507_141500_bbctwo_through_the_keyhole", "00:29:22.28" ], [
"20080507_143500_bbcone_chucklevision", "00:15:25.44" ], [ "20080507_144500_bbctwo_flog_it", "00:44:54.44" ], [
"20080507_151000_bbcone_young_dracula", "00:28:19.32" ], [ "20080507_153500_bbcone_blue_peter", "00:24:48.32" ], [
"20080507_160000_bbcone_newsround", "00:11:04.16" ], [ "20080507_161500_bbcone_the_weakest_link", "00:45:31.44" ], [
"20080507_161500_bbctwo_escape_to_the_country", "00:44:34.28" ], [ "20080507_170000_bbcone_bbc_news_at_six", "00:29:16.28" ], [
"20080507_170000_bbctwo_eggheads", "00:29:32.84" ], [ "20080507_173000_bbcone_bbc_london_news", "00:28:22.20" ], [
"20080507_173000_bbctwo_great_british_menu", "00:29:45.24" ], [ "20080507_180000_bbcfour_world_news_today", "00:30:49.40" ], [
"20080507_180000_bbcone_the_one_show", "00:29:59.28" ], [ "20080507_180000_bbcthree_holby_city", "00:58:11.12" ], [
"20080507_183000_bbcfour_pop_goes_the_sixties", "00:04:11.20" ], [ "20080507_183000_bbcone_street_doctor", "00:31:29.36" ], [
"20080507_190000_bbcfour_bbc_young_musician_of_the_year_2008", "01:00:49.24" ], [ "20080507_190000_bbcthree_glamour_girls",
"00:29:01.32" ], [ "20080507_190000_bbctwo_city_salute_with_princes_william_and", "01:01:21.36" ], [
"20080507_200000_bbcfour_clarissa_and_the_king_s_cookbook", "00:29:59.32" ], [ "20080507_200000_bbcone_the_apprentice",
"00:59:58.44" ], [ "20080507_200000_bbcthree_two_pints_of_lager_and", "00:30:02.28" ], [
"20080507_200000_bbctwo_dan_cruickshank_s_adventures", "00:59:39.32" ], [
"20080507_203000_bbcfour_illuminations_treasures_of_the", "00:29:24.64" ], [ "20080507_203000_bbcthree_ideal", "00:29:55.32" ], [
"20080507_210000_bbcfour_my_secret_agent_auntie_storyville", "01:00:23.32" ], [ "20080507_210000_bbcone_bbc_news_at_ten",
"00:27:05.28" ], [ "20080507_210000_bbctwo_the_apprentice_you_re_fired", "00:32:04.20" ], [ "20080507_213000_bbctwo_newsnight",
"00:49:55.32" ], [ "20080507_222000_bbctwo_parallel_worlds_parallel_lives", "01:01:23.36" ], [
"20080507_230000_bbcfour_clarissa_and_the_king_s_cookbook", "00:29:38.44" ], [ "20080507_232000_bbcthree_two_pints_of_lager_and",
"00:30:23.32" ], [ "20080507_233000_bbcfour_illuminations_treasures_of_the", "00:30:23.16" ], [ "20080507_235000_bbcthree_ideal",
"00:29:37.24" ], [ "20080508_000000_bbcfour_bbc_young_musician_of_the_year_2008", "01:00:30.52" ], [
"20080508_002000_bbcone_weatherview", "00:04:15.40" ], [ "20080508_002500_bbcone_seaside_rescue", "00:29:41.16" ], [
"20080508_005000_bbcthree_the_wall", "00:45:15.04" ], [ "20080508_005500_bbcone_meet_the_immigrants", "00:30:12.28" ], [
"20080508_010000_bbcfour_my_secret_agent_auntie_storyville", "01:00:45.40" ], [ "20080508_013500_bbcthree_glamour_girls",
"00:29:24.32" ], [ "20080508_020000_bbcfour_clarissa_and_the_king_s_cookbook", "00:28:45.80" ], [
"20080508_023000_bbcfour_illuminations_treasures_of_the", "00:27:54.40" ], [
"20080508_030000_bbctwo_shakespeare_the_animated_tales", "01:58:57.68" ], [ "20080508_050000_bbcone_breakfast", "03:15:02.28" ],
[ "20080508_062500_bbctwo_newsround", "00:04:02.32" ], [ "20080508_063000_bbctwo_hider_in_the_house", "01:03:25.36" ], [
"20080508_073000_bbctwo_jackanory_junior", "00:15:28.28" ], [ "20080508_080000_bbctwo_boogie_beebies", "00:15:43.24" ], [
"20080508_081500_bbcone_missing_live", "00:44:30.24" ], [ "20080508_084000_bbctwo_something_special", "00:21:13.16" ], [
"20080508_093000_bbctwo_schools_primary_history", "00:19:14.16" ], [ "20080508_095000_bbctwo_schools_megamaths", "00:19:23.16" ],
[ "20080508_100000_bbcone_to_buy_or_not_to_buy", "00:44:34.12" ], [ "20080508_101000_bbctwo_schools_primary_geography",
"00:20:58.40" ], [ "20080508_103000_bbctwo_schools_science_clips", "00:09:17.36" ], [
"20080508_104000_bbctwo_schools_science_clips", "00:09:16.24" ], [ "20080508_104500_bbcone_cash_in_the_attic", "00:30:59.20" ], [
"20080508_105000_bbctwo_schools_hands_up", "00:11:09.40" ], [ "20080508_110000_bbctwo_the_daily_politics", "00:30:46.24" ], [
"20080508_111500_bbcone_bargain_hunt", "00:43:46.24" ], [ "20080508_113000_bbctwo_working_lunch", "00:29:49.08" ], [
"20080508_120000_bbcone_bbc_news_at_one", "00:30:43.32" ], [ "20080508_120000_bbctwo_open_gardens", "00:29:48.36" ], [
"20080508_124000_bbcone_doctors", "00:28:40.28" ], [ "20080508_140500_bbcone_space_pirates", "00:28:31.28" ], [
"20080508_141500_bbctwo_through_the_keyhole", "00:29:45.16" ], [ "20080508_143500_bbcone_chucklevision", "00:15:32.40" ], [
"20080508_144500_bbctwo_flog_it", "00:44:56.36" ], [ "20080508_150500_bbcone_stake_out", "00:28:18.16" ], [
"20080508_153000_bbctwo_ready_steady_cook", "00:45:30.32" ], [ "20080508_153500_bbcone_hider_in_the_house", "00:28:21.24" ], [
"20080508_160000_bbcone_newsround", "00:11:14.32" ], [ "20080508_161500_bbcone_the_weakest_link", "00:45:05.44" ], [
"20080508_161500_bbctwo_escape_to_the_country", "00:44:34.20" ], [ "20080508_170000_bbcone_bbc_news_at_six", "00:28:51.36" ], [
"20080508_170000_bbctwo_eggheads", "00:29:51.48" ], [ "20080508_173000_bbcone_bbc_london_news", "00:27:37.28" ], [
"20080508_173000_bbctwo_great_british_menu", "00:30:23.32" ], [ "20080508_175500_bbcone_disasters_emergency_committee_myanmar",
"00:04:14.24" ], [ "20080508_180000_bbcfour_world_news_today", "00:27:06.40" ], [ "20080508_180000_bbcone_the_one_show",
"00:30:29.28" ], [ "20080508_182700_bbcfour_disasters_emergency_committee_myanmar", "00:06:31.24" ], [
"20080508_183000_bbcfour_in_search_of_medieval_britain", "00:29:43.48" ], [ "20080508_183000_bbcone_eastenders", "00:31:14.20" ],
[ "20080508_183000_bbctwo_women_in_black", "00:29:48.28" ], [ "20080508_185700_bbcthree_disasters_emergency_committee_myanmar",
"00:02:50.40" ], [ "20080508_190000_bbcfour_bbc_young_musician_of_the_year_2008", "01:00:34.36" ], [
"20080508_190000_bbcone_holby_blue", "00:58:43.36" ], [ "20080508_190000_bbctwo_living_the_dream_revisited", "01:00:52.32" ], [
"20080508_200000_bbcfour_inside_the_medieval_mind", "00:59:59.32" ], [ "20080508_200000_bbcone_the_invisibles", "00:58:35.40" ],
[ "20080508_200000_bbcthree_page_three_teens", "00:58:17.20" ], [ "20080508_204500_bbctwo_heroes_unmasked", "00:17:51.16" ], [
"20080508_205700_bbcthree_disasters_emergency_committee_myanmar", "00:03:43.40" ], [ "20080508_210000_bbcfour_crusades",
"00:49:35.44" ], [ "20080508_210000_bbcone_bbc_news_at_ten", "00:26:30.24" ], [ "20080508_210000_bbcthree_eastenders",
"00:29:03.28" ], [ "20080508_212700_bbctwo_disasters_emergency_committee_myanmar", "00:03:05.12" ], [
"20080508_213000_bbctwo_newsnight", "00:49:08.28" ], [ "20080508_213500_bbcone_disasters_emergency_committee_myanmar",
"00:04:13.16" ], [ "20080508_222000_bbctwo_two_pints_of_lager_and", "00:28:45.32" ], [ "20080508_224000_bbcfour_chinese_school",
"01:00:45.28" ], [ "20080508_224000_bbcone_this_week", "00:44:50.40" ], [ "20080508_232500_bbcone_holiday_weather", "00:04:45.28"
], [ "20080508_233000_bbcone_panorama", "00:29:58.32" ], [ "20080508_234000_bbcfour_inside_the_medieval_mind", "00:59:59.20" ], [
"20080508_235500_bbcthree_page_three_teens", "00:57:44.40" ], [ "20080509_000000_bbcone_johnny_s_new_kingdom", "00:29:48.24" ], [
"20080509_003000_bbcone_the_twenties_in_colour_the_wonderful", "00:34:07.32" ], [
"20080509_004000_bbcfour_bbc_young_musician_of_the_year_2008", "01:00:34.48" ], [
"20080509_013000_bbcone_bill_oddie_s_wild_side", "00:29:42.20" ], [ "20080509_014000_bbcfour_in_search_of_medieval_britain",
"00:30:34.36" ], [ "20080509_021000_bbcfour_inside_the_medieval_mind", "00:56:51.40" ], [ "20080509_024500_bbcthree_dog_borstal",
"00:26:50.40" ], [ "20080509_030000_bbctwo_shakespeare_shorts", "01:58:57.12" ], [ "20080509_050000_bbcone_breakfast",
"03:15:01.28" ], [ "20080509_050000_bbctwo_tikkabilla", "00:30:45.32" ], [ "20080509_062500_bbctwo_newsround", "00:03:48.08" ], [
"20080509_063000_bbctwo_hider_in_the_house", "01:03:46.28" ], [ "20080509_073000_bbctwo_jackanory_junior", "00:14:57.28" ], [
"20080509_080000_bbctwo_boogie_beebies", "00:15:47.36" ], [ "20080509_081500_bbcone_missing_live", "00:45:02.28" ], [
"20080509_084000_bbctwo_something_special", "00:22:42.24" ], [ "20080509_094500_bbctwo_schools_the_way_things_work",
"00:15:16.20" ], [ "20080509_100000_bbcone_to_buy_or_not_to_buy", "00:45:05.24" ], [
"20080509_100000_bbctwo_schools_the_way_things_work", "00:15:36.20" ], [ "20080509_104500_bbcone_cash_in_the_attic",
"00:29:41.40" ], [ "20080509_104500_bbctwo_schools_something_special", "00:15:24.12" ], [
"20080509_110000_bbctwo_the_daily_politics", "00:30:04.68" ], [ "20080509_111500_bbcone_bargain_hunt", "00:44:39.28" ], [
"20080509_113000_bbctwo_working_lunch", "01:01:11.24" ], [ "20080509_120000_bbcone_bbc_news_at_one", "00:30:58.20" ], [
"20080509_124000_bbcone_doctors", "00:28:54.48" ], [ "20080509_140500_bbcone_space_pirates", "00:28:58.24" ], [
"20080509_141500_bbctwo_through_the_keyhole", "00:29:32.40" ], [ "20080509_143500_bbcone_chucklevision", "00:15:33.28" ], [
"20080509_144500_bbctwo_flog_it", "00:44:57.44" ], [ "20080509_153000_bbctwo_ready_steady_cook", "00:44:43.36" ], [
"20080509_153500_bbcone_the_slammer", "00:29:01.48" ], [ "20080509_160000_bbcone_newsround", "00:10:42.28" ], [
"20080509_161500_bbcone_the_weakest_link", "00:45:13.32" ], [ "20080509_161500_bbctwo_escape_to_the_country", "00:44:54.24" ], [
"20080509_170000_bbcone_bbc_news_at_six", "00:28:56.28" ], [ "20080509_170000_bbctwo_eggheads", "00:30:18.88" ], [
"20080509_173000_bbcone_bbc_london_news", "00:28:36.12" ], [ "20080509_180000_bbcfour_world_news_today", "00:31:04.96" ], [
"20080509_180000_bbcone_the_one_show", "00:30:21.28" ], [ "20080509_180000_bbcthree_top_gear", "01:01:32.40" ], [
"20080509_180000_bbctwo_wildebeest_the_super_herd", "00:30:16.28" ], [ "20080509_183000_bbcfour_transatlantic_sessions",
"00:29:31.60" ], [ "20080509_183000_bbcone_after_you_ve_gone", "00:31:07.32" ], [
"20080509_183000_bbctwo_the_trees_that_made_britain", "00:29:59.20" ], [
"20080509_190000_bbcfour_bbc_young_musician_of_the_year_2008", "00:59:24.28" ], [ "20080509_190000_bbcone_eastenders",
"00:30:11.32" ], [ "20080509_190000_bbcthree_last_man_standing", "00:58:01.36" ], [ "20080509_190000_bbctwo_gardeners_world",
"01:00:50.20" ], [ "20080509_193000_bbcone_a_question_of_sport", "00:30:29.44" ], [ "20080509_200000_bbcthree_doctor_who",
"00:45:17.28" ], [ "20080509_204500_bbcthree_doctor_who_confidential", "00:15:41.20" ], [
"20080509_210000_bbcfour_kings_of_cool_crooners", "00:49:55.24" ], [ "20080509_210000_bbcone_bbc_news_at_ten", "00:26:18.36" ], [
"20080509_210000_bbcthree_eastenders", "00:29:49.16" ], [ "20080509_213000_bbcthree_two_pints_of_lager_and", "00:31:21.28" ], [
"20080509_213000_bbctwo_newsnight", "00:30:51.24" ], [ "20080509_220000_bbcthree_two_pints_of_lager_and", "00:32:03.40" ], [
"20080509_220000_bbctwo_newsnight_review", "00:33:24.36" ], [ "20080509_223500_bbcone_national_lottery_euromillions_draw",
"00:04:25.24" ], [ "20080509_223500_bbctwo_later_with_jools_holland", "01:02:42.16" ], [ "20080509_232000_bbcthree_scallywagga",
"00:29:36.28" ], [ "20080509_235000_bbcthree_ideal", "00:29:55.36" ], [
"20080510_001000_bbcfour_bbc_young_musician_of_the_year_2008", "00:59:52.24" ], [ "20080510_001500_bbcone_weatherview",
"00:05:26.44" ], [ "20080510_002000_bbcone_dan_cruickshank_s_adventures", "00:59:58.36" ], [
"20080510_002000_bbcthree_two_pints_of_lager_and", "00:30:57.36" ], [ "20080510_005000_bbcthree_two_pints_of_lager_and",
"00:30:18.32" ], [ "20080510_012000_bbcthree_glamour_girls", "00:30:30.28" ], [ "20080510_024000_bbcfour_transatlantic_sessions",
"00:30:09.40" ], [ "20080510_025000_bbcthree_scallywagga", "00:28:22.40" ], [ "20080510_031500_bbcthree_ideal", "00:26:53.24" ],
[ "20080510_050000_bbcone_breakfast", "03:59:51.24" ], [ "20080510_050000_bbctwo_fimbles", "00:21:22.20" ], [
"20080510_052000_bbctwo_tikkabilla", "00:41:40.28" ], [ "20080510_070000_bbctwo_sorcerer_s_apprentice", "00:33:19.36" ], [
"20080510_090000_bbcone_saturday_kitchen", "01:30:10.32" ], [ "20080510_090000_bbctwo_hedz", "00:29:30.36" ], [
"20080510_093000_bbctwo_the_slammer", "00:29:05.24" ], [ "20080510_104500_bbctwo_sportsround", "00:15:33.32" ], [
"20080510_110000_bbcone_bbc_news", "00:09:39.36" ], [ "20080510_111000_bbcone_football_focus", "00:49:42.60" ], [
"20080510_120000_bbcone_racing_from_ascot_and_haydock", "01:35:19.36" ], [ "20080510_123000_bbctwo_the_surgery", "00:19:22.32" ],
[ "20080510_125000_bbctwo_sound", "00:31:56.36" ], [ "20080510_132000_bbctwo_the_sky_at_night", "00:20:52.16" ], [
"20080510_133500_bbcone_rugby_league_challenge_cup", "02:39:42.16" ], [ "20080510_161500_bbcone_bbc_news", "00:12:56.32" ], [
"20080510_162500_bbcone_bbc_london_news", "00:05:26.20" ], [ "20080510_163000_bbcone_outtake_tv", "00:29:52.12" ], [
"20080510_170000_bbcone_the_kids_are_all_right", "00:41:29.24" ], [ "20080510_174500_bbcone_doctor_who", "00:46:25.24" ], [
"20080510_180000_bbcfour_in_search_of_medieval_britain", "00:29:55.48" ], [ "20080510_180000_bbcthree_radio_1_s_big_weekend",
"00:27:36.36" ], [ "20080510_180000_bbctwo_dad_s_army", "00:29:16.28" ], [ "20080510_183000_bbcfour_the_book_quiz", "00:30:33.76"
], [ "20080510_183000_bbcone_i_d_do_anything", "00:59:44.20" ], [ "20080510_183000_bbcthree_doctor_who_confidential",
"00:44:31.32" ], [ "20080510_183000_bbctwo_secrets_of_the_forbidden_city", "01:30:59.24" ], [
"20080510_190000_bbcfour_a_perfect_spy", "00:58:49.48" ], [ "20080510_191500_bbcthree_top_gear", "01:02:04.28" ], [
"20080510_200000_bbcfour_a_perfect_spy", "00:57:20.44" ], [ "20080510_200000_bbctwo_have_i_got_a_bit_more_news_for_you",
"00:41:05.24" ], [ "20080510_202000_bbcone_casualty", "00:51:18.32" ], [ "20080510_204000_bbctwo_comedy_map_of_britain",
"01:00:13.32" ], [ "20080510_205500_bbcfour_a_perfect_spy", "00:57:43.36" ], [ "20080510_205500_bbcthree_two_pints_of_lager_and",
"00:30:03.28" ], [ "20080510_211000_bbcone_love_soup", "00:29:55.32" ], [ "20080510_212500_bbcthree_two_pints_of_lager_and",
"00:55:30.36" ], [ "20080510_214000_bbcone_bbc_news", "00:20:44.24" ], [ "20080510_214000_bbctwo_the_apprentice", "00:59:53.28"
], [ "20080510_215500_bbcfour_christina_a_medieval_life", "01:00:36.40" ], [
"20080510_224000_bbctwo_the_apprentice_you_re_fired", "00:31:48.20" ], [ "20080510_225500_bbcfour_how_to_build_a_cathedral",
"00:59:39.40" ], [ "20080510_231500_bbcthree_radio_1_s_big_weekend", "00:59:00.44" ], [
"20080510_235500_bbcfour_in_search_of_medieval_britain", "00:30:29.44" ], [ "20080511_001500_bbcthree_two_pints_of_lager_and",
"00:30:34.32" ], [ "20080511_002500_bbcfour_the_book_quiz", "00:29:55.24" ], [ "20080511_004500_bbcthree_two_pints_of_lager_and",
"00:29:38.36" ], [ "20080511_005500_bbcfour_christina_a_medieval_life", "01:00:23.32" ], [
"20080511_011500_bbcthree_glamour_girls", "00:29:38.16" ], [ "20080511_014500_bbcone_weatherview", "00:06:05.16" ], [
"20080511_014500_bbcthree_should_i_smoke_dope", "00:54:01.32" ], [ "20080511_015500_bbcfour_how_to_build_a_cathedral",
"00:58:40.16" ], [ "20080511_024000_bbcthree_scallywagga", "00:28:29.24" ], [ "20080511_030500_bbcthree_glamour_girls",
"00:28:24.28" ], [ "20080511_050000_bbcone_breakfast", "02:59:47.28" ], [ "20080511_050000_bbctwo_fimbles", "00:21:25.24" ], [
"20080511_052000_bbctwo_tikkabilla", "00:41:17.24" ], [ "20080511_070000_bbctwo_trapped", "00:28:58.28" ], [
"20080511_073000_bbctwo_raven_the_secret_temple", "00:29:07.24" ], [ "20080511_080000_bbcone_the_andrew_marr_show", "00:59:57.24"
], [ "20080511_080000_bbctwo_hider_in_the_house", "00:59:01.20" ], [ "20080511_090000_bbcone_in_the_light_of_the_spirit",
"01:00:46.28" ], [ "20080511_100000_bbcone_countryfile", "01:00:06.32" ], [ "20080511_110000_bbcone_the_politics_show",
"01:00:54.60" ], [ "20080511_113000_bbctwo_premiership_rugby", "00:30:22.12" ], [
"20080511_120000_bbctwo_rugby_league_challenge_cup", "02:00:04.36" ], [ "20080511_123000_bbcone_eastenders_omnibus",
"01:51:57.24" ], [ "20080511_140000_bbctwo_world_cup_rowing", "01:00:50.32" ], [
"20080511_142500_bbcone_a_heroes_welcome_the_windsor_castle", "01:01:36.28" ], [ "20080511_150000_bbctwo_paralympic_world_cup",
"01:48:54.12" ], [ "20080511_152500_bbcone_final_score", "00:54:28.96" ], [ "20080511_162000_bbcone_points_of_view",
"00:14:49.40" ], [ "20080511_163500_bbcone_songs_of_praise", "00:35:14.16" ], [ "20080511_165000_bbctwo_storm_geese",
"00:09:48.24" ], [ "20080511_170000_bbctwo_bbc_young_musician_of_the_year_2008", "02:04:19.32" ], [
"20080511_171000_bbcone_bbc_news", "00:17:40.32" ], [ "20080511_172500_bbcone_bbc_london_news", "00:09:02.20" ], [
"20080511_173500_bbcone_seaside_rescue", "00:30:58.20" ], [ "20080511_180000_bbcfour_inside_the_medieval_mind", "01:00:11.56" ],
[ "20080511_180000_bbcthree_radio_1_s_big_weekend", "00:59:35.28" ], [ "20080511_183500_bbcone_i_d_do_anything_results",
"00:30:19.12" ], [ "20080511_190000_bbcthree_doctor_who", "00:45:53.28" ], [ "20080511_190500_bbctwo_wild_china", "01:00:25.28"
], [ "20080511_193000_bbcfour_terry_jones_medieval_lives", "00:30:21.28" ], [ "20080511_194500_bbcthree_doctor_who_confidential",
"00:44:30.40" ], [ "20080511_200000_bbcfour_christina_a_medieval_life", "00:59:58.96" ], [ "20080511_200500_bbctwo_coast",
"00:59:26.36" ], [ "20080511_203000_bbcthree_little_britain", "00:28:54.24" ], [ "20080511_210000_bbcone_bbc_news", "00:20:12.32"
], [ "20080511_210500_bbctwo_russia_a_journey_with_jonathan_dimbleby", "01:00:34.40" ], [
"20080511_212000_bbcone_match_of_the_day", "01:32:12.36" ], [ "20080511_214500_bbcthree_scallywagga", "00:28:45.24" ], [
"20080511_220500_bbctwo_graham_norton_uncut", "00:45:48.32" ], [ "20080511_221000_bbcthree_radio_1_s_big_weekend", "01:29:30.32"
], [ "20080511_224500_bbcfour_kings_of_cool_crooners", "00:41:40.36" ], [
"20080511_225000_bbctwo_a_game_of_two_eras_1957_v_2007", "00:59:56.24" ], [ "20080511_234000_bbcthree_sound", "00:30:40.24" ] , ]

reH = re.compile("(\d\d):(\d\d):(\d\d).(\d+)")
def h2Sec(h): 
  m = reH.match(h)
  return int(m.group(1)) * 3600 + int(m.group(2)) * 60 + int(m.group(3)) + 1

videoFiles = dict( map(lambda x: (x[0], (x[1], h2Sec(x[1]))), videoFiles))


duplicates = [
  [u'20080404_233000_bbcthree_gavin_and_stacey', u'20080405_031000_bbcthree_gavin_and_stacey', u'20080405_221500_bbcthree_gavin_and_stacey'] ,
  [u'20080405_000000_bbcthree_gavin_and_stacey', u'20080405_224500_bbcthree_gavin_and_stacey'] ,
  [u'20080405_003000_bbcthree_gavin_and_stacey', u'20080405_231500_bbcthree_gavin_and_stacey'] ,
  [u'20080405_010000_bbcthree_gavin_and_stacey', u'20080405_234500_bbcthree_gavin_and_stacey'] ,
  [u'20080406_200000_bbcthree_gavin_and_stacey', u'20080406_224500_bbcthree_gavin_and_stacey', u'20080407_213000_bbcthree_gavin_and_stacey', u'20080408_001500_bbcthree_gavin_and_stacey', u'20080411_231500_bbcthree_gavin_and_stacey', u'20080412_014000_bbcthree_gavin_and_stacey', u'20080412_222000_bbcthree_gavin_and_stacey', u'20080413_013000_bbcthree_gavin_and_stacey'] ,
  [u'20080413_200000_bbcthree_gavin_and_stacey', u'20080413_224500_bbcthree_gavin_and_stacey', u'20080414_213000_bbcthree_gavin_and_stacey', u'20080418_231500_bbcthree_gavin_and_stacey', u'20080419_014000_bbcthree_gavin_and_stacey', u'20080419_221000_bbcthree_gavin_and_stacey', u'20080420_002000_bbcthree_gavin_and_stacey'] ,
  [u'20080420_200000_bbcthree_gavin_and_stacey', u'20080420_224500_bbcthree_gavin_and_stacey', u'20080421_213000_bbcthree_gavin_and_stacey', u'20080422_004000_bbcthree_gavin_and_stacey', u'20080425_230500_bbcthree_gavin_and_stacey', u'20080426_023000_bbcthree_gavin_and_stacey', u'20080426_220000_bbcthree_gavin_and_stacey', u'20080427_004500_bbcthree_gavin_and_stacey'] ,
  [u'20080401_002000_bbcthree_pulling', u'20080403_234500_bbcthree_pulling', u'20080404_022500_bbcthree_pulling'] ,
  [u'20080406_203000_bbcthree_pulling', u'20080406_231500_bbcthree_pulling', u'20080407_220000_bbcthree_pulling', u'20080408_004500_bbcthree_pulling', u'20080410_231000_bbcthree_pulling', u'20080411_014000_bbcthree_pulling'] ,
  [u'20080413_203000_bbcthree_pulling', u'20080413_231500_bbcthree_pulling', u'20080414_220000_bbcthree_pulling', u'20080415_004000_bbcthree_pulling', u'20080418_013500_bbcthree_pulling'] ,
  [u'20080420_203000_bbcthree_pulling', u'20080420_231500_bbcthree_pulling', u'20080421_220000_bbcthree_pulling', u'20080422_011000_bbcthree_pulling', u'20080424_230000_bbcthree_pulling', u'20080425_015500_bbcthree_pulling'] ,
  [u'20080427_200000_bbcthree_pulling', u'20080427_224500_bbcthree_pulling', u'20080428_220000_bbcthree_pulling', u'20080429_011500_bbcthree_pulling', u'20080501_225500_bbcthree_pulling', u'20080502_012000_bbcthree_pulling'] ,
  [u'20080414_190000_bbcone_eastenders', u'20080414_210000_bbcthree_eastenders'] ,
  [u'20080415_183000_bbcone_eastenders', u'20080415_210000_bbcthree_eastenders'] ,
  [u'20080417_183000_bbcone_eastenders', u'20080417_210000_bbcthree_eastenders'] ,
  [u'20080418_190000_bbcone_eastenders', u'20080418_210000_bbcthree_eastenders'] ,
  [u'20080421_190000_bbcone_eastenders', u'20080421_210000_bbcthree_eastenders'] ,
  [u'20080422_183000_bbcone_eastenders', u'20080422_210000_bbcthree_eastenders'] ,
  [u'20080424_183000_bbcone_eastenders', u'20080424_210000_bbcthree_eastenders'] ,
  [u'20080425_190000_bbcone_eastenders', u'20080425_210000_bbcthree_eastenders'] ,
  [u'20080428_190000_bbcone_eastenders', u'20080428_210000_bbcthree_eastenders'] ,
  [u'20080429_183000_bbcone_eastenders', u'20080429_210000_bbcthree_eastenders'] ,
  [u'20080505_190000_bbcone_eastenders', u'20080505_210000_bbcthree_eastenders'] ,
  [u'20080506_183000_bbcone_eastenders', u'20080506_210000_bbcthree_eastenders'] ,
  [u'20080508_183000_bbcone_eastenders', u'20080508_210000_bbcthree_eastenders'] ,
  [u'20080509_190000_bbcone_eastenders', u'20080509_210000_bbcthree_eastenders'] ,
  [u'20080402_120000_bbctwo_see_hear', u'20080408_235500_bbcone_see_hear'] ,
  [u'20080405_200000_bbcthree_two_pints_of_lager_outtakes', u'20080407_180000_bbcthree_two_pints_of_lager_outtakes'] ,
  [u'20080406_024500_bbcthree_two_pints_of_lager_outtakes', u'20080408_014500_bbcthree_two_pints_of_lager_outtakes'] ,
  [u'20080404_193000_bbcthree_eastenders_ricky_and_bianca', u'20080404_214500_bbcthree_eastenders_ricky_and_bianca', u'20080406_014500_bbcthree_eastenders_ricky_and_bianca', u'20080406_180000_bbcthree_eastenders_ricky_and_bianca', u'20080407_004000_bbcthree_eastenders_ricky_and_bianca'] ,
  [u'20080412_120500_bbctwo_sound', u'20080413_180000_bbcthree_sound'] ,
  [u'20080419_120500_bbctwo_sound', u'20080420_180000_bbcthree_sound'] ,
  [u'20080426_125000_bbctwo_sound', u'20080427_180000_bbcthree_sound'] ,
  [u'20080403_201500_bbcthree_doctor_who_confidential_kylie_special', u'20080404_011000_bbcthree_doctor_who_confidential_kylie_special'] ,
  [u'20080401_200000_bbcfour_tv_s_believe_it_or_not', u'20080402_021500_bbcfour_tv_s_believe_it_or_not'] ,
  [u'20080510_125000_bbctwo_sound', u'20080511_234000_bbcthree_sound'] ,
  [u'20080502_200000_bbcfour_james_taylor_one_man_band', u'20080504_214500_bbcfour_james_taylor_one_man_band'] ,
  [u'20080430_200000_bbcthree_two_pints_of_lager_and', u'20080430_233500_bbcthree_two_pints_of_lager_and'] ,
  [u'20080507_200000_bbcthree_two_pints_of_lager_and', u'20080507_232000_bbcthree_two_pints_of_lager_and'] ,
  [u'20080405_181000_bbcthree_doctor_who_confidential', u'20080406_030000_bbcthree_doctor_who_confidential'] ,
  [u'20080426_180500_bbcthree_doctor_who_confidential', u'20080427_020000_bbcthree_doctor_who_confidential'] ,
  [u'20080510_183000_bbcthree_doctor_who_confidential', u'20080511_194500_bbcthree_doctor_who_confidential'] ,
  [u'20080404_205000_bbctwo_torchwood_declassified', u'20080415_185000_bbctwo_torchwood_declassified'] ,
  [u'20080413_195000_bbcthree_doctor_who_confidential', u'20080414_033500_bbcthree_doctor_who_confidential', u'20080418_205000_bbcthree_doctor_who_confidential'] ,
  [u'20080420_194500_bbcthree_doctor_who_confidential', u'20080421_031000_bbcthree_doctor_who_confidential'] ,
  [u'20080427_194500_bbcthree_doctor_who_confidential', u'20080428_031000_bbcthree_doctor_who_confidential', u'20080502_204500_bbcthree_doctor_who_confidential'] ,
  [u'20080425_190000_bbcthree_almost_famous', u'20080425_233500_bbcthree_almost_famous'] ,
  [u'20080401_190000_bbcfour_tv_s_believe_it_or_not', u'20080402_011500_bbcfour_tv_s_believe_it_or_not'] ,
  [u'20080402_200000_bbcone_the_apprentice', u'20080405_221500_bbctwo_the_apprentice'] ,
  [u'20080409_200000_bbcone_the_apprentice', u'20080412_234500_bbctwo_the_apprentice'] ,
  [u'20080416_200000_bbcone_the_apprentice', u'20080419_214500_bbctwo_the_apprentice'] ,
  [u'20080423_200000_bbcone_the_apprentice', u'20080426_214500_bbctwo_the_apprentice'] ,
  [u'20080507_200000_bbcone_the_apprentice', u'20080510_214000_bbctwo_the_apprentice'] ,
  [u'20080402_003000_bbcthree_lily_allen_and_friends', u'20080404_203000_bbcthree_lily_allen_and_friends', u'20080405_013000_bbcthree_lily_allen_and_friends', u'20080406_010000_bbcthree_lily_allen_and_friends'] ,
  [u'20080409_231500_bbcthree_the_wall', u'20080413_020000_bbcthree_the_wall'] ,
  [u'20080415_220000_bbcthree_the_wall', u'20080416_231500_bbcthree_the_wall', u'20080417_024000_bbcthree_the_wall', u'20080420_005000_bbcthree_the_wall'] ,
  [u'20080422_220000_bbcthree_the_wall', u'20080423_232000_bbcthree_the_wall', u'20080424_023000_bbcthree_the_wall', u'20080427_011500_bbcthree_the_wall'] ,
  [u'20080429_220000_bbcthree_the_wall', u'20080430_005500_bbcthree_the_wall', u'20080501_010500_bbcthree_the_wall', u'20080504_014500_bbcthree_the_wall'] ,
  [u'20080506_220000_bbcthree_the_wall', u'20080507_005500_bbcthree_the_wall', u'20080508_005000_bbcthree_the_wall'] ,
  [u'20080401_010000_bbcfour_the_book_quiz', u'20080401_023000_bbcfour_the_book_quiz', u'20080401_223000_bbcfour_the_book_quiz'] ,
  [u'20080407_193000_bbcfour_the_book_quiz', u'20080408_000500_bbcfour_the_book_quiz', u'20080408_013500_bbcfour_the_book_quiz', u'20080408_220000_bbcfour_the_book_quiz', u'20080409_024500_bbcfour_the_book_quiz', u'20080412_184000_bbcfour_the_book_quiz', u'20080413_015500_bbcfour_the_book_quiz'] ,
  [u'20080414_193000_bbcfour_the_book_quiz', u'20080415_011500_bbcfour_the_book_quiz', u'20080418_010500_bbcfour_the_book_quiz', u'20080419_190500_bbcfour_the_book_quiz'] ,
  [u'20080421_193000_bbcfour_the_book_quiz', u'20080422_014000_bbcfour_the_book_quiz', u'20080425_010500_bbcfour_the_book_quiz', u'20080426_184000_bbcfour_the_book_quiz', u'20080427_014500_bbcfour_the_book_quiz'] ,
  [u'20080428_193000_bbcfour_the_book_quiz', u'20080429_000000_bbcfour_the_book_quiz', u'20080429_014000_bbcfour_the_book_quiz', u'20080502_011000_bbcfour_the_book_quiz', u'20080504_022500_bbcfour_the_book_quiz'] ,
  [u'20080505_180000_bbcfour_the_book_quiz', u'20080506_012000_bbcfour_the_book_quiz', u'20080506_025000_bbcfour_the_book_quiz', u'20080507_013500_bbcfour_the_book_quiz', u'20080510_183000_bbcfour_the_book_quiz', u'20080511_002500_bbcfour_the_book_quiz'] ,
  [u'20080410_213000_bbcthree_lily_allen_my_favourite_bits', u'20080411_004000_bbcthree_lily_allen_my_favourite_bits', u'20080411_234500_bbcthree_lily_allen_my_favourite_bits', u'20080412_024000_bbcthree_lily_allen_my_favourite_bits', u'20080413_003000_bbcthree_lily_allen_my_favourite_bits', u'20080413_210000_bbcthree_lily_allen_my_favourite_bits'] ,
  [u'20080401_013000_bbcfour_legends_marty_feldman_six_degrees_of', u'20080406_230500_bbcfour_legends_marty_feldman_six_degrees_of', u'20080407_020500_bbcfour_legends_marty_feldman_six_degrees_of'] ,
  [u'20080420_210000_bbcthree_amy_my_body_for_bucks', u'20080420_234500_bbcthree_amy_my_body_for_bucks', u'20080423_200000_bbcthree_amy_my_body_for_bucks'] ,
  [u'20080428_193000_bbcone_panorama', u'20080501_231000_bbctwo_panorama'] ,
  [u'20080414_193000_bbcone_panorama', u'20080417_232500_bbcone_panorama'] ,
  [u'20080407_193000_bbcone_panorama', u'20080410_235000_bbcone_panorama'] ,
  [u'20080425_210000_bbcfour_bbc_four_sessions_van_morrison', u'20080426_001000_bbcfour_bbc_four_sessions_van_morrison', u'20080427_215000_bbcfour_bbc_four_session_van_morrison'] ,
  [u'20080421_200000_bbcthree_natalie_cassidy_s_diet_secrets', u'20080423_012500_bbcthree_natalie_cassidy_s_diet_secrets', u'20080428_004500_bbcthree_natalie_cassidy_s_diet_secrets', u'20080429_190000_bbcthree_natalie_cassidy_s_diet_secrets', u'20080430_014000_bbcthree_natalie_cassidy_s_diet_secrets'] ,
  [u'20080405_205000_bbcfour_verity_lambert_drama_queen', u'20080406_012500_bbcfour_verity_lambert_drama_queen', u'20080407_200000_bbcfour_verity_lambert_drama_queen', u'20080408_003500_bbcfour_verity_lambert_drama_queen', u'20080408_020500_bbcfour_verity_lambert_drama_queen'] ,
  [u'20080427_001500_bbcfour_selling_power_the_admen_and_no_10', u'20080427_021500_bbcfour_selling_power_the_admen_and_no_10'] ,
  [u'20080420_100000_bbcone_countryfile', u'20080427_100000_bbcone_countryfile'] ,
  [u'20080421_200000_bbcfour_how_to_build_a_cathedral', u'20080421_234000_bbcfour_how_to_build_a_cathedral', u'20080422_021000_bbcfour_how_to_build_a_cathedral', u'20080424_010500_bbcfour_how_to_build_a_cathedral', u'20080427_190000_bbcfour_how_to_build_a_cathedral', u'20080428_005000_bbcfour_how_to_build_a_cathedral', u'20080510_225500_bbcfour_how_to_build_a_cathedral', u'20080511_015500_bbcfour_how_to_build_a_cathedral'] ,
  [u'20080416_120000_bbctwo_see_hear', u'20080422_235500_bbcone_see_hear'] ,
  [u'20080406_231500_bbcone_the_sky_at_night', u'20080412_123500_bbctwo_the_sky_at_night'] ,
  [u'20080504_234500_bbcone_the_sky_at_night', u'20080510_132000_bbctwo_the_sky_at_night'] ,
  [u'20080407_190000_bbcfour_the_sky_at_night', u'20080407_233500_bbcfour_the_sky_at_night', u'20080410_231500_bbcfour_the_sky_at_night'] ,
  [u'20080411_180000_bbcone_the_one_show', u'20080414_180000_bbcone_the_one_show'] ,
  [u'20080425_220000_bbcfour_van_morrison_on_later', u'20080426_021000_bbcfour_van_morrison_on_later'] ,
  [u'20080406_190000_bbcone_tiger_spy_in_the_jungle', u'20080423_002500_bbcone_tiger_spy_in_the_jungle'] ,
  [u'20080413_190000_bbcone_tiger_spy_in_the_jungle', u'20080420_150500_bbcone_tiger_spy_in_the_jungle', u'20080430_000000_bbcone_tiger_spy_in_the_jungle'] ,
  [u'20080416_200000_bbcfour_the_saint_and_the_hanged_man', u'20080416_235000_bbcfour_the_saint_and_the_hanged_man', u'20080417_021000_bbcfour_the_saint_and_the_hanged_man', u'20080420_001000_bbcfour_the_saint_and_the_hanged_man'] ,
  [u'20080403_224500_bbcfour_david_ogilvy_the_first_mad_man', u'20080501_005000_bbcfour_david_ogilvy_original_mad_man', u'20080504_005500_bbcfour_david_ogilvy_original_mad_man'] ,
  [u'20080407_200000_bbctwo_clowns', u'20080422_005000_bbcone_clowns'] ,
  [u'20080421_193000_bbcthree_glamour_girls', u'20080421_231500_bbcthree_glamour_girls', u'20080423_190000_bbcthree_glamour_girls', u'20080424_003500_bbcthree_glamour_girls', u'20080426_231500_bbcthree_glamour_girls'] ,
  [u'20080428_193000_bbcthree_glamour_girls', u'20080428_231500_bbcthree_glamour_girls', u'20080430_190000_bbcthree_glamour_girls', u'20080501_015000_bbcthree_glamour_girls', u'20080503_011500_bbcthree_glamour_girls', u'20080504_001500_bbcthree_glamour_girls'] ,
  [u'20080505_193000_bbcthree_glamour_girls', u'20080505_231500_bbcthree_glamour_girls', u'20080507_190000_bbcthree_glamour_girls', u'20080508_013500_bbcthree_glamour_girls', u'20080510_012000_bbcthree_glamour_girls', u'20080511_011500_bbcthree_glamour_girls', u'20080511_030500_bbcthree_glamour_girls'] ,
  [u'20080407_180000_bbctwo_the_undercover_diplomat', u'20080417_235500_bbcone_the_undercover_diplomat'] ,
  [u'20080507_200000_bbcfour_clarissa_and_the_king_s_cookbook', u'20080507_230000_bbcfour_clarissa_and_the_king_s_cookbook', u'20080508_020000_bbcfour_clarissa_and_the_king_s_cookbook'] ,
  [u'20080418_203000_bbcfour_amazing_journey_the_story_of_the_who', u'20080420_215000_bbcfour_amazing_journey_the_story_of_the_who'] ,
  [u'20080505_200000_bbcfour_christina_a_medieval_life', u'20080505_225000_bbcfour_christina_a_medieval_life', u'20080506_015000_bbcfour_christina_a_medieval_life', u'20080510_215500_bbcfour_christina_a_medieval_life', u'20080511_005500_bbcfour_christina_a_medieval_life', u'20080511_200000_bbcfour_christina_a_medieval_life'] ,
  [u'20080404_004500_bbcfour_the_lost_world_of_tibet', u'20080412_183000_bbctwo_the_lost_world_of_tibet'] ,
  [u'20080401_213000_bbcfour_a_year_in_tibet', u'20080403_001000_bbcfour_a_year_in_tibet'] ,
  [u'20080403_200000_bbcfour_a_year_in_tibet', u'20080404_021500_bbcfour_a_year_in_tibet', u'20080408_210000_bbcfour_a_year_in_tibet'] ,
  [u'20080401_193000_bbctwo_johnny_s_new_kingdom', u'20080509_000000_bbcone_johnny_s_new_kingdom'] ,
  [u'20080501_232500_bbcthree_should_i_smoke_dope', u'20080511_014500_bbcthree_should_i_smoke_dope'] ,
  [u'20080415_200000_bbcthree_young_mums_mansion', u'20080415_232500_bbcthree_young_mums_mansion'] ,
  [u'20080414_180000_bbcthree_young_mums_mansion_friends_and_family', u'20080415_030500_bbcthree_young_mums_mansion_friends_and_family'] ,
  [u'20080507_210000_bbcfour_my_secret_agent_auntie_storyville', u'20080508_010000_bbcfour_my_secret_agent_auntie_storyville'] ,
  [u'20080408_200000_bbcfour_chinese_school', u'20080408_223000_bbcfour_chinese_school', u'20080409_014500_bbcfour_chinese_school', u'20080410_210000_bbcfour_chinese_school', u'20080411_004500_bbcfour_chinese_school'] ,
  [u'20080415_200000_bbcfour_chinese_school', u'20080415_235000_bbcfour_chinese_school', u'20080416_022500_bbcfour_chinese_school', u'20080417_223500_bbcfour_chinese_school'] ,
  [u'20080422_200000_bbcfour_chinese_school', u'20080422_225000_bbcfour_chinese_school', u'20080423_014500_bbcfour_chinese_school', u'20080424_223500_bbcfour_chinese_school'] ,
  [u'20080429_200000_bbcfour_chinese_school', u'20080429_224000_bbcfour_chinese_school', u'20080430_015500_bbcfour_chinese_school', u'20080501_224000_bbcfour_chinese_school'] ,
  [u'20080506_200000_bbcfour_chinese_school', u'20080506_233500_bbcfour_chinese_school', u'20080507_020500_bbcfour_chinese_school', u'20080508_224000_bbcfour_chinese_school'] ,
  [u'20080507_203000_bbcfour_illuminations_treasures_of_the', u'20080507_233000_bbcfour_illuminations_treasures_of_the', u'20080508_023000_bbcfour_illuminations_treasures_of_the'] ,
  [u'20080428_183000_bbcfour_art_of_eternity', u'20080429_021000_bbcfour_art_of_eternity'] ,
  [u'20080402_215000_bbcfour_mark_lawson_talks_to_barry_cryer', u'20080403_014000_bbcfour_mark_lawson_talks_to_barry_cryer'] ,
  [u'20080409_223000_bbcfour_mark_lawson_talks_to_david_renwick', u'20080410_012500_bbcfour_mark_lawson_talks_to_david_renwick'] ,
  [u'20080406_200000_bbctwo_louis_theroux_s_african_hunting_holiday', u'20080415_003000_bbcone_louis_theroux_s_african_hunting_holiday'] ,
  [u'20080418_004000_bbcthree_dawn_gets_naked', u'20080423_003000_bbcthree_dawn_gets_naked'] ,
  [u'20080501_200000_bbcfour_inside_the_medieval_mind', u'20080502_021000_bbcfour_inside_the_medieval_mind', u'20080504_180000_bbcfour_inside_the_medieval_mind', u'20080505_021000_bbcfour_inside_the_medieval_mind'] ,
  [u'20080424_200000_bbcfour_inside_the_medieval_mind', u'20080425_000500_bbcfour_inside_the_medieval_mind', u'20080425_020500_bbcfour_inside_the_medieval_mind', u'20080427_180000_bbcfour_inside_the_medieval_mind', u'20080428_015000_bbcfour_inside_the_medieval_mind'] ,
  [u'20080508_200000_bbcfour_inside_the_medieval_mind', u'20080508_234000_bbcfour_inside_the_medieval_mind', u'20080509_021000_bbcfour_inside_the_medieval_mind', u'20080511_180000_bbcfour_inside_the_medieval_mind' ] ,
  [u'20080417_200000_bbcfour_inside_the_medieval_mind', u'20080418_000500_bbcfour_inside_the_medieval_mind', u'20080418_020500_bbcfour_inside_the_medieval_mind', u'20080420_180000_bbcfour_inside_the_medieval_mind', u'20080421_014000_bbcfour_inside_the_medieval_mind'] ,
  [u'20080410_183000_bbcfour_in_search_of_medieval_britain', u'20080410_224500_bbcfour_in_search_of_medieval_britain', u'20080411_014500_bbcfour_in_search_of_medieval_britain', u'20080412_181000_bbcfour_in_search_of_medieval_britain'] ,
  [u'20080508_183000_bbcfour_in_search_of_medieval_britain', u'20080509_014000_bbcfour_in_search_of_medieval_britain', u'20080510_180000_bbcfour_in_search_of_medieval_britain', u'20080510_235500_bbcfour_in_search_of_medieval_britain'] ,
  [u'20080501_183000_bbcfour_in_search_of_medieval_britain', u'20080501_234000_bbcfour_in_search_of_medieval_britain', u'20080502_014000_bbcfour_in_search_of_medieval_britain', u'20080503_181000_bbcfour_in_search_of_medieval_britain', u'20080504_015500_bbcfour_in_search_of_medieval_britain'] ,
  [u'20080417_183000_bbcfour_in_search_of_medieval_britain', u'20080417_233500_bbcfour_in_search_of_medieval_britain', u'20080418_013500_bbcfour_in_search_of_medieval_britain'] ,
  [u'20080424_233500_bbcfour_in_search_of_medieval_britain', u'20080425_013500_bbcfour_in_search_of_medieval_britain', u'20080426_181000_bbcfour_in_search_of_medieval_britain', u'20080427_011500_bbcfour_in_search_of_medieval_britain'] ,
  [u'20080408_190000_bbcthree_dawn_gets_a_baby', u'20080409_005500_bbcthree_dawn_gets_a_baby'] ,
  [u'20080408_183000_bbcthree_dog_borstal', u'20080409_022500_bbcthree_dog_borstal'] ,
  [u'20080409_183000_bbcthree_dog_borstal', u'20080410_025000_bbcthree_dog_borstal'] ,
  [u'20080414_183000_bbcthree_dog_borstal', u'20080416_012000_bbcthree_dog_borstal'] ,
  [u'20080415_183000_bbcthree_dog_borstal', u'20080416_015000_bbcthree_dog_borstal'] ,
  [u'20080421_021000_bbcthree_dog_borstal', u'20080422_031000_bbcthree_dog_borstal'] ,
  [u'20080421_024000_bbcthree_dog_borstal', u'20080424_031500_bbcthree_dog_borstal'] ,
  [u'20080428_024000_bbcthree_dog_borstal', u'20080429_031500_bbcthree_dog_borstal'] ,
  [u'20080407_183000_bbcone_watchdog', u'20080414_020500_bbcone_watchdog'] ,
  [u'20080414_183000_bbcone_watchdog', u'20080421_024500_bbcone_watchdog'] ,
  [u'20080421_183000_bbcone_watchdog', u'20080428_005000_bbcone_watchdog'] ,
  [u'20080428_183000_bbcone_watchdog', u'20080505_001000_bbcone_watchdog'] ,
  [u'20080402_200000_bbctwo_dan_cruickshank_s_adventures', u'20080412_015500_bbcone_dan_cruickshank_s_adventures'] ,
  [u'20080416_200000_bbctwo_dan_cruickshank_s_adventures', u'20080426_002500_bbcone_dan_cruickshank_s_adventures'] ,
  [u'20080409_200000_bbctwo_dan_cruickshank_s_adventures', u'20080419_001500_bbcone_dan_cruickshank_s_adventures'] ,
  [u'20080423_200000_bbctwo_dan_cruickshank_s_adventures', u'20080503_003000_bbcone_dan_cruickshank_s_adventures'] ,
  [u'20080423_120000_bbctwo_stand_by_your_man', u'20080429_233000_bbcone_stand_by_your_man'] ,
  [u'20080420_160500_bbcone_lifeline', u'20080423_130000_bbctwo_lifeline'] ,
  [u'20080409_200000_bbcfour_frankie_howerd_rather_you_than_me', u'20080410_022500_bbcfour_frankie_howerd_rather_you_than_me', u'20080411_210000_bbcfour_frankie_howerd_rather_you_than_me', u'20080412_010500_bbcfour_frankie_howerd_rather_you_than_me', u'20080413_214500_bbcfour_frankie_howerd_rather_you_than_me', u'20080414_014000_bbcfour_frankie_howerd_rather_you_than_me'] ,
  [u'20080402_200000_bbcfour_hughie_green_most_sincerely', u'20080402_225000_bbcfour_hughie_green_most_sincerely', u'20080404_210000_bbcfour_hughie_green_most_sincerely', u'20080406_214500_bbcfour_hughie_green_most_sincerely'] ,
  [u'20080416_210000_bbcfour_fantabulosa_kenneth_williams', u'20080417_005000_bbcfour_fantabulosa_kenneth_williams', u'20080419_225000_bbcfour_fantabulosa_kenneth_williams', u'20080420_021000_bbcfour_fantabulosa_kenneth_williams'] ,
  [u'20080401_190000_bbcone_holby_city', u'20080407_004000_bbcone_holby_city'] ,
  [u'20080408_190000_bbcone_holby_city', u'20080414_010500_bbcone_holby_city'] ,
  [u'20080415_190000_bbcone_holby_city', u'20080421_004500_bbcone_holby_city'] ,
  [u'20080422_190000_bbcone_holby_city', u'20080423_180000_bbcthree_holby_city', u'20080428_012000_bbcone_holby_city'] ,
  [u'20080429_190000_bbcone_holby_city', u'20080430_180000_bbcthree_holby_city', u'20080505_004000_bbcone_holby_city'] ,
  [u'20080506_190000_bbcone_holby_city', u'20080507_180000_bbcthree_holby_city'] ,
  [u'20080403_183000_bbcone_eastenders', u'20080403_210000_bbcthree_eastenders'] ,
  [u'20080404_190000_bbcone_eastenders', u'20080404_211500_bbcthree_eastenders'] ,
  [u'20080407_190000_bbcone_eastenders', u'20080407_210000_bbcthree_eastenders'] ,
  [u'20080408_183000_bbcone_eastenders', u'20080408_210000_bbcthree_eastenders'] ,
  [u'20080410_183000_bbcone_eastenders', u'20080410_210000_bbcthree_eastenders'] ,
  [u'20080411_190000_bbcone_eastenders', u'20080411_210000_bbcthree_eastenders'] ,
  [u'20080502_220000_bbcthree_two_pints_of_lager_and', u'20080503_004500_bbcthree_two_pints_of_lager_and'] ,
  [u'20080504_221500_bbcthree_two_pints_of_lager_and', u'20080505_011000_bbcthree_two_pints_of_lager_and'] ,
  [u'20080509_213000_bbcthree_two_pints_of_lager_and', u'20080510_002000_bbcthree_two_pints_of_lager_and'] ,
  [u'20080510_205500_bbcthree_two_pints_of_lager_and', u'20080511_001500_bbcthree_two_pints_of_lager_and'] ,
  [u'20080510_212500_bbcthree_two_pints_of_lager_and', u'20080511_004500_bbcthree_two_pints_of_lager_and'] ,
  [u'20080404_190000_bbcfour_sacred_music', u'20080405_012000_bbcfour_sacred_music', u'20080406_180000_bbcfour_sacred_music', u'20080407_010500_bbcfour_sacred_music'] ,
  [u'20080411_190000_bbcfour_sacred_music', u'20080412_000500_bbcfour_sacred_music', u'20080413_180000_bbcfour_sacred_music', u'20080414_004000_bbcfour_sacred_music'] ,
  [u'20080502_190000_bbcfour_darcey_bussell_s_ten_best_ballet_moments', u'20080503_011000_bbcfour_darcey_bussell_s_ten_best_ballet_moments'] ,
  [u'20080406_190000_bbcfour_mozart_sacred_music', u'20080407_000500_bbcfour_mozart_sacred_music'] ,
  [u'20080418_183500_bbcfour_the_creation_by_haydn_at_the_barbican', u'20080419_011000_bbcfour_the_creation_by_haydn_at_the_barbican'] ,
  [u'20080502_210500_bbcfour_hotel_california_from_the_byrds_to', u'20080504_225000_bbcfour_hotel_california_from_the_byrds_to'] ,
  [u'20080425_200000_bbcfour_soul_britannia', u'20080426_011000_bbcfour_soul_britannia', u'20080427_235000_bbcfour_soul_britannia'] ,
  [u'20080418_232000_bbcfour_the_who_at_the_electric_proms', u'20080420_235000_bbcfour_the_who_at_the_electric_prom'] ,
  [u'20080403_190000_bbctwo_10_things_you_didn_t_know_about', u'20080413_005500_bbcfour_10_things_you_didn_t_know_about'] ,
  [u'20080506_180000_bbcthree_dog_borstal', u'20080507_024000_bbcthree_dog_borstal'] ,
  [u'20080430_003000_bbcthree_scallywagga', u'20080503_014500_bbcthree_scallywagga', u'20080504_023000_bbcthree_scallywagga', u'20080505_004500_bbcthree_scallywagga'] ,
  [u'20080429_213000_bbcthree_scallywagga', u'20080503_215500_bbcthree_scallywagga', u'20080504_214500_bbcthree_scallywagga'] ,
  [u'20080507_003000_bbcthree_scallywagga', u'20080510_025000_bbcthree_scallywagga', u'20080511_024000_bbcthree_scallywagga', u'20080511_214500_bbcthree_scallywagga'] ,
  [u'20080506_213000_bbcthree_scallywagga', u'20080509_232000_bbcthree_scallywagga'] ,
  [u'20080505_220000_bbcthree_placebo', u'20080506_011500_bbcthree_placebo'] ,
  [u'20080428_213000_bbcthree_ideal', u'20080429_004500_bbcthree_ideal', u'20080430_203000_bbcthree_ideal', u'20080501_000500_bbcthree_ideal', u'20080502_234500_bbcthree_ideal', u'20080503_021000_bbcthree_ideal'] ,
  [u'20080505_213000_bbcthree_ideal', u'20080506_004500_bbcthree_ideal', u'20080507_203000_bbcthree_ideal', u'20080507_235000_bbcthree_ideal', u'20080509_235000_bbcthree_ideal', u'20080510_031500_bbcthree_ideal'] ,
  [u'20080419_180000_bbcthree_gaffes_galore_outtakes', u'20080420_032000_bbcthree_gaffes_galore_outtakes'] ,
  [u'20080401_180000_bbctwo_around_the_world_in_80_gardens', u'20080420_234500_bbcone_around_the_world_in_80_gardens'] ,
  [u'20080402_012000_bbcone_life_in_cold_blood', u'20080429_190000_bbcfour_life_in_cold_blood'] ,
  [u'20080409_005500_bbcone_life_in_cold_blood', u'20080430_190000_bbcfour_life_in_cold_blood'] ,
  [u'20080406_173000_bbcone_seaside_rescue', u'20080424_003500_bbcone_seaside_rescue'] ,
  [u'20080420_173000_bbcone_seaside_rescue', u'20080508_002500_bbcone_seaside_rescue'] ,
  [u'20080414_213500_bbcone_meet_the_immigrants', u'20080424_010500_bbcone_meet_the_immigrants'] ,
  [u'20080421_213500_bbcone_meet_the_immigrants', u'20080501_002500_bbcone_meet_the_immigrants'] ,
  [u'20080428_213500_bbcone_meet_the_immigrants', u'20080508_005500_bbcone_meet_the_immigrants'] ,
  [u'20080423_225500_bbcfour_the_last_duel', u'20080424_020500_bbcfour_the_last_duel'] ,
  [u'20080407_193000_bbctwo_delia', u'20080417_180000_bbctwo_delia'] ,
  [u'20080414_193000_bbctwo_delia', u'20080418_183000_bbctwo_delia'] ,
  [u'20080405_164500_bbctwo_watching_the_wild', u'20080506_195000_bbctwo_watching_the_wild'] ,
  [u'20080416_190000_bbctwo_natural_world', u'20080426_012500_bbcone_natural_world'] ,
  [u'20080402_190000_bbctwo_natural_world', u'20080406_160500_bbctwo_natural_world', u'20080412_025500_bbcone_natural_world'] ,
  [u'20080423_190000_bbctwo_natural_world', u'20080427_171000_bbctwo_natural_world', u'20080503_013000_bbcone_natural_world'] ,
  [u'20080409_190000_bbctwo_natural_world', u'20080419_011500_bbcone_natural_world'] ,
  [u'20080509_183000_bbcfour_transatlantic_sessions', u'20080510_024000_bbcfour_transatlantic_sessions'] ,
  [u'20080502_183000_bbcfour_transatlantic_sessions', u'20080503_021000_bbcfour_transatlantic_sessions'] ,
  [u'20080405_172000_bbcone_doctor_who', u'20080406_190000_bbcthree_doctor_who'] ,
  [u'20080412_174500_bbcone_doctor_who', u'20080418_200000_bbcthree_doctor_who'] ,
  [u'20080419_172000_bbcone_doctor_who', u'20080425_200000_bbcthree_doctor_who'] ,
  [u'20080426_172000_bbcone_doctor_who', u'20080427_190000_bbcthree_doctor_who', u'20080502_200000_bbcthree_doctor_who'] ,
  [u'20080503_172000_bbcone_doctor_who', u'20080504_190000_bbcthree_doctor_who', u'20080509_200000_bbcthree_doctor_who'] ,
  [u'20080510_174500_bbcone_doctor_who', u'20080511_190000_bbcthree_doctor_who'] ,
  [u'20080404_200000_bbctwo_torchwood', u'20080415_180000_bbctwo_torchwood'] ,
  [u'20080506_190000_bbcfour_bbc_young_musician_of_the_year_2008', u'20080507_003500_bbcfour_bbc_young_musician_of_the_year_2008', u'20080509_190000_bbcfour_bbc_young_musician_of_the_year_2008', u'20080510_001000_bbcfour_bbc_young_musician_of_the_year_2008'] ,
  [u'20080508_190000_bbcfour_bbc_young_musician_of_the_year_2008', u'20080509_004000_bbcfour_bbc_young_musician_of_the_year_2008'] ,
  [u'20080505_190000_bbcfour_bbc_young_musician_of_the_year_2008', u'20080505_235000_bbcfour_bbc_young_musician_of_the_year_2008'] ,
  [u'20080401_210000_bbcfour_the_hard_sell', u'20080406_022500_bbcfour_the_hard_sell'] ,
  [u'20080505_193000_bbcone_panorama', u'20080508_233000_bbcone_panorama'] ,
  [u'20080421_183000_bbcfour_abroad_again_in_britain', u'20080422_004000_bbcfour_abroad_again_in_britain'] ,
  [u'20080404_183000_bbcfour_transatlantic_sessions', u'20080405_022000_bbcfour_transatlantic_sessions'] ,
  [u'20080421_193000_bbcone_panorama', u'20080424_233500_bbcone_panorama'] ,
  [u'20080407_222500_bbcone_inside_sport', u'20080414_033000_bbctwo_inside_sport'] ,
  [u'20080414_220500_bbcone_inside_sport', u'20080421_031000_bbctwo_inside_sport'] ,
  [u'20080421_220500_bbcone_inside_sport', u'20080428_023000_bbctwo_inside_sport'] ,
  [u'20080409_130000_bbctwo_world_swimming_championships', u'20080409_180000_bbctwo_world_swimming_championships'] ,
  [u'20080413_174000_bbctwo_london_marathon_highlights', u'20080414_041000_bbctwo_london_marathon_highlights'] ,
  [u'20080415_200000_bbctwo_age_of_terror', u'20080421_014500_bbcone_age_of_terror'] ,
  [u'20080422_200000_bbctwo_age_of_terror', u'20080428_022000_bbcone_age_of_terror'] ,
  [u'20080427_231500_bbcthree_page_three_teens', u'20080504_004500_bbcthree_page_three_teens', u'20080508_235500_bbcthree_page_three_teens'] ,
  [u'20080424_233000_bbcthree_child_stars', u'20080426_003500_bbcthree_child_stars'] ,
  [u'20080424_200000_bbcthree_child_stars', u'20080426_234500_bbcthree_child_stars', u'20080427_024500_bbcthree_child_stars', u'20080504_231500_bbcthree_child_stars'] ,
  [u'20080508_182700_bbcfour_disasters_emergency_committee_myanmar', u'20080508_185700_bbcthree_disasters_emergency_committee_myanmar', u'20080508_205700_bbcthree_disasters_emergency_committee_myanmar', u'20080508_212700_bbctwo_disasters_emergency_committee_myanmar'] ,
  [u'20080508_175500_bbcone_disasters_emergency_committee_myanmar', u'20080508_213500_bbcone_disasters_emergency_committee_myanmar'] ,
  [u'20080415_183000_bbcfour_pop_goes_the_sixties', u'20080429_183000_bbcfour_pop_goes_the_sixties'] ,
  [u'20080422_183000_bbcfour_pop_goes_the_sixties', u'20080430_183000_bbcfour_pop_goes_the_sixties'] ,
  [u'20080407_183000_bbcfour_pop_goes_the_sixties', u'20080423_183000_bbcfour_pop_goes_the_sixties'] , 
]

dupHash = {}
for dup in duplicates:
  for v in dup:
    dupHash[v] = dup

def reportError(line, errstr):
  error = True
  return "Error in line " + str(line) + ": " + errstr

def isAnchor(anchor):
  return anchor in anchors
  
def isItem(item):
  return item in items  
  
def isTime(s):
  m = re.match('(\d+).(\d+)', s)
  if m == None: 
    return False
  if int(m.group(1)) < 0 : 
    return False
  if int(m.group(2)) < 0 or int(m.group(2)) > 60: 
    return False
  return True

def sec2String(s):
  miliseconds= s * 1000
  minutes, milliseconds = divmod(miliseconds, 60000)
  seconds = float(milliseconds) / 1000
  return "%d.%02d" % (minutes, seconds)

def overlaps(segment1,segment2):
  ''' checks if two time segments overlap '''
  video1, start1, end1 = segment1
  video2, start2, end2 = segment2
  if video1 != video2: return False
  if start1 <= start2 and start2 <= end1: return True
  if start2 <= start1 and start1 <= end2: return True
  return False

def isRank(s):
  try:
    i =  int(s)
    return i > 0
  except:
    return False
  
def isScore(s):
  try:
    i =  float(s)
    return True
  except:
    return False

def ToSec(s):
  m = re.match('(\d+).(\d+)', s)
  if m == None: return 0
  return int(m.group(1)) * 60 + int(m.group(2))
  
def timesort(l):
  return (l['video'], l['start'], l['end'])
  
reVideo = re.compile('^(\d\d\d\d)(\d\d)(\d\d).*')
def video2day(v):
  m = reVideo.match(v)
  year = int(m.group(1))
  mon = int(m.group(2))
  day = int(m.group(3))
  mon = mon - 4
  return mon * 30 + day

reVideo = re.compile('^(\d\d\d\d)(\d\d)(\d\d).*')
def video2date(v):
  m = reVideo.match(v)
  year = int(m.group(1))
  mon = int(m.group(2))
  day = int(m.group(3))
  return '%02d.%02d.' % (day, mon)
  
def overlap(s1, s2):
  if s1['start'] <= s2['start'] and s2['start'] <= s1['end']: return True
  if s2['start'] <= s1['start'] and s1['start'] <= s2['end']: return True
  return False
  
def mean(v):
  if len(v) == 0: return 0.0
  return sum(v) / float(len(v))

class Stat:
  def __init__(self, relType="segment"):
    self.relTypeVal = relType
    
  def perQuery(self):
    return True
  
  def forAll(self):
    return True
    
  def format(self):
    return "%d"
    
  def relType(self):
    return self.relTypeVal

  def fullName(self):
    if self.relType() != "segment":
      return self.name() + '_' + self.relType()
    return self.name()    

class Measure:
  def __init__(self, relType="segment"):
    self.relTypeVal = relType
    
  def perQuery(self):
    return True
  
  def forAll(self):
    return True
  
  def relType(self):
    return self.relTypeVal
    
  def format(self):
    return "%.4f"
    
  def fullName(self):
    if self.relType() != "segment":
      return self.name() + '_' + self.relType()
    return self.name()

class Ap(Measure):
  def __init__(self, relType="segment"):
    Measure.__init__(self, relType=relType)
    
  def name(self):
    return "map"
  
  def calc(self, rels, nrel=None):
    rels = map(lambda x: 1 if type(x) == int and x > 0 else 0, rels)
    if nrel == None:
      nrel = sum(rels)
    ap = 0.0
    crel = 0
    for rank, r in enumerate(rels):
      if r >= 1:
        crel += 1.0
        ap += crel / (1.0+rank)
    if ap == 0: return 0.0
    return ap/nrel
  
  def agg(self):
    return mean  

class PrecisionAt(Measure):
  def __init__(self, n, relType="segment"):
    Measure.__init__(self, relType=relType)
    self.n = n
  
  def name(self):
    return "P_" + str(self.n)
  
  def calc(self, rels, nrel=None):
    rels = map(lambda x: 1 if type(x) == int and x > 0 else 0, rels)
    return sum(rels[:self.n]) / float(self.n)
    
  def agg(self):
    return mean
    
class JudgedAt(Measure):
  def __init__(self, n, relType="segment"):
    Measure.__init__(self,relType=relType)
    self.n = n
  
  def name(self):
    return "Judged_" + str(self.n)
  
  def calc(self, rels, nrel=None):
    rels = map(lambda x: 0 if x == '-' else 1, rels)
    return sum(rels[:self.n]) / float(self.n)
    
  def agg(self):
    return mean
    
class NumRel(Stat):
  def __init__(self, relType="segment"):
     Stat.__init__(self, relType=relType)
  
  def name(self):
    return "num_rel"
  
  def calc(self, rels, nrel=None):
    if nrel == None:
      nrel = sum(map(lambda x: 1 if type(x) == int and x > 0 else 0, rels))
    return nrel
  
  def agg(self):
    return sum
    
class NumRet(Stat):
  def __init__(self, relType="segment"):
     Stat.__init__(self, relType=relType)
  
  def name(self):
    return "num_ret"
  
  def calc(self, rels, nrel=None):
    return len(rels)    
    
  def agg(self):
    return sum

class NumQ(Stat):
  def __init__(self, relType="segment"):
    Stat.__init__(self, relType=relType)

  def name(self):
    return "num_q"
  
  def calc(self, rels, nrel=None):
    return 1
    
  def agg(self):
    return sum
    
  def perQuery(self):
    return False

class NumRelRet(Stat):
  def __init__(self, relType="segment"):
    Stat.__init__(self, relType=relType)
  
  def name(self):
    return "num_rel_ret"
  
  def calc(self, rels, nrel=None):
    rels = map(lambda x: 1 if type(x) == int and x > 0 else 0, rels)
    return sum(rels)

  def agg(self):
    return sum
    
class VideosRet(Stat):
  def __init__(self):
    Stat.__init__(self, "ranking")
  
  def name(self):
    return "videos_ret"
  
  def fullName(self):
    return self.name()
  
  def calc(self, trecs, nrel=None):
    rels = set(map(lambda trec: trec['target'][0], trecs))
    return len(rels)

  def agg(self):
    return mean

class VideosRel(Stat):
  def __init__(self):
    Stat.__init__(self, "qrel")
  
  def name(self):
    return "videos_rel"
  
  def fullName(self):
    return self.name()
  
  def calc(self, qrel, nrel=None):
    return len(qrel)

  def agg(self):
    return mean

class LengthRet(Stat):
  def __init__(self):
    Stat.__init__(self, "ranking")
  
  def name(self):
    return "avglength_ret"
  
  def fullName(self):
    return self.name()
  
  def calc(self, trecs, nrel=None):
    lengths = map(lambda trec: trec['target'][2]-trec['target'][1], trecs)
    return sum(lengths) / float(len(lengths))

  def agg(self):
    return mean

class LengthRel(Stat):
  def __init__(self):
    Stat.__init__(self, "qrel")
  
  def name(self):
    return "avglength_rel"
  
  def fullName(self):
    return self.name()
  
  def calc(self, qrel, nrel=None):
    lengths = []
    for video, segments in qrel.iteritems():
       lengths.extend(map(lambda trec: trec[2]-trec[1], segments))
    return sum(lengths) / float(len(lengths))

  def agg(self):
    return mean
    

class RelJudge(Stat):
  def __init__(self, relType="segment"):
    Stat.__init__(self, relType=relType)
  
  def name(self):
    return "relString"
  
  def calc(self, rels, nrel=None):
    def transform(r):
      if type(r) == int and r > 0:
        return '1'
      elif type(r) == int and r == 0:
        return '0'
      else:
        return str(r)      
    return ''.join(map(transform,rels)) + ' ' + str(len(rels))

  def forAll(self):
    return False

  def format(self):
    return "%s"

  def agg(self):
    return None

runTypes = [
  'S',   # search run
  'LA',  # linking run using only the anchor segment
  'LC',  # linking run using both the anchor segment and its context
]


segmentationNames = [
  ('Ss', 'speech sentence'),
  ('Sp', 'speech segment'),
  ('Sh', 'shot'),
  ('F',  'fixed length'),
  ('L',  'lexical cohesian'),
  ('O',  'other segmentation'),
]
segmentations = map(lambda x: x[0], segmentationNames)

asrFeaturesNames = [
  ('I', "Limsi"),
  ('U', "Lium"),
  ('S', "Subtitles"),
  ('N', "No ASR info"),
]
asrFeatures = map(lambda x: x[0], asrFeaturesNames)

additionalFeatureNames = [
  ('M', 'metadata'),
  ('V', 'visual'),
  ('O', 'other'),
  ('N', 'No additional features'),
]
additionalFeatures = map(lambda x: x[0], additionalFeatureNames)

def checkRunName(runName):
  import os, sys
  runName = os.path.basename(runName)
  generalPattern = 'me13sh_(?P<team>[^_]+)_(?P<runType>[^_]+)_(?P<segmentation>[^_]+)_(?P<asrFeature>[^_]+)_(?P<additionalFeatures>[^_]+)_(?P<description>.+)(\..+)?$'
  result = {}
  result['filename'] = runName

  m = re.match(generalPattern, runName)
  if m == None:
    print >>sys.stderr, "Error: invalid run name " + runName + ". Run names should follow the pattern:"
    print >>sys.stderr, "me13sh_TEAM_RunType_Segmentation_TranscriptType_AdditionalFeatures[.???], where"
    print >>sys.stderr, "   RunType is one of [" + ','.join(runTypes) + ']'
    print >>sys.stderr, "   Segmentation is a combination of [" + ','.join(segmentations) + ']'
    print >>sys.stderr, "   TranscriptType is one of [" + ','.join(asrFeatures) + ']'
    print >>sys.stderr, "   AdditionalFeatures is a combination of [" + ','.join(additionalFeatures) + ']'    
    sys.exit(1)

  result['team'] = m.group('team')

  runType = m.group('runType')
  if runType not in runTypes:
    return False, "Invalid run type '" + runType + "'. Valid run types are :" + ','.join(runTypes)
  result['runType'] = runType
  
  segmentation = m.group('segmentation')
  pattern = '(' + '|'.join(segmentations) + ')'
  mS = re.match('^'+pattern+'+$', segmentation)
  if mS == None:
    return False, "Invalid segmentation '" + segmentation + "'. Segmentations should be one of: " + ','.join(segmentations)
  segmentation = re.findall(pattern, segmentation)
  result['segmentation'] = segmentation  

  asrFeature = m.group('asrFeature')
  pattern = '(' + '|'.join(asrFeatures) + ')'
  mS = re.match('^'+pattern+'+$', asrFeature)
  if mS == None:
    return False, "Invalid transcript type '" + asrFeature + "'. Transcript type should be one of: " + ','.join(asrFeatures)
  asrFeature = re.findall(pattern, asrFeature)
  result['asrFeature'] = asrFeature  

  additionalFeature = m.group('additionalFeatures')
  if additionalFeature != None:
    pattern = '(' + '|'.join(additionalFeatures) + ')'
    mS = re.match('^'+pattern+'+$', additionalFeature)
    if mS == None:
      return False, "Invalid additionalFeature '" + additionalFeature + "'. Additional features should be one of: " + ','.join(additionalFeatures)

    additionalFeature = re.findall(pattern, additionalFeature)
    result['additionalFeatures'] = additionalFeature 
  else:
    result['additionalFeatures'] = '' 
  return True, result

def toSegment(anchor):
  return (anchor['video'], anchor['start'], anchor['end'])

def segToString(seg):
  return "%s %s %s" % (seg[0], sec2String(seg[1]), sec2String(seg[2]))

def sameVideo(s1,s2):
  return s1[0] == s2[0]

def statval(l,percentiles=None):
  n = len(l)
  s = sum(l)
  s2 = sum([ x**2 for x in l ])
  mi = min(l)
  ma = max(l)
  e = s/float(n)
  e2 = s2/float(n)
  v = e2 - e*e
  sd = 0
  if v > 0:
    sd = sqrt(v)
  result = { 'n': n, 'sum': s, 'e': e, 'v': v, 'sd':sd, 'min': mi, 'max': ma }
  if percentiles != None:
    l = sorted(l)
    p = dict(map(lambda x: (x,l[int(len(l)*x)]), percentiles))
    result['percentiles'] = p
  return result
  
