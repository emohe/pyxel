# Fuse results 
import time
import sys

def norm_results(filename,outfile):
    #Function that normalizes the results in  the file 'filename' doing (x-xmin)/(xmax-xmin)
    #The result is stored in a new file 'outfile'
    
    #Read file
    f=open(filename,'r')
    all_lines=f.readlines()
    f.close()
    
    #Open file for writing
    f=open(outfile,'w')
 
    #Initialize values
    max_val=0
    min_val=1
    
    for l in all_lines: 
        
        line=l.split(' ')
        similarity=float(line[6])
        max_val=max(similarity,max_val)
        min_val=min(similarity,min_val) # Obtain min and max values of the results file
        
    for l in all_lines:
        line=l.split(' ')
        line[6]=str((float(line[6])-min_val)/(max_val-min_val)) #Normalize result
        f.write(' '.join(line))
            
        
    f.close()
        
        
filename_visual=sys.argv[1] #path to file with the visual results
filename_text=sys.argv[2] #path to file with the text results
path=sys.argv[3] #Path where aux files will be saved
fusion=sys.argv[4] #Fusion options: '0.7' '0.5' 'max'

t=time.time()
norm_results(filename_visual,filename_visual[:-4]+'_norm.txt')
norm_results(filename_text,filename_text[:-4]+'_norm.txt')

f=open(filename_visual[:-4]+'_norm.txt','r')
all_lines_visual=f.readlines()
f.close()
f=open(filename_text[:-4]+'_norm.txt','r')
all_lines_text=f.readlines()
f.close()

f=open(path+'results_file.txt','w') 
#Start processing file line by line (every line contains a video segment to be compared with the anchor)
for l in range(0,len(all_lines_visual)):
    
    line_visual=all_lines_visual[l].split(' ')
    line_text=all_lines_text[l].split(' ')
    
    similarity_visual=line_visual[6]
    similarity_text=line_text[6]
    if fusion=='max':
        new_similarity=max(float(similarity_visual),float(similarity_text))
    else:
        new_similarity= float(fusion)*float(similarity_visual)+(1-float(fusion))*float(similarity_visual)
    line_visual[6]=str(new_similarity)
    f.write(' '.join(line_visual))

        