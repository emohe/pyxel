# Build histogram showing relevant - not relevant results related to their simmilarity measure
# Amaia Salvador - December 2013

import numpy as np
import matplotlib.pyplot as plt


def return_lines(line,linelist):
    #Return index list of all lines in 'linelist' that correspond to the same anchor in 'line'
    index_list=[]
    for l in range(len(linelist)):
        text=linelist[l].split(' ')
        if text[0]==line[1]:
            index_list.append(l)
    return index_list
        
    
#Read files to be proccessed

f=open('./me13sh_UPC_LA_Sh_N_V_BoF.eval', 'r')
#'gt_lines' is the file which contains the results for the different anchors. It contains 0s for the non-relevant results and 1s for the relevant ones. The rest are labeled as '-'.
gt_lines=f.readlines()
f.close()

f=open('./me13sh_UPC_LA_Sh_N_V_BoF.txt','r')
#'all_lines' contains all results. Every line has its simmilarity measure at the end.
all_lines=f.readlines()
f.close()

#Initialize arrays to store the similarity measure
smeasure_array_rel=[]
smeasure_array_notrel=[]

#Lines in 'gt_lines' storing the ones and zeros appear in steps of 8 starting by the 4th line(if the first one is considered the 0th)
for line in range(4,237,8):
    #Get lines in 'all_lines' related to the anchor we are treating
    i=return_lines(gt_lines[line].split('\t'),all_lines)
    
    #Get the relevance array (array of ones, zeros and '-'s)
    relevance=gt_lines[line].split('\t')
    relevance = relevance[2].split(' ')
    relevance=list(relevance[0])
    
    #Go through all the lines that are related to the anchor, if any:
    if i:
        for index in range(len(relevance)-1):
            #Only treat the ones that are either 0 or 1 (skip those ones for which we don't have any data).
            if relevance[index]=='0' or relevance[index]=='1':
                
            #Get the similarity measure 
                aux_array = all_lines[i[index]].split(' ')
                smeasure = (aux_array[len(aux_array)-2])
                
            #Update final arrays
                if relevance[index]=='1':    
                    smeasure_array_rel.append(smeasure)
                    
                elif relevance[index]=='0':
                    smeasure_array_notrel.append(smeasure)
                        
                 
#Plot histograms

#Convert the arrays from string to float
smeasure_array_notrel=map(float,smeasure_array_notrel)
smeasure_array_rel=map(float,smeasure_array_rel)

nbins=100;
f1=plt.figure()
plt.hist(smeasure_array_rel,nbins,range=[0, 1])
plt.axis([0, 1, 0, len(smeasure_array_rel)])
f1.suptitle('Histogram of relevant data')
plt.xlabel('Similarity measure')
plt.ylabel('#Occurrences')

f2=plt.figure()
plt.hist(smeasure_array_notrel,nbins,range=[0, 1])
plt.axis([0, 1, 0, len(smeasure_array_notrel)])
f2.suptitle('Histogram of non relevant data')
plt.xlabel('Similarity measure')
plt.ylabel('#Occurrences')
      
f1.show()
f2.show()

