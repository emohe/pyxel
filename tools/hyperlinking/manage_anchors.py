# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 12:47:47 2014

@author: asalvador
"""
import os

def get_keyframes(phrase,filename):
#Function to get all keyframes in the current anchor from the file that contains them all    

    keyframes=[]
    
    #Get next anchor to stop adding lines
    next_anchor=phrase[7:]
    next_anchor=int(next_anchor)+1
    next_anchor='anchor_'+str(next_anchor)
    
    #Variable to control the addition of lines
    allow_write=0
    
    for line in filename:
        
        
        l=line.split('\n')
        
        if phrase==l[0]:
            allow_write=1
            
        if next_anchor == l[0]:
            allow_write=0
            
        if allow_write==1:
            keyframes.append(line)
            
    #Should remove first line, that will be 'anchor_XX' 
    # ...      
    keyframes.pop(0)
    return keyframes


frames_path  ="/projects/retrieval/mediaeval/sh13/shotDetection/"          
tools_path='/mnt/work/cventura/mediaeval/SH_task/executables/'
descriptors_path='/mnt/work/cventura/mediaeval/SH_task/features/SURFBoF/'
savepath='/imatge/asalvador/workspace/python/hyperlinking/anchor_frames/'
savepathbof='/imatge/asalvador/workspace/python/hyperlinking/anchor_bofs/'

anchor_kf=open('/work/cventura/mediaeval/SH_task/test_queries/anchor_interval/anchorstartend/all_anchors_label','r').readlines()
#'all_lines' contains all results. Every line has its simmilarity measure at the end. third column of all_lines contains segment names

for i in range(1,99):
    current_anchor='anchor_'+str(i)
    frames=get_keyframes(current_anchor,anchor_kf)
    
    anchor_file_frames=open(savepath+'anchor_'+str(i)+'_frames.txt','w')
    for item in frames:
        anchor_file_frames.write(item)
        
    anchor_file_frames.close()
    anchor_file_frames=savepath+'anchor_'+str(i)+'_frames.txt'
        
    script_call= tools_path + 'xml_paths_features_db' + ' '+anchor_file_frames + ' ' + '/projects/retrieval/mediaeval/sh13/shotDetection/' + ' ' + descriptors_path + ' ' + savepathbof+'anchor_'+str(i)+'_bofs.txt'
    os.system(script_call)
        
