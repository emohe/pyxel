# Merge results files into new file
#Amaia Salvador - January 2014

f=open('./me13sh_UPC_LA_Sh_N_V_BoF.txt','r')
f2=open('./me13sh_UPC_LA_Sh_N_V_BoF.txt','r')


all_lines=f.readlines()
all_lines2=f2.readlines()

f.close()
f2.close()

f = open('merged_results.txt', 'w')

mode='mean'

for l in range(len(all_lines)):
    
    line=all_lines[l].split(' ')
    line2=all_lines2[l].split(' ')
    
    if mode=='mean':
        line[6]=str((float(line[6])+float(line2[6]))/2)
    elif mode=='high':
        line[6]=str(max(float(line[6]),float(line2[6])))

    f.write(' '.join(line))
    
f.close()