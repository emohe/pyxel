import sys
import os
#Re-rank
def rank_results(filename,outfile):
    #Function that normalizes the results file in 'file_lines' doing (x-xmin)/(xmax-xmin)
    #The result is stored in a new file 'outfile'
    
    #Read file
    f=open(filename,'r')
    all_lines=f.readlines()
    f.close()
    all_lines.sort(key=lambda l: float(l.split()[6]),reverse=True)
    f=open(outfile,'w')
    f.writelines(all_lines)
    f.close()
   
       
 
path=sys.argv[1] #path where all files to rerank are

dirList=os.listdir(path)

# Build the list with the names
for filename in dirList:
    
    rank_results(path+filename,path+filename)