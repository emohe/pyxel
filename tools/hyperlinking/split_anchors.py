import sys

def split_anchors(filename,path):
    #Function that normalizes the results file in 'file_lines' doing (x-xmin)/(xmax-xmin)
    #The result is stored in a new file 'outfile'
    
    #Read file
    f=open(filename,'r')
    all_lines=f.readlines()
    current_anchor=''
    
    for l in all_lines:
        line=l.split(' ')
        
        anchor=line[0]
        
        if current_anchor!=anchor:

            #Open file for writing
            f.close()
            f=open(path+anchor+'.txt','w')
            current_anchor=anchor
            
        f.write(' '.join(line))
    f.close()
       
 
     
        
filename=sys.argv[1] #path to file with the visual results
savepath=sys.argv[2] #path to store splitted files

split_anchors(filename,savepath)