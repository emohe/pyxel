# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 13:11:25 2014

@author: asalvador
"""
#RUn example : visual_results.py text_file.txt /imatge/asalvador/workspace/python/hyperlinking/DCU1/
import os
import time
import sys

filename=sys.argv[1]
path=sys.argv[2]
print filename
print path
frames_path  ="/projects/retrieval/mediaeval/sh13/shotDetection/"          
tools_path='/mnt/work/cventura/mediaeval/SH_task/executables/'
descriptors_path='/mnt/work/cventura/mediaeval/SH_task/features/SURFBoF/'
t=time.time()
f=open(path+filename,'r')
#'all_lines' contains all results. Every line has its simmilarity measure at the end. third column of all_lines contains segment names
all_lines=f.readlines()
f.close()
f=open(path+'visual_results.txt','w')
new_file=[]
current_anchor=''
#Start processing file line by line (every line contains a video segment to be compared with the anchor)
for l in all_lines:
    
    line=l.split(' ')
    
    #Get segment name
    segment=line[2]
    
    #Get time frames
    begin=line[3]
    end=line[4]
    
    anchor=line[0]
    if anchor != current_anchor:
        
        descriptor_paths_query=open('/mnt/work/asalvador/hyperlinking_task/anchor_bofs/'+anchor+'_bofs.txt','r').readlines()
        current_anchor=anchor
    
    #Step 1: Find which are the frames that fall inside the given time frame for the sequence.
    bashcommand='/mnt/work/asalvador/hyperlinking_task/interval2paths '+segment+' '+begin+' '+end+' '+frames_path+ ' >'+path+'shot_sequence.txt'
    os.system(bashcommand)
    #Retrieve the result stored in file
    shot_seq=path+'shot_sequence.txt'

    #Step 2. From frames to frame descriptors
    # load shot descriptors having shot paths for both query and segment
    script_call= tools_path + 'xml_paths_features_db' + ' '+shot_seq + ' ' + '/projects/retrieval/mediaeval/sh13/shotDetection/' + ' ' + descriptors_path + ' ' + path+'bofs_seq.txt'
    os.system(script_call)
    #Retrieve the results stored in txt files
    descriptor_paths_seq=path+'bofs_seq.txt'
    
    #Step 3: Descriptor similarity
    # Compute similarity given the descriptors
    similarity=0
    #Every frame of the anchor will be compared against all frames in the sequence.
    #The highest similarity will be stored for every anchor frame
    #Finally, the highest of the highest will be the result that we will generate and put in the new results file.
    for descriptor in descriptor_paths_query:
        #Call to compute and obtainthe similarity results for a query frame against all shots
        script_call= tools_path + 'ranker_SURFBoF' + ' '+descriptor[0:-1] + ' ' + descriptor_paths_seq + ' ' + path
        os.system(script_call)
        #We read the file that 'ranker_SURFBof' Generates
        f2=open(path+'rank0.txt','r')
        sim_lines=f2.readlines()
        f2.close()
        #os.remove('./rank0.txt')
        #We take the first line, which corresponds to the best match
        
        best_result=sim_lines[0].split(' ')[1]
        
        #get similarity value.
        aux_similarity=1-float(best_result)
        #Current similarity is compared with the previous one (the one obtained comparing the previous anchor frame with all shots)
        #If higher, we keep it.        
        similarity=max(similarity, aux_similarity)
        
    
    #Write new file that is the same as 'all_lines' but changing the similarity term by the computed one
    line[6]=str(similarity)
    f.write(' '.join(line))
    
elapsed=time.time()-t
timefile=open(path+'time_info.txt','w')
timefile.write(str(elapsed))
timefile.close()
f.close()
    