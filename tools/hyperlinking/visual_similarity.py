# Function that generates visual scores from the ranked list results of the participants that used textual data-
# Amaia Salvador - January 2014

import os
import sys


def get_keyframes(phrase,filename):
#Function to get all keyframes in the current anchor from the file that contains them all    

    keyframes=[]
    
    #Get next anchor to stop adding lines
    next_anchor=phrase[7:]
    next_anchor=int(next_anchor)+1
    next_anchor='anchor_'+str(next_anchor)
    
    #Variable to control the addition of lines
    allow_write=0
    
    for line in filename:
        
        
        l=line.split('\n')
        
        if phrase==l[0]:
            allow_write=1
            
        if next_anchor == l[0]:
            allow_write=0
            
        if allow_write==1:
            keyframes.append(line)
            
    #Should remove first line, that will be 'anchor_XX' 
    # ...      
    keyframes.pop(0)
    return keyframes
            
savepath=sys.argv[1]
filename=sys.argv[2]   
frames_path  ="/projects/retrieval/mediaeval/sh13/shotDetection/"          
tools_path='/mnt/work/cventura/mediaeval/SH_task/executables/'
descriptors_path='/mnt/work/cventura/mediaeval/SH_task/features/SURFBoF/'
#Example of descriptor file:
#/work/cventura/mediaeval/SH_task/features/SURFBoF/20080511_234000_bbcthree_sound/shots/000000000/000000264-sparse.xml

#File containing all frames for all anchors
anchor_kf=open('/work/cventura/mediaeval/SH_task/test_queries/anchor_interval/anchorstartend/all_anchors_label','r').readlines()

f=open(savepath+filename,'r')
#'all_lines' contains all results. Every line has its simmilarity measure at the end. third column of all_lines contains segment names
all_lines=f.readlines()
f.close()

#The file we will generate with the visual results
f=open('visual_results.txt','w')

new_file=[]
current_anchor=''
#Start processing file line by line (every line contains a video segment to be compared with the anchor)
for l in all_lines:
    
    line=l.split(' ')
    
    #Get segment name
    segment=line[2]
    
    #Get time frames
    begin=line[3]
    end=line[4]
    
    #Anchor name and list of shots inside
    anchor=line[0]
    if anchor != current_anchor:
        
        anchor_kf_list=get_keyframes(anchor,anchor_kf)
    
        anchor_file_aux=open('./anchor_aux.txt','w')
        for item in anchor_kf_list:
            anchor_file_aux.write(item)
        anchor_file_aux.close()
        anchor_file_aux='./anchor_aux.txt'
        #The same is done for the anchor frames
        script_call= tools_path + 'xml_paths_features_db' + ' '+anchor_file_aux + ' ' + '/projects/retrieval/mediaeval/sh13/shotDetection/' + ' ' + descriptors_path + ' ' + './bofs_query.txt'
        os.system(script_call)
        descriptor_paths_query=open('./bofs_query.txt','r').readlines()
        current_anchor=anchor
    else:
        anchor_file_aux='./anchor_aux.txt'
        descriptor_paths_query=open('./bofs_query.txt','r').readlines()
    
        
    #Step 1: Find which are the frames that fall inside the given time frame for the sequence.
    bashcommand='./interval2paths '+segment+' '+begin+' '+end+' '+frames_path+ ' > shot_sequence.txt'
    os.system(bashcommand)
    #Retrieve the result stored in file
    shot_seq='./shot_sequence.txt'

    #Step 2. From frames to frame descriptors
    # load shot descriptors having shot paths for both query and segment
    script_call= tools_path + 'xml_paths_features_db' + ' '+shot_seq + ' ' + '/projects/retrieval/mediaeval/sh13/shotDetection/' + ' ' + descriptors_path + ' ' + './bofs_seq.txt'
    os.system(script_call)
    
    
    #Retrieve the results stored in txt files
    descriptor_paths_seq='./bofs_seq.txt'
    descriptor_paths_query=open('./bofs_query.txt','r').readlines()
    #Step 3: Descriptor similarity
    # Compute similarity given the descriptors
    similarity=0
    #Every frame of the ancher will be compared against all frames in the sequence.
    #The highest similarity will be stored for every anchor frame
    #Finally, the highest of the highest will be the result that we will generate and put in the new results file.
    for descriptor in descriptor_paths_query:
        #Call to compute and obtainthe similarity results for a query frame against all shots
        script_call= tools_path + 'ranker_SURFBoF' + ' '+descriptor[0:-1] + ' ' + descriptor_paths_seq + ' ' + './'
        os.system(script_call)
        #We read the file that 'ranker_SURFBof' Generates
        f2=open('./rank0.txt','r')
        sim_lines=f2.readlines()
        f2.close()
        #os.remove('./rank0.txt')
        #We take the first line, which corresponds to the best match
        
        best_result=sim_lines[0].split(' ')[1]
        
        #get similarity value.
        aux_similarity=1-float(best_result)
        #Current similarity is compared with the previous one (the one obtained comparing the previous anchor frame with all shots)
        #If higher, we keep it.        
        similarity=max(similarity, aux_similarity)
        
    
    #Write new file that is the same as 'all_lines' but changing the similarity term by the computed one
    line[6]=str(similarity)
    f.write(' '.join(line))
    

    
f.close()