# -*- coding: utf-8 -*-
"""
Created on Fri Apr  4 17:08:49 2014

@author: xavi
"""

# Add the root of all Python modules to the search path
import os
import sys

pathHome = os.path.expanduser('~')
pathDirPython=os.path.join( pathHome, 'workspace','python')
sys.path.append(pathDirPython)
