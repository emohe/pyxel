#! /usr/bin/env python
from subprocess import call

f1 = open('train2008_complete_path.txt', 'r')
results_dir = '/work/cventura/pascal_2008/3_features/'
binary = '/work/cventura/dev/new-imageplus/tools/bpt_population/bin/release/bpt_population'
codebook_file = '/work/cventura/pascal_2008/3_features/codebook_SURFBoF/codebook_5perc.xml'
for line in f1:
  print line
  call(["srun", binary, "--VDSURFBoF=1", "--SURFBoFCodebookFilename=" + codebook_file, line.rstrip('\n'), results_dir])

f1.close()
