import os, sys, glob
import numpy as np
from os.path import expanduser
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import plyvel
import time

##############################################
#Script to generate leveldb for OXFORD dataset
# it can be easily addapted to any dataset
#
# Eva mohedano - 11 Mar 2015
############################

oxford_folder = 'liberty'




# Path settings######################################
# Deffining Home path for the user
HOME = expanduser("~") # use that to ensure that it is going to work in all platforms

# Checking 'workspace' and 'work' folders exist in the home directory:
if not os.path.isdir( os.path.join( HOME, 'workspace' ) ):
    print "Generating 'workspace' folder in "+HOME+" "
    os.mkdir( os.path.join( HOME, 'workspace' ) )

if not os.path.isdir( os.path.join( HOME, 'work' ) ):
    print "Generating 'work' folder in "+HOME+" "
    os.mkdir( os.path.join( HOME, 'work' ) )


# Path to caffe folder
CAFFE = os.path.join(HOME, 'workspace', 'caffe')

# Path to the dataset
OXFORD =  os.path.join(HOME,'data', oxford_folder)

# Path where siamese models are located
SIAMESE = os.path.join(CAFFE, 'examples', 'siamese')

# Path where we generate the output for this script
OUT = os.path.join( HOME, 'work', __file__.split('.')[0] )

# If output for the script does not exist, generate it
if not os.path.isdir( OUT ):
    os.mkdir( OUT )

#Import caffe########################################
sys.path.insert(0, CAFFE+'/python')
import caffe
from caffe.proto import caffe_pb2 #This is necessary to generate the leveldb file


#Import 'utils/LevelDict'
sys.path.insert(0, 'utils')

print "Imports done. Paths sets to: "
print "\t CAFFE: ", CAFFE
print "\t OXFORD: ", OXFORD
print "\t SIAMESE: ", SIAMESE
print "\t OUT: ", OUT
print
print


#Processing dataset##################################
list_files = os.listdir( os.path.join(OXFORD) )

#Get labels of classes - as many rows as total number of patches
data = np.loadtxt( os.path.join( OXFORD, "info.txt"  ), dtype=int )

#ignore second column
labels = data[:,0]

#Print dimensions dataset
n_patches_with_label = np.shape(labels)[0]
print "Number of patches for "+oxford_folder+ " is ", n_patches_with_label

# Processing maps########################

# List of .mbp images within the folder
list_map_files = glob.glob( os.path.join( OXFORD, '*.bmp' ) )

# Generate index of integers for the file
id_file_maps = np.arange( len(list_map_files) ).astype(np.int)


#Init the counter for the patches
count = 0

#Init leveldb where data is going to be stored
db = plyvel.DB( os.path.join( OUT, oxford_folder+'_leveldb' ), create_if_missing=True)

print "Generating leveldb for %s" %OXFORD
t_init = time.time()
#Processing GreyScaleMap of patches ####
for id_ in id_file_maps:
    tmap0 = time.time()
    
    #Check if we are trying to store a patch without label:
    if count >= n_patches_with_label:
        print "Finished processing patches: "
        print "Number patches %d, number labels %d" %( count, n_patches_with_label )
    
        #Finish the loop
        break
    
    # Generate filename to read patches
    file_map = os.path.join( OXFORD, 'patches%04d.bmp' % id_ )
    
    # Read map with patches
    img=mpimg.imread(file_map) # 0, 255 range
    
    # Compute number of patches per row/col. We know that patches are 64x64 resolution
    #n_patches_row = img.shape[0]/64
    #n_patches_col = img.shape[1]/64
    
    #print "Number of patches per row ", n_patches_row
    #print "Number of patches per col ", n_patches_row
    
    # For each patch of the map...
    for i in range(16):
        for j in range(16):
            #mpimg.imsave(os.path.join( OUT,'images', "test"+str(count)+"_"+str(labels[count])+".jpg"), img[64*i:64*(i+1),64*j:64*(j+1)].squeeze())
            arr = img[64*i:64*(i+1),64*j:64*(j+1)].squeeze() #(16,16) array
            arr = np.reshape( arr, (1, np.shape(arr)[0], np.shape(arr)[1]) ) #caffe requieres images (chan, y, x), chan=1 (greyscale)
            
            #Convert to datum objet and assign label
            datum = caffe.io.array_to_datum( arr, labels[count] )
            
            #Store in the dataset
            db.put( '%08d_%s' % (count, file_map), datum.SerializeToString() )
            count +=1

#When map is over print message
    print 'Map %d (from %d) processed. Patches stored in %s' %(id_,len(id_file_maps), os.path.join( OUT, oxford_folder+'_leveldb' ))
print 'Time elapsed: %.4f' % (time.time() - tmap0)

db.close()
print
print 'DONE! total elapsed time: %.4f' %( time.time()-t_init )

'''
    print
    print
    
    # Filling leveldb for caffe
    print "Init the leveldb object"
    db = plyvel.DB( os.path.join( OUT, oxford_folder+'_leveldb' ), create_if_missing=True)
    
    count = 0
    img = np.reshape(img, (1,np.shape(img)[0], np.shape(img)[1] ) ) # add color channel
    print "Re-shape image for datum object, shape ", np.shape( img )
    datum = caffe.io.array_to_datum( img, labels[count] )
    
    db.put( '%08d_%s' % (count, file_map), datum.SerializeToString() )
    print '%08d images in %s' %( count, os.path.join( OUT, oxford_folder+'_leveldb' ) )
    db.close()
    '''

