from UserDict import DictMixin
import leveldb
import json


class LevelDict(DictMixin):
    """Dict Wrapper around the Google LevelDB Database"""
    def __init__(self, path):
        """Constructor for LevelDict"""
        self.path = path
        self.db = leveldb.LevelDB(self.path)
    
    def __getitem__(self, key):
        return self.db.Get(key)
    
    def __setitem__(self, key, value):
        self.db.Put(key, value)
    
    def __delitem__(self, key):
        self.db.Delete(key)
    
    def __iter__(self):
        for k, v in self.db.RangeIter():
            yield v

    def keys(self):
        return [k for k, v in self.db.RangeIter()]
    
    def iteritems(self):
        return self.db.RangeIter()
    
    def rangescan(self, start=None, end=None):
        if start is None and end is None:
            return self.db.RangeIter()
        elif end is None:
            return self.db.RangeIter(start)
        else:
            return self.db.RangeIter(start, end)
