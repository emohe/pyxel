import numpy as np
import os

def get_class_label_correspondence(dir_path):

    #Get the class name and label assigned to each class in the training and validation files.

    files = os.listdir(dir_path)
    i = 0
    for f in files:
        print f, ': ', i
        i = i+1

def read_txt(txt_file):

    ''' Reads the contents of a text file
	'''

    with open(os.path.join(txt_file),"rb") as f:

        lines = f.readlines()

    return lines

def get_num_samples(lines):
    ''' Get the number of relevant images for a class

		Reads:

		lines - list of strings containing all the images and whether they are relevant to the class or not
				fullpath/image1 -1 (notrelevant)
				fullpath/image2 1 (relevant)
	'''


    num = 0

    for line in lines:

        if line.split()[1] == '1':
            num = num + 1

    return num

def build_dictionary():

    # Dictionary to read addition data from flickr
    dictionary = {'viking.txt':0,
            'marinera.txt':1,
            'correfoc.txt':2,
            'bud.txt':3,
            'songkran.txt':4,
            'lantern.txt':5,
            'diwali.txt':6,
            'santjordi.txt':7,
            'fasnatch.txt':8,
            'onbashira.txt':9,
            'infiorata.txt':10,
            'gion.txt':11,
            'harbin.txt':12,
            'dunkerque.txt':13,
            'tomatina.txt':14,
            'october.txt':15,
            'falles.txt':16,
            'quebec.txt':17,
            'silleteros.txt':18,
            'obon.txt':19,
            'rath.txt':20,
            'candelaria.txt':21,
            'tango.txt':22,
            'oranges.txt':23,
            'lewes.txt':24,
            'buffalo.txt':25,
            'midsommar.txt':26,
            'jaisalmer.txt':27,
            'venice.txt':28,
            'atiatihan.txt':29,
            'renaixement.txt':30,
            'sol.txt':31,
            'ballon.txt':32,
            'holi.txt':33,
            'nottinghill.txt':34,
            'rio.txt':35,
            'castellers.txt':36,
            'patrick.txt':37,
            'camel.txt':38,
            'sanfermin.txt':39,
            'muertos.txt':40,
            'chinese.txt':41,
            'sandfest.txt':42,
            'heiva.txt':43,
            'samba.txt':44,
            'queen.txt':45,
            'macys.txt':46,
            'timkat.txt':47,
            'maslenitsa.txt':48,
            'marathon.txt':49
            }

    return dictionary

def make_dir(new_path):

    if not os.path.exists(new_path):
        os.makedirs(new_path)
    else:
        print "The directory already existed. Files will be overwritten."
