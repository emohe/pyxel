# -------------------------------------------------------------------------------
# Name:        Chalearn LAP evaluation scripts
# Purpose:     Provide an evaluation script for Cultural Event Recognition
#
# Author:      Junior Fabian A.
#              ISE Lab @ CVC
#
# Created:     19/01/2015
# Copyright:   (c) Chalearn LAP 2015
# Licence:     GPL
#
# To run this script: python program/evaluate.py input output
#
#-------------------------------------------------------------------------------

import os,sys,glob
import numpy as np

#This folder contains the files with the ground truth
dirRef = sys.argv[1] + "/ref/"

#This folder contains the files with the confidences of your classifier
dirRes = sys.argv[1] + "/res/"
#dirRes = sys.argv[1] + "/"

#This script creates a file (scores.txt) containing the final mean average precision (mAP)
dirOut = sys.argv[2] + "/"

filesIn = glob.glob(dirRes +  "*.txt")

mAP = 0
#For each event
for fIn in filesIn:
    event = fIn.split("/")
    event = event[len(event)-1].split(".")[0]
    fIn = open(fIn, "rb")
    linesIn = fIn.readlines()
    fIn.close()
    fVal = 0
    event = event[4:] # matthias: remove the first four characters - they are added 2 times - thats too much

    # In this case, the ground truth is the file with the labels of Validation set

    if os.path.exists(dirRef + event + "_val.txt"):
        fVal = open(dirRef + event + "_val.txt", "rb")

    # In this case, the ground truth is the file with the labels of Test set [Final Phase]      
    elif os.path.exists(dirRef + event + "_test.txt"):
        fVal = open(dirRef + event + "_test.txt", "rb")
    else:
        print "File X not found: " + dirRes +  event + "\n"
        continue

    linesVal = fVal.readlines()
    fVal.close()
    fValPos = []
    fValNeg = []

    for lineVal in linesVal:
        if lineVal.split()[1] == "-1":
            fValNeg.append(lineVal.split()[0])
        if lineVal.split()[1] == "1":
            fValPos.append(lineVal.split()[0])

    In = {}

    for line in linesIn:
        img, score = line.split()
        In[img] = score

    resNeg = []
    resPos = []
    for line in fValNeg:
        resNeg.append(line + " " + In[line] + "\n")

    for line in fValPos:
        resPos.append(line + " " + In[line] + "\n")

    numberPoints = 200

    upperBound = 1.0
    lowerBound = 0.0
    rang = upperBound - lowerBound
    var = rang / numberPoints

    bound = lowerBound
    x = []
    y = []
    tics = 0
    while ((bound <= upperBound) and (tics < numberPoints)):
        tics = tics + 1
        negHits = 0
        posHits = 0
        for i in resNeg:
            res = float(i.split()[1])
            if res <= bound:
                negHits = negHits + 1
        for i in resPos:
            res = float(i.split()[1])
            if res > bound:
                posHits = posHits + 1

        #True positive
        TP = float(posHits)
        #False negative
        FN = float(len(resPos)) - TP
        #True negative
        TN = float(negHits)
        #False positive
        FP = float(len(resNeg)) - TN

        try:
            recall = TP / (TP + FN)
            precision = TP / (TP + FP)
            x.append(recall)
            y.append(precision)

        except:
            continue

        bound = bound + var

    auc = np.trapz(y, x) * -1
    mAP = mAP + auc

fileO = open(dirOut + "scores.txt", "wb")
fileO.write("mAP: " + str(mAP/50))
fileO.close()
