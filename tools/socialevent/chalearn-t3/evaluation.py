import numpy as np
import pickle
import os
import csv
import getpass
import sys
import chalearn_tools as ct
 
def store_evaluation_file(f, image_list, weight_list):
    '''

    Fills up the txt file 'f' with an entry for each image and its score

    '''
    i = 0

    image_list = np.array(image_list)[np.argsort(weight_list)[::-1]]
    weight_list = weight_list[np.argsort(weight_list)[::-1]]

    for image in image_list:
        f.write(image + ' ' + str(weight_list[i]) + '\n')
        
        i = i + 1

def normalize_data(feats):

    # Column normalization of features

    for i in range(np.shape(feats)[1]):
    
        feats[:,i] = ( feats[:,i] - np.min(feats[:,i]) ) / ( np.max(feats[:,i]) - np.min(feats[:,i] ) )

    return feats

if __name__== "__main__":

    config_name = str(sys.argv[1])
    layer_name = str(sys.argv[2])
    test_set = str(sys.argv[3])

    username = str(getpass.getuser())

    path_to_files = '/imatge/' + username + '/work/chalearn/'

    path_to_binary = path_to_files + '/binary_files/' + config_name

    image_names = pickle.load(open(path_to_binary + '/image_names_' + test_set +'.p', "rb") )
    feats = pickle.load(open(path_to_binary + '/features_' + config_name + '_' + layer_name + '_' + test_set + '.p', "rb") )

    path_to_train_labels = path_to_files + 'images/chalearn15-t3/Train_Labels/'
    
    save_path = path_to_files + '/evaluation_files/' + config_name + '_' + test_set + '/'

    ct.make_dir(save_path)

    files = os.listdir(path_to_train_labels)

    i = 0

    feats = normalize_data(feats)
    
    for f in files:
    
        evaluation_file = open(save_path + f.split('_train')[0] + '.txt', "w")
        
        weight_list = feats[:,i]
        store_evaluation_file(evaluation_file,image_names,weight_list)

        evaluation_file.close()
        i = i + 1
    
    
    