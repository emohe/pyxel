import numpy as np
import pickle
import os
import getpass
import chalearn_tools as ct
from sklearn.svm import SVC
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report

caffe_root = '/usr/local/opt/caffe/'

# Make sure that caffe is on the python path:
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe

def loadNetCNN(deploy_file, model_file, mean_file):

    '''
    Function to initialize the network

    :param deploy_file: deploy file from Caffe
    :param model_file: model file from caffe
    :param mean_file: mean file from caffe
    :return: net: the loaded network
    '''

    net = caffe.Classifier(deploy_file, model_file, gpu=True)
    net.set_phase_test()
    net.set_mode_gpu()
    net.set_mean('data', mean_file)  # ImageNet mean

    net.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB
    net.set_input_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
    return net

def extract_CNN_feature( net, imagepath, layer_name ):

    '''
    Function to extract features from a CNN given the image path.

    :param net: The loaded network
    :param imagepath: Path to the image
    :param layer_name: The name of the layer to extract
    :return: meanfeat: the extracted feature
    '''
    scores = net.predict([caffe.io.load_image(imagepath)])


    feat = net.blobs[layer_name].data

    # Average of the 10 responses
    meanfeat = np.average( feat, axis = 0 )
    return meanfeat

def fit_classifier(feature_matrix,labels):

    '''
    Function that fits the training data to the classifier.
    This could be used to train our own classifier from layers 6 or 7 instead of taking the softmax layer.

    :param feature_matrix: features stored in each row of the matrix
    :param labels: class indices of each row of the matrix
    :return: clf: the trained classifier
    '''

    param_grid = [
        {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
        {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']},
    ]

    clf = tune_parameters(feature_matrix,labels,param_grid)

    clf.fit(feature_matrix,labels)

    return clf

def tune_parameters(feature_matrix,labels,param_grid):

    '''
    Function to tune an SVM classifier and choose its parameters

    :param feature_matrix: training data
    :param labels: labels for training data
    :param param_grid: grid of parameters to try
    :return: clf.best_estimator_ the best classifier

    '''
    X_train, X_test, y_train, y_test = train_test_split(feature_matrix, labels, test_size=0.2, random_state=0)

    score = 'accuracy'

    clf = GridSearchCV(SVC(C=1), param_grid, cv=5, scoring=score)

    clf.fit(X_train, y_train)

    print "Best score during training: ", clf.best_score_

    print "Accuracy for validation set:"
    print classification_report(y_test,clf.predict(X_test))

    return clf.best_estimator_


if __name__ == '__main__':

    username = str(getpass.getuser())

    config_name = str(sys.argv[1])
    layer_name = str(sys.argv[2])
    test_set = str(sys.argv[3])
    layer_size = int(float(sys.argv[4]))

    path_to_files = '/imatge/' + username + '/work/chalearn/'

    path_to_binary = path_to_files + '/binary_files/' + config_name

    ct.make_dir(path_to_binary)

    path_to_model = path_to_files +'/models/' + config_name

    # Net files

    # You have to save your model files like this, so that the script can read any model that you may train
    mean_file = path_to_model + '/meanfile.npy'
    model_file = path_to_model + '/model.caffemodel'
    deploy_file = path_to_model + '/deploy.prototxt'


    # Testing data file

    if test_set == 'val':
        test_folder = path_to_files + 'images/chalearn15-t3/VAL/'
    if test_set == 'train':
        test_folder = path_to_files + 'images/chalearn15-t3/TRAIN/'
    if test_set == 'test':
        test_folder = path_to_files + 'images/chalearn15-t3/TEST/TEST/'


    # Load the net
    net = loadNetCNN(deploy_file,model_file,mean_file)

    image_names = []
    predicted_labels = []
    weights = []

    # Initialize the matrix to store the test feature vectors
    imlist = os.listdir(test_folder)
    feature_matrix = np.zeros( ( len( imlist ) ,layer_size) )

    i = 0

    for line in imlist:

        print i

        # Select image
        image_path = os.path.join(test_folder,line)

        # Extract the according feature
        feat = extract_CNN_feature(net,image_path,layer_name)


        # Add it to the matrix
        feature_matrix[i,:] = np.squeeze(feat)

        # Also store the names, weights and predicted labels in separated lists
        image_names.append(line)
        weights.append( str( np.max( feat ) ) )
        predicted_labels.append( str( np.argmax( feat ) ) )

        i += 1

    # Store all generated objects
    pickle.dump(feature_matrix, open(path_to_binary + '/features_' + config_name + '_' + layer_name + '_' + test_set + '.p', "wb") )
    pickle.dump(image_names, open(path_to_binary + '/image_names_' + test_set + '.p', "wb") )

    print "Done. Stored prediction."

