import numpy as np
import os, re
import csv
import pandas as pd
import getpass
import chalearn_tools as ct
import sys
import pickle

def prepare_training_data(train_file,val_file,dir_path,image_path,percentage):
    ''' Function to generate train and validation files from the training data to fit to Caffe.

        Reads:

	train_file : file to write the training images
	val_file : file to write the validation images
	dir_path : directory where the per class .txt files are located
	image_path : path where the images are located
    percentage : percentage of images to be used as training images (100-percentage will be used as validation)


    '''

    # List all files
    files = os.listdir(dir_path)
    training_samples_per_class = []
    num_training_samples = 0
    num_validation_samples = 0
    # Class counter
    i = 0

    # For every class...
    for f in files:

        # Read the lines of the corresponding file
        lines = ct.read_txt(os.path.join(dir_path,f) )

        # Count the positive examples so that we can separate in two halfs for train/val
        num_samples = ct.get_num_samples(lines)

        training_samples_per_class.append(num_samples*percentage)

        num_training_samples += percentage * num_samples
        num_validation_samples += (1 - percentage) * num_samples
        #print "NumSamples for class",f, ': ', num_samples

		# Sample counter
        ii = 0

        # For each image (line in the file)
        for line in lines:

            # If it is relevant to the class
            if line.split()[1] == '1':

            # And we are in the first half of samples
                if ii < percentage * num_samples:
                    # Write the full path and class number to the training file
                    train_file.write(os.path.join(image_path,line.split()[0] ) + '\t' + str(i) + '\n')
                else:
                    # else write it to the validation file
                    val_file.write(os.path.join(image_path,line.split()[0] ) + '\t' + str(i) + '\n' )

                ii += 1

        i += 1

    print "Number of provided images used for training: ", num_training_samples
    print "Number of provided images used for validation: ", num_validation_samples

    return training_samples_per_class


def prepare_additional_data_similarity(train_file,path_to_files,dictionary,training_samples_per_class,percentage_to_keep,flickr_similarities):

    ''' Function to generate train and validation files from the additional data from flickr.

    Reads:

	train_file : file to write the training images
	dir_path : directory where the images are located
    dict: dictionary of name and class number correspondence

    '''

    # Read the csv file with pandas

    # Get the folder names containing images of each class
    class_names = os.listdir(path_to_files)

    selected_samples = 0
    total_samples = 0

    # For every folder/class...
    for c in class_names:

        # Get all the images it contains
        image_names = os.listdir(os.path.join(path_to_files,c))

        i = 0

        # Take the index corresponding to this class
        class_number = dictionary[c]

        num_to_add = int(percentage_to_keep * training_samples_per_class[class_number])

        image_names = pickle.load( open(flickr_similarities + str(class_number) + '.p', 'rb') )


        # And for the first num_to_add images...
        for image_name in image_names[0:num_to_add]:
            print image_name[0][0], class_number
            train_file.write(image_name[0][0] + '\t' + str(class_number) + '\n')

            total_samples += 1

    print "Number of images added to training: ", total_samples

def prepare_additional_data_filtered(train_file,path_to_files,dictionary,filtered_csv):

    ''' Function to generate train and validation files from the additional data from flickr.

    Reads:

	train_file : file to write the training images
	dir_path : directory where the images are located
    dict: dictionary of name and class number correspondence
    filtered_csv: file containing the remaining images from filtering. only the ids in that file should be added
    '''

    # Read the csv file with pandas
    filtered_ids = pd.read_csv(filtered_csv,sep='\t',header=None)

    # Get the folder names containing images of each class
    class_names = os.listdir(path_to_files)
    print class_names
    selected_samples = 0
    total_samples = 0

    # For every folder/class...
    for c in class_names:

        # Get all the images it contains
        image_names = os.listdir(os.path.join(path_to_files,c))

        i = 0
        print c
        # Take the index corresponding to this class
        class_number = dictionary[c]

        # And for every image...
        for image_name in image_names:

            # Create its id to search the csv file
            id = int(float(image_name.split('_')[0]))

            # And if the csv file contains it
            if any(filtered_ids[1] ==id):
                 # We add the image to the training file
                train_file.write(os.path.join(path_to_files,c) + '/' + image_name + '\t' + str(class_number) + '\n')
                selected_samples = selected_samples + 1

            i = i + 1
            total_samples += 1

    print total_samples, selected_samples

def create_real_validation(validation_file,dir_path,image_path):

    files = os.listdir(dir_path)

    # Class counter
    i = 0
    ii = 0
    # For every class...
    for f in files:

        # Read the lines of the corresponding file
        lines = ct.read_txt(os.path.join(dir_path,f) )

        # For each image (line in the file)
        for line in lines:

            # If it is relevant to the class
            if line.split()[1] == '1':

            # And we are in the first half of samples

                validation_file.write(os.path.join(image_path,line.split()[0] ) + '\t' + str(i) + '\n')
                ii = ii + 1

        i += 1

    print "Number of real validation samples: ", ii


if __name__ == "__main__":

    username = str(getpass.getuser())

    config_name = str( sys.argv[1])
    additional_data_val = sys.argv[2]


    path_to_files = '/imatge/' + username + '/work/chalearn/'

    train_images_path = path_to_files + 'images/chalearn15-t3/TRAIN/'
    train_labels_path = path_to_files + 'images/chalearn15-t3/Train_Labels/'

    save_path = path_to_files + 'txt_for_caffe/' + config_name + '/'

    ct.make_dir(save_path)

    # This is still hardcoded...but it should be a parameter (we have 3 flickr subsets)
    path_to_additional_files = path_to_files + 'images/flickr9k/pictures/'

    # This one will contain a partition of the training files
    val_file = open(save_path + 'val_' + config_name + '.txt','w')

    # This one contains the real validation data (added later on, when it became available).
    real_val_file = open(save_path + 'realval_' + config_name + '.txt','w')

    val_images = path_to_files + 'images/chalearn15-t3/VAL/'
    val_labels_path = path_to_files + 'images/chalearn15-t3/Val_Labels/'

    if additional_data_val == '0':
        train_file = open(save_path + 'train_' + config_name + '.txt', 'w')
    elif additional_data_val =='1':
        train_file = open(save_path + 'train_' + config_name + '_w_additional_similarity.txt', 'w')
    else:
        train_file = open(save_path + 'train_' + config_name + '_w_additional_filtered.txt', 'w')


    # Write the real validation file for later use
    create_real_validation(real_val_file,val_labels_path,val_images)

    percentage = 1

        # Load from training images
    training_samples_per_class = prepare_training_data(train_file,val_file,train_labels_path,train_images_path,percentage)

    # Load from additional images, if specified:

    if additional_data_val=='1':

        # Reset training file, only adding the new images
        #train_file.close()
        #train_file = open(save_path + 'train_' + config_name + '_w_additional_similarity.txt', 'w')

        dictionary = ct.build_dictionary()
        path_to_flickr_similarities = path_to_files + 'flickr_similarity/' + config_name + '_21/'
        percentage_to_keep = 0.7
        prepare_additional_data_similarity(train_file,path_to_additional_files,dictionary,training_samples_per_class,percentage_to_keep,path_to_flickr_similarities)

    if additional_data_val == '2':

        #train_file.close()
        #train_file = open(save_path + 'train_' + config_name + '_w_additional_filtered.txt', 'w')

        dictionary = ct.build_dictionary()
        csv_file = path_to_files + 'flickr_filtered/flickrImageScores_threshold0.9_withLocationsAndInterestingness.csv'
        prepare_additional_data_filtered(train_file,path_to_additional_files,dictionary,csv_file)


    # Close files
    train_file.close()
    val_file.close()
