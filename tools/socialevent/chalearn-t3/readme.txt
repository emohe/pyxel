================================================================

Instructions on how to use the python scripts for ChaLearn 2015.

Author: Amaia Salvador Aguilera (amaia91@gmail.com)
If you have any questions just drop me an e-mail.

================================================================

STEP 1: DIRECTORY STRUCTURE 

- First, you need to make sure that you have a folder under your /imatge/username/work called 'chalearn'. This is what the folder should contain:

'images' - this directory includes two sub_directories:
	'chalearn15-t3' - this is a symbolic link to the training images. cd into 'images' directory and run "ln -s /projects/retrieval/chalearn15-t3/ ."
	'flickr_additional' - this contains the symbolic link to the additional 91k flickr images. Create this folder, cd into it and run "ln -s /imatge/dmanchon/work/chalearn/* ."
  'pictures'  - this contains the 21k flickr subset.
	
'txt_for_caffe' - Path where the training and validation files will be stored for each configuration. Just create it, no need to put anything in there. More details on how this directory is used will be given later.

'models' - Path to store your trained caffe models. This is where it all starts getting a bit tricky. What you need to do is to store them in a folder, which should have the name of the configuration. All your models
	should be stored in separate folders under this directory. Each one of these folders should contain the caffe model, the deploy file and the mean file. Something like this:

	configuration_1/
		model.caffemodel
		deploy.prototxt
		meanfile.npy
	configuration_2/
		model.caffemodel
		deploy.prototxt
		meanfile.npy


	It is mandatory that you name the three files as indicated above so that the scripts can work.
	
'binary_files' - path where the binary files will be stored for each configuration. Just create it, no need to put anything in there. More details on how this directory is used will be given later.

'flickr_filtered' - path with csv files containing the list of flickr images filtered with temporal constraints using different thresholds. The data in this folder is no longer used in my code, 
	but in case you need it you can just copy it from my work folder /imatge/asalvador/chalearn/flickr_filtered.
	
'flickr_similarity' - path where the flickr similarities wrt a trained network will be stored. For each configuration, the scripts will generate a folder containing 50 binary files containing the flickr images
	for each class sorted by relevance according to the network used. The folder will have the name of the configuration used.
	
'evaluation_files' - Path where the results files will be stored to be uploaded to codalab for evaluation. Each configuration tested will have its own folder with its distinctive name.

IMPORTANT: Currently there is no script available that generates this directory structure automatically. Please do it manually for the moment. Or if you are up for it you can write one yourself... :)
Please read further to understand how these paths will be used and how the data is going to be stored in them.

STEP 2: PREPARE DATA (prepare_data.py)

How to run it: 

>>> python prepare_data.py configuration_name additional_data_val

Example
>>> python prepare_data.py bvlc_reference_caffenet 0

Now: this 'configuration_name' CANNOT change throughout the whole procedure. 
As for the 'additional_data_val':
  -Type 0 if you want to use the training images provided (the ratio of train/val is hard coded!).
  -Type 1 if you want to use flickr images according to similarity to the trained model (in this case, read STEP 6 before reading further) - the 21k flickr subset is used (hardcoded).
  -Type 2 if you want to use flickr images based on GPS and time filtering (the csv file used to filter is hard coded!).

When it finishes, this script will create a folder under /txt_for_caffe/ called 'configuration_name', which will contain the training and validation files ready for Caffe.

** Note that Using additional_data_val equal to 1, the script will make a partition from training data. If you want to switch to the real train/val data, you only need to take the txt for the real validation data
   (this script already writes it, using the function create_real_validation),and use the validation data txt that you have been using so far for training. 
   Once you have those two files, you can fine tune the network that you fine tuned before (using as training images those images that the network has not seen).
   I hope this last comment is clear... if you have any questions please let me know!
   
STEP 3: TRAIN NETWORK

Now that you have your train and validation txt files, you can train your network. Follow the tutorial on Caffe and the one that I wrote to know what steps you need to do and what additional files are needed:

http://caffe.berkeleyvision.org/gathered/examples/finetune_flickr_style.html
https://docs.google.com/document/d/1zPR3JAIsWihOP0Zy1NJwAHKdfYJxWZkm4X9vw68sb_Q/edit?usp=sharing

For this project, we fine-tuned CaffeNet with the provided ChaLearn data. So, provided that at this point you would have the necessary files, you would run something like:

>>> /path/to/your/caffe/installation/build/caffe.bin train -solver /path/to/your/solver/solver.prototxt -weights /path/to/caffe/models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel -gpu 0

When you have your model trained, make sure you save the files in 'models' under a folder named 'configuration_name'. Make sure you change the file names accordingly as well!

STEP 4: EXTRACT FEATURES (prediction.py)

So now that you have your model, you can extract features for the evaluation dataset (or validation, for the moment). You need to run this script as follows:

>>> srun -w c6 --gres=gpu:1 python prediction.py configuration_name layer_name test_set layer_size &

Example:

>>> srun -w c6 --gres=gpu:1 python prediction.py bvlc_reference_caffenet fc8 val 1000 &

You need to run it in the computational server (srun), so that it will use GPU to extract features. Again, the configuration_name needs to be the same all the time. Make sure that is true.
You also need to pass the 'layer_name', which, if you want this script to work, should be the last layer of the network (softmax). You should also indicate whether you want to extract features on the 'train' or 'val' sets
(test_set parameter). You also need to pass the layer_size of the layer you want to extract (so that you can preallocate the space of the feature matrix beforehand).
This script will create a folder with the configuration_name under 'binary_files'. It will contain: a binary file with all the features extracted for every image and a binary file with a list of the corresponding image names.

STEP 5: STORE EVALUATION FILES (evaluation.py)

Now you have all your data ready, let's put it in a valid format to evaluate our results with CodaLab. Just run:

>>> python evaluation.py configuration_name layer_name test_set

Example:

>>> python evaluation.py bvlc_reference_caffenet fc8 val

This will create a folder under 'evaluation_files' with the configuration_name, which will contain the 50 txt files with the scores obtained for each image in each category.
You also need to pass the layer_name that you used before during prediction. Just put them in a zip file and upload it to CodaLab!

STEP 6: SELECTING FLICKR DATA BASED ON SIMILARITY (select_additional_data.py)

This step only applies if you have already trained a network (which you should have stored in the 'models' folder) and you want to use it to select images from flickr to add as training samples and train again.
If this is the case, you can run this script as follows:

>>> srun -w c6 --gres=gpu:1 python select_additional_data.py configuration_name layer_name flickr_subset &

Example:

>>> srun -w c6 --gres=gpu:1 python select_additional_data.py last_layer_only fc8_chalearn 21

This will use the 'configuration_name' model to extract features for the flickr images using the specified 'layer_name' (which again should always be the softmax if you want this to work).
The flickr images for each class will be sorted according to their score for that class in the softmax, and the sorted list of names will be stored in a folder called configuration_name under the
'flickr_similarity' folder. You also have to indicate which flickr subset to use (the 91k one or the 21k one)

The files in this folder will be used to add training samples for Caffe if you run the prediction.py script with additional_data_bool equal to 1.
Once you do that, you will have some new training samples in your train and val txt files under 'txt_for_caffe/configuration_name', which you can use to train again (continuing to steps 3-5).

STEP 7 (IF REQUIRED): STORE FEATURES AS CSV FILES (to_csv.py)

You can store the feature vectors as csv files with this script. To do this you will need to create an additional folder in /work/chalearn called 'csv_feats'. You can call the script as:

>>> python to_csv.py configuration_name layer_name test_set
