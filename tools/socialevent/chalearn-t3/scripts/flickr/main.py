import json
import flickrapi
import itertools


def main():
    # Read config
    config = json.loads(open("flickr.key.json","r").read())
    api_key = config["key"]
    api_secret = config["secret"]
    events = json.loads(open("events.json","r").read())
    # Config flickr
    flickr = flickrapi.FlickrAPI(api_key, api_secret)
    db = open("db.json","wb")
    db.write("[\n")

    for event in events:
        name = event.keys()[0]
        print name
        tags = event[name]["tags"]
        try:
            text = event[name]["text"]
        except:
            text = ""
        #Get the place_id

        geo_query = flickr.places.find(query=event[name]["location"])
        for place in geo_query.find('places').findall('place'):
            #if place.get("place_type") == "country":
            woeid = place.get("woeid")
            break

        f = open(name+".txt","wb")
        # Get pictures
        formats = "url_z,url_sq,url_t,url_s,url_q,url_m,url_n,url_z,url_c,url_l,url_o"
        query = flickr.walk(tag_mode='all',privacy_filter=1,content_type=1,
            extras=formats+",geo,date_upload,date_taken,owner_name,tags,views,description",
            text=text,
            sort="interestingness-desc",
            woeid=woeid,
            has_geo=1,
            tags=tags)

        for photo in itertools.islice(query,0,200):
            try:
                url = None
                for format in formats.split(","):
                    url = photo.get(format)
                    if url != None:
                        break
                print url
                f.write(url+"\n")
                db.write(str({"category":name,"url":url, "info":photo.items()})+",\n")

            except:
                print "Error: "+str(photo.keys())

        f.close()
    db.write("]")
    db.close()

if __name__ == "__main__":
    main()
