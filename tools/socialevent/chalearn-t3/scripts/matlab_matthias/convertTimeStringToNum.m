function dateNumber = convertTimeStringToNum(timestr, formats)

if isempty(timestr)
    dateNumber=NaN;
    return
end

nFormats = numel(formats);

for i=1:nFormats
     try
        dateNumber = datenum(timestr,formats{i});
        break
     catch
         dateNumber=NaN;
     end
end