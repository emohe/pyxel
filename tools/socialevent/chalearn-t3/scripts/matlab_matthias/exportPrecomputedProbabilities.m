function exportPrecomputedProbabilities(filenames, probabilityMatrix, folder, GT)


mkdir_force(folder);

dataCell={};
dataCell(:,1) = num2cell(filenames);
dataCell(:,1) = cellfun(@(x) [generateFileName(x,6) '.jpg'], dataCell(:,1), 'UniformOutput', false); %reconstruct file name

%GENEATE RANDOM DATA
%svmResult.decisionValues = rand(size(svmResult.decisionValues)); 
%svmResult.decisionValues = svmResult.decisionValues ./ repmat(sum(svmResult.decisionValues,2),1,50);

for i=1:GT.nClasses
    className = GT.classNames{i,2};
    dataCell(:,2) = num2cell(probabilityMatrix(:,i));
    cell2csv([folder filesep className], dataCell, ' ');
end

%C:\Program Files (x86)\Python\python.exe program/evaluate.py input output
