function pdf=fitMyCircularGaussian(mu,sigma,x)

center = round(numel(x)/2);
shift = round(mu-center);
model = normpdf(x,center,sigma);
model=normalizeMinMax(model');
model = circshift(model,[shift,0]); %model must be a column!
pdf=model;