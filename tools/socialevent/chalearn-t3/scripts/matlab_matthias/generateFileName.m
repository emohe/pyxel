%Erzeugt die Dateinamen mit f�hrenden Nullen aus der Nummer des Kaders/Frames!
% INPUTS
%   - numberOfFrame: positive ganze Zahl
%   - TotalLengthOfFileName: anzahl Stellen, die der Dateiname haben soll,
%
% OUTPUTS
%   - fileName: zahl mit f�hrenden nullen als string!

function fileName = generateFileName(numberOfFrame, TotalLengthOfFileName)

numberOfFrameStr = int2str(numberOfFrame);
numOfDigits = length(numberOfFrameStr); %todo: gibts da was besseres?
if numOfDigits > TotalLengthOfFileName
    error(['The frame number (' num2str(numberOfFrame) ') is longer than the specified total length of the file name (' num2str(TotalLengthOfFileName) ' digits)']);
end

numOfZeros = TotalLengthOfFileName-numOfDigits;

if numOfZeros > 0 && numOfZeros < TotalLengthOfFileName
    fileName = [strrep(int2str(zeros(1,numOfZeros)), ' ','')  numberOfFrameStr];
else
    fileName = int2str(numberOfFrame);
end


