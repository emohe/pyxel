function [filenames, probabilityMatrix] = importPrecomputedProbabilities(inputFolder)

%inputFolder = InputProbabilities_ImageNet;

d=dir([inputFolder filesep '*.txt']);
nClasses= size(d,1);

probabilityMatrix = [];
for i=1:nClasses
    currentMatrix = csv2cell([inputFolder filesep d(i).name],'fromfile',' ');
    %first cell element contains the filenames of the images
    filenames = cell2mat(cellfun(@(x) str2num(x(1:end-4)), currentMatrix(:,1), 'UniformOutput', false)); %get file numbers (remove '.jpg')
    %second cell element contains the labels
    probabilities = cell2mat(cellfun(@(x) str2num(x), currentMatrix(:,2), 'UniformOutput', false)); %get labels
    
    %sort filenames:
    [fileNamesSorted, sortIdx] = sort(filenames);
    probabilitiesSorted = probabilities(sortIdx); %use classID as label
    
    %store labels:
    probabilityMatrix(:,i) = probabilitiesSorted;
end

filenames = fileNamesSorted;