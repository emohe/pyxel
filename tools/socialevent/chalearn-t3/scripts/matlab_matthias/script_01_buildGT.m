clear all

dataset = 'train';

%get folder with labels
switch dataset
    case 'train'
        inputFolder = 'E:\Datasets\ChaLearn - Cultural Event Recognition\data\training\TrainingLabels\Train_Labels';
    case 'validation'
        inputFolder = 'E:\Datasets\ChaLearn - Cultural Event Recognition\data\validation\validationLabels';
    case 'test'
end

%% Build GT: read all trainings labels for each class and generate one label vector that contains the class ID for each photo.

%get all text files
d=dir([inputFolder filesep '*.txt']);
nClasses= size(d,1);

%resort manually to have the same ids like the organizers: the problem is the different alphabetical sorting produced by the dir command
helper = d(10); d(10) = d(11); d(11) = helper;
helper = d(17); d(17) = d(18); d(18) = helper;
helper = d(45); d(45) = d(46); d(46) = helper;


labelMatrix = [];
classNames = {};
for i=1:nClasses
    formatSpec = '%s%s%[^\n\r]'; %format of GT files
    fileID = fopen([inputFolder filesep d(i).name],'r'); %load file, scan and close again
    currentGT = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
    fclose(fileID);
    %first cell element contains the filenames of the images
    currentGT{1} = cellfun(@(x) str2num(x(1:end-4)), currentGT{1}, 'UniformOutput', false); %get file numbers (remove '.jpg')
    %second cell element contains the labels
    currentGT{2} = cellfun(@(x) str2num(x), currentGT{2}, 'UniformOutput', false); %get labels
    fileNames = cell2mat(currentGT{1});
    labels = cell2mat(currentGT{2});
    
    %sort filenames:
    [fileNamesSorted, sortIdx] = sort(fileNames);
    labels(labels==-1) = 0; %set -1 to 0
    labelsSorted = labels(sortIdx) .* i; %use classID as label
    
    %store labels:
    labelMatrix(:,i) = labelsSorted;
    
    labelVector = sum(labelMatrix,2); %generate vector of labels (value range: 1...nClasses)
    switch dataset
        case 'train'
            classNames(end+1,:) = {i strrep(d(i).name(1:end),'_train.txt','.txt')}; %store class name for later use (to know which ID is which class)
        case 'validation'
            classNames(end+1,:) = {i strrep(d(i).name(1:end),'_val.txt','.txt')}; %store class name for later use (to know which ID is which class)
    end
end

figure; imagesc(labelMatrix);
figure; plot(labelVector);
figure; hist(labelVector,1:1:nClasses); 
saveas(gcf,['E:\CulturalEventRec\myData\classCardinalities_' dataset '.png']);
GT.labels = labelVector;
GT.files  = fileNamesSorted;
GT.classNames = classNames;
GT.nClasses = nClasses;
ExportMAT(GT,['E:\CulturalEventRec\myData\GT_' dataset '.mat']);

