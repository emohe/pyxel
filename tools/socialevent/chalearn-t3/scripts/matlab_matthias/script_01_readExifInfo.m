dataset = 'train';

switch dataset
    case 'train'
        InputFolder = 'E:\Datasets\ChaLearn - Cultural Event Recognition\data\training\trainingImages'
    case 'validation'
        InputFolder = 'E:\Datasets\ChaLearn - Cultural Event Recognition\data\validation\images'
    case 'test'
end

%% Quick and dirty export of exif information:

d=dir([InputFolder filesep '*.jpg']);
nFiles = size(d,1);
EXIF.TIME = {};
EXIF.LOC = {};
EXIF.TEXT = {};
for i=1:nFiles
    i
    info = imfinfo([InputFolder filesep d(i).name]);

    try
       dateTimeCamera = info.DigitalCamera.DateTimeOriginal;
    catch
       dateTimeCamera ='';
    end
    
    try
       dateTimeCameraDigitized = info.DigitalCamera.DateTimeDigitized;
    catch
       dateTimeCameraDigitized ='';
    end
    
    try
       software = info.Software;
    catch
       software ='';
    end
    
    try
       ImageDescription = info.ImageDescription;
    catch
       ImageDescription ='';
    end
    
    try
       DateTime = info.DateTime;
    catch
       DateTime ='';
    end
    
    try
       ModifyDate = info.ModifyDate;
    catch
       ModifyDate ='';
    end
    
    try
       TimeZoneOffset = info.TimeZoneOffset;
    catch
       TimeZoneOffset ='';
    end
    
    try
       DateTimeOriginal = info.DateTimeOriginal;
    catch
       DateTimeOriginal ='';
    end
    
    try
       Flash = info.Flash;
    catch
       Flash ='';
    end
    
    try
       UserComment = info.UserComment;
    catch
       UserComment ='';
    end
    
    try
       SubSecTimeOriginal = info.SubSecTimeOriginal;
    catch
       SubSecTimeOriginal ='';
    end
    try
        SubSecTimeDigitized = info. 	SubSecTimeDigitized;
    catch
        SubSecTimeDigitized ='';
    end
    
    try
       XPKeywords = info.XPKeywords;
    catch
       XPKeywords ='';
    end
       
    try
       GPSInfo = info.GPSInfo;
       %disp('Found GPS INFO')
       GPSInfo
       i
       try
           GPSLatitude = GPSInfo.GPSLatitude;
       catch 
           GPSLatitude ='';
       end
       
       try
           GPSLongitude = GPSInfo.GPSLongitude;
       catch 
           GPSLongitude ='';
       end
       
       try
           GPSLatitudeRef = GPSInfo.GPSLatitudeRef;
       catch 
           GPSLatitudeRef ='';
       end
       
       try
           GPSLongitudeRef = GPSInfo.GPSLongitudeRef;
       catch 
           GPSLongitudeRef ='';
       end

       try
           GPSTimeStamp = GPSInfo.GPSTimeStamp;
       catch 
           GPSTimeStamp ='';
       end
       
       try
           GPSDateStamp = GPSInfo.GPSDateStamp;
       catch 
           GPSDateStamp ='';
       end
       
    catch
       GPSInfo ='';
       GPSLatitude ='';
       GPSLongitude ='';
       GPSLatitudeRef ='';
       GPSLongitudeRef ='';
       GPSDateStamp='';
       GPSTimeStamp='';
    end
    
    try
       GPS = info.gps;
       disp('Found GPS INFO')
       GPS
    catch
       GPS ='';
    end
    
    EXIF.TIME(end+1,:) = {d(i).name dateTimeCamera dateTimeCameraDigitized   ...
        DateTime ModifyDate TimeZoneOffset DateTimeOriginal SubSecTimeOriginal...
        SubSecTimeDigitized };
    
    EXIF.LOC(end+1,:) = {d(i).name GPSLatitude GPSLongitude GPSLatitudeRef GPSLongitudeRef};
    
    EXIF.TEXT(end+1,:) = {d(i).name software ImageDescription Flash UserComment XPKeywords};
end

%% How much metadata do we have? (Just for analysis. This code generates no output that is later used)

counterLoc=0;
counterTime=0;
counterText=0;
counterTimeConsistent=0;
timeDiff = [];
for i=1:nFiles
    %is there time info?
    if ~isempty(EXIF.TIME{i,2}) || ~isempty(EXIF.TIME{i,3}) || ~isempty(EXIF.TIME{i,4})
        counterTime = counterTime+1;
    end
    
%     %is the time info consistent among all 3 entries?
%     if strcmp(EXIF.TIME{i,2},EXIF.TIME{i,3}) && strcmp(EXIF.TIME{i,3}, EXIF.TIME{i,4}) && ~isempty(EXIF.TIME{i,2}) && ~isempty(EXIF.TIME{i,3}) && ~isempty(EXIF.TIME{i,4})
%         counterTimeConsistent = counterTimeConsistent+1;
%     end
     %is the time info consistent among all 3 entries?
    if strcmp(EXIF.TIME{i,2},EXIF.TIME{i,3}) && ~isempty(EXIF.TIME{i,2}) && ~isempty(EXIF.TIME{i,3})
        counterTimeConsistent = counterTimeConsistent+1;
    end
    
    if ~isempty(EXIF.TIME{i,2}) && ~isempty(EXIF.TIME{i,4})
        %compute time differences:
        try 
            date1  = datenum(EXIF.TIME{i,2},'yyyy:mm:dd HH:MM:SS');
            date2  = datenum(EXIF.TIME{i,4},'yyyy:mm:dd HH:MM:SS');
            timeDiff(end+1) = (date1-date2)
            
            %Debug:
%             if timeDiff(end)<-1
%                  EXIF.TIME{i,2}
%                  EXIF.TIME{i,4}
%                  pause
%             end
        catch
        end
    end
    
    %is there complete location info?
    if ~isempty(EXIF.LOC{i,2}) %&& ~isempty(EXIF.LOC{i,3}) && ~isempty(EXIF.LOC{i,4}) && ~isempty(EXIF.LOC{i,5})
        counterLoc = counterLoc+1;
    end
    
    %is there text info?
    if ~isempty(EXIF.TEXT{i,3})
        counterText = counterText+1;
    end
end

percentage.time = counterTime./nFiles*100;
percentage.loc = counterLoc./nFiles*100;
percentage.text = counterText./nFiles*100;
percentage

%% Get a time stamp per file:
%Get time data form the 3 most promising Exif Fields and take the minimum as our estimate.

EXIF.TIME_FILTERED=[];
EXIF.TIME_YMD=[];
for i=1:nFiles
    dateNumbers=[NaN NaN NaN];
    %get date 1:
    if ~isempty(EXIF.TIME{i,2})
        dateNumbers(1) = convertTimeStringToNum(EXIF.TIME{i,2}, {'yyyy:mm:dd HH:MM:SS' 'yyyy-mm-dd HH:MM:SS' 'yyyy-mm-ddTHH:MM:SS' 'HH:MM:SS yyyy:mm:dd'});
        if isnan(dateNumbers(1))
            disp(['the following date information could not be processed (index=' num2str(i) '):'])
            EXIF.TIME{i,2}
        end
    end

    %get date 2:
    if ~isempty(EXIF.TIME{i,3})
        dateNumbers(2) = convertTimeStringToNum(EXIF.TIME{i,3}, {'yyyy:mm:dd HH:MM:SS' 'yyyy-mm-dd HH:MM:SS' 'yyyy-mm-ddTHH:MM:SS' 'HH:MM:SS yyyy:mm:dd'});
        if isnan(dateNumbers(2))
            disp(['the following date information could not be processed (index=' num2str(i) '):'])
            EXIF.TIME{i,3}
        end
    end

    %get date 3:
    if ~isempty(EXIF.TIME{i,4})
        dateNumbers(3) = convertTimeStringToNum(EXIF.TIME{i,4}, {'yyyy:mm:dd HH:MM:SS' 'yyyy-mm-dd HH:MM:SS' 'yyyy-mm-ddTHH:MM:SS' 'HH:MM:SS yyyy:mm:dd'});
        if isnan(dateNumbers(3))
            disp(['the following date information could not be processed (index=' num2str(i) '):'])
            EXIF.TIME{i,4}
        end
    end
    
    %get minimum of time as representative (the earliest time stamp)
    EXIF.TIME_FILTERED(i) = nanmin(dateNumbers);
    
    %if the date in the future - or too far in the past: do not use it!
    if (EXIF.TIME_FILTERED(i) > datenum(date) || EXIF.TIME_FILTERED(i) < datenum('2000-01-01') ) &&  ~isnan(EXIF.TIME_FILTERED(i))
        disp(['removing time: ' num2str(EXIF.TIME_FILTERED(i)) ', which refers to: ' datestr(EXIF.TIME_FILTERED(i))]);
        EXIF.TIME_FILTERED(i) = NaN;
    end

    %get Year, month and day separately:
    EXIF.TIME_YMDHMS(i,:) = datevec(EXIF.TIME_FILTERED(i));
    
    if ~isnan(EXIF.TIME_YMDHMS(i,1))
        %get the number of the day in the year:
        %trick: set years to a constant value, e.g. the year 2000:
        EXIF.TIME_D(i)= daysact('2000-01-01', ['2000-' num2str(EXIF.TIME_YMDHMS(i,2)) '-' num2str(EXIF.TIME_YMDHMS(i,3))])+1;
        if EXIF.TIME_D(i)==0
            EXIF.TIME_YMDHMS
            pause
        end
    else
        EXIF.TIME_D(i)=NaN;
    end
end

EXIF.TIME_YMD = EXIF.TIME_YMDHMS(:,1:3);

%show result (helps to see outliers)
nValidTimes = sum(~isnan(EXIF.TIME_FILTERED));
figure; plot(EXIF.TIME_FILTERED,'.'); title('absolute timestamps per file')

figure; plot( EXIF.TIME_D,'.'); title('relative timestamps per file (number of day in a year)')

ExportMAT(EXIF, ['E:\CulturalEventRec\myData\EXIF_' dataset '.mat']);

%%
% k=0;
% for i=1:nFiles
%     if ~isempty(EXIF.TIME{i,4})
%         k=k+1;
%     end
% end
% 
%     %TODO: make a list of all times and take the minimum.
%     if ~isempty(EXIF.TIME{i,2})
%         finalTime = EXIF.TIME{i,2};
%     elseif isempty(EXIF.TIME{i,2}) && ~isempty(EXIF.TIME{i,3})
%         finalTime = EXIF.TIME{i,3};
%     elseif isempty(EXIF.TIME{i,2}) && isempty(EXIF.TIME{i,3}) && ~isempty(EXIF.TIME{i,4})
%         finalTime = EXIF.TIME{i,4};
%     else
%         finalTime ='';
%     end
%     
% 
%     
%     if strcmp(finalTime,'0000:00:00 00:00:00')
%         finalTime='';
%     end
%     EXIF.TIME_OUT(end+1,:) = {EXIF.TIME{i,1} finalTime};
% end