clear all

dataset = 'validation';

switch dataset
    case 'train'
        GT = ImportMAT2('E:\CulturalEventRec\myData\GT_train.mat'); %load GT (see script: buildGT):
        EXIF = ImportMAT2('E:\CulturalEventRec\myData\EXIF_train.mat'); %load EXIF data (see script: readeExifInfo):
    case 'validation'
        GT = ImportMAT2('E:\CulturalEventRec\myData\GT_train.mat'); %load GT (see script: buildGT):
        GT_val = ImportMAT2('E:\CulturalEventRec\myData\GT_validation.mat'); %load GT (see script: buildGT):
        EXIF = ImportMAT2('E:\CulturalEventRec\myData\EXIF_train.mat'); %load EXIF data (see script: readeExifInfo):
        EXIF_val = ImportMAT2('E:\CulturalEventRec\myData\EXIF_validation.mat'); %load EXIF data (see script: readeExifInfo):
        %Merge GT
        GT.labels = [GT.labels; GT_val.labels];
        GT.files = [GT.files; GT_val.files];
        [GT.files sortIDX] = sort(GT.files);
        GT.labels = GT.labels(sortIDX);
        %Merge EXIF Data (just TIME_D - thats all we need here:
        EXIF.TIME_D = [EXIF.TIME_D EXIF_val.TIME_D];
        EXIF.TIME_D = EXIF.TIME_D(sortIDX);
    case 'test'
end


%% Get the distribution of dates for each class
classModels = [];
x=[1:366];
for idxClass=1:GT.nClasses
    %get times of class members
    mask = GT.labels==idxClass;
    nClassMembers=sum(mask);
    currentTimes = EXIF.TIME_D(mask);
    currentTimesNoNan = currentTimes(~isnan(currentTimes)); %remove nans
    nTimeStampsAvailable = numel(currentTimesNoNan);
    portionOfTimeInformation = nTimeStampsAvailable/nClassMembers;
    %plot as a histogram
    %figure; hist(currentTimesNoNan,x); title('distribution of timestamps in class');
    %get the histogram data:
    distribOrig = hist(currentTimesNoNan,x);
    
   
    %fit a gaussian model:
    %f = fit(x',distrib','gauss2'); %not robust when data too sparse or not really gaussian (e.g. bimodal)
    %model = f(x);
    
    %remove outliers first:
    %currentTimesNoNanOrig = currentTimesNoNan; %backup
    center = round(numel(x)/2);
    shift = round(mean(currentTimesNoNan)-center);
    currentTimesNoNanShifted=mod(currentTimesNoNan-shift,max(x)); %rotate
    
    %figure; hist(currentTimesNoNanShifted,x); title('distribution of timestamps in class');
    distribShifted = hist(currentTimesNoNan,x);
    
    isoutlier = currentTimesNoNanShifted-mean(currentTimesNoNanShifted)>= 2*std(currentTimesNoNanShifted);
    currentTimesNoNanShifted=currentTimesNoNanShifted(~isoutlier);
    currentTimesNoNanNew=mod(currentTimesNoNanShifted+shift,max(x)); %rotate back
    %figure; hist(currentTimesNoNanNew,x); title('distribution of timestamps in class');
    distrib = hist(currentTimesNoNanNew,x);
    
    %fit a gaussian model:
    mu = mean(currentTimesNoNanNew);
    sigma = std(currentTimesNoNanNew);
    
%     shift = round(mu-183);
%     x2 = circshift(x',[shift,0]);
%     distrib2 = circshift(distrib',[shift,0]);
%     figure; plot(distrib2);
%     model = normpdf(x,center,sigma);
%     
    model=fitMyCircularGaussian(mu,sigma,x);
    %figure; plot(model);
    
    %model2 (robust statistics):
    %mu = median(currentTimesNoNanNew);
    distrib = hist(currentTimesNoNanNew,x);
    maxVal = max(distrib);
    mask = distrib==maxVal;
    mu=mean(find(mask));
    %sigma = iqr(currentTimesNoNanNew);
    sigma = diff(prctile(currentTimesNoNanNew, [20; 80]));
    if sigma == 0
        sigma = std(currentTimesNoNanNew); %fallback if iqr=0
    end
    %sigma = diff(prctile(currentTimesNoNan, [10; 90]));
    model2=fitMyCircularGaussian(mu,sigma,x);
   
    %height = normrnd(mu,sigma,100,1);  % Simulate heights
    %[mu,s,muci,sci] = normfit(height);
    %figure; hist(height,x);
    %distrib2 = hist(height,x);
    %f2 = fit(x',distrib2','gauss2');
    
    %store in a global matrix:
    classModels(idxClass,:)=model2;
    
    %plot results
    figure; plot(x,distribOrig./max(distribOrig), 'b:'); hold on;
            plot(x,distrib./max(distribOrig),'b');
            plot(model,'r-');
            plot(model2,'g-');
            legend('removed outliers', 'sample distribution without outliers', 'Gaussian fit', 'fit with (more) robust statistics','Location','NorthOutside');
            title(GT.classNames{idxClass,2},'interpreter','none');
            xlabel('days of the year from January 1 - December 31')
            xlim([1 max(x)]);
            ylabel('likelihood');
    saveas(gcf,['E:\CulturalEventRec\myData\temporalModels\' num2str(idxClass) '_' GT.classNames{idxClass,2} '.png']);
    
    %pause
    close all
    
end

dlmwrite('E:\CulturalEventRec\myData\temporalModels\modelMatrix.csv', classModels, 'delimiter','\t');
ExportMAT(classModels, 'E:\CulturalEventRec\myData\temporalModels\modelMatrix.mat');


