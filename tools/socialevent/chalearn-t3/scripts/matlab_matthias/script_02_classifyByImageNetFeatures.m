%% Classify Fotos by ImageNet DNN Output (1001 Dimensional Feature vectors)
clear all;

dataset = 'train';
type = 'full' %'top5' or 'full'

%%
switch dataset
    case 'train'
        GT = ImportMAT2('E:\CulturalEventRec\myData\GT_train.mat'); %load GT (see script: buildGT):
        
        %load training data
        D=csv2cell(['E:\CulturalEventRec\ImageNetFeatures\' type filesep 'features.train.csv'],'fromfile');
        ImID = cell2mat(cellfun(@(x) str2num(x(1:end-4)), D(:,1), 'UniformOutput', false)); %get file numbers (remove '.jpg')
        %Features = cell2mat(cellfun(@(x) str2num(x), D(:,2:end), 'UniformOutput', false)); %get numbers as 
        Features = dlmread(['E:\CulturalEventRec\ImageNetFeatures\' type filesep 'features.train.csv'],',',0,1);
        
        %sort feature vectors, to match gt (sort according to filename in ascending order):
        [ImID, sortIDX] = sort(ImID);
        Features = Features(sortIDX,:);
        if ~isequal (GT.files,ImID)
            disp('The order of files in GT and in the feature matrix might not be the same, check ImID and GT.files. They must be equal')
        end
        
        %same for validation set...
        
        %load validation data:
        D_val=csv2cell(['E:\CulturalEventRec\ImageNetFeatures\' type filesep 'features.val.csv'],'fromfile');
        ImID_val = cell2mat(cellfun(@(x) str2num(x(1:end-4)), D_val(:,1), 'UniformOutput', false)); %get file numbers (remove '.jpg')
        %Features = cell2mat(cellfun(@(x) str2num(x), D(:,2:end), 'UniformOutput', false)); %get numbers as 
        Features_val = dlmread(['E:\CulturalEventRec\ImageNetFeatures\' type filesep 'features.val.csv'],',',0,1);
        
        %load validation labels:
        GT_val = ImportMAT2('E:\CulturalEventRec\myData\GT_validation.mat'); %load GT (see script: buildGT):
        
        %sort feature vectors, to match gt (sort according to filename in ascending order):
        [ImID_val, sortIDX] = sort(ImID_val);
        Features_val = Features_val(sortIDX,:);
        if ~isequal (GT_val.files,ImID_val)
            disp('The order of files in GT and in the feature matrix might not be the same, check ImID_val and GT_val.files. They must be equal')
        end
        
        
    case 'validation'
            %TODO: read additionally GT for validation set and merge them into one trainin set
            %TODO: read features of test set

    case 'test'
         end

%% Do a classification with SVM:

%important info: 
% The function 'svmpredict' has three outputs. The first one, predictd_label, is a vector of predicted labels. The second output,
% accuracy, is a vector including accuracy (for classification), mean squared error, and squared correlation coefficient (for regression).
% The third is a matrix containing decision values or probability estimates (if '-b 1' is specified). If k is the number of classes
% in training data, for decision values, each row includes results of  predicting k(k-1)/2 binary-class SVMs. For classification, k = 1 is a
% special case. Decision value +1 is returned for each testing instance, instead of an empty vector. For probabilities, each row contains k values
% indicating the probability that the testing instance is in each class. Note that the order of classes here is the same as 'Label' field
% in the model structure.

SVM.param.name = 'testSVM';
SVM.param.kernelType = 'linear';
SVM.param.SVMType    = 'liblinear'; %'epsilon-SVR';C-SVC
SVM.param.verbose = false;
SVM.param.gamma = [0.001 0.01 0.1 1 10 100 1000]; %for RBF estimate during model selection
%SVM.param.costs = []; %[3 1] 
SVM.param.cost = [0.1 1 10 100 1000 10000];
SVM.param.degree = 2; %just for polynomial kernel
SVM.param.shrinking=false;
SVM.param.probability_estimates = true;
SVM.param.doCrossVal = true;
SVM.param.train.parallel = 0; %deactivate for large scale experiment
SVM.param.train.doModelSelection = false;
SVM.param.train.visualize = true;
SVM.param.train.recompute=true;
SVM.param.eval.maxNumObservationsPerRun=10000;
SVM.param.eval.visualize  = true;
SVM.param.eval.recompute=true;

%sort GT (to get probabilities in the right order): 
[GT.labelsSorted, GT.sortIDX] = sort(GT.labels);

%% 1. do model selection (applying cross validation on training set):
accuracies = [];
for i=1:numel(SVM.param.cost)
    if strcmp(SVM.param.kernelType,'linear')
        SVM.param.gamma=1;
    end
    for g=1:numel(SVM.param.gamma)
        currentParam = SVM.param;
        currentParam.cost = SVM.param.cost(i);
        currentParam.gamma = SVM.param.gamma(g);
        svmModel = trainSVMClassifier(Features(GT.sortIDX,:), GT.labels(GT.sortIDX), currentParam);
        %svmResult = testSVMClassifier(Features(GT.sortIDX,:), GT.labels(GT.sortIDX), svmModel, SVM.param);
        %accuracies(end+1,:) = [currentParam.cost currentParam.gamma svmResult.accuracy(1)];
        accuracies(end+1,:) = [currentParam.cost currentParam.gamma svmModel];
    end
end
accuracies
    
%% 2. train on training set with optimized parameters and without crossvalidation:
SVM.param.doCrossVal = false;
SVM.param.cost=100;
SVM.param.gamma=1;
SVM.param.kernelType = 'linear';
SVM.param.SVMType    = 'C-SVC'; %'epsilon-SVR', C-SVC;

svmModel = trainSVMClassifier(Features(GT.sortIDX,:), GT.labels(GT.sortIDX), SVM.param);

% %% test on training set:
% svmResult = testSVMClassifier(Features, GT.labels(GT.sortIDX), svmModel, SVM.param);
% maskCorrect = svmResult.predictedLabels == GT.labels;
% accuracy = sum(maskCorrect) ./ numel(GT.labels)

%% 3 test on validation set! 

svmResult = testSVMClassifier(Features_val, GT_val.labels, svmModel, SVM.param);

%The probabilities of LibLinear are not good!
% if strcmp(SVM.param.SVMType,'liblinear')
%     nSamples = size(svmResult.decisionValues,1);
%     minPerClass = repmat(min(svmResult.decisionValues,[],1),nSamples,1);
%     maxPerClass = repmat(max(svmResult.decisionValues,[],1),nSamples,1);
% 
%     svmResult.decisionValues = svmResult.decisionValues-minPerClass;
%     svmResult.decisionValues = svmResult.decisionValues ./ (maxPerClass+abs(minPerClass));
% end

%figure; plot(svmResult.decisionValues(1,:));


%% Generate output and evaluate with official script:

%folder = ['E:\CulturalEventRec\ImageNetFeatures\result_' type];
evalFolder = 'E:\CulturalEventRec\Track4';
folder = [evalFolder  filesep 'input\res'];

exportPrecomputedProbabilities(ImID_val, svmResult.decisionValues, folder, GT);

cd(evalFolder);

command = ['python program/evaluate.py input output'];

dos(command,'-echo');

score = fileread([evalFolder filesep 'output' filesep 'scores.txt']);

disp(score);


