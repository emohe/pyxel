% Filter Flickr Data:
clear all;

%postfix='_withLocations'; %the smaller set with approx 500 images per class
postfix=''; %the 90k set

dataset = 'train';

%% Load precomputed data
MDataTable = ImportMAT2(['E:\CulturalEventRec\myData\MDataTable' postfix '.mat']);
classModels = ImportMAT2('E:\CulturalEventRec\myData\temporalModels\modelMatrix.mat');
GT = ImportMAT2(['E:\CulturalEventRec\myData\GT_' dataset '.mat']);
nImages = size(MDataTable,1);

%% Compare temporal distributions:
x=[1:366];
for c=1:GT.nClasses
    %get day distribution for the flickr images of the event
    mask = cell2mat(MDataTable(:,13))==c;
    days = cell2mat(MDataTable(mask,12));
    
    %show temporal model of event
    %category = unique(MDataTable(mask,2));
    figure; subplot(2,1,1); plot(classModels(c,:)); title(['Temporal model of event: ' GT.classNames(c,2)], 'interpreter','none');
    xlim([min(x) max(x)]);
    %show flickr distribution
    subplot(2,1,2); hist(days,x);  title(['Distribution of Flickr dates of event: ' GT.classNames(c,2)], 'interpreter','none');
    xlim([min(x) max(x)]);
    mkdir_force(['E:\CulturalEventRec\myData\flickrDistributions' postfix]);
    saveas(gcf,['E:\CulturalEventRec\myData\flickrDistributions' postfix '\' num2str(c) '_' GT.classNames{c,2} '.png']);
    close all
end

%% Compute score per image:

imageScores = cell(nImages,3);
imageScores(:,1) = (MDataTable(:,2));
imageScores(:,2) = (MDataTable(:,1));

for i = 1:nImages
    %get day:
    day = MDataTable{i,12};
    classID = MDataTable{i,13};
    model = classModels(classID,:); %temporal class model
    imageScores{i,3} = model(day);
end

%Export:
cell2csv(['E:\CulturalEventRec\myData\flickrImageScoresALL' postfix '.csv'], imageScores, '\t');

%Do some thresholding:
thresholds = [0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9];

for i=1:numel(thresholds)
    scores = cell2mat(imageScores(:,3));
    scores02 = scores; scores02(scores02<thresholds(i))=0; 
    imageScores02=imageScores; 
    imageScores02(:,3) = num2cell(scores02);
    mask = scores02>0;
    imageScores02=imageScores02(mask,:);
    cell2csv(['E:\CulturalEventRec\myData\flickrImageScores_threshold' num2str(thresholds(i)) postfix '.csv'], imageScores02, '\t');
end



%figure; plot(sort(scores02));
