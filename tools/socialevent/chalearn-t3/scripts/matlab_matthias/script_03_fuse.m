%% Load existing probabilities and fuse them with temporal data
clear all

dataset='validation';

%load validation labels:
GT_val = ImportMAT2('E:\CulturalEventRec\myData\GT_validation.mat'); %load GT (see script: buildGT):

%% Define Inputs:

InputProbabilities_DNN = 'E:\CulturalEventRec\DNNoutput\all_layers_8020_5000';

InputProbabilities_ImageNet = 'E:\CulturalEventRec\ImageNetFeatures\result_full';

InputTemporalModel='E:\CulturalEventRec\myData\temporalModels\modelMatrix.mat'; %Load class models

%% Load Inputs:

[filenames, probabilityMatrix_DNN] = importPrecomputedProbabilities(InputProbabilities_DNN);

[filenamesImageNet, probabilityMatrix_ImageNet] = importPrecomputedProbabilities(InputProbabilities_ImageNet);

tempModels=ImportMAT2(InputTemporalModel);

%% I. Refine (weight) by temporal models:

inputProb = probabilityMatrix_ImageNet;

%Load class models
classModels=ImportMAT2('E:\CulturalEventRec\myData\temporalModels\modelMatrix.mat');
nClasses=size(classModels,1);

%Load temporal features of validation set (EXIF Days of Year)
EXIF = ImportMAT2(['E:\CulturalEventRec\myData\EXIF_' dataset '.mat']); %EXIF data is sorted by filename!

%apply class models to validation data: for each sample in the validation set, get probabilities for all 50 classes.
nSamples = numel(EXIF.TIME_D);
temporalProbabilities = nan(nSamples,nClasses);
for i=1:nSamples
    currentDay = EXIF.TIME_D(i);
    if ~isnan(currentDay)
        temporalProbabilities(i,:) = classModels(:,currentDay);
    end
end

%weight class probabilities (if temporal information is available):
mask = ~isnan(temporalProbabilities(:,1));

%Variant A: multiply:
decisionValuesNew=inputProb; %set input
decisionValuesNew(mask,:) = inputProb(mask,:) .* temporalProbabilities(mask,:);
% decisionValuesNewNorm = decisionValuesNew;

%Variant B: fuse both sources and change weights only for images where temporal model does not fit at all
%i.e. probability of classification is high, but probability of time is very low!
%probabilityMatrix_Final=probabilityMatrix_DNN.*probabilityMatrix_ImageNet; %FUSE Both SOURCES!

probabilityMatrix_Final=probabilityMatrix_DNN;

% difference = probabilityMatrix_Final-temporalProbabilities;
% mask = difference >0.5; %high disagreement
mask = temporalProbabilities<0.05;
sum(sum(mask))
probabilityMatrix_Final(mask) = 0;

%Variant C: Add Bonus for temporal match:
probabilityMatrix_Final=probabilityMatrix_DNN;
mask = ~isnan(temporalProbabilities(:,1));
probabilityMatrix_Final(mask,:) = min(probabilityMatrix_Final(mask,:) + temporalProbabilities(mask,:)./2,[],1);

%maxVal = max(decisionValuesNewNorm,[],1);
%decisionValuesNewNorm = decisionValuesNewNorm ./ repmat(maxVal,nSamples,1); %bringt nix. -> 51.8%
%decisionValuesNewNorm(mask,:) = decisionValuesNew(mask,:) ./ repmat(sum(decisionValuesNew(mask,:),2),1,nClasses);  .* repmat(sum(inputProb(mask,:),2),1,nClasses); %BAAAAD
%%%decisionValuesNewNorm = decisionValuesNew ./ repmat(sum(decisionValuesNew,2),1,nClasses); %BAAAAD!



% figure; subplot(3,1,1); plot(inputProb(1,:));
%         subplot(3,1,2); plot(decisionValuesNew(1,:));
%         subplot(3,1,3); plot(decisionValuesNewNorm(1,:));

% probabilityMatrix_Final=decisionValuesNewNorm;

%probabilityMatrix_Final=decisionValuesNew.*probabilityMatrix_ImageNet; %FUSE ALL 3 SOURCES!



%probabilityMatrix_Final(mask,:) = min(probabilityMatrix_Final(mask,:) + temporalProbabilities(mask,:),1); %only increase weight by temporal information but crop at 1: BAAAAAD

%probabilityMatrix_Final = decisionValuesNew.*probabilityMatrix_DNN;

%% II. Train a new classifier:

%TODO:

%Get training probabilities of DNN and ImageNet

%Train SVM

%Get validation probabilities of DNN and ImageNet

%Test SVM


%% Export Results
folder = 'E:\CulturalEventRec\FusedOutput';

exportPrecomputedProbabilities(filenames, probabilityMatrix_Final, folder, GT_val);
