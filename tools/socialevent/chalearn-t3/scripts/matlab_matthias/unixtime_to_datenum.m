function dn = unixtime_to_datenum( unix_time )
    dn = unix_time/86400 + 719529;         %# == datenum(1970,1,1)
end

function dn = unixtime_in_ms_to_datenum( unix_time_ms )
    dn = unix_time_ms/86400000 + 719529;   %# == datenum(1970,1,1)
end