import getpass
import sys
import os
import numpy as np
import pickle
import chalearn_tools as ct
from prediction import loadNetCNN, extract_CNN_feature

caffe_root = '/usr/local/opt/caffe/'

# Make sure that caffe is on the python path:
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe

def compute_additional_features(image_path, net, layer_name, dictionary, save_path):

    class_names = os.listdir(image_path)

    # For every folder/class...
    for c in class_names:

        ids = list()
        features = list()

        # Get all the images it contains
        image_names = os.listdir(os.path.join(image_path,c))

        # Take the index corresponding to this class
        class_number = dictionary[c]

        # And for every image...

        for image_name in image_names:

            ids.append(os.path.join(image_path,c) + '/' + image_name)
            feat = extract_CNN_feature(net,os.path.join(image_path,c) + '/' + image_name,layer_name)
            features.append(feat[class_number])

        sorted_images = np.array(ids)[np.argsort(np.array(features))[::-1]]

        pickle.dump(sorted_images, open( os.path.join(save_path,str(class_number) ) + '.p', "wb") )


if __name__ == "__main__":

    username = str(getpass.getuser())

    config_name = str( sys.argv[1])
    layer_name = str(sys.argv[2])
    flickr_number = str(sys.argv[3])

    path_to_files = '/imatge/' + username + '/work/chalearn/'
    
    if flickr_number == '21':
    
        path_to_additional_files = path_to_files + 'images/pictures/'
    
    else:
        path_to_additional_files = path_to_files + 'images/flickr_additional/'
    path_to_model = path_to_files +'/models/' + config_name

    mean_file = path_to_model + '/meanfile.npy'
    model_file = path_to_model + '/model.caffemodel'
    deploy_file = path_to_model + '/deploy.prototxt'

    # Load the net
    net = loadNetCNN(deploy_file,model_file,mean_file)

    save_path = path_to_files + 'flickr_similarity/' + config_name + '_' + flickr_number

    ct.make_dir(save_path)

    dictionary = ct.build_dictionary()

    compute_additional_features(path_to_additional_files,net,layer_name,dictionary,save_path)


    
