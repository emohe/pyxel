import csv
import numpy as np
import os
import sys
import getpass
import pickle
import chalearn_tools as ct

def to_csv(to_write,csv_file):

    with open(csv_file, 'wb') as f:
        writer = csv.writer(f, delimiter=',', lineterminator='\n')

        writer.writerows(to_write)



if __name__=="__main__":

    config_name = str(sys.argv[1])
    layer_name = str(sys.argv[2])
    test_set = str(sys.argv[3])

    username = str(getpass.getuser())

    path_to_files = '/imatge/' + username + '/work/chalearn/'

    path_to_binary = path_to_files + '/binary_files/' + config_name

    image_names = pickle.load(open(path_to_binary + '/image_names_' + test_set + '.p', "rb") )
    feats = pickle.load(open(path_to_binary + '/features_' + config_name + '_' + layer_name + '_' + test_set + '.p', "rb") )

    image_names = np.reshape(np.array(image_names),((np.shape(image_names)[0],1)) )

    print np.shape(image_names), np.shape(feats)
    to_write = np.hstack((image_names,feats))


    path_to_csv_feats = path_to_files + '/csv_feats/' + config_name

    ct.make_dir(path_to_csv_feats)

    csv_file = path_to_csv_feats + '/' + 'csv_feats_' + config_name + '_' + layer_name + '_' + test_set + '.csv'

    to_csv(to_write,csv_file)