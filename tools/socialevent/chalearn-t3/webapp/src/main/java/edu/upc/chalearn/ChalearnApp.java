package edu.upc.chalearn;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import edu.upc.chalearn.commands.SetupCommand;
import edu.upc.chalearn.config.ChalearnConfig;
import edu.upc.chalearn.factories.MongoClientFactory;
import edu.upc.chalearn.resources.CategoryResource;
import edu.upc.chalearn.resources.PhotoResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


/**
 * Created by dmanchon on 14/03/15.
 */
public class ChalearnApp extends Application<ChalearnConfig>{


    public static void main(String[] args) throws Exception {
        new ChalearnApp().run(args);
    }

    @Override
    public String getName() {
        return "chalearn";
    }

    @Override
    public void initialize(Bootstrap<ChalearnConfig> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets", "/assets", "index.html"));
        bootstrap.addCommand(new SetupCommand());
    }

    @Override
    public void run(ChalearnConfig chalearnConfig, Environment environment) throws Exception {
        final DB db = MongoClientFactory.getClient().getDB(chalearnConfig.getCollection());
        DBCollection collection = db.getCollection("photos");

        final PhotoResource photoResource = new PhotoResource(collection);
        final CategoryResource categoryResource = new CategoryResource(collection);

        environment.jersey().setUrlPattern("/api/*");
        environment.jersey().register(photoResource);
        environment.jersey().register(categoryResource);

    }
}
