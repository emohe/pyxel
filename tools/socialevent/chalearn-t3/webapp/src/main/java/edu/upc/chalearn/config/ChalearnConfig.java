package edu.upc.chalearn.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by dmanchon on 14/03/15.
 */
public class ChalearnConfig extends Configuration {
    @JsonProperty
    private String version;
    @JsonProperty
    private String collection = "chalearn-dev";
    @JsonProperty
    private String importFile;

    @JsonProperty
    public String getImportFile() {
        return importFile;
    }

    @JsonProperty
    public String getCollection() {
        return collection;
    }

    @JsonProperty
    public void setCollection(String collection) {
        this.collection = collection;
    }

    @JsonProperty
    public String getVersion() {
        return version;
    }

    @JsonProperty
    public void setVersion(String version) {
        this.version = version;
    }
}
