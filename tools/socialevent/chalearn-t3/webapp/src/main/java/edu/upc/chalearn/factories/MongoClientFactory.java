package edu.upc.chalearn.factories;

import com.mongodb.MongoClient;

import java.net.UnknownHostException;

/**
 * Created by dmanchon on 15/03/15.
 */
public class MongoClientFactory {
    private static MongoClient client;

    public static MongoClient getClient() throws UnknownHostException {
        if (client == null) {
            client = new MongoClient();
        }
        return client;
    }
}
