package edu.upc.chalearn.factories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;

/**
 * Created by dmanchon on 15/03/15.
 */
public class ObjectMapperFactory {
    private static ObjectMapper objectMapper;

    public static ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        return objectMapper;
    }
}
