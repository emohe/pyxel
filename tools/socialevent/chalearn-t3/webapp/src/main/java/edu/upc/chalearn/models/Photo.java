package edu.upc.chalearn.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.geojson.GeoJsonObject;
import org.geojson.Point;

import java.util.List;

/**
 * Created by dmanchon on 14/03/15.
 */
public class Photo {
    private long id;
    private String url;
    private String owner;
    private String category;
    private String[] tags;
    private Point gps;
    private int numberOfviews;
    private String title;

    public Photo() {

    }

    public Photo(long id, String url, String category, String[] tags, String owner, Point gps, int numberOfviews,String title) {
        this.title = title;
        this.id = id;
        this.url = url;
        this.category = category;
        this.tags = tags;
        this.owner = owner;
        this.gps = gps;
        this.numberOfviews = numberOfviews;
    }

    @JsonProperty
    public int getNumberOfviews() {
        return  numberOfviews;
    }

    @JsonProperty
    public Point getGps() {
        return gps;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    @JsonProperty
    public String getOwner() {
        return owner;
    }

    @JsonProperty
    public String getCategory() {
        return category;
    }

    @JsonProperty
    public String[] getTags() {
        return tags;
    }

    @JsonProperty
    public String getUrl() {
        return url;
    }

    @JsonProperty("_id")
    public long getId() {
        return id;
    }
}
