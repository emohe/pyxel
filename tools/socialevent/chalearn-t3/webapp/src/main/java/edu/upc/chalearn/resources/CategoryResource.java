package edu.upc.chalearn.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import edu.upc.chalearn.factories.ObjectMapperFactory;
import edu.upc.chalearn.models.Photo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmanchon on 15/03/15.
 */
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
public class CategoryResource {
    private final Logger logger = LoggerFactory.getLogger(CategoryResource.class);
    private DBCollection collection;
    private final ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();

    public CategoryResource(DBCollection collection) {
        this.collection = collection;
    }

    @Path("/categories")
    @GET
    @Timed
    public List<String> getCategories() throws IOException {
        List<String> categories = this.collection.distinct("category");
        return categories;
    }

    @Path("/category/{name}")
    @GET
    @Timed
    public List<Photo> getCategory(@PathParam("name") String name) throws IOException {
        List<Photo> photos = new ArrayList<Photo>();
        DBCursor objects = this.collection.find(new BasicDBObject("category",name));
        for(DBObject photo: objects){
            photos.add(objectMapper.readValue(photo.toString(), Photo.class));

        }
        return photos;
    }

}
