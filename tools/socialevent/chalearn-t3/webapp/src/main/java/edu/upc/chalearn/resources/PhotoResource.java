package edu.upc.chalearn.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.mongodb.DBCollection;
import edu.upc.chalearn.factories.ObjectMapperFactory;
import edu.upc.chalearn.models.Photo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by dmanchon on 14/03/15.
 */

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
public class PhotoResource {
    private final Logger logger = LoggerFactory.getLogger(PhotoResource.class);
    private DBCollection collection;
    private final ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();

    public PhotoResource(DBCollection collection) {
        this.collection = collection;
    }

    @Path("/photo/{id}")
    @GET
    @Timed
    public Photo getPhoto(@PathParam("id") long id) throws IOException {
        return objectMapper.readValue(this.collection.findOne(id).toString(), Photo.class);
    }
}
