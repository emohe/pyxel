app.controller('MainCtrl', function ($scope, api) {
    'use strict';
    $scope.COLS = 6;
    $scope.name = "Chalearn";

    getCategories();

    $scope.range = function range(start, count) {
        return Array.apply(0, Array(count))
            .map(function (element, index) {
                return index + start;
            });
    };

    function getCategory(category) {
        api.category(category).success(function(photos){
            $scope.photos = photos;
            $scope.rows = parseInt(photos.length / $scope.COLS);
            $scope.last = photos.length % $scope.COLS;
        }).error(function(error){});
    };

    function getCategories() {
        api.categories().success(function(categories) {
            $scope.categories = categories;
            $scope.category = categories[0];
            getCategory($scope.category);
        }).error(function (error) {
            $scope.error = error;
        });
    }

    $scope.setCategory = function(index) {
        $scope.category = $scope.categories[index];
        getCategory($scope.category);
    };


});