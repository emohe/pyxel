app.factory('api', ['$http', function($http) {
    'use strict';

    var base = "http://178.62.195.161:8080/api/";
    return {
        categories: function () {
            return $http.get(base+"categories");
        },

        category: function(category) {
            return $http.get(base+"category/"+category);
        }

    }
}]);