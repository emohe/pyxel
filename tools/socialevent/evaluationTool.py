# evaluation tool
import os
import pickle
from sklearn.svm import SVC
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import precision_recall_fscore_support
import pylab as pl
import numpy as np
from sklearn.svm import SVC
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import auc
from modules.classification.Annotation import Annotation
from modules.classification.AnnotatedSemanticClass import AnnotatedSemanticClass
import time

def evaluate(annGT,annRes):

	pathHome = os.path.expanduser('~')
    	pathWork = os.path.join( pathHome, 'work')  
    	pathDirGt = os.path.join( pathWork)
        pathDirRes = os.path.join( pathWork, '6_results' )
        pathgt = os.path.join( pathDirGt, annGT )
        pathres = os.path.join( pathDirRes, annRes )
        pathPR = os.path.join( pathDirRes, 'PandR.txt' )
        outPR=open(pathPR,'w')
        ogt=open(pathgt,'r')
        ores=open(pathres,'r')
    
        groundTruthAnnotation=pickle.load(ogt)
        predictedAnnotation=pickle.load(ores)

	groundTruthClassNames=groundTruthAnnotation.getSemanticClassNames()
	
	for semanticClass in groundTruthClassNames:
            #print semanticClass
            #print groundTruthClassNames[semanticClass]
            groundTruthDocIdList=groundTruthAnnotation.getClassList(semanticClass)
            predictedResultsDocIdList=predictedAnnotation.getClassList(semanticClass)
            #print groundTruthAnnotation.getSemanticClassNames
            #print groundTruthClassNames
            
            valid=False			
	    print semanticClass
            #print groundTruthDocIdList
            #print predictedResultsDocIdList

            for docId in groundTruthDocIdList:
                if docId ==1:
                   valid=True
                    
            if (valid):
                precision, recall, thresholds = precision_recall_curve(groundTruthDocIdList, predictedResultsDocIdList)
            
            
                print precision
               
                
                print recall
                area = auc(recall, precision)
                outPR.write(semanticClass+'\n')
                outPR.write('Precision:  ')
                outPR.write(precision)
                outPR.write('\n')
                outPR.write('Recall:  ')
                outPR.write(recall)
                outPR.write('\n'+'\n')
                pl.clf()
                pl.plot(recall, precision, label='Precision-Recall curve')
                pl.xlabel('Recall')
                pl.ylabel('Precision')
                pl.ylim([0.0, 1.05])
                pl.xlim([0.0, 1.0])
                #pl.title('Precision-Recall for' %(str)groundTruthClassNames[semanticClass])
                pl.legend(loc="lower left")
                #pl.show()

                pathRes = os.path.join( pathDirRes, semanticClass )
                pl.savefig(pathRes)
                pl.close()
        outPR.close()
       

if __name__ == "__main__":

	HOME = os.path.expanduser('~')
	classificationPath = os.path.join(HOME,'work', 'mediaeval','2013-sed','classification')
	datasetPath = os.path.join(classificationPath,'2_datasets')
        gtFile = os.path.join(classificationPath, datasetPath, 'annotation.p' )
	#gtf = 'eyetrackerMini_gt.p'
	#grf = 'visual_results.p'
	#gtPath = os.path.join(gtpath, gtf )
	#resPath = os.path.join(DATASETS_PATH, grf )
        evaluate(gtFile,'visual_results.p')