from ConfigParser import ConfigParser

import os
import sys

from modules.features.VisualVocabulary import VisualVocabulary

def main(pathFileDatasetTrain, pathDirImages,pathVocabulary):
   # Define the default values for the options
   
    _flagVerbose=False
    
    vocabularySize=256                  # Amount of visual words
    maxNumImages=10                     # Maximum amount of images to consider

    # Init the Visual Vocabulary
    visualVocabulary = VisualVocabulary(flagVerbose=_flagVerbose)
    
    
    visualVocabulary.buildFromImageCollection( pathFileDatasetTrain, 
                                              pathDirImages, 
                                              'jpg', 
                                              vocabularySize, 
                                              maxNumImages )
    
    visualVocabulary.saveToDisk( pathVocabulary )
    
    if __name__ == "__main__":
        main(sys.argv[1:])

def run(env):

#    pathHome = os.path.expanduser('~')
#    pathWork = os.path.join( pathHome,'work','mediaeval','2013-sed','classification' )    
#    pathDirImages = os.path.join( pathWork, '1_images', 'train' )
#    pathFileDatasetTrain = os.path.join(pathWork, '2_datasets', 'train.txt')
#    pathVocabulary = os.path.join(pathWork, '3_vocabulary', 'vocabulary.p')

    
       
    config = ConfigParser()
    config.read(os.path.join(__location__, 'settings.ini'))

    pathImages = config.get(env,'root') + config.get('common','pathImages')
    pathDatasets = config.get( env, 'root')+config.get('common','pathDatasets')

    main(pathImages, pathDatasets)




__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))