# Add the root of all Python modules to the search path
import os

from modules.computation.srun import run

pathHome = os.path.expanduser('~')
pathWork = os.path.join( pathHome, 'work','mediaeval','2013-sed','classification' )

pathDirImages = os.path.join( pathWork, '1_images', 'train' )
pathFileDatasetTrain = os.path.join(pathWork, '2_datasets', 'train.txt')
pathDirVocabulary = os.path.join( pathWork, '3_vocabulary' )

pathHome = os.path.expanduser('~')
pathScript = os.path.join( pathHome,
                          'workspace',
                          'python',
                          'modules',
                          'features',
                          'VisualVocabulary.py')
                        
combinationsSizeMax = [ [256, 100] , [512, 100], [1024,100], [4096,100] ]                   
                        
# For each combintaion of parameters to consider...
for s,m in combinationsSizeMax:
    
    # Build a command for each visual vocabulary configuration that is to be generated
    parameters = [' --']    
    parameters.append(pathScript) 
    parameters.append(' --in=' + pathFileDatasetTrain)
    parameters.append(' --visual=' + pathDirImages)
    parameters.append(' --ext=jpg')
    parameters.append(' --max=' + str(m) )
    parameters.append(' --size=' + str(s)) 
    parameters.append(' --out=' + os.path.join(pathDirVocabulary, 'size_' + str(s) + '_max_' + str(m)  + '.p'))

    print parameters
    
    # Run in the computation service
    run('ipython',parameters,mem=8000,max_jobs=10,print_command=True)




