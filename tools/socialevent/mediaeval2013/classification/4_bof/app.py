from ConfigParser import ConfigParser
from modules.features.BofExtractor import BofExtractor

import os

def processLabel(bofExtractor, pathDirBofs, datasetLabel,pathDirImagesBase,pathDirDatasets):
    
    # Directory where the images are located
    pathDirImages = os.path.join( pathDirImagesBase, datasetLabel ) 
    if not os.path.exists(pathDirImages):
        print 'ERROR: Directory not found ' + pathDirImages
        return
     
    # File containing the image IDs   
    pathFileDataset = os.path.join( pathDirDatasets, datasetLabel + '.txt' )
    if not os.path.exists(pathFileDataset):
        print 'ERROR: File not found ' + pathFileDataset
        return    
        
    print 'Extracting BoF from ' + pathFileDataset + "..."

    # Directory for BoFs
    pathDirBofLabel = os.path.join( pathDirBofs, datasetLabel )    
    if not os.path.exists(pathDirBofLabel):
        os.makedirs(pathDirBofLabel)
        
    bofExtractor.processTxtFile( pathFileDataset, pathDirImages, 'jpg', pathDirBofLabel)


def main(pathFileVocabulary, pathDirBofs, pathDirImagesBase, pathDirDatasets):

    
        # Init the BoF extractor
    bofExtractor = BofExtractor(pathFileVocabulary, flagVerbose=False)
    
    # Process the train dataset
    processLabel(bofExtractor, pathDirBofs,'train', pathDirImagesBase,pathDirDatasets)
    
    # Process the test dataset
    processLabel(bofExtractor, pathDirBofs, 'test', pathDirImagesBase, pathDirDatasets)
    
    print "Extracting BoF... done."



def run(env):
    
#    pathHome = os.path.expanduser('~')
        
#    pathWork = os.path.join( pathHome, 'work', 'mediaeval','2013-sed','classification' )
#    pathDirImagesBase = os.path.join(pathWork, '1_images')
#    pathDirDatasets = os.path.join(pathWork, '2_datasets')
#    pathFileVocabulary = os.path.join( pathWork, '3_vocabulary','vocabulary-i100-k4096.p' )
#    pathDirBofs = os.path.join(pathWork, '4_bof')
    
       
    config = ConfigParser()
    config.read(os.path.join(__location__, 'settings.ini'))

    pathFileVocabulary = config.get(env,'root') + config.get('common','pathFileVocabulary')
    pathDirBofs = config.get( env, 'root')+config.get('common','pathDirBofs')
    pathDirImagesBase = config.get(env,'root') + config.get('common','pathDirImagesBase')
    pathDirDatasets = config.get( env, 'root')+config.get('common','pathDirDatasets')


    main(pathFileVocabulary, pathDirBofs, pathDirImagesBase, pathDirDatasets)




__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))