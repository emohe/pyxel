# -*- coding: utf-8 -*-
"""
Do not run this script in the computation server 
Created on Wed Apr  9 13:18:37 2014

@author: xgiro
"""

import os

from modules.computation.srun import run
from modules.computation.Dataset import Dataset

def processLabel( pathScript,
                 pathDirImagesBase, fileImageExtension,
                 pathDirDatasets, datasetLabel,
                 pathFileVocabulary, 
                 pathDirBofs ):
    
    # Directory where the images are located
    pathDirImages = os.path.join( pathDirImagesBase, datasetLabel ) 
    if not os.path.exists(pathDirImages):
        print 'ERROR: Directory not found ' + pathDirImages
        return
     
    # File containing the image IDs   
    pathFileDataset = os.path.join( pathDirDatasets, datasetLabel + '.txt' )
    if not os.path.exists(pathFileDataset):
        print 'ERROR: File not found ' + pathFileDataset
        return    
        
    print 'Extracting BoF from ' + pathFileDataset + "..."

    # Directory for BoFs
    pathDirBofLabel = os.path.join( pathDirBofs, datasetLabel )    
    if not os.path.exists(pathDirBofLabel):
        os.makedirs(pathDirBofLabel)
                
    # For each file ID in the dataset...
    dataset = Dataset( pathFileDataset )  
    rootNames = dataset.getRootNames()
        
    for r in rootNames:
    # Run the extraction script for each imageby providing parameters as
    # command line arguments
        
        # Build a command for each visual vocabulary configuration that is to be generated
        parameters = [' --']    
        parameters.append(pathScript) 
        parameters.append(' --in=' + os.path.join(pathDirImages, r + fileImageExtension) )
        parameters.append(' --codebook=' + pathFileVocabulary )
        parameters.append(' --out=' + os.path.join(pathDirBofs, r  + '.p' ) )
    
        #print parameters
        
        # Run in the computation service
        run('ipython',parameters,mem=1000,max_jobs=10,print_command=True)        
    
# Main
if __name__ == "__main__":
    
    # Path to the user's home directory
    pathHome = os.path.expanduser('~')

    # Path to the script
    pathScript = os.path.join( pathHome,
                              'workspace',
                              'python',
                              'modules',
                              'features',
                              'BofExtractor.py')                              

        
    pathWork = os.path.join( pathHome, 
                            'work', 
                            'mediaeval',
                            '2013-sed',
                            'classification' )
    pathDirImagesBase = os.path.join(pathWork, '1_images')
    pathDirDatasets = os.path.join(pathWork, '2_datasets')
    pathFileVocabulary = os.path.join( pathWork, 
                                      '3_vocabulary',
                                      'vocabulary.p' )
    pathDirBofs = os.path.join(pathWork, '4_bof')
    
    # Process the train dataset
    processLabel(   pathScript,
                    pathDirImagesBase, '.jpg   ',
                    pathDirDatasets, 'train',
                    pathFileVocabulary,
                    pathDirBofs )
                 
    # Process the test dataset
    processLabel(   pathScript,
                    pathDirImagesBase, '.jpg',
                    pathDirDatasets, 'test',
                    pathFileVocabulary,
                    pathDirBofs )                 
    
    print "Extracting BoF... done."
        