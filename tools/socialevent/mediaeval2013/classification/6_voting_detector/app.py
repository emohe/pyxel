from ConfigParser import ConfigParser
from modules.computation.Dataset import Dataset
import os

def main(pathImages, pathDatasets):
  print "Social Event Detection 2013"
    
    # For each data partition (train & test)
  for partition in os.listdir(pathImages):
        
        # If it is a directory 
    dirPartition = os.path.join( pathImages, partition )
    if os.path.isdir(dirPartition):
      # Define the filename to contain the list of images to process
      filenameOut = os.path.join( pathDatasets, partition +'_19_7'+ '.txt')
            
      dataset = Dataset( filenameOut, 
                         flagSaveInMemory=False, 
                         flagVerbose=True)
    
      dataset.build( dirPartition, '.jpg' )




def run(env):
    
#    pathHome = os.path.expanduser('~')
#    pathWork = os.path.join( pathHome, 'work','mediaeval','2013-sed','classification' )
#    pathImages = os.path.join(pathWork, '1_images')
#    pathDatasets = os.path.join( pathWork, '2_datasets' )
    
       
    config = ConfigParser()
    config.read(os.path.join(__location__, 'settings.ini'))

    pathImages = config.get(env,'root') + config.get('common','pathImages')
    pathDatasets = config.get( env, 'root')+config.get('common','pathDatasets')

    main(pathImages, pathDatasets)




__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))