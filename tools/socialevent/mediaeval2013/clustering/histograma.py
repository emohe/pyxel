import os
import pandas as pd
import matplotlib.pyplot as plt
from modules.socialevent.ImageCollection import ImageCollection

# Point to the local path where the metadata is stored
HOME = os.path.expanduser('~')
WORK_PATH = HOME+'/work/mediaeval/2013-sed'

# Create the mini-clusters
# WARNING: This is absolute wrong, as we should be using ground truth data
# instead of the clusters that are automatically generated !!
col = ImageCollection()
col.initial_setup(WORK_PATH)
col.pre_process_dates()
col.create_mini_clusters(15,15) #k,d

# Compute the histogram of distances between 10,000 pairs of images from the
# same cluster
col.TrainingData_time_user_label_gps(col.images,10000)
distances=pd.DataFrame(col.train[0][:10000])
bins = 10
hist1 = plt.hist(distances[1],bins)
#convert array distances[1] to vector
dist = []
for d in distances[1]:
    dist.append(d)

dist.sort()
# get a threshold
# if the diference between two distances above the threshold is removed from 
# the dist vecto
threshold = 1000 
ref = 0.0
for d in dist:
    if ((d - ref) > threshold):
        dist.remove(d)
    ref = d

mean = sum(dist)/len(dist)




