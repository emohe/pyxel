import os
import matplotlib.pyplot as plt

from modules.socialevent.GroundTruth import GroundTruth
from modules.socialevent.Histograms import Histograms
from modules.socialevent.Metadata import Metadata
from modules.socialevent.Trainer import Trainer

# Point to the local path where the ground truth and metadata are stored
METADATA_FILE = 'sed2013_dataset_train.csv'
GROUND_TRUTH_FILE = 'sed2013_dataset_train_gs.csv'

path_home = os.path.expanduser('~')

path_work = os.path.join( path_home, 'work/mediaeval/2013-sed/' )
path_datasets = os.path.join( path_work, 'datasets' )

path_histograms = os.path.join( path_work, 'histograms' )

# Load the ground truth data from a file
gtPath = os.path.join(path_datasets, GROUND_TRUTH_FILE)
groundTruth = GroundTruth(gtPath)

# Load the metadata
metadataPath = os.path.join(path_datasets, METADATA_FILE)
metadata = Metadata(metadataPath)

# Run the training process of estimating the average and stanard deviation
# of the distances between pairs of photos in the same event
trainer = Trainer( groundTruth, metadata, 10000)

# Init the structure to generate histograms
histograms = Histograms( path_histograms )
nof_bins = 100

# Time distances
distances_time = trainer.get_distances_time()
histograms.save_histogram(distances_time, nof_bins, 'Distances for time', 'seconds', 'time-noisy.png' )

# Geolocation distances
distances_geo = trainer.get_distances_geo()
histograms.save_histogram(distances_geo, nof_bins, 'Distances for geolocation', 'Km', 'geo-noisy.png' )

# Filter the distances which are over a certain threshold
distances_geo_filtered = [elem for elem in distances_geo if elem < 20 ]
histograms.save_histogram(distances_geo_filtered, nof_bins, 'Filtered Distances for geolocation', 'Km', 'geo-clean.png' )

# User distances
nof_bins = 2
distances_user = trainer.get_distances_user()
histograms.save_histogram(distances_user, nof_bins, 'Distances for users', 'boolean', 'user-noisy.png' )

# Text distances
nof_bins = 5
distances_text = trainer.get_distances_tags()
histograms.save_histogram(distances_text, nof_bins, 'Distances for tags', 'Jaccard', 'tags-noisy.png' )
