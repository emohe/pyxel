# -*- coding: utf-8 -*-
# generar un html

METADATA_FILE_TRAIN = 'sed2013_task2_dataset_train.csv' 
TAG_FILE_TRAIN = 'sed2013_task2_dataset_train_tags.csv'
METADATA_FILE_TEST = 'sed2013_task2_dataset_test.csv'
TAG_FILE_TEST = 'sed2013_task2_dataset_test_tags.csv'
GROUND_TRUTH_FILE = 'sed2013_task2_dataset_train_gs.csv'

import os
from modules.socialevent.Buffer import Buffer
from modules.socialevent.classification.Metadata import Metadata

# Pathnames
HOME = os.path.expanduser('~')
DATASETS_PATH = HOME+'/work/mediaeval/2013-sed/datasets'

# Load the metadata
metadataPathTrain = os.path.join(DATASETS_PATH, METADATA_FILE_TRAIN)
tagsPathTrain = os.path.join(DATASETS_PATH, TAG_FILE_TRAIN)
metadataPathTest = os.path.join(DATASETS_PATH, METADATA_FILE_TEST)
tagsPathTest = os.path.join(DATASETS_PATH, TAG_FILE_TEST)
gtPath = os.path.join(DATASETS_PATH, GROUND_TRUTH_FILE)




html = Buffer()
met = Metadata(metadataPathTrain,tagsPathTrain)
# Crear dos instancias de la clase buffer:

page = html.buffer()


# Llenar cara buffer:

page.inicio( 'Train Images' )
page.encabezado( '2', 'Esta es página 1')

urls = met.get_URL()

for url in urls:
    page.liga( url , ' ' )

page.final()
    

# Vaciar cada buffer a un archivo:

fp = open( 'page.html', 'w' )
fp.write( page.pop() )

