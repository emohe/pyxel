import sys
import getopt
import json

CLUSTER_DEV_FILE='./data/SED_2014_Dev_Clusters.json'
METADATA_DEV_FILE='./data/SED_2014_Dev_Metadata.json'
METADATA_TEST_FILE='./data/SED_2014_Test_Metadata.json'

def main():
  print "Process original json files to more standard format"
  #input_name = None
  #output_name = None
  input_name = METADATA_TEST_FILE
  output_name = './data/SED_2014_Test_Metadata_outputFile.json'

  try:
    opts, args = getopt.getopt(sys.argv[1:], "i:o:")
  except getopt.GetoptError:
    print 'error1'
    sys.exit(2)

  for opt, arg in opts:
    if opt == "-i":
      input_name = arg
    elif opt == "-o":
      output_name = arg

  if (not input_name) or (not output_name):
    print 'error2'
    sys.exit(2)

  file_in = open(input_name,'r')
  file_out = open(output_name,'wb')

  file_out.write('[')
  for i,line in enumerate(file_in.readlines()):
    # Dont put comma in the first line
    if i != 0:
      file_out.write(','+line)
    else:
      file_out.write(line)

  file_out.write(']\r\n')

  file_in.close()
  file_out.close()

if __name__ == "__main__":
  main()
