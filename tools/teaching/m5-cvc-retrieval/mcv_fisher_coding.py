""" 
    
    FISHER coding
    
    Master in Computer Vision - Barcelona
    
    Author: Francesco Ciompi
         
"""

from sklearn import mixture
import pickle                    # save/load data files
from numpy import *
import platform
import mcv_descriptors 
from PIL import Image     # python image library
if not platform.node().lower().startswith('compute'):
    from pylab import *  
from mcv_tools import *
import multiprocessing as mp

import warnings
warnings.filterwarnings("ignore")


""" 
    Implementation based on the papers:

    [1] "The devil is in the details: an evaluation of recent feature encoding methods"
    Ken Chatfield, Victor Lempitsky, Andrea Vedaldi, Andrew Zisserman, 2011
    
    [2] "Large-Scale Image Retrieval with Compressed Fisher Vectors"
    Florent Perronnin, Yan Liu, Jorge Sanchez and Herve Poirier, 2010    

"""


#
#    Gaussian Mizture Model (GMM) estimation of data X as a mixture of K pdf
#
def GMM_estimate(X,K):

    clf = mixture.GMM(n_components=K,covariance_type='diag',n_iter=5,thresh=0.1,min_covar=0.01)

    print 'GMM fitting with '+str(K)+' pdfs on '+str(shape(X)[1])+' dimensions'
    clf.fit(X)
    
    # assign values
    pi_k = clf.weights_
    mu_k = clf.means_
    sigma_k = clf.covars_
    
    print 'GMM fitting finished!'
    
    return clf, pi_k, mu_k, sigma_k
    
    

#
#    Fisher encoding
# 
def fisher_encoding(X,gmm,K,D):

    # the input is one image!
    
    N = shape(X)[0]
        
    pi_k = gmm.weights_
    mu_k = gmm.means_
    SIGMA_k = gmm.covars_
        
    # Eq. (2) in [1]
    logprob, P = gmm.eval(X)    
    Pi_k = tile(pi_k,(N,1))    
    den = reshape(sum(P * Pi_k,axis=1),(N,1))    
    qki = ((P * Pi_k)/tile(den,(1,K))).T
        
    # Eq. (3) in [1]
    F = zeros((1,2*D*K))
    pos = 0
    for k in range(K):
    
        u_k = 1/(N*sqrt(pi_k[k])) * sum(qki[k] *  dot(sqrt(linalg.inv(SIGMA_k)),(X - tile(mu_k[k],(N,1))).T),axis=1)
        v_k = 1/(N*sqrt(2*pi_k[k])) * sum(qki[k] * ((X - tile(mu_k[k],(N,1))).T * dot(linalg.inv(SIGMA_k),(X - tile(mu_k[k],(N,1))).T)-1),axis=1)
                
        F[0,pos*D:(pos+1)*D] = u_k; print ''+str(pos*D)+' - '+str((pos+1)*D)+''; pos+=1; 
        F[0,pos*D:(pos+1)*D] = v_k; print ''+str(pos*D)+' - '+str((pos+1)*D)+''; pos+=1; 
    
    return F        
    

#
#    gamma_ti
#        
""" Gamma function as described in [2]"""
def gamma_ti(X,gmm):

    # X = T x D matrix (see [2] for notation)

    wi = gmm.weights_
    logprob, P = gmm.eval(X)
    T = shape(X)[0]    
    D = shape(X)[1]
    N = len(wi)
    gamma = zeros((N,T))

    SIGMA_k = gmm.covars_
    sigmai = sqrt(diag(SIGMA_k))
    ui = gmm.means_
    
    # Eq. (5) in [2]
    for t in range(T):
        gamma[:,t] = (wi * P[t]) / sum(wi * P[t])    
    
    # Eq. (7) in [2]
    wiX = sum(gamma,axis=1)/T
    
    # Eq. (8) in [2]
    uiX = double(zeros((N,D)))
    for i in range(N):
        NUM = double(zeros((1,D)))
        for t in range(T):    
            xt = X[t]
            gti = gamma[i,t]
            NUM = NUM + xt*gti
        if sum(gamma[i]) > 0:
            uiX[i,:] = NUM / sum(gamma[i])

    # delta_i^X
    deltaiX = (uiX - ui)/reshape(sigmai,(shape(uiX)[0],-1))
        
    # b_i^X, section 4.1.1 in [2]
    biX = wiX > 0

    # u_i^X, section 4.1.2 in [2]
    uiX = deltaiX > 0

    return biX, uiX


#
#    Fisher similarity (dot product) with binarized codes
#
""" Based on [2], formula (11) """
def  fisher_bin_dot(b1,b2,u1,u2):

    N = len(b1)
    D = shape(u1)[1]
    
    bin_dot = 0

    for i in range(N):
        
        bin_dot += double(b1[i])*double(b2[i])*(D-2*hamming(u1[i],u2[i]))
        
    return bin_dot/(sqrt(D*sum(b1))*sqrt(D*sum(b2)))
        

clf = []
meanX = []
stdX = []

def FisherVector(img, params, clf, meanX, stdX):
    feats,pos = getFeatImgSample(img,params,0,False,False)
    feats = normalizeFeat(feats,meanX,stdX)[0]
    return FisherEncodingSingleImage(feats, params, False, model=(clf,meanX,stdX))

def FisherEncodingSingleImage(feats,params,showFig,imfile=None,model=None):

    global clf, meanX, stdX
    if not (model is None):
        clf = model[0]
        meanX = model[1]
        stdX = model[2]
    else:
        print imfile
    # 'normal' fisher coding
          
    if params['fisher-mode'] == 'normal':
        F = fisher_encoding(feats,clf,params['K'],shape(feats)[1])
        if imfile is None:
            return F
        else:
            f = open(imfile+'.pkl', 'wb');
            pickle.dump(F,f)
            f.close()
           
    # 'binary' (space save) fisher coding
    elif params['fisher-mode'] == 'binary':
        b1, u1 = gamma_ti(feats,clf)
        if imfile is None:
            return (b1,u1)
        else:
            f = open(imfile+'.pkl', 'wb');
            pickle.dump(b1,f)
            pickle.dump(u1,f)
            f.close()
            
#
#    Fisher vector of an entire dataset
#
def computeFisherVectors(TRAINFILES,params,runmultiprocess,fisherDir,showFig='False'):
    
    global clf, meanX, stdX

    #    
    # 1. join descriptors, to compute GMM
    #    
    NR = len(TRAINFILES)*params['nPt']
    if params['descriptor'][0] == 'hog':
        D = params['nf_hog']        
    elif params['descriptor'][0] == 'SIFT':
        D = params['nf_sift']
    elif params['descriptor'][0] == 'SURF':
        D = params['nf_surf']

    # initialize data matrix
    X = zeros((NR,D))
    idxs = zeros((len(TRAINFILES),2))

    if runmultiprocess: 
        pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
        PoolResults = []

    idx = 0
    n_im = -1
    for imfile in TRAINFILES:
        n_im += 1
        print imfile
        img = array(Image.open(imfile).convert('L'))
        
        if runmultiprocess:         ########### start change
            PoolResults.append(pool.apply_async(getFeatImgSample, args=(img,params,n_im,True,False)))    
        else:
            feats, im_idx = getFeatImgSample(img,params,n_im,True,showFig);
            print shape(feats)
            if shape(feats)[0]>0:
                idxs[im_idx,0]=idx
                idxs[im_idx,1]=shape(feats)[0]
                X[idx:idx+shape(feats)[0]] = feats
                idx+=shape(feats)[0]

    if runmultiprocess:
        pool.close()
        while (len(PoolResults)>0):
            try:
                feats, im_idx = PoolResults[0].get(timeout=0.001)
                PoolResults.pop(0)
                
                print shape(feats)
                if shape(feats)[0]>0:
                    idxs[im_idx,0]=idx
                    idxs[im_idx,1]=shape(feats)[0]
                    X[idx:idx+shape(feats)[0]] = feats
                    idx+=shape(feats)[0]
            except:
                pass
                
        pool.join()

    print 'Size data: [ ' + str(shape(X)[0]) + ' , ' + str(shape(X)[1]) + ' ]'
    
    #    
    # 2. normalize descriptors
    #    
    X,meanX,stdX = normalizeFeat(X)


    #
    # 3. compute Gaussian Mixture Models over library data
    #    
    print 'compute GMM over X'
    clf, pi_k, mu_k, sigma_k = GMM_estimate(X,params['K'])
    f = open(fisherDir+'../FISHER_GMM_'+params['descriptor'][0]+'.pkl', 'wb');
    pickle.dump(clf,f)
    pickle.dump(meanX,f)
    pickle.dump(stdX,f)
    f.close()

    #
    # 4. encode library images
    #
    if runmultiprocess: 
        pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
        PoolResults = []

    n_im = -1
    for imfile in TRAINFILES:
        n_im += 1
        print 'fisher vector: '+imfile
               
        imfile2 = fisherDir+imfile.split('/')[-1][0:-5]
        feats = X[idxs[n_im,0]:idxs[n_im,0]+idxs[n_im,1]]
        
        if runmultiprocess:         ########### start change
            PoolResults.append(pool.apply_async(FisherEncodingSingleImage, args=(feats,params,False,imfile2)))    
        else:
            FisherEncodingSingleImage(feats,params,showFig,imfile2)

    if runmultiprocess:
        pool.close()
        pool.join()
            
    
    print 'Fisher encoding finished!'
    

    
