"""
	Locality Sensitive Hashing (LSH)

	Master in Computer Vision - Barcelona
	
	Author: Francesco Ciompi	
		
"""

import pickle					# save/load data files
import mcv_tools as mcvtools
from numpy import *
import random as rnd
import platform
if not platform.node().lower().startswith('compute'):
    from pylab import *       # matplotlib, for graphical plots
import mcv_product_quantization as pq

"""

	Based on the paper:
	[1] A. Gionis, P. Indyk, R. Motwani, "Similarity Search in High Dimensions via Hashing"
	

"""


#
#	compute lsh descriptors from product quantization codes
#
def computeLSH(filesDir,params,lshDir):

	files = mcvtools.getFilelist(filesDir,'pkl')[0]			# "mcvtools."
	
	C = params['K']			# maximum value of the coordinates of "p"	
	l = params['l']			# number of buckets/hashing tables	
	nmax = params['nmax']	# maximum number of bits per bucket
	d = params['pq_m']		# number of integer per vector 'p'
	
	# generate (and store) buckets
	G = buckets(C,l,d,nmax)
	print 'Random buckets: '
	print str(G)+'\n'
	
	f = open(lshDir+'../lsh_G.pkl', 'wb');
	pickle.dump(G,f)
	f.close()
	
	#
	# pre-processing of descriptor (already encoded with product-quantization)
	#
	# this step is Algorithm in Figure (1) in [1]
	#
	for filename in files:
	
		print 'processing file '+filename
		
		f = open(filename, 'rb')
		qv = pickle.load(f)
		f.close()
			
		print 'product quantization code: '+str(qv.T)
					
		# preprocessing of the vector (section 3.1 in [1])
		g = preproc(qv,C,G)
		
		print 'projected codes in hashing tables:'
		for gg in g:
			print gg
				
		f = open(lshDir+filename.split('/')[-1][0:-4]+'.pkl', 'wb');
		pickle.dump(g,f)
		pickle.dump(G,f)
		f.close()	

	print "Computing LSH finished!"
    
#
#	Compute LSH distance between a new descriptor 'q' and a library lsh descriptor 'p'
#	The descriptor 'q' is first quantized and then preprocessed
#
def lsh_distance(q,vp,params,codebook,G):

	# 1. 'q' is quantized through product quantization
	qq = pq.quantize_vector(q,params['pq_m'],codebook,params['K'])
	
	
	# 2. the quantized version of 'q' is pre-processed
	vq = preproc(qq,params['K'],G)
	
	# 3.the Hamming distance between p and q through LSH is computed
	d = 0
	for j in range(len(vp)):
		d+= mcvtools.hamming(vp[j],vq[j])
		
	return d		
	


#
#	from integer to unary code
#
#	this is the functional "v" in [1]	
def unaryC(x,C):

	y = zeros((1,C))
	y[0,0:x] = 1
	
	return y


#
#	generate buckets / hasing tables
#
def buckets(C,l,d,nmax):
	
	G = []
	
	for j in range(l):
		
		G.append(rnd.sample(xrange(C*d), rnd.randint(1,nmax)))		
		
	return G
	
	
#
#	lsh preprocess descriptor
#	
def preproc(p,C,G):
	
	# joint unary version 'v(p)' of coordinates in 'p'
	vp = []
	for cw in p:
		vp.append(unaryC(cw+1,C))		
	vp = array(vp).flatten()
		
	# projection on buckets
	g = []
	for I in G:
		g.append(vp[array(I)-1]) 
	
	return g





