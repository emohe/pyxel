''' 
***************************************************************
CODE M5: WEEKS 4,5

created: 23/05/2013
updated: 30/08/2013

author: Francesco Ciompi
***************************************************************
TASK
> retrieve images containing similar objects, based on a query

DATA
> ImageNet dataset
> 2 classes: dog, car
> ~ 5000 images per class
> query based on PASCAL images, similar images retrieved from ImageNet (large scale) library

METHODS
> Fisher vector (binary version)
> Product quantization
> Relevance feedback
> Sparse coding

OPTIONS (implemented in product quantization)
> Relevance feedback
> Inverted file
> Locality Sensitive Hashing
***************************************************************
'''
from sys import exit,path            # system functions for escape
import platform
import time
import os
from numpy import *
path.append('.')  


from PIL import Image     # python image library
from sklearn.lda import *
from numpy import *    # numpy, for maths computations
if not platform.node().lower().startswith('compute'):
    from pylab import *       # matplotlib, for graphical plots
from mcv_fisher_coding import *
from mcv_pca import *    # principal component analysis    
from mcv_tools import *
from mcv_product_quantization import *
from mcv_sparse_coding import *
from mcv_lsh import *
#from mcv_svm import *
from sklearn.grid_search import GridSearchCV 
import pickle
from sklearn.svm import SVC

# ******************  MAIN **************************
if __name__ == "__main__":
    gtfile='C:\Users\Amaia\Dropbox\Work\TFM\data/gt'
    
    LibraryDir='C:\Users\Amaia\Dropbox\Work\TFM\data/frames' #this is the directory containing a folder for each shot
    QueryDir='C:\Users\Amaia\Dropbox\Work\TFM\data\queries'
    OutputFolder  = 'C:/Users/Amaia/Dropbox/Work/TFM/output/'
    qrelfile='C:\Users\Amaia\Dropbox\Work\TFM\data/gt'
    fisherDir = OutputFolder + 'fisher/'
    lshDir    = OutputFolder + 'lsh/'
    pqDir     = OutputFolder + 'pq/'
    sparseDir = OutputFolder + 'sparse/'
    classifierDir= OutputFolder + 'classifier/'

    if not os.path.isdir(OutputFolder): os.mkdir(OutputFolder)
    if not os.path.isdir(fisherDir):    os.mkdir(fisherDir)
    if not os.path.isdir(lshDir):       os.mkdir(lshDir)
    if not os.path.isdir(pqDir):        os.mkdir(pqDir)
    if not os.path.isdir(sparseDir):    os.mkdir(sparseDir)   
    if not os.path.isdir(classifierDir):os.mkdir(classifierDir)  
    
    # library files
    
    #libFiles_car = getFilelist(LibraryDir+'car','JPEG')[0]        #full path
    #libFiles_dog = getFilelist(LibraryDir+'dog','JPEG')[0]    
    #LIBFILES = libFiles_car + libFiles_dog
    
    LIBFILES=get_filepaths(LibraryDir)
    QUERYFILES=get_filepaths(QueryDir)

    # query files
    #QUERYFILES = getFilelist(QueryDir+'dog','jpg')[0] + \
    #             getFilelist(QueryDir+'car','jpg')[0]
      
    # fisher-mode: normal, binary
    # pq_m: number of splits in product quantization; pq_descr: descriptor used to create vector ('gist')
    # pq_dist_mode: sdc (symmetric) / adc (asymmetric)
    # pca: True/False, to apply PCA to data
    # lda: True/False, to aplly LDA to data
    # inverted-file: apply inverted file / look up table to product quantization
    # relevance_feedback
    # lsh: l = number of buckets/hashing tables used; nmax = maximum number of bits per bucket
    
    # codesize: number of clusters for kmeans
    # K: number of clusters in product quantization
    
    params = {    'patchSize':48, 'gridSpacing':48, 'keypointDetector':'MSER', 'descriptor':['SIFT'],\
                    'K': 20, 'nPt': 300, \
                    'codesize':160,\
                    'harris_sigma':3, 'harris_min_dist':10, 'harris_threshold':0.1, 'harris_wid':5,\
                    'nf_sift':128, \
                    'nf_hog':81,\
                    'nf_gist':960,\
                    'nf_surf':128, 'surf_extended':1,'surf_hessianThreshold':0.1, 'surf_nOctaveLayers':1, 'surf_nOctaves':1,\
                    'classifier':'svm','svm_kernel':'linear',\
                    'fusion': 'off',\
                    'classes':['car','dog','bicycle','motorbike','person'],\
                    'ecoc-coding':'OneVsAll',\
                    'fisher':False,\
                    'fisher-mode':'binary',\
                    'pq_m': 16,'pq_dist_mode':'sdc',\
                    'pca': False,\
                    'lda': False,\
                    'vlad':False,\
                    'knn': False, 'type_knn': 'kd_tree',\
                    'inverted-file': False,\
                    'relevance_feedback': False,\
                    'lsh': False, 'l': 8, 'nmax': 10}

    #
    #    FISHER VECTOR
    #    > any descriptor can be used, as in BoW
    #
    #    PRODUCT QUANTIZATION
    #    > needs GLOBAL image descriptors: GIST, CENTRIST, FISHER VECTOR etc.
    #
    #    SPARSE_CODING
    #    > any descriptor can be used, as in BoW
    #
    print params
    runmultiprocess = True

    query = True

    showFig = False
    
    # 
    # query technique
    #
    fisher = False
    product_quantization = True
    sparse_coding = False
    
    #
    # check for incmpatibility
    #
    #if product_quantization and params['descriptor'][0] != 'gist':
    #    print 'ERROR: product quantization needs an image global descriptor!'
    #    exit(-1)
    
    if params['inverted-file'] and params['lsh']:
        print 'ERROR: Inverted file and LSH are incompatibles!'
        exit(-1)
    
    if fisher and params['keypointDetector'] == 'void':
        print 'ERROR: fisher vector is based on keypoints and local image descriptors!'
        exit(-1)

    if not query:
        #
        # PREPARE SEARCH LIBRARY
        #
    
        # 1. compute fisher vector of the entire train dataset
        if fisher: computeFisherVectors(LIBFILES,params,runmultiprocess,fisherDir,showFig)
    
        # 2. compute product quantization of the entire train dataset and store the codebook
        if product_quantization: computeProductQuantization(LIBFILES,params,showFig,pqDir)
    
        # 2b. compute locality sensitive hashing on product quantization and store codes
        if product_quantization and params['lsh']: computeLSH(pqDir,params,lshDir)
    
        # 3. compute sparse coding of the entire train dataset and store the codebook
        if sparse_coding: computeSparseCoding(LIBFILES,params,sparseDir,showFig)
        

    else:
        shotlist=os.listdir(LibraryDir)
        #
        # QUERY: image retrieval
        #    
        numEval = 5 # eval de first numEval retrieval results
        
        MAP1 = 0
        MAP5 = 0
        MAP10 = 0
        MAP = 0
    
    
        if sparse_coding:
            LIBRARY,UNUSED = getFilelist(sparseDir,'pkl')
        elif fisher:
            LIBRARY,UNUSED = getFilelist(fisherDir,'pkl')
        elif product_quantization:            # library of quantized files (with option LSH)
            if params['lsh'] and ~params['inverted-file']:
                LIBRARY = os.listdir(lshDir)
            else:
                LIBRARY = os.listdir(pqDir)
        
                
        print LIBRARY, pqDir
        if numEval > len(LIBRARY):
            numEval = len(LIBRARY)
            print 'Max num of evals:' + str(len(LIBRARY))

    
        #
        #    FISHER VECTOR
        #
    
        if fisher:
            
            f = open(fisherDir+'../FISHER_GMM_'+params['descriptor'][0]+'.pkl', 'rb')
            clf = pickle.load(f)
            meanX = pickle.load(f)
            stdX = pickle.load(f)
            f.close()                
        
            if params['fisher-mode'] == 'binary':
                b_fisher = [None]*len(LIBRARY)
                u_fisher = [None]*len(LIBRARY)
                for nfile in range(0,len(LIBRARY)):
                
                    if params['fisher-mode'] == 'binary':
                
                        print LIBRARY[nfile]
                    
                        f = open(LIBRARY[nfile], 'rb');
                        b_fisher[nfile] = pickle.load(f)
                        u_fisher[nfile] = pickle.load(f)
                        f.close()
                    
                    elif params['fisher-mode'] == 'normal':    
                        pass    
            
            aux=1
            for filename in QUERYFILES:
                print filename
                print "----> " + str(aux) + "/" + str(len(QUERYFILES));aux+=1
                img = array(Image.open(filename).convert('L'))
                
                FV=FisherVector(img, params, clf, meanX, stdX)
                
                fdist = zeros((1,len(LIBRARY)))
    
                cont = 0    
                for nfile in range(0,len(LIBRARY)):
                    if params['fisher-mode'] == 'binary':
                        # fisher dot product
                        fdist[0,cont] = 1 - fisher_bin_dot(FV[0],b_fisher[nfile],FV[1],u_fisher[nfile])
                        
                    elif params['fisher-mode'] == 'normal':    
                            
                        pass    
                            
                    cont+=1
                    
                                
                # argsort: from smaller to larger value
                idxs = argsort(abs(fdist))[0]
            
                MAP1 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 1)
                MAP5 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 5)
                MAP10 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 10)
                MAP += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, numEval)
                
                # show result
                if showFig: 
                    showQueryResult(filename,LibraryDir,LIBRARY,idxs,'FISHER VECTOR')
                    raw_input("Press enter to continue")
    
        #
        #    PRODUCT QUANTIZATION
        #
    
        if product_quantization:
            
            # load descriptor statistics
            f = open(pqDir+'../pq_stats.pkl', 'rb')
            meanX = pickle.load(f)
            stdX = pickle.load(f)    
            f.close()
            
            # load codebook
            f = open(pqDir+'../pq_codebook_'+params['descriptor'][0]+'.pkl', 'rb')
            CODEBOOK =     pickle.load(f)
            f.close()
            
            tcount = 0
            if params['descriptor'][0] !='gist':
                 f = open(pqDir+params['keypointDetector']+'_'+params['descriptor'][0]+'_centroids.km', 'rb');
                 centroids=pickle.load(f)
                 meanXkm=pickle.load(f)
                 stdXkm=pickle.load(f)
                 f.close() # store centroids                      
                
            for filename in QUERYFILES:
             
                print filename
                
                # load query image
                img = Image.open(filename).convert('L')
                wi,hi = img.size
                if wi > hi:
                    img = array(img.resize((320, 240), Image.ANTIALIAS)) # resize to a unique size
                else:
                    img = array(img.resize((240, 320), Image.ANTIALIAS)) # resize to a unique size
                
                x = getFeatImg(img,params)[0]; print shape(x)
               
                
                #from here I added
                if params['descriptor'][0] != 'gist':
                     x=randSubsample(x, params['nPt'])
                     if params['descriptor'][0] == 'hog':
                         D = params['nf_hog']        
                     elif params['descriptor'][0] == 'SIFT' or params['descriptor'][0] == 'sift':
                         D = params['nf_sift']
                     elif params['descriptor'][0] == 'SURF' or params['descriptor'][0] == 'surf':
                         D = params['nf_surf']
    
                     x = normalizeFeat(x,meanXkm,stdXkm)[0]
                     
                     if params['vlad']:
                     
                         code,distance = vq(x,centroids)
                         hist = zeros((1,params['codesize']*D))
                 
                         for i in range(shape(code)[0]):
                    
                              hist[0,code[i]*D:code[i]*D+D]+=x[i] - centroids[code[i]]
                         histNorm=hist
                     else:
                         code,distance = vq(x,centroids)
                         hist = zeros((1,params['codesize']))
              
                         for c in code:
                             hist[0,c-1] = hist[0,c-1] + 1
                             
                         histNorm = hist/(sum(hist)+0.0001)    # normalize
              
                     x=histNorm
                   
                     
                x = normalizeFeat(x,meanX,stdX)[0]
                      
                # compute query results: idxs are the indices of (sorted) most similar images
                ts = time.time()         
                
                idxs = pq_query(x,CODEBOOK,params,LIBRARY,lshDir,pqDir)
                print idxs
                tcount += time.time()-ts
                # show result
                if not params['relevance_feedback']:
                    MAP1=MeanAveragePrecision(qrelfile,idxs,shotlist,1,filename)
                    print filename, idxs, shotlist

                    if showFig: 
                        showQueryResult(filename,LibraryDir,LIBRARY,idxs,'PRODUCT QUANTIZATION')
                        raw_input("Press enter to continue")

                else:    
                        
                    # Relevance Feedback, based on Rocchio algorithm
                    # http://en.wikipedia.org/wiki/Rocchio_algorithm
                    weights = showQueryResult(filename,LibraryDir,LIBRARY,idxs,'PRODUCT QUANTIZATION',True)

                    
                    a = 1.0     # Original Query Weight
                    b = 0.75    # Related Images Weight
                    c = 0.25    # Non-Related Images Weight                    
                    Qo = x      # Original query vector   
                    
                        
                    sumDr = zeros(shape(x)); nDr = 0
                    sumDnr = zeros(shape(x)); nDnr = 0
                    idx = 0
                        
                    print 'w '+str(len(weights))
    
                    for widx in weights:
                        try:
                            print LIBRARY[idxs[idx]]
                            img = array(Image.open(LibraryDir+'car/'+LIBRARY[idxs[idx]].split('/')[-1][0:-4]+'.JPEG'))
                        except:
                            img = array(Image.open(LibraryDir+'dog/'+LIBRARY[idxs[idx]].split('/')[-1][0:-4]+'.JPEG'))
                        xl = getFeatImg(img,params)[0]


                        if params['descriptor'][0] != 'gist':


                            f = open(pqDir+params['keypointDetector']+'_'+params['descriptor'][0]+'_centroids.km', 'rb')
                            centroids=pickle.load(f)
                            meanXkm=pickle.load(f)
                            stdXkm=pickle.load(f)
                            f.close()

                            xl=randSubsample(xl, params['nPt'])
                            if params['descriptor'][0] == 'hog':
                                D = params['nf_hog']
                            elif params['descriptor'][0] == 'SIFT' or params['descriptor'][0] == 'sift':
                                D = params['nf_sift']
                            elif params['descriptor'][0] == 'SURF' or params['descriptor'][0] == 'surf':
                                D = params['nf_surf']

                            xl = normalizeFeat(xl,meanXkm,stdXkm)[0]

                            if params['vlad']:

                                code,distance = vq(xl,centroids)
                                hist = zeros((1,params['codesize']*D))

                                for i in range(shape(code)[0]):

                                    hist[0,code[i]*D:code[i]*D+D]+=xl[i] - centroids[code[i]]
                                histNorm=hist
                            else:
                                code,distance = vq(xl,centroids)
                                hist = zeros((1,params['codesize']))

                                for c in code:
                                    hist[0,c-1] = hist[0,c-1] + 1

                                histNorm = hist/(sum(hist)+0.0001)    # normalize

                            xl=histNorm



                        xl = normalizeFeat(xl,meanX,stdX)[0]

                        idx+=1
                            
                        if widx == 1:
                            sumDr += xl; nDr+=1
                        elif widx == -1:
                            sumDnr +=xl; nDnr+=1
                            
                    if nDr > 0: kDr = 1.0/nDr
                    else: kDr=0
                    if nDnr > 0: kDnr = 1.0/nDnr
                    else: kDnr=0
    
                    Qm = 0.5*(a*Qo + b*kDr*sumDr - c*kDnr*sumDnr) # eq. (x) in [y]
                        
                    # new query
                    idxs = pq_query(Qm,CODEBOOK,params,LIBRARY,lshDir)
                    MAP1 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 1)
                    MAP5 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 5)
                    MAP10 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 10)
                    MAP += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, numEval)

                    if showFig: 
                        showQueryResult(filename,LibraryDir,LIBRARY,idxs,'PRODUCT QUANTIZATION')    
                        raw_input("Press enter to continue")

                    
            print 'Query time for PQ per image: %2.2f sec' % (tcount/len(QUERYFILES))
                
    
        #
        #    SPARSE CODING
        #
    
        if sparse_coding:
        
            f = open(sparseDir+'../SPARSE_CODING_'+params['descriptor'][0]+'.pkl', 'rb')
            centroids = pickle.load(f)
            meanX = pickle.load(f)
            stdX = pickle.load(f)
            f.close()    

	    f = open(classifierDir+'svm_model', 'rb');
            svm_model=pickle.load(f)
            f.close()
            
            f = open(classifierDir+'idxs_class1', 'rb');
            idxs_class1=pickle.load(f)
            f.close() 
            
            f = open(classifierDir+'idxs_class2', 'rb');
            idxs_class2=pickle.load(f)
            f.close()  
	
            print "Predicted classes for query files"
            for filename in QUERYFILES:
             
                print filename
                
                # load query image  
                img = Image.open(filename).convert('L')
                wi,hi = img.size
                if wi > hi:
                    img = array(img.resize((320, 240), Image.ANTIALIAS)) # resize to a unique size
                else:
                    img = array(img.resize((240, 320), Image.ANTIALIAS)) # resize to a unique size
                
                x = getFeatImg(img,params)[0]; print shape(x)
                x = normalizeFeat(x,meanX,stdX)[0]
                
                # sparse code
                A_query = sparsecode(x,centroids)
                A_query=max(A_query.transpose()).toarray()
               
                Am=zeros((1,shape(A_query)[1]))
                
                for i in range(shape(A_query)[1]):
	           
                    Am[0,i] = A_query[0][i]
                  
                y_predicted=svm_model.predict(Am)
                print y_predicted
               
		if y_predicted==1:
		    idxs = idxs_class1
		if y_predicted==-1:
		    idxs = idxs_class2
                
                MAP1 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 1)
                MAP5 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 5)
                MAP10 += MeanAveragePrecision(filename, LibraryDir, LIBRARY, idxs, 10)
     
        print 'MAP 1: %2.2f ' % (MAP1/len(QUERYFILES))
        #print 'MAP 5: %2.2f ' % (MAP5/len(QUERYFILES))
        #print 'MAP 10: %2.2f ' % (MAP10/len(QUERYFILES))
