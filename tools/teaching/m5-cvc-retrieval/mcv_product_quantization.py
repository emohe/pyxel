""" 
    
    PRODUCT QUANTIZATION
    
    Master in Computer Vision - Barcelona
    
    Author: Francesco Ciompi
         
"""

from numpy import *
import os
import time
from scipy.cluster.vq import *    # for k-means
import pickle                    # save/load data files
import platform
if not platform.node().lower().startswith('compute'):
    from pylab import *       # matplotlib, for graphical plots
from PIL import Image     # python image library    
import multiprocessing as mp
from mcv_tools import *
from mcv_lsh import *
from sklearn.neighbors import NearestNeighbors

""" 
    Based on paper:

    [1] Product quantization for nearest neighbor search
        Herve Jegou, Matthijs Douze, Cordelia Schmid

    1. given the library, we first compute the codebook by splitting the descriptor into 'm' parts
    2. the same library is then quantized by applying the codebook -> product quantization
    3. given a test codeword, [...]

"""


#
#    COMPUTE THE CODEBOOK FOR VECTOR QUANTIZATION ON A DATASET
#
def computeProductQuantization(TRAINFILES,params,showFig=False,Folder=''):

    #
    # Product Quantization work with single vectors. It is then applied
    # to images described by a unique vector (e.g., GIST) or the result 
    # of a vector description of the image (Fisher Vector, Sparse coding etc.)
    #

    if not params['fisher']:
        #    
        # 1. join descriptors
        #    
        NR = len(TRAINFILES)
        if params['descriptor'][0] == 'hog':
            D = params['nf_hog']        
        elif params['descriptor'][0] == 'SIFT' or params['descriptor'][0] == 'sift':
            D = params['nf_sift']
        elif params['descriptor'][0] == 'SURF' or params['descriptor'][0] == 'surf':
            D = params['nf_surf']
        elif params['descriptor'][0] == 'gist':
            D = params['nf_gist']    
        
        # initialize data matrix
        if params['descriptor'][0] == 'gist':
            X = zeros((NR,D))        # for gist [len(TRAINFILES),960]
        else:
            X=zeros((NR*params['nPt'],D))
        print shape(X)
        #params['descriptor'][0] = params['pq_descr'] # global descriptor
    
        runmultiprocess = False
        if runmultiprocess: 
            pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
            PoolResults = []
        
        idxs = zeros((len(TRAINFILES),2))
        idx = 0
        n_im = -1
        for imfile in TRAINFILES:
            n_im += 1
            print ''+str(n_im)+ "/"+str(len(TRAINFILES))+' '+imfile
            img = Image.open(imfile).convert('L')
            wi,hi = img.size
            if wi > hi:
                img = array(img.resize((320, 240), Image.ANTIALIAS)) # resize to a unique size
            else:
                img = array(img.resize((240, 320), Image.ANTIALIAS))    
            
            if runmultiprocess:         ########### start change
                if params['descriptor'][0] != 'gist':
                    PoolResults.append(pool.apply_async(getFeatImgSample, args=(img,params,n_im,True,False)))
                else:
                    PoolResults.append(pool.apply_async(getFeatImgSample, args=(img,params,n_im,False,False)))    
            else:
                if params['descriptor'][0] != 'gist':
                    feats, im_idx = getFeatImgSample(img,params,n_im,True,showFig);
                else:
                    feats, im_idx = getFeatImgSample(img,params,n_im,False,showFig);
            
                print shape(feats)
                
                if shape(feats)[0]>0:
                    idxs[im_idx,0]=idx
                    idxs[im_idx,1]=shape(feats)[0]
                    X[idx:idx+shape(feats)[0]] = feats
                    idx+=shape(feats)[0]
                    
                    
    
        
        
        if runmultiprocess:
            pool.close()
            while (len(PoolResults)>0):
                try:
                    feats, im_idx = PoolResults[0].get(timeout=0.001)
                    PoolResults.pop(0)
                    print shape(feats)
                    if shape(feats)[0]>0:
                        idxs[im_idx,0]=idx
                        idxs[im_idx,1]=shape(feats)[0]
                        X[idx:idx+shape(feats)[0]] = feats
                        idx+=shape(feats)[0]
                        
                except:
                    pass
                    
            pool.join()
    
        
        #print "***********--X" 
        #print X
        #    
        # 2. normalize descriptors
        #    
    
        
        if params['descriptor'][0] != 'gist':
            
            X,meanX,stdX = normalizeFeat(X)
            idxs = zeros((len(TRAINFILES),2))
            idx = 0
            n_im = -1
        #Kmeans on data
            print "Clustering..."
            ts=time.time()
            centroids,variance = kmeans(X,params['codesize'],15,10-3)
            te=time.time()
            code,distance = vq(X,centroids)    # Assigns a code from a code book to each observation. 
            
            f = open(Folder+params['keypointDetector']+'_'+params['descriptor'][0]+'_centroids.km', 'wb');
            pickle.dump(centroids,f);
            pickle.dump(meanX,f);
            pickle.dump(stdX,f);
            f.close() # store centroids
            print "Codebook stored! Kmeans time: ", te-ts
            
            print "Building histograms..."
            if params['vlad']:
                X = zeros((NR,params['codesize']*D))
            else:
                X = zeros((NR,params['codesize']))
            #After kmeans, create histograms and store them
            for imfile in TRAINFILES:
                n_im += 1
                print ''+str(n_im)+ "/"+str(len(TRAINFILES))+' '+imfile
                img = Image.open(imfile).convert('L')
                wi,hi = img.size
                if wi > hi:
                    img = array(img.resize((320, 240), Image.ANTIALIAS)) # resize to a unique size
                else:
                    img = array(img.resize((240, 320), Image.ANTIALIAS))    
            
            
                feats, im_idx = getFeatImgSample(img,params,n_im,True,showFig)
                
                feats = normalizeFeat(feats,meanX,stdX)[0] 
                
                if params['vlad']:
                  
                    code,distance = vq(feats,centroids)
                    hist = zeros((1,params['codesize']*D))
                    for i in range(shape(code)[0]):
                    
                        hist[0,code[i]*D:code[i]*D+D]+=feats[i] - centroids[code[i]]
                    histNorm=hist
                    
                
                else:
                    code,distance = vq(feats,centroids)
                    hist = zeros((1,params['codesize']))
                
                    for c in code:
                        hist[0,c-1] = hist[0,c-1] + 1
                        
                    histNorm = hist/(sum(hist)+0.0001)    # normalize
                
                
                
                if shape(histNorm)[0]>0:
                    idxs[im_idx,0]=idx
                    idxs[im_idx,1]=shape(histNorm)[0]
                    X[idx:idx+shape(histNorm)[0]] = histNorm
                    idx+=shape(histNorm)[0]
                    
            print "Histograms ready!"
            
            
        X,meanX,stdX = normalizeFeat(X)
    else:
        #here we should add code to use fisher vectors as input to product quantization. to be done if we have time
        X=[]
        
    
    f = open(Folder+'../pq_stats.pkl', 'wb');
    pickle.dump(meanX,f)
    pickle.dump(stdX,f)
    f.close()


    #
    # 3. compute and store codebook
    #
    compute_codebook(X,params['pq_m'],params['K'],params['descriptor'][0],Folder)
    
    
    #
    # 4. apply product quantization to dataset and store descriptors
    #
    f = open(Folder+'../pq_codebook_'+params['descriptor'][0]+'.pkl', 'rb')
    CODEBOOK = pickle.load(f)
    f.close()
        
    n_im = -1
    for imfile in TRAINFILES:
        n_im += 1
        imfile=os.path.normpath(imfile)
        print 'product quantization: '+imfile

        feats = X[idxs[n_im,0]:idxs[n_im,0]+idxs[n_im,1]]
        qv = quantize_vector(feats,params['pq_m'],CODEBOOK,params['K'])
        head,frameID=os.path.split(imfile)
        head,shotID=os.path.split(head)
        nPath=os.path.join(Folder,shotID)
        if not os.path.isdir(nPath): os.mkdir(nPath)
        f = open(os.path.join(nPath,frameID[0:-4])+'.pkl', 'wb');
        pickle.dump(qv,f)
        f.close()
        print 'stored!'



#
#    COMPUTE THE CODEBOOK FOR VECTOR QUANTIZATION
#
def compute_codebook(X,m,k,descr,Folder):
    """ compute the codebook 'C' for input data matrix 'X' and number of splits 'm' 
    codebook is computed with k-means clustering, with parameter 'k' """
    
    D = shape(X)[1] # dimensionality of descriptor
    D_star = D/m    # size of subvectors
    if mod(D,m) != 0:
        print 'ERROR: descriptor size not an integer multiple of subvectors number!'
        exit(-1)
    
    CODEBOOK = zeros((k*m,D_star))
    
    for i in range(m):
        x = X[:,D_star*i:D_star*(i+1)]
        print 'create codebook of '+str(k)+' words for '+str(i)+'/'+str(m)+' data size = '+str(shape(x))
        print k
        centroids,variance = kmeans2(x,k,minit='points')
        print "CODEBOOK shape: " + str(shape(CODEBOOK))
        print "CODEBOOK shape ["+str(i*k)+":"+str((i+1)*k)+"] - centroids shape ["+ str(shape(centroids))+"]"    
        #print centroids    
        CODEBOOK[i*k:(i+1)*k] = centroids
        print 'stored '+str(k)+' centroids for subvector '+str(i)
    
        
    f = open(Folder+'../pq_codebook_'+descr+'.pkl', 'wb')
    pickle.dump(CODEBOOK,f);
    f.close()


#
#    APPLY VECTOR QUANTIZATION TO A VECTOR 'v'
#
def quantize_vector(v,m,codebook,k):
    """ 
    vector 'v', split into 'm' parts 
    """

    D = shape(v)[1]         # dimensionality of descriptor
    D_star = D/m    # size of subvectors    
        
    CODE = zeros((m,1))    
        
    for i in range(m):
        u = v[:,D_star*i:D_star*(i+1)]    # eq.(8) in [1]
        C_i = codebook[i*k:(i+1)*k]
        code,distance = vq(u,C_i)
        CODE[i] = code    # eq.(9) in [1]
    
    return CODE.astype(int)
    
    
#
#    COMPUTE LOOK-UP-TABLE FOR THE 'INVERTED-FILE' OPTION
#
def ivlut(x,codebook,m,k):

    # look-up-table: k x m matrix with distance, for each piece of vector 'x', wrt to each 
    # word of the corresponding codebook, learned with product quantization method

    LUT = zeros((k,m)) # look-up-table
    D = shape(x)[1] # dimensionality of descriptor
    D_star = D/m    # size of subvectors
    for i in range(m):    
        x_star = x[:,D_star*i:D_star*(i+1)]
        centroids = codebook[i*k:(i+1)*k]
        code,distance = vq(centroids,x_star)        
        LUT[:,i] = distance
    
    return LUT
                    

#
#    COMPUTE DISTANCE BETWEEN TWO VECTORS USING VECTOR QUANTIZATION
#
def pq_distance(x,y,dist_mode,codebook,m,k,vec_mode='coded'):

    # the sqrt is avoided for practical computation (as stated in [1])
    # since sqrt is a monotonically increasing function
    
    # yq is the already quantized vector, fetched from the database
    
    # vec_mode:
    #    'coded': x is full vector and y is the coded vector
    #    'full': x and y are full vectors

    if len(x) != len(y) and vec_mode != 'coded':
        print 'ERROR: the two vectors must be equal in size!'
        exit(-1)

    D = len(x)         # dimensionality of descriptor
    D_star = D/m    # size of subvectors

    if vec_mode != 'coded':
        code_y = quantize_vector(y,m,codebook,k)
    else:
        code_y = y
       
    if dist_mode == 'sdc':
    
        # symmetric distance computation, eq.(12) in [1]
        code_x = quantize_vector(x,m,codebook,k)
        
        d = 0.0
        for j in range(m):
            C_j = codebook[j*k:(j+1)*k]
            q_x = C_j[code_x[j]]
            q_y = C_j[code_y[j]]
            d+=sum((q_x-q_y)**2)
        
        return d
        
    elif dist_mode == 'adc':
    
        # asymmetric distance computation, eq.(13) in [1]
        d = 0.0
        for j in range(m):
            C_j = codebook[j*k:(j+1)*k]
            u_x = x[D_star*j:D_star*(j+1)]
            q_y = C_j[code_y[j]]
            d+=sum((u_x-q_y)**2)
        
        return d
            

    
#
#    compute query product-quantization
#    
def pq_query(x,CODEBOOK,params,LIBRARY,lshDir,pqDir):

    # compute query results
    if params['inverted-file']:
    
        # compute look-up-table    
        LUT = ivlut(x,CODEBOOK,params['pq_m'],params['K'])
    elif params['lsh']:
        
        # load pre-compute buckets
        f = open(lshDir+'../lsh_G.pkl', 'rb');
        G = pickle.load(f)
        f.close()
                                
    vdist = zeros((1,len(LIBRARY)-1)) # -1 because of the centroids file
    library_qv = []          
    # scan the library
    shotcount = 0
    print LIBRARY
    for pqfile in LIBRARY:

        if pqfile.find('.km')==-1: #the file containing the centroids of the clustering with kmeans is stored in the same directory of the pqfiles. I put a different extension so that we can detect it and not process it here.
            #ADD CODE HERE - LIBRARY CONTAINS DIRECTORIES OF SHOTS ( WITH THE DESCRIPTORS OF
            #THEIR FRAMES INSIDE).
            print pqfile.find('.km')
            print pqfile
            # ALGORITHM:
            # 1. COMPUTE DISTANCE OF EACH FRAME OF EACH SHOT TO QUERY
            # 2. FOR EACH SHOT, SAVE THE SCORE OF THE BEST FRAME AS ITS REPRESENTATION
            # 3. SORT THE SHOTS BY THEIR SAVED SCORES
            cont = 0

            allframes=os.listdir(os.path.join(pqDir,pqfile))
            print len(allframes)
            shotdist = zeros((1,len(allframes)))
            for frame in allframes:
                f = open(os.path.join(pqDir,pqfile,frame),'rb')
                qv = pickle.load(f)
                f.close()

                if params['inverted-file']:

                    # compute distance with the inverted-file technique
                    for s in range(params['pq_m']):

                        shotdist[0,cont]+=LUT[qv[s],s]

                elif params['lsh']:

                    # compute distance with locality sensitive hashing
                    shotdist[0,cont] = lsh_distance(x,qv,params,CODEBOOK,G)


                elif params['knn']:
                #we store all the qv of the library
                    qv_array = []
                    for i in range(len(qv)):
                        qv_array.append(qv[i][0])
                    library_qv.append(qv_array)
                    #print 'qv: ', qv_array

                else:

                    # compute distance with product quantization
                    shotdist[0,cont] = pq_distance(x,qv,params['pq_dist_mode'],CODEBOOK,params['pq_m'],params['K'])

                cont+=1

            vdist[0,shotcount]=shotdist.min()
            shotcount+=1

    if params['knn']:
        if params['type_knn'] == 'kd_tree':
            neigh=NearestNeighbors(n_neighbors=cont,algorithm='kd_tree')
        else:
            neigh=NearestNeighbors(n_neighbors=cont,algorithm='ball_tree')
            neigh.fit(library_qv)

        qx = pq.quantize_vector(x,params['pq_m'],CODEBOOK,params['K'])
        qx_array = []
        for i in range(len(qv)):
            qx_array.append(qx[i][0])
        dist, index = neigh.kneighbors(qx_array,cont,return_distance=True)
        idxs = []
        for i in range(len(LIBRARY)):
            idxs.append(index[0][i])
    else:
        idxs = argsort(vdist)[0]
    
    return idxs




