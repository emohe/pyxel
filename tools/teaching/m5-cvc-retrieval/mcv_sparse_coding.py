""" 
	
	SPARSE CODING
	
	Master in Computer Vision - Barcelona
	
	Author: Francesco Ciompi
		 
"""

import numpy as np
import scipy
import scipy.sparse
import platform
#import spams
import pickle					# save/load data files
from PIL import Image 	# python image library
from mcv_tools import *
if not platform.node().lower().startswith('compute'):
    from pylab import *       # matplotlib, for graphical plots
from scipy.cluster.vq import *	# for k-means
from mcv_kmeans import *            # mcv personal library of tools


"""

	References:
	[1] SPAMS library: http://spams-devel.gforge.inria.fr/index.html
	[2] SPAMS Python library documentation (python-interface.pdf)
	[3] "Linear Spatial Pyramid Matching Using Sparse Coding for Image Classification", CVPR2009

	INPUT, as in [2], chapter 12
	X = double m x n matrix (input signals) m is the signal size (i.e. descriptor length), n is the number of signals to decompose i.e., keypoints in the image)
	D = double m x p matrix (dictionary) p is the number of elements in the dictionary (i.e., the number of clusters K)
	when mode=2: min_{alpha} 0.5||x-Dalpha||_2^2 + lambda1||alpha||_1 +0.5 lambda2||alpha||_2^2

	OUTPUT
	A: double sparse p x n matrix (output coefficients), such as D*A = X

	The content presented in [3] differs from [2] in nomenclature
	The correspondance is as follows: ([2] -> [3])
	X -> X'
	D -> V' (vocabulary, i.e., the set of centroids)
	U -> A' (matrix of sparse code, used to approximate X as a combination of centroids: X ~= D*A)
	
"""


#
#	Sparse coding of an entire dataset
#
def computeSparseCoding(TRAINFILES,params,sparseDir,showFig='False'):
	
	#	
	# 1. join descriptors, to compute vocabulary
	#	
	NR = len(TRAINFILES)*params['nPt']
	if params['descriptor'][0] == 'hog':
		D = params['nf_hog']		
	elif params['descriptor'][0] == 'sift' or params['descriptor'][0] == 'SIFT':
		D = params['nf_sift']
	elif params['descriptor'][0] == 'surf' or params['descriptor'][0] == 'SURF':
		D = params['nf_surf']

	# initialize data matrix
	X = zeros((NR,D))

	idx = 0
	for imfile in TRAINFILES:
		print imfile
		img = Image.open(imfile).convert('L')
		wi,hi = img.size
		if wi > hi:
			img = array(img.resize((320, 240), Image.ANTIALIAS)) # resize to a unique size
		else:
			img = array(img.resize((240, 320), Image.ANTIALIAS)) #
		
		feats,pos = getFeatImg(img,params,showFig,imfile);
		print shape(feats)
		if feats is not None:
			feats = randSubsample(feats, params['nPt'])
			X[idx:idx+shape(feats)[0]] = feats
			idx+=shape(feats)[0]
	
	#	
	# 2. normalize descriptors
	#	
	X,meanX,stdX = normalizeFeat(X)


	#
	# 3. compute k-means vocabulary over library data
	#	
	print 'compute vocabulary over X'
	centroids,variance = kmeans(X,params['K'],5,10-3)
	f = open(sparseDir+'../SPARSE_CODING_'+params['descriptor'][0]+'.pkl', 'wb');
	pickle.dump(centroids,f)
	pickle.dump(meanX,f)
	pickle.dump(stdX,f)
	f.close()
	
	# re-load
	f = open(sparseDir+'../SPARSE_CODING_'+params['descriptor'][0]+'.pkl', 'rb')
	centroids = pickle.load(f)
	meanX = pickle.load(f)
	stdX = pickle.load(f)
	f.close()


	#
	# 4. encode library images
	#
	for imfile in TRAINFILES:
	
		print 'sparse code: '+imfile
		img = Image.open(imfile).convert('L')
		wi,hi = img.size
		if wi > hi:
			img = array(img.resize((320, 240), Image.ANTIALIAS)) # resize to a unique size
		else:
			img = array(img.resize((240, 320), Image.ANTIALIAS))
		feats,pos = getFeatImg(img,params,showFig,imfile)
		feats = normalizeFeat(feats,meanX,stdX)[0]

		# sparse code
		A = sparsecode(feats,centroids)
		f = open(sparseDir+imfile.split('/')[-1][0:-5]+'.pkl', 'wb');
		pickle.dump(A,f)
		f.close()
	        

	print 'Sparse coding finished!'

#
#	sparse coding of an image through its matrix of descriptors X
#
def sparsecode(X,centroids):
			
	# dense sift descriptors
	X = np.asfortranarray(X.T)
	D = np.asfortranarray(centroids.T)
	
	print shape(X)
	print shape(D)

	# sparse coding matrix through 'lasso' method
	# to avoid segmentation fault, we require "p > n"...
	A = spams.lasso(X,D,mode=2,lambda1=0.0,return_reg_path = False);
	
	return A







