""" 
    
    Library of tools for machine learning and data processing
    
     Master in Computer Vision - Barcelona
 
     Week 1 - Task 3: This code is already prepared to deal with HOG (mcv_descriptors.py), SIFT and SURF -> include SC and BRIEF
    
     Week 2 - Task 1: This code is already prepared to deal with 'keypointDetector' Harris (mcv_harris.py), surf and Sift (mcv_sift.py)
     
     Week 2 - Task 3: This code is already prepared to deal with GIST -> include CENTRIST
     
     Author: Francesco Ciompi, Ramon Baldrich, Jordi Gonzalez
         
"""

# Master in Computer Vision Libraries
import mcv_dsift as dsift            # dense SIFT [cite source]
import mcv_sift as sift                # alternative code from book
from mcv_descriptors import *
import pandas as pd
import mcv_harris as harris
#import mcv_centrist as centrist

# External Libraries
import os                            # operating system
from numpy import *                    # numpy, for maths computations
from PIL import Image                 # python image library
import random as rnd                # random value generator
import cv2.cv as cv                            # opencv
import cv2                            # opencv2

#import opencv
import platform
if not platform.node().lower().startswith('compute'):
    from pylab import *       # matplotlib, for graphical plots
#import leargist

def read_gt_file(filename):
    f= open(filename, "r")
    splitted=list()
    file_list=f.readlines()

    for line in file_list:

        #Split the lines
        newline=line.split('\t')
        #Remove the '\n' at the end of the lines
        newline[3]=newline[3][0:1]

        #Save them all
        splitted.append(newline)
    return splitted

def relnotrel(filename,ranking,shotlist,it,queryname):
    #filename: the ground truth file
    #ranking: the list of retrieved shots sorted by relevance
    #shotlist: list of all shots in the database
    #it: number of ranked elements to consider to obtain the MAP
    #queryname: the name of the query

    shotgt=pd.read_csv(filename,delim_whitespace=True,header=None)
    shotgt.columns=['query','-','shot','rel']
    head,queryID=os.path.split(queryname)
    P = zeros((1,5))
    for res in range(it):
        shotID=ranking[res]
        expression='query=="'+queryID[0:-4]+'" & shot=="'+shotlist[shotID] + '" & rel==1'
        print expression
        result=shotgt.query(expression)

        if result.empty:
            P[0,res]=0
        else:
            P[0,res]=1

    return P




def get_filepaths(directory):
    """
    This function will generate the file names in a directory 
    tree by walking the tree either top-down or bottom-up. For each 
    directory in the tree rooted at directory top (including top itself), 
    it yields a 3-tuple (dirpath, dirnames, filenames).
    """
    file_paths = []  # List which will store all of the full filepaths.

    # Walk the tree.
    for root, directories, files in os.walk(directory):
        for filename in files:
            # Join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)  # Add it to the list.

    return file_paths  # Self-explanatory.
#
#     get list of files in 'path' with extension 'ext'
#
def getFilelist(path,ext='',queue=''):
    # returns a list of filenames (absolute, relative) for all 'ext' images in a directory
    if ext != '':
        return [os.path.join(path,f) for f in os.listdir(path) if f.endswith(''+queue+'.'+ext+'')],  [f for f in os.listdir(path) if f.endswith(''+queue+'.'+ext+'')]    
    else:
        return [os.path.join(path,f) for f in os.listdir(path)]    


#
#    detect random points in the image domain
#
def detectPoints(imgSize,nPt,mode='rnd'):    
    """ compute the position of 'nPt' points in the image domain 'dataSize' with modality 'mode' 
    if 'dense' or a specific detector is declared, nPt is bypassed """
    
    if mode == 'rnd':
        return rnd.sample(range(0,imgSize[0]*imgSize[1]),  nPt);    
    else:
        print'Unkwnown Detection Mode!'
        return None

#
#     random subsample over the rows of data matrix 'data'
#
def randSubsample(data, nPt):
        
    print shape(data)[0] # = 502
    print nPt    
    if shape(data)[0] > nPt:
        idx = array(rnd.sample(range(0,shape(data)[0]), nPt));
        return data[idx]
    else:
        return data


#==============================================================================
# #
# #     Compute feature matrix per list of images
# #
# def getFeatList(imFilesPath,descriptor):
#     
#     if descriptor == 'hog':
#         FEAT = zeros((len(imFilesPath),468),float)
#         cont = 0
#         for filename in imFilesPath:
#             print filename
#             img = array(Image.open(filename),double)
#             H = hog(img)
#             print H
#             FEAT[cont,:] = H
#             cont+=1
#             print cont            
#            
#    return FEAT
#==============================================================================

def getFeatImgSample(img,params,n_im,subsample,showFig):
    feats,pos = getFeatImg(img,params,showFig);
    if subsample and feats is not None:
        feats = randSubsample(feats, params['nPt'])
    return (feats,  n_im)
    
#    
#     compute feature matrix per image
#
def getFeatImg(img,params,showFig=False,filename=''):
    """     Compute features corresponding to the selcted 'descriptor' for image 'img'
    The descriptor is applied to patches (patchSize) over a dense matrix (gridSpacing) 
    in case of SIFT and HOG, and to keypoints in all the others. It also implements the
    early fusion as a combination of descriptors """
        


    # ####################################    
    #     Descriptors positions    
    # ####################################
    
    # the patch is centered in the keypoint    
    if params['keypointDetector'] == "SIFT" or params['keypointDetector'] == "SURF" or params['keypointDetector'] == "MSER" or params['keypointDetector'] =='Dense' :
        detector = cv2.FeatureDetector_create(params['keypointDetector'])
        keypoints = None
        keypoints = detector.detect(img,None)
        positions = zeros((2,shape(keypoints)[0]))
        for kp in range(shape(keypoints)[0]):  positions[0][kp] = keypoints[kp].pt[1]; positions[1][kp] = keypoints[kp].pt[0]    

        POSITIONS = positions
        
    elif params['keypointDetector'] == 'sift':
        
        
        # sift keypoints and descriptors
        # work with original image file
        detector = cv2.FeatureDetector_create("SIFT")
       

        keypoints = detector.detect(img)
        #keypoints, descriptors = descriptor.compute(img, keypoints)
        
        print 'found '+str(shape(keypoints)[0])+' SIFT keypoints'
            
        # extract keypoints positions
        positions = zeros((2,shape(keypoints)[0]))
       
        for kp in range(shape(keypoints)[0]):  positions[0][kp] = keypoints[kp].pt[1]; positions[1][kp] = keypoints[kp].pt[0]    
        
        #sift_descriptors = array(descriptors)
        POSITIONS = positions
        #skp, sd = descriptor.compute(img, skp)
        # sift modules defined in mcv_sift.py        
        #name = sift.process_image(filename,"./tmp.sift",params)
        #sift_keypoints, sift_descriptors = sift.read_features_from_file("./tmp.sift")
        #sift_keypoints, sift_descriptors = sift.process_image(img,params)
        #POSITIONS = fliplr(sift_keypoints[:,0:2]).T
        #print shape(POSITIONS)

    #
    #    SURF keypoints    
    # 
    elif params['keypointDetector'] == 'surf':
        
        # mask to define points
        pos = ones((shape(img)),uint8)
        mask = cv.CreateMat(shape(img)[0], shape(img)[1], cv.CV_8UC1)
        cv.Convert(cv.fromarray(pos), mask)                
        #mask = pos
        
        # SURF params
        blockSize = 64000 # bytes
        storage = cv.CreateMemStorage(blockSize)    
        surf_params = ( params['surf_extended'], params['surf_hessianThreshold'], params['surf_nOctaveLayers'], params['surf_nOctaves'] )
            
        # compute SURF descriptors in keypoints
        #keypoints,descriptors = cv.ExtractSURF(img,mask,storage,surf_params)
        keypoints,descriptors = cv.ExtractSURF(cv.fromarray(img),mask,storage,surf_params)
                    
        print 'found '+str(shape(keypoints)[0])+' SURF keypoints'
            
        # extract keypoints positions
        positions = zeros((2,shape(descriptors)[0]))
        k = 0
        for kp in keypoints: positions[0][k] = kp[0][1]; positions[1][k] = kp[0][0]; k+=1    
        
        surf_descriptors = array(descriptors)
        POSITIONS = positions

    #
    #    no keypoints: used in image retrieval and global descriptors    
    # 
    elif params['keypointDetector'] == 'void' and params['descriptor'][0] == 'gist':
    
        # ####################################            
        #     GIST        
        # ####################################
            
        # it requires PIL image
        gist_vector = leargist.color_gist(Image.fromarray(img))
        DESCRIPTORS = reshape(gist_vector,(1,params['nf_gist']))
        POSITIONS = []

        
    # LOCAL DESCRIPTORS

    if POSITIONS == None:

        print 'Image skipped for lack of key-points'
        DESCRIPTORS = None

    else:

        if showFig:
            ion()        
            figure(1)
            imshow(img,hold=False)
            gray()
            if len(POSITIONS)>0: scatter(POSITIONS[1,:],POSITIONS[0,:])
            draw()    


    
        # ####################################    
        #     Compute image descriptor(s)    
        # ####################################
        #
        #     Include (optional) early fusion
        #
        # ####################################
            
        nf = 0
        for descriptor in params['descriptor']:
            if descriptor == 'SIFT' or descriptor == 'sift' : nf = nf + params['nf_sift']
            if descriptor == 'hog': nf = nf + params['nf_hog']
            if descriptor == 'SURF' or descriptor == 'surf': nf = nf + params['nf_surf']
            if descriptor == 'sc': nf = nf + params['nf_sc']
            if descriptor == 'brief': nf = nf + params['nf_brief']
            if descriptor == 'gist': nf = nf + params['nf_gist']
            if descriptor == 'centrist': nf = nf + params['nf_centrist']
        
        
        if params['keypointDetector'] != 'void': # if not image retrieval
            DESCRIPTORS = zeros((shape(POSITIONS)[1],nf))
        
            
        descrPos = 0        
        for descriptor in params['descriptor']:
        
            if descriptor == "SIFT" or descriptor == "SURF" :
               
               
                extractor = cv2.DescriptorExtractor_create(descriptor)
                keypoints, descriptors = extractor.compute(img, keypoints)
                
                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]


            # ##############################################    
            #     Scale Invariant Feature Transform (SIFT)    
            # ##############################################
            if descriptor == 'sift':
        
                if params['keypointDetector'] == 'dense':
                
                    print 'SIFT ('+str(params['nf_sift'])+' elems.)'

                    # dense sift implementation found in mcv_dsift.py
                    if positions == None: # clear previous positions
                        #feaArr,positions = extractor.process_image(Image)
                        extractor = dsift.DsiftExtractor(params['gridSpacing'],params['patchSize'])
                        descriptors,positions = extractor.process_image(img)
                    else:
                        extractor = dsift.DsiftExtractor(params['gridSpacing'],params['patchSize'])
                        descriptors,positions = extractor.process_image(img, positions)
                            
                

            
                else:
                
                    #pass
                    # SIFT descriptor on any other keypoint detector
                    print 'SIFT ('+str(params['nf_sift'])+' elems.)'

                    #sift.process_image(filename,"./tmp.sift",params)
                    #sift_keypoints, sift_descriptors = sift.read_features_from_file("./tmp.sift")
                    #extractor = dsift.DsiftExtractor(params['gridSpacing'],params['patchSize'])
                    #sift_descriptors,Null = extractor.process_image(img, positions)
                    descriptor = cv2.DescriptorExtractor_create("SIFT")
                    keypoints, descriptors = descriptor.compute(img, keypoints)
            
            
                # update descriptors
                #hstack((DESCRIPTORS,descriptors))    
                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]
            
    
    
            # ####################################
            #     Speeded Up Robust Features (SURF)    
            # ####################################    
            elif descriptor == 'surf':    
            
                # update descriptors
                DESCRIPTORS[:,descrPos:descrPos+shape(surf_descriptors)[1]] = surf_descriptors
                descrPos = descrPos+shape(surf_descriptors)[1]


        
            # ########################################
            #     Histogram of Oriented Gradient (HOG)    
            # ########################################
            elif descriptor == 'hog':
                    
                print 'HOG ('+str(params['nf_hog'])+' elems.)'        
                    
                if size(positions) > 0:
                    nd = shape(positions)[1]
                    descriptors = zeros((nd,params['nf_hog']))
                
                    for k in range(0,nd):
                        r = positions[0,k]; c = positions[1,k];
                        patch = img[r:r+params['patchSize'],c:c+params['patchSize']]
                        if size(patch) == params['patchSize']*params['patchSize']: # discard incomplete patches
                                      
                                      # hog is found in mcv_descriptors.py
                                      descriptors[k,:] = hog(patch).transpose()
                            
                    hstack((DESCRIPTORS,descriptors))    
                        

                
                else:
                
                    descriptors = None
                    positions = None
            
                # update descriptors
                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]

            elif descriptor == 'gist':

                print 'GIST'
                gist_vector = leargist.color_gist(Image.fromarray(img))
                print shape(gist_vector)
                #descriptors = reshape(gist_vector,(1,params['nf_gist']))
                
                DESCRIPTORS = reshape(gist_vector,(1,params['nf_gist']))
                #DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                #descrPos = descrPos+shape(descriptors)[1]                #DESCRIPTORS = )
                POSITIONS = []
            
        
            # ####################################            
            #     Week 1 - Task 3: Shape Context (SC)
            # ####################################
            elif descriptor == 'sc':
                
                pass
            
            
            
        
            # ########################################################            
            #     Week 1 - Task 3: Binary Robust Independent Elementary Features (BRIEF)
            # ########################################################
            elif descriptor == 'brief':
                
                print 'BRIEF'
                
                # Initiate STAR detector
                star = cv2.FeatureDetector_create("STAR")

                # Initiate BRIEF extractor
                brief = cv2.DescriptorExtractor_create("BRIEF")

                # find the keypoints with STAR
                kp = star.detect(img,None)

                # compute the descriptors with BRIEF
                kp, descriptors = brief.compute(img, kp)

                #ndes = size(descriptors,0)
                #mdes = size(descriptors,1)
                print shape(kp)                
                
                #DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                DESCRIPTORS = descriptors
                #descrPos = descrPos+shape(descriptors)[1]
                
        
        
            # ####################################            
            #     Week 2 - Task 3: CENTRIST        
            # ####################################
            elif descriptor == 'centrist':
                
                print 'CENTRIST'       
                    
                if size(positions) > 0:
                    nd = shape(positions)[1]
                    descriptors = zeros((nd,params['nf_centrist']))
                
                    for k in range(0,nd):
                        r = positions[0,k]; c = positions[1,k];
                        patch = img[r:r+params['patchSize'],c:c+params['patchSize']]
                        if size(patch) == params['patchSize']*params['patchSize']: # discard incomplete patches
                                      
                                    hist, descriptor_cent = centrist.centrist(patch)

                                    descriptors[k,:] = hist.transpose()
                                    #descriptors[k,:] = reshape(descriptor_cent,(1,-1))
                                    
                    hstack((DESCRIPTORS,descriptors))
                else:
                
                    descriptors = None
                    positions = None
            
                # update descriptors
                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]
                
        
        
    print '*****************************'    
    return DESCRIPTORS,POSITIONS



#
#     normalize feature matrix
#
def normalizeFeat(x,mean_x=None,std_x=None):
    
    if mean_x == None and std_x is None:
        mean_x = x.mean(axis=0)
        std_x = x.std(axis=0)
        std_x[std_x==0] = 1
                
    return (x-tile(mean_x,(shape(x)[0],1)))/tile(std_x,(shape(x)[0],1)),mean_x,std_x
    
    
#    
#     accuracy of a confusion matrix    
#
def accuracy(confMat):
    
    return sum(diag(confMat))/sum(confMat.flatten())
    
    
#    
#     compute the row with minimum distance between vector 'v' and matrix 'm'
#
def yMinDist(v,m,metric):

    Dist = zeros((1,shape(m)[0]))
    
    for row in range(0,shape(m)[0]):
        
        if metric == 'euclidean':

            Dist[0,row] = sqrt(sum((m[row,:]-v)**2))
            
    return argmin(Dist[0,:])
    
        
#
#    Hamming distance
#
""" Calculate the Hamming distance between two given strings """
def hamming(a, b):
    
    return sum(logical_xor(a,b).astype(int))
    
    

def MeanAveragePrecision(filename,ranking,shotlist,it,queryname):
    #hitmiss = checkHitsMiss(filename, trainDir, LIBRARY, idxs, k)
    relist=relnotrel(filename,ranking,shotlist,it,queryname)
    map = 0
    accu = 0
    for k in range(0,len(relist)):
        if relist[0,k]==1:
            accu += 1
            map += double(accu)/double(k+1)
    return (map/accu) if accu >0 else 0
        
    
def checkHitsMiss(filename, trainDir, LIBRARY, idxs, k):
    hit_miss = zeros((k,1))
    for i in range(0,k):
        isreallycar = False
        if len(filename.split('car'))>1: isreallycar = True
        iscar = False
        if os.path.isfile(trainDir+'car/'+LIBRARY[idxs[i]].split('/')[-1][0:-4]+'.JPEG'):
            iscar = True
        if isreallycar == iscar:
            hit_miss[i]=1
    return hit_miss
   
#
#    show first 10 results of image-based query
#    
def showQueryResult(filename,trainDir,LIBRARY,idxs,method,relevance_feedback=False):
            
    #
    #    QUERY RESULTS VISUALIZATION (first 10 results)
    #                    
    ion()
    figure('IMAGE RETRIEVAL'); subplot(353); title('QUERY')
    img = array(Image.open(filename))
    imshow(img,cmap=gray)
    axis('off')

    for i in range(0,10):
        subplot(3,5,6+i)
        try:
            img = array(Image.open(trainDir+'car/'+LIBRARY[idxs[i]].split('/')[-1][0:-4]+'.JPEG'))
        except:
            img = array(Image.open(trainDir+'dog/'+LIBRARY[idxs[i]].split('/')[-1][0:-4]+'.JPEG'))
        imshow(img)
        axis('off')
    
    if relevance_feedback:
                
        mouse = MouseMonitor()
        mouse.weights = []
        mouse.cont = 0
        connect('button_press_event', mouse.mycall)
        
        show(block=True)

        weights = mouse.weights
        
        del(mouse)    

        return weights

    else:
        show()


class MouseMonitor():

    event = None
    xdatalist = []
    ydatalist = []
    weights = []
    cont = 0

    def mycall(self, event):

        self.event = event
        self.xdatalist.append(event.x)
        self.ydatalist.append(event.y)
        self.cont+=1

        if event.button == 1:
            self.weights.append(1)
            print 'result '+str(self.cont)+'. RELEVANT'
        elif event.button == 3:
            self.weights.append(-1)
            print 'result '+str(self.cont)+'. NOT RELEVANT'


    
    
    
    
    
    
    
    
    

        
        
    
    
