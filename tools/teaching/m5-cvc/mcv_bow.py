""" 
    
    Bag of Word (BoW) library
    
    Master in Computer Vision - Barcelona
    
    Week 1 - Task 2: Implement the function trainSVM found at mcv_svm.py   

    Week 1 - Task 3: This code is already prepared to deal with HOG, SIFT, SURF, SC and BRIEF (Shape Context and BRIEF not used in mcv_tools.py)    

    Week 1 - Task 4: Compress the descriptor, using PCA: add into mcv_bow.py and implement in mcv_pca.py
    
    Week 2 - Task 2: Use the early fusion strategies as implemented in this code.

    Week 3 - Task 1: implement Spatial Pyramids (pyrspaces)
    
    Week 3 - Task 2: Use the late fusion strategies as implemented in this code.
           
    Author: Francesco Ciompi, Ramon Baldrich, Jordi Gonzalez
         
"""

from mcv_svm_roc import *        # mcv module for SVM ROC curve
from mcv_svm import *            # mcv module for SVM
from mcv_tools import *            # mcv personal library of tools
import time

#from numpy import *                # numpy, for maths computations
import numpy as np
import random as rnd            # random library
#from pylab import *                # matplotlib, for graphical plots
import pickle                    # save/load data files
from scipy.cluster.vq import *    # for k-means
from sys import exit    
import skimage
import multiprocessing as mp
from sklearn import grid_search, svm
from sklearn.decomposition import PCA
from sklearn.metrics import recall_score
from sklearn.preprocessing import Imputer
import numpy as np


#
#     compute image descriptors
#
def getDescriptor(trainFiles,outputDir,bowParams,showFig):
    
    # the features subsampling is done before storing, to save space!
    runmultiprocess = True
    print 'computing descriptors'
    if runmultiprocess: pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
    for filesList in trainFiles:
        for fileName in filesList:
            if runmultiprocess:
                pool.apply_async(getDescriptorImage, args=(fileName, outputDir, bowParams, False))
            else:
                getDescriptorImage(fileName, outputDir, bowParams, showFig)
    if runmultiprocess:
        pool.close()
        pool.join()
    print 'done'

#
#     compute one single  image descriptors
#
def getDescriptorImage(fileName,outputDir,bowParams,showFig):
    if bowParams['fusion'] == 'early':
        #img = array(Image.open(fileName).convert('L'))
        img = array(Image.open(fileName).convert('L'))
        imgRGB = array(Image.open(fileName))

        # getFeatImg can be found at: mcv_tools.py
        feats,pos = getFeatImg(img,imgRGB,bowParams,showFig,fileName);
        print "shape feats", shape(feats)
        if feats is not None:
            
            # randSubsample can be found at: mcv_tools.py
            feats = randSubsample(feats, bowParams['nPt'])

            descr = '';
            for d in bowParams['descriptor']: descr = ''+descr+d+'_'
            descr = descr[0:-1]
            f = open(''+outputDir+''+fileName.split('/')[-1][0:-4]+'_'+bowParams['keypointDetector']+'_'+descr+'.pkl', 'wb');
            pickle.dump(feats,f);
            f.close()
        print fileName


    elif bowParams['fusion'] == 'late' or bowParams['fusion'] == 'off':
        for descriptor in bowParams['descriptor']:
            img = array(Image.open(fileName).convert('L'))
            imgRGB = array(Image.open(fileName))
            params = bowParams.copy(); params['descriptor'] = [''+descriptor+'']
            feats,pos = getFeatImg(img,imgRGB,params,showFig,fileName);
            if feats is not None:
                feats = randSubsample(feats, bowParams['nPt'])
                f = open(''+outputDir+''+fileName.split('/')[-1][0:-4]+'_'+bowParams['keypointDetector']+'_'+descriptor+'.pkl', 'wb'); 
                pickle.dump(feats,f);
                f.close()
            print fileName


#
#     compute vocabulary from image descriptors
#
def getVocabulary(inDir,ext,classifierDir, bowParams,showFig=False):
    if bowParams['fusion'] == 'late':
        nv = len(bowParams['descriptor']) # number of vocabularies
    else:
        nv = 1

    for voc in range(nv):

        if bowParams['fusion'] == 'early':

            descr = '';
            for d in bowParams['descriptor']: descr = ''+descr+d+'_'
            descr = descr[0:-1]

            nf = 0
            for d in bowParams['descriptor']:
                if d == 'SIFT': nf = nf + bowParams['nf_sift']
                if d == 'hog': nf = nf + bowParams['nf_hog']
                if d == 'SURF': nf = nf + bowParams['nf_surf']
                if d == 'sc': nf = nf + bowParams['nf_sc']
                if d == 'BRIEF': nf = nf + bowParams['nf_brief']
                if d == 'oppsift': nf = nf + bowParams['nf_oppsift']
                if d == 'lbp': nf = nf + bowParams['nf_lbp']
                if d == 'centrist': nf = nf + bowParams['nf_centrist']
                if d == 'gist': nf = nf + bowParams['nf_gist']
                if d == 'color_naming': nf = nf + bowParams['nf_colornaming']

        else:
            
            descr = bowParams['descriptor'][voc]
            if descr == 'SIFT': nf = bowParams['nf_sift']
            if descr == 'hog': nf = bowParams['nf_hog']
            if descr == 'SURF': nf = bowParams['nf_surf']
            if descr == 'sc': nf = bowParams['nf_sc']
            if descr == 'BRIEF': nf = bowParams['nf_brief']
            if descr == 'oppsift': nf = bowParams['nf_oppsift']
            if descr == 'lbp': nf =  bowParams['nf_lbp']
            if descr == 'centrist': nf = bowParams['nf_centrist']
            if descr == 'gist': nf = bowParams['nf_gist']
            if descr == 'color_naming': nf = bowParams['nf_colornaming']

        print 'checking the number of descriptors...'
        nd = getNumDescriptor(inDir,ext,''+bowParams['keypointDetector']+'_'+descr+'') # number of collected descriptors
        print 'found '+str(nd)+' descriptors'

        # initialize matrix
        print 'initialize descriptors matrix...'            
        X = zeros((nd,nf))
            
        files,Null = getFilelist(inDir,ext,''+bowParams['keypointDetector']+'_'+descr+'')
        print ''+bowParams['keypointDetector']+'_'+descr+''
        ridx = 0
        print 'collecting descriptors'
        for file in files:
            print 'loading '+file
            with open(file, 'rb') as f:
                    feats = pickle.load(f)
                    s = shape(feats)
                    print "s: ", s
                    print "ridx: ", ridx
                    X[ridx:ridx+s[0],:] = feats
                    ridx = ridx + s[0]
                    
                    if showFig:
                        figure(2)
                        imshow(feats)    
                        draw()                    
            
        # normalize D matrix: normalizeFeat is found at mcv_tools.py
        X,meanX,stdX= normalizeFeat(X)
        
                        
        # k-means
        print 'clustering'
        print shape(X)
       
        ts = time.time()
        centroids,variance = kmeans(X,bowParams['K'],15,10-3)
        te = time.time()
        print 'Kmeans: %2.2f sec' % (te-ts)
        
        code,distance = vq(X,centroids)    # Assigns a code from a code book to each observation. 
        
        f = open(classifierDir+bowParams['keypointDetector']+'_'+descr+'_centroids.pkl', 'wb');
        pickle.dump(centroids,f);
        pickle.dump(meanX,f);
        pickle.dump(stdX,f);
        #pickle.dump(pca,f);
        f.close() # store centroids
        print 'codebook STORED!'

#
#     assign words of vocabulary to each image
#
def getAssignment(inDir,trainLabels,classifierDir,trainDataDir,bowParams,showFig):    

    runmultiprocess= False
            
    if bowParams['fusion'] == 'late':
        nv = len(bowParams['descriptor']) # number of vocabularies
    else:
        nv = 1

    for voc in range(nv):

        if bowParams['fusion'] == 'early':

            descr = '';
            for d in bowParams['descriptor']: descr = ''+descr+d+'_'
            descr = descr[0:-1]

            nf = 0
            
            for d in bowParams['descriptor']:
                if d == 'SIFT': nf = nf + bowParams['nf_sift']
                if d == 'hog': nf = nf + bowParams['nf_hog']
                if d == 'SURF': nf = nf + bowParams['nf_surf']
                if d == 'sc': nf = nf + bowParams['nf_sc']
                if d == 'BRIEF': nf = nf + bowParams['nf_brief']
                if d == 'oppsift': nf = nf + bowParams['nf_oppsift']
                if d == 'lbp': nf = nf + bowParams['nf_lbp']
                if d == 'centrist': nf = nf + bowParams['nf_centrist']
                if d == 'gist': nf = nf + bowParams['nf_gist']
                if d == 'color_naming': nf = nf + bowParams['nf_colornaming']
        

        else:
            
            descr = bowParams['descriptor'][voc]
            if descr == 'SIFT': nf = bowParams['nf_sift']
            if descr == 'hog': nf = bowParams['nf_hog']
            if descr == 'SURF': nf = bowParams['nf_surf']
            if descr == 'sc': nf = bowParams['nf_sc']
            if descr == 'BRIEF': nf = bowParams['nf_brief']
            if descr == 'oppsift': nf = bowParams['nf_oppsift']
            if descr == 'lbp': nf =  bowParams['nf_lbp']
            if descr == 'centrist': nf =  bowParams['nf_centrist']
            if descr == 'gist': nf = bowParams['nf_gist']
            if descr == 'color_naming': nf = bowParams['nf_colornaming']
            params = bowParams.copy(); params['descriptor'] = [''+descr+''] # update descriptor for late fusion
    
        if bowParams['spyramid']:
            
            pyrconf_=[bowParams['pyrconf'][0],bowParams['pyrconf'][1]]
            nh=0
            while pyrconf_[0] >= 1 or pyrconf_[1] >= 1:
                if pyrconf_[0] == 0:
                    nh += pyrconf_[1]
                elif pyrconf_[1] == 0:
                    nh += pyrconf_[0]
                else:
                    nh += pyrconf_[0]*pyrconf_[1]

                pyrconf_ = [pyrconf_[0]/2,pyrconf_[1]/2]

            print nh

        # initialize signatures (if knn)
        if bowParams['classifier'] == 'knn':
            if bowParams['spyramid']:
                signatures = zeros((max(trainLabels),nh*bowParams['K']))
            else:
                signatures = zeros((max(trainLabels),bowParams['K']))
            nPoints_per_class = zeros((max(trainLabels),1))
            for i in range(0,int(max(trainLabels))):
                nPoints_per_class[i] = sum(trainLabels == i+1)    
            nPoints_per_class[nPoints_per_class == 0] = 1
                
        # prepare training data (if svm) 
        if bowParams['classifier'] =='svm':
            if bowParams['spyramid']:
                trainData = zeros((len(trainLabels),nh*bowParams['K']))
            else:
                trainData = zeros((len(trainLabels),bowParams['K']))        
                
        f = open(classifierDir+bowParams['keypointDetector']+'_'+descr+'_centroids.pkl', 'rb')
        global meanX
        global stdX
        global centroids 
        centroids = pickle.load(f)
        meanX = pickle.load(f)
        stdX = pickle.load(f)
        f.close()

        count = 0
  
        print 'assigning words'
        if runmultiprocess: 
            pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
            PoolResults = []
        idx_img = 0        
        for filesList in inDir:
      
            for fileName in filesList:
                if bowParams['classifier'] == 'knn':
                    idx = trainLabels[idx_img]
                if bowParams['classifier'] == 'svm':
                    idx = idx_img                    

                if runmultiprocess:
                    if bowParams['fusion'] == 'late':
                        PoolResults.append(pool.apply_async(getAssignmentImage, args=(fileName, idx, params, False)))
                    elif bowParams['fusion'] == 'early':
                        PoolResults.append(pool.apply_async( getAssignmentImage,args=(fileName, idx, bowParams, False)))
                else:
                    if bowParams['fusion'] == 'late':
                        signature,idx = getAssignmentImage(fileName, idx, params, showFig)
                    elif bowParams['fusion'] == 'early':
                        signature,idx = getAssignmentImage(fileName, idx, bowParams, showFig)

                    if bowParams['classifier'] == 'knn':
                        signatures[idx-1,:] = signatures[idx-1,:] + signature/nPoints_per_class[idx-1]
                    if bowParams['classifier'] == 'svm':
                        print shape(trainData)
                        print shape(signature)
                        trainData[idx,:] = signature                    
                idx_img += 1
                
        if runmultiprocess:
            while (len(PoolResults)>0):
                try:
                    signature,idx = PoolResults[0].get(timeout=0.001)
                    PoolResults.pop(0)
                    if bowParams['classifier'] == 'knn':
                        signatures[idx-1,:] = signatures[idx-1,:] + signature/nPoints_per_class[idx-1]
                    if bowParams['classifier'] == 'svm':
                        trainData[idx,:] = signature                    
                except:
                    pass
                
            pool.close()
            pool.join()
            
    
        # store training data
    
        # knn
        if bowParams['classifier'] == 'knn':
            f = open(trainDataDir+descr+'_'+bowParams['keypointDetector']+'_bow_signatures_K_'+str(bowParams['K'])+'.pkl', 'wb')
            pickle.dump(signatures,f);
            f.close()        
    
        # svm
        if bowParams['classifier'] =='svm':
            f = open(trainDataDir+descr+'_'+bowParams['keypointDetector']+'_svm_traindata_K_'+str(bowParams['K'])+'.pkl', 'wb')
            pickle.dump(trainData,f);
            pickle.dump(trainLabels,f);
            f.close()        
        print 'assignment done'

#
#     assign words of vocabulary to a single image
#
def getAssignmentImage(fileName,Id, params,showFig):    
    print fileName

    img = array(Image.open(fileName).convert('L'))
    imgRGB = array(Image.open(fileName))
    
    # getFeatImg is found at mcv_tools.py
    feats,pos = getFeatImg(img,imgRGB,params)
    #feats = randSubsample(feats, params['nPt'])
         # normalizeFeat is found at mcv_tools.py
    feats = normalizeFeat(feats,meanX,stdX)[0]                
    code,distance = vq(feats,centroids) # nearest-neighbour


  
       
    if params['spyramid']:
         S = shape(img)
         hc = codeHist(code,params['K'],params['spyramid'],params['pyrconf'],S,pos,imgRGB,showFig)       
    else:
         hc = codeHist(code,params['K'])

    # cumulate normalized descriptors in 'signatures'
    
    
    signature = hc

        
    if showFig:
        ion()
        figure('Words assignment')
        subplot(2,1,1)
        imshow(hstack((imgRGB,imgRGB)),hold=False); title('words assignment')
        scatter(pos[1,:],pos[0,:],10,code)
        subplot(2,1,2)
        bar(range(0,shape(hc)[1]),hc.flatten(),width=1,hold=False); title('image signature')
        draw()

    return signature,Id

#
#     check number of descriptors in a directory
#
def getNumDescriptor(inDir,ext,queue):

    nd = 0 # number of descriptors
    files,Null = getFilelist(inDir,ext,queue);
    for file in files:
        # print file
        f = open(file, 'rb')
        feats = pickle.load(f)
        f.close()
        s = shape(feats)
        nd = nd + s[0]

    return nd

def pyrspaces(S,pyrconf,pyrs, weight=1.0):

    dr = round(S[0]/pyrconf[0])
    dc = round(S[1]/pyrconf[1])
    # each sublist is [r0,r1,c0,c1]

    for r in range(pyrconf[0]):
        r0 = r*dr; r1 = r0+dr
        for c in range(pyrconf[1]):
            c0 = c*dc; c1 = c0+dc; pyrs.append([r0,r1,c0,c1,weight])      
    pyrconf_=[pyrconf[0]/2,pyrconf[1]/2]
 
    if (pyrconf_[0]>=1 or pyrconf_[1]>=1):   
        #Trick to solve the 3x1 configuration
        if pyrconf_[0]==0:
             pyrconf_[0]=1
        elif pyrconf_[1]==0:
             pyrconf_[1]=1
        pyrs=pyrspaces(S,pyrconf_,pyrs,weight/2)

    return pyrs

#     compute occurrency (histogram) of the code in BoW; it includes Spatial Pyramid option
#
def codeHist(code,K,pyr=False,pyrconf=[],S=[1,1],pos=[],img=None,showFig=False):

    #
    #    spatial pyramid
    #
    if pyr:

        #Get coordinates of regions at this level
        pyrs = []
        pyrs = pyrspaces(S,pyrconf,pyrs)    # coords of regions in the image
        
        nh=shape(pyrs)[0]
        #Define the histogram
        pyrHist = ones((1,nh*K))
        
        #Express the positions in x,y coordinates
        pos_ = zeros((2,len(pos)))
        for k in range(len(pos)):
            pos_[0,k] = pos[k].pt[0]; pos_[1,k] = pos[k].pt[1]
        
        cont = 0
        for regs in pyrs:
            pyrcode = []
            for p in range(shape(pos_)[1]):
                #print all([pos[0,p] >= regs[0] , pos[0,p] < regs[1] , pos[1,p] >= regs[2] , pos[1,p] < regs[3]])
                if all([pos_[0,p] >= regs[0] , pos_[0,p] < regs[1] , pos_[1,p] >= regs[2] , pos_[1,p] < regs[3]]):
                    pyrcode.append(code[p])
                
            h = codeHist(array(pyrcode),K) # recursive use
          
            pyrHist[0,cont:cont+K] = multiply(h,regs[4])
            cont+=K
        #Recursivity should be added here
        
        pyrHist = pyrHist/(sum(pyrHist)+0.0001) # normalize
        #print "Histogram", pyrHist
        return pyrHist

    #
    # normal histogram
    #
    else:

        hist = zeros((1,K))
        for c in code:
            hist[0,c-1] = hist[0,c-1] + 1

        histNorm = hist/(sum(hist)+0.0001)    # normalize
        return histNorm


# plot roc curve in bag of word for svm classifier
def bow_roc(svm_model,x1,x2,name1,name2,nF=20):
    
    nF = 20 # number of folds
    y1 = ones((shape(x1)[0],1))
    y2 = -1*ones((shape(x2)[0],1))
    train_y = list(y1)+list(y2); 
    train_x = map(list,x1)+map(list,x2);
    
    # plot_svm_roc found at mcv_svm_roc.py
    xy_roc,auc = plot_svm_roc(train_x,train_y,svm_model,nFolds=nF)
                    
    figure(1)
    plot(xy_roc[:,0],xy_roc[:,1])
    xlabel('FP'); ylabel('TP')
    title(''+name1+' vs '+name2+'')
    show()
    
    # area under curve
    return auc


#
#     TRAIN SVM in BAG OF WORDS
#
def train_svm_bow(classifierDir, trainDataDir, bowParams):
    params = bowParams.copy()
    if bowParams['fusion'] == 'late':
        nv = len(bowParams['descriptor']) # number of vocabularies
    else:
        nv = 1
        
    for voc in range(nv):

        if bowParams['fusion'] == 'early':

            descr = '';
            for d in bowParams['descriptor']: descr = ''+descr+d+'_'
            descr = descr[0:-1]

            nf = 0
            for d in bowParams['descriptor']:
                if d == 'SIFT': nf = nf + bowParams['nf_sift']
                if d == 'hog': nf = nf + bowParams['nf_hog']
                if d == 'SURF': nf = nf + bowParams['nf_surf']
                if d == 'sc': nf = nf + bowParams['nf_sc']
                if d == 'oppsift': nf = nf + bowParams['nf_oppsift']
                if d == 'BRIEF': nf = nf + bowParams['nf_brief']
                if d == 'lbp': nf = nf + bowParams['nf_lbp']
                if d == 'gist': nf = nf + bowParams['nf_gist']
                if d == 'centrist': nf = nf + bowParams['nf_centrist']
                if d == 'color_naming': nf = nf + bowParams['nf_colornaming']
        
        else:
        
            descr = bowParams['descriptor'][voc]
            if descr == 'SIFT': nf = bowParams['nf_sift']
            if descr == 'hog': nf = bowParams['nf_hog']
            if descr == 'SURF': nf = bowParams['nf_surf']
            if descr == 'sc': nf = bowParams['nf_sc']
            if descr == 'BRIEF': nf = bowParams['nf_brief']
            if descr == 'lbp': nf = bowParams['nf_lbp']
            if descr == 'oppsift': nf = bowParams['nf_oppsift']
            if descr == 'centrist': nf = bowParams['nf_centrist']
            if descr == 'gist': nf = bowParams['nf_gist']
            if descr == 'color_naming': nf = bowParams['nf_colornaming']
            params = bowParams.copy(); params['descriptor'] = [''+descr+''] # update descriptor for late fusion
        
        
        f = open(trainDataDir+descr+'_'+params['keypointDetector']+'_svm_traindata_K_'+str(params['K'])+'.pkl', 'rb')
        trainData = pickle.load(f);
        trainLabels = pickle.load(f)
        f.close()
                                
        #
        #     one-vs-all ECOC coding            
        #        
        if bowParams['ecoc-coding'] == 'OneVsAll':
            
            for cidx in range(0,bowParams['nc']):
                    
                
                idx1 = where(trainLabels==cidx+1)
                idx2 = where(trainLabels!=cidx+1)
 
                x1 = trainData[idx1]
                x2 = trainData[idx2]
                x2 = randSubsample(x2,len(idx1[0]))
                                
                print 'class 1: '+str(shape(x1)[0])+' samples'
                print 'class 2: '+str(shape(x2)[0])+' samples'
                        
                print 'TRAIN SVM: Class '+str(cidx+1)+' vs All'
                print ''
                        
                # Week 1 - Task 2: Implement the function trainSVM found at mcv_svm.py        
                #svm_model = trainSVM(x1,x2)

                svm_model,best_acc,best_c,best_g=trainSVM(x1,x2)
                svm_save_model(classifierDir+'svm_bow_'+params['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+params['classes'][cidx]+'_vs_all.model', clf.best_estimator_)
            
                ## roc curve (debug)
                if bowParams['ROC']:
                    AUC = bow_roc(svm_model,x1,x2,params['classes'][cidx],'all')
                    

        
        #
        #     one-vs-one ECOC coding
        #        
        elif bowParams['ecoc-coding'] == 'OneVsOne':
       
            ''' 
            idx1 = where(trainLabels==1)
            idx2 = where(trainLabels==2)
            idx3 = where(trainLabels==3)
            idx4 = where(trainLabels==4)
            idx5 = where(trainLabels==5)
       
                  
            x1 = trainData[idx1]
            x2 = trainData[idx2]
            x3 = trainData[idx3]
            x4 = trainData[idx4]
            x5 = trainData[idx5]
        
            
            x1 = map(list,x1)
            x2 = map(list,x2)
            x3 = map(list,x3)
            x4 = map(list,x4)
            x5 = map(list,x5)
   
            X = x1+x2+x3+x4+x5
            y1 = ones((shape(x1)[0],1))
            y2 = 2*ones((shape(x2)[0],1))
            y3 = 3*ones((shape(x3)[0],1))
            y4 = 4*ones((shape(x4)[0],1))
            y5 = 5*ones((shape(x5)[0],1))
            
            Y = list(y1)+list(y2)+list(y3)+list(y4)+list(y5)
            prob = svm_problem(Y,X)
            best_c=0
            best_g=0
            best_acc=0
    
            gamma_array= [0.001,0.005,0.01,0.05,0.08,0.1,0.2,0.5,0.8,1,2,5,10,30]
            c_array= [1,1.5,2,3,4,5,6,7,8,9,10,15,17,22,27,52,102,202]
    
    #RBF SVM
            for g in gamma_array:
                for c in c_array:
                    param=svm_parameter('-t 2 ' + '-g ' + str(g)+' -c ' +str(c) + ' -v 5 -q')
                    outp=svm_train(prob,param)
                    if outp>best_acc:
                       best_g=g
                       best_c=c
                       best_acc=outp
                       cl_type=2
    #Linear SVM  
            for c in c_array:
                param=svm_parameter('-t 0 ' + '-c ' +str(c) + ' -v 5 -q')
                outp=svm_train(prob,param)
                if outp>best_acc:
                   best_g=g
                   best_c=c
                   best_acc=outp
                   cl_type=0
            '''      
            for cidx1 in range(0,bowParams['nc']-1):
                for cidx2 in range(cidx1+1,bowParams['nc']):
                    
                    print 'training SVM '+str(cidx1)+' - '+str(cidx2)+''

                    idx1 = where(trainLabels==cidx1+1)
                    idx2 = where(trainLabels==cidx2+1)
                    
                  
                    x1 = trainData[idx1]
                    x2 = trainData[idx2]
                    

                    #x1 = randSubsample(x1,114)
                    #x2 = randSubsample(x2,114)


                    print ''+params['classes'][cidx1]+': '+str(shape(x1)[0])+' samples'
                    print ''+params['classes'][cidx2]+': '+str(shape(x2)[0])+' samples'
                        
                    print 'TRAIN SVM: Class '+params['classes'][cidx1]+' vs '+params['classes'][cidx2]+''
                    print ''

                    #svm_model,best_acc,best_c,best_g=trainSVM(x1,x2,best_c,best_acc,cl_type)
                    svm_model=trainSVM(x1,x2)
                    '''
                    svm_model=trainSVM(x1,x2)
                    f = open(classifierDir+'svm_bow_'+params['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+params['classes'][cidx1]+'_vs_'+params['classes'][cidx2]+'.model', 'wb')
                    pickle.dump(svm_model,f);
                    f.close()        
                    '''
                    svm_save_model(classifierDir+'svm_bow_'+params['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+params['classes'][cidx1]+'_vs_'+params['classes'][cidx2]+'.model', svm_model)
            
                    ## roc curve (debug)
                    #if bowParams['ROC']:
                     #   AUC = bow_roc(svm_model,x1,x2,params['classes'][cidx1],params['classes'][cidx2])

                        
                        

    

