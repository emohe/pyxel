"""

    Bag of Words: TESTING script
    
    Master in Computer Vision - Barcelona
    
    Author: Francesco Ciompi, Ramon Baldrich, Jordi Gonzalez

"""
import time
import os
from sys import exit,path   
path.append('.') 
from mcv_bow import *
#from pylab import *
import mcv_ecoc as ecoc            # mcv library for error-correcting output codes
from sklearn.svm import SVC


# ******************  MAIN **************************
if __name__ == "__main__":
    
    DatasetFolder    = '/share/apps/master_data/module5/datasets/Full/pascal/'
    DataOutputFolder ='../output/'

    trainDir = DatasetFolder + 'train'
    testDir  = DatasetFolder + 'test'
    valDir   = DatasetFolder + 'validation'
    
    nc = 5 # number of classes

    descriptorDir = DataOutputFolder + 'descriptors/'
    classifierDir = DataOutputFolder + 'classifiers/'
    trainDataDir  = DataOutputFolder + 'trainData/'
    testDataDir   = DataOutputFolder + 'rocData/'

    if not os.path.isdir(testDataDir):  os.mkdir(testDataDir)
    
    #
    # test files
    #    
    testFiles_car,filenames_car = getFilelist(''+testDir+'/car/','jpg'); # class 1
    testFiles_dog,filenames_dog = getFilelist(''+testDir+'/dog/','jpg'); # class 2 
    testFiles_bicycle,filenames_bike = getFilelist(''+testDir+'/bicycle/','jpg'); # class 3
    testFiles_motorbike,filenames_moto = getFilelist(''+testDir+'/motorbike/','jpg'); # class 4
    testFiles_person,filenames_pedestrian = getFilelist(''+testDir+'/person/','jpg'); # class 5
            
    testFiles = [];
    testFiles.append(testFiles_car); n1 = len(testFiles_car)
    testFiles.append(testFiles_dog); n2 = len(testFiles_dog)
    testFiles.append(testFiles_bicycle); n3 = len(testFiles_bicycle)
    testFiles.append(testFiles_motorbike); n4 = len(testFiles_motorbike)
    testFiles.append(testFiles_person); n5 = len(testFiles_person)

    #
    # OPTIONS
    #
    # 1. show figures
    showFig = True
    #
    # 2. compute Receive Operator Curve (ROC) for both train and test
    ROC = True


    # ######################################################################################
    #         BAG OF WORDS - PARAMETERS DEFINITION        
    # ######################################################################################
    #
    #     define a dictionary structure to store bag of words parameters 
    #
    # > descriptor: sift, hog, surf; combination of descriptors is allowed, e.g. ['sift','hog'], for fusion
    # > keypointDetector: dense, sift, harris
    # > nf_descriptor_name: number of features
    # > K: number of clusters / size of BOW vocabulary (best value ~ 200)
    # > nPt: numer of keypoints to selecte from each training image
    # > classifier: knn, svm    
    # > fusion: off, early, late
    # > classes: list of class names
    # > ecoc-coding: OneVsAll
    bowParams = {    'patchSize':48, 'gridSpacing':48, 'keypointDetector':'MSER',\
                       'harris_sigma':3, 'harris_min_dist':1, 'harris_threshold':0.01, 'harris_wid':5,\
                       'descriptor':['BRIEF'],\
                       # try HOG, SC (implement!), BRIEF
                       'K': 400, 'nPt': 2000, \
                       'nf_brief':32,\
                       'nf_gist':960,\
                       'nf_centrist':1024,\
                       'nf_sc':60,\
                       'nf_sift':128, 'sift_edge-thresh':10, 'sift_peak-thresh':5,\
                       'nf_surf':64, 'surf_extended':1,'surf_hessianThreshold':5.0, 'surf_nOctaveLayers':1, 'surf_nOctaves':1,\
                       'nf_hog':81,\
                       'nf_lbp':128,\
                       'nf_oppsift':384,\
                       'classifier':'svm',\
                       'ecoc-coding':'OneVsOne',\
                       'fusion': 'early',\
                       'nc': nc,\
                       'ROC': False,\
                       'PCA':False,\
                       'classes':['car','dog','bicycle','motorbike','person'],\
                       'spyramid':True,\
                       'pyrconf':[1,3]}
                       



    if len(bowParams['descriptor'])>1 and bowParams['fusion'] == 'off':
        print 'ERROR: DESCRIPTORS incompatible with FUSION type!'
        exit(-1)
        
    if bowParams['descriptor'][0] == 'surf' and bowParams['keypointDetector']!= 'surf':
        print 'ERROR: SURF descriptor is only compatible with SURF keypoints!'
        exit(-1)


    # confusion matrix
    confMat = zeros((nc,nc))
    
    #weights_late=[[0.6],[0.6],[0.6],[0.6],[0.6]],[[0.4],[0.4],[0.4],[0.4],[0.4]]
    weights_late=[[0.0],[0.3],[0.0],[0.3],[0.3]],[[0.0],[0.2],[1.0],[0.2],[0.2]],[[0.0],[0.1],[0.0],[0.1],[0.1]],[[1.0],[0.4],[0.0],[0.4],[0.4]],[[0.0],[0.0],[0.0],[0.0],[0.0]]
    print bowParams

    ts = time.time()
    # compute number of rounds per test image    
    if bowParams['fusion'] == 'late':
        nv = len(bowParams['descriptor']) # number of vocabularies
    else:
        nv = 1
        
    # working copy of bowParams
    params = bowParams.copy();

    if bowParams['fusion'] == 'early' or bowParams['fusion'] == 'off':
        descr = '';
        for d in bowParams['descriptor']: descr = ''+descr+d+'_'
        descr = descr[0:-1]
        
    if bowParams['classifier'] == 'knn':
        f = open(trainDataDir+descr+'_'+params['keypointDetector']+'_bow_signatures_K_'+str(params['K'])+'.pkl', 'rb')
        signatures = pickle.load(f)
        f.close()

    if bowParams['spyramid']:
        if bowParams['pyrconf'][0]==3 and bowParams['pyrconf'][1]==1 or bowParams['pyrconf'][0]==1 and bowParams['pyrconf'][1]==3:
             nh=4
        elif bowParams['pyrconf'][0]==4 and bowParams['pyrconf'][1]==4:
             nh=21
        elif bowParams['pyrconf'][0]==2 and bowParams['pyrconf'][1]==2:
             nh=5

        
        '''
        pyrconf_=[bowParams['pyrconf'][0],bowParams['pyrconf'][1]]
        nh=0
        while pyrconf_[0] >= 1 or pyrconf_[1] >= 1:
            if pyrconf_[0] == 0:
                nh += pyrconf_[1]
            elif pyrconf_[1] == 0:
                nh += pyrconf_[0]
            else:
                nh += pyrconf_[0]*pyrconf_[1]

            pyrconf_ = [pyrconf_[0]/2,pyrconf_[1]/2]
        '''


       # nh = bowParams['pyrconf'][0]*bowParams['pyrconf'][1] # number of histograms to join

    # scan groups of files per class
    y_test = 0
    for files in testFiles:
            
        y_test+=1
            
        if ROC:    # initialize test descriptor matrix for roc curve in binary problem
            if bowParams['spyramid']:
                test_class_x = zeros((len(files),nh*params['K']))
            else:
                test_class_x = zeros((len(files),params['K']))
            print shape(test_class_x)
            test_class_y = ones((len(files),1))
            classCont = 0            
                    
        # scan all files of one class
        for testFile in files:
                
            # load test image file                    
            img = array(Image.open(testFile).convert('L'))
            imgRGB = array(Image.open(testFile))
                
            if bowParams['fusion'] == 'late':
                code_late = []

            for voc in range(nv):

                # update descriptor field for late fusion
                if bowParams['fusion'] == 'late' or bowParams['fusion'] == 'off':
                    descr = bowParams['descriptor'][voc]
                    params = bowParams.copy(); params['descriptor'] = [''+descr+'']    

                # load centroids for the corresponding classifier
                f = open(classifierDir+bowParams['keypointDetector']+'_'+descr+'_centroids.pkl', 'rb')
                centroids = pickle.load(f)
                meanX = pickle.load(f)
                stdX = pickle.load(f)
                #pca=pickle.load(f)
                f.close()                        
                # compute and normalize features
                feats,pos = getFeatImg(img,imgRGB,params)
                feats = normalizeFeat(feats,meanX,stdX)[0]
            
                # compute histogram of words in the image
                codes = vq(feats,centroids)[0]
                
                if params['spyramid']:
                    S = shape(img)
                    h = codeHist(codes,params['K'],bowParams['spyramid'],bowParams['pyrconf'],S,pos,imgRGB,showFig)
                else:
                    h = codeHist(codes,params['K'])                                

               # if ROC:    # store descriptor
                #    test_class_x[classCont] = h
                #    classCont+=1
                                
                # test knn
                if bowParams['classifier'] == 'knn' and bowParams['fusion'] != 'late':                
                    y_auto = yMinDist(h,signatures,'euclidean')+1
                
                # test svm
                if bowParams['classifier'] == 'svm':

                    # initialize ECOC matrix for this multi-class svm                    
                    # the modules for ecoc can be found in mcv_ecoc.py
                    M = ecoc.matrix(nc,params['ecoc-coding'])
                        
                    code = zeros((1,shape(M)[1]))
                    decis = zeros((1,shape(M)[1]))   #<--- versio poal
                    
                    # one-vs-all
                    if bowParams['ecoc-coding'] == 'OneVsAll':
                        for cidx in range(0,nc):
                            f = open(classifierDir+'svm_bow_'+bowParams['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+params['classes'][cidx]+'_vs_all.model', 'rb')
                            clf = pickle.load(f);
                            #svm_model = svm_load_model(classifierDir+'svm_bow_'+bowParams['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+params['classes'][cidx]+'_vs_all.model')
                            y_bin=clf.predict(map(list,h))
                            #y_bin, evals, deci = svm_predict(list([-1]),map(list,h),svm_model)
                            code[0,cidx] = y_bin[0]
                        
                    # one-vs-one    
                    elif bowParams['ecoc-coding'] == 'OneVsOne':
                        cidx=0

                        for cidx1 in range(0,bowParams['nc']-1):
                            for cidx2 in range(cidx1+1,bowParams['nc']):
                                class1 = params['classes'][cidx1]
                                class2 = params['classes'][cidx2]
                                svm_model = svm_load_model(classifierDir+'svm_bow_'+bowParams['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+class1+'_vs_'+class2+'.model')
                                y_bin, evals, deci = svm_predict(list([-1]),map(list,h),svm_model)
                                '''
                                f = open(classifierDir+'svm_bow_'+bowParams['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+class1+'_vs_'+class2+'.model', 'rb')
                                svm_model = pickle.load(f);
                                f.close()
                                y_bin=svm_model.predict(map(list,h))
                                deci=svm_model.predict_log_proba(map(list,h))
                                '''
                               
                                code[0,cidx] = y_bin[0]
                                decis[0,cidx] = deci[0][0]    #<--- versio poal
                                

                                cidx+=1
                        voting = zeros((shape(M)[0],1))
                        voting_c = zeros((2,shape(M)[0]*(shape(M)[0]-1)/2))
                        vcount=0
                        for vidx in range(0,shape(M)[0]-1):
                            for cidx in range(vidx+1,shape(M)[0]):
                                voting_c[0,vcount] = vidx
                                voting_c[1,vcount] = cidx
                                vcount+=1
                        
                        for didx in range(0,len(decis[0])):
                            if decis[0,didx]>0:
                                voting[int(voting_c[0,didx])]+=1
                                #voting[voting_c[0,didx]]+=decis[0,didx]
                            else:
                                voting[int(voting_c[1,didx])]+=1
                                #voting[voting_c[1,didx]]+=-decis[0,didx]
                        
                    if bowParams['fusion'] == 'late':
                    
                        voting=multiply(weights_late[voc],voting) # weights should be an array specifying the weight for each class for the current descriptor
                     
                        code_late.append(voting)
                    else:
                        print voting
                        # the modules for ecoc can be found in mcv_ecoc.py
                        y_auto = ecoc.decode(code,M)
                        y_auto = y_auto[0][0]
                        print y_test, y_auto
                        y_auto=voting.argmax()+1
                    
                        confMat[y_test-1,y_auto-1] = confMat[y_test-1,y_auto-1] + 1

            # decoding for late fusion
            if bowParams['fusion'] == 'late':
                M_late = tile(M,(1,nv))
                
                # the modules for ecoc can be found in mcv_ecoc.py
                if bowParams['ecoc-coding']=='OneVsAll':

                    code_late = reshape(array(code_late).flatten(),(1,nv*nc))
                    y_auto = ecoc.decode(code_late,M_late)[0][0]
                else:
                    '''
                    cl = np.array(code_late)
                    cl[0,:] = cl[0,:]*0.3
                    cl[1,:] = cl[1,:]*0.2
                    cl[2,:] = cl[2,:]*0.1
                    cl[3,:] = cl[2,:]*0.4
                    cl[4,:] = cl[2,:]*0.0
                    code_late = cl.tolist()
                    '''
                    code_sum=sum(code_late,axis=0)
                    y_auto=code_sum.argmax()+1

                print 'late result: '+str(y_test)+' '+str(y_auto)
                confMat[y_test-1,y_auto-1] = confMat[y_test-1,y_auto-1] + 1


            # show result of one test
           # if showFig:
           #     ion()
           #     figure('test')
           #     subplot(2,1,1); imshow(hstack((imgRGB,imgRGB)),hold=False)
           #     title('auto label: '+bowParams['classes'][int(y_auto)-1]+'')
           #     scatter(pos[1,:],pos[0,:],20,codes)
           #     subplot(2,1,2); imshow(confMat,hold=False)
           #     title('confusion matrix')
           #     draw()
        
        if ROC:
            # save test data for this class, to be used in the ROC evaluation
            f = open(testDataDir +'roc_tmp_data_'+str(y_test)+'.pkl', 'wb')
            pickle.dump(test_class_x,f);
            pickle.dump(test_class_y,f);
            f.close()
    
    print confMat        
    print 'Accuracy = '+str(accuracy(confMat))                    
    
    
    #
    # ROC curve
    #                    
    if ROC:
        if bowParams['classifier'] == 'svm':
            
            #
            # one-vs-all ECOC coding            
            #    
            if bowParams['ecoc-coding'] == 'OneVsAll':
            
                for c1 in range(0,nc):                                
                                
                    className = bowParams['classes'][c1]
                    
                    # retrieve the positive class from file
                    f = open(testDataDir +'roc_tmp_data_'+str(c1+1)+'.pkl', 'rb')
                    test_class_x = pickle.load(f)
                    test_class_y = pickle.load(f)
                    f.close()
                    x1 = test_class_x; del(test_class_x)
                    y1 = test_class_y; del(test_class_y)            
                                        
                    # retrieve all the other (negative) classes from files
                    yy = range(0,nc)
                    yy.remove(cidx)                
                    
                    x2 = zeros((1,shape(x1)[1]))
                    
                    for c2 in yy:
                        f = open(testDataDir +'roc_tmp_data_'+str(c2+1)+'.pkl', 'rb')
                        test_class_x = pickle.load(f)
                        f.close()
                        
                        x2 = vstack((x2,randSubsample(test_class_x,int(shape(x1)[0]/len(yy)))))
                    
                    x2 = x2[1:shape(x2)[0],:]
                    y2 = -1*ones((shape(x2)[0],1))    
                        
                    # join data and labels to compute ROC curve    
                    test_x = vstack((x1,x2)); del(x1); del(x2)
                    test_y = vstack((y1,y2)); del(y1); del(y2)     
                    
                    # load trained SVM model
                    svm_model = svm_load_model(classifierDir+'svm_bow_'+bowParams['keypointDetector']+'_'+descr+'_K_'+str(params['K'])+'_'+className+'_vs_all.model')
                    
                    # compute ROC curve under 'test' condition (see mcv_svm_roc.py)
                    xy_roc,auc = plot_svm_roc(map(list,test_x), list(test_y), svm_model)
                    
                    
                    print 'AUC = '+str(auc)
                    figure(1)
                    plot(xy_roc[:,0],xy_roc[:,1])
                    xlabel('FP'); ylabel('TP')
                    title('Class '+str(c1+1)+' vs All')
                    show()
                    
    te = time.time()
    print 'Elapsed time', te-ts
        

    




