"""

    Bag of Words: TRAINING script
    
    Master in Computer Vision - Barcelona
    
    Week 1 - Task 1: Consider different vocabulary sizes (play with bowParams - 'K')   

    Week 1 - Task 2: Consider K-nn vs. svm classifiers (bowParams - 'classifier'), implement in mcv_svm.py   

    Week 1 - Task 3: Include Shape Context and BRIEF descriptors (play with bowParams - 'descriptor')   

    Week 1 - Task 4: Compress the descriptor, using PCA: add into mcv_bow.py and implement in mcv_pca.py
    
    Author: Francesco Ciompi, Ramon Baldrich, Jordi Gonzalez

"""

from sys import exit,path,platform            # system functions for escape
import time
import os
path.append('.') 
path.append('./svm_code')
path.append('./sift_code')

from mcv_bow import *            # mcv library of bag of words functions
from mcv_svm_roc import *        # mcv library for roc curve
from mcv_svm import *            # mcv module for SVM
import pickle                    # save/load data files1
# ******************  MAIN **************************
if __name__ == "__main__":
                
    # define datasets and directories

    DatasetFolder    = '/share/apps/master_data/module5/datasets/Full/pascal/'
    DataOutputFolder ='/home/1357250/M5/output/'

    trainDir = DatasetFolder + 'train'
    valDir   = DatasetFolder + 'validation'
    nc = 5 # number of classes

    #descriptorDir = './descriptors/';
    descriptorDir = DataOutputFolder + 'descriptors/'
    classifierDir = DataOutputFolder + 'classifiers/'
    trainDataDir  = DataOutputFolder + 'trainData/'
   
    # TASKS
    #
    # 1. compute image descriptor (SIFT, HOG, SURF ...) in DENSE configuration or in Keypoints    
    computeDescriptors = True
    #
    # 2. construct words vocabulary through K-MEANS clustering
    computeVocabulary = True
    #
    # 3. assign words of vocabulary back to each training image
    computeAssignment = True
    #
    # 4. train SVM classifier on set of words for each class
    train_svm = True

    #
    # OPTIONS
    #
    # 1. show figures during TASKS 1-4
    showFig = False

    #
    # train files: 5 categories, cars, dogs, bicycles, motorbikes and persons.
    # getFilelist is found at: mcv_tools.py
    #    
    trainFiles_car,filenames_car = getFilelist(''+trainDir+'/car/','jpg') # class 1
    trainFiles_dog,filenames_dog = getFilelist(''+trainDir+'/dog/','jpg') # class 2 
    trainFiles_bicycle,filenames_bike = getFilelist(''+trainDir+'/bicycle/','jpg') # class 3
    trainFiles_motorbike,filenames_moto = getFilelist(''+trainDir+'/motorbike/','jpg') # class 4
    trainFiles_person,filenames_pedestrian = getFilelist(''+trainDir+'/person/','jpg') # class 5

    trainFiles = []
    trainFiles.append(trainFiles_car); n1 = len(trainFiles_car)
    trainFiles.append(trainFiles_dog); n2 = len(trainFiles_dog)
    trainFiles.append(trainFiles_bicycle); n3 = len(trainFiles_bicycle)
    trainFiles.append(trainFiles_motorbike); n4 = len(trainFiles_motorbike)
    trainFiles.append(trainFiles_person); n5 = len(trainFiles_person)

        
    trainLabels = zeros((1,n1+n2+n3+n4+n5))
    trainLabels[0,0:n1] = 1
    trainLabels[0,n1:n1+n2] = 2
    trainLabels[0,n1+n2:n1+n2+n3] = 3
    trainLabels[0,n1+n2+n3:n1+n2+n3+n4] = 4
    trainLabels[0,n1+n2+n3+n4:n1+n2+n3+n4+n5] = 5
    trainLabels = trainLabels.flatten()

            
    # ######################################################################################
    #         BAG OF WORDS - PARAMETERS DEFINITION        
    # ######################################################################################
    #
    # define a dictionary structure to store bag of words parameters 
    #
    # > descriptor: sift, hog, surf; combination of descriptors is allowed, e.g. ['sift','hog'], for fusion
    # > keypointDetector: dense, sift, harris --> 'gridSpacing'/'patchSize' (16) is related with nPt
    # > nf_descriptor_name: number of features
    # > K: number of clusters / size of BOW vocabulary (best value ~ 200)
    # > nPt: numer of keypoints to selecte from each training image, related to gridSpacing
    # > classifier: knn, svm    
    # > fusion: off, early, late
    # > classes: list of class names
    # > ecoc-coding: OneVsAll
    # > spyramid: spatial pyramid; pyrconf: [vertical_regions,horizontal_regions] in spatial pyramid (e.g., [2,2], [1,3] etc.)
    bowParams = {    'patchSize':48, 'gridSpacing':48, 'keypointDetector':'MSER',\
                       'harris_sigma':3, 'harris_min_dist':1, 'harris_threshold':0.01, 'harris_wid':5,\
                       #If brief is used, it should be the first one on the list (in early fusion)
                       'descriptor':['BRIEF'],\
                       'K': 400, 'nPt': 2000, \
                       'nf_brief':32,\
                       'nf_centrist':1024,\
                       'nf_sc':60,\
                       'nf_gist':960,\
                       'nf_colornaming':11,\
                       'nf_sift':128, 'sift_edge-thresh':10, 'sift_peak-thresh':5,\
                       'nf_oppsift':384,\
                       'nf_lbp':128,\
                       'nf_surf':64, 'surf_extended':1,'surf_hessianThreshold':5.0, 'surf_nOctaveLayers':1, 'surf_nOctaves':1,\
                       'nf_hog':81,\
                       'classifier':'svm',\
                       'ecoc-coding':'OneVsOne',\

                       'fusion': 'early',\
                       'nc': nc,\
                       'ROC': False,\
                       'classes':['car','dog','bicycle','motorbike','person'],\
                       'spyramid':True,\
                       'pyrconf':[2,2]}


    if len(bowParams['descriptor'])>1 and bowParams['fusion'] == 'off':
        print 'ERROR: DESCRIPTORS incompatible with FUSION type!'
        exit(-1)
        
    if bowParams['descriptor'][0] == 'surf' and bowParams['keypointDetector']!= 'surf':
        print 'ERROR: SURF descriptor is only compatible with SURF keypoints!'
        exit(-1)
    print bowParams
    #for descriptor in bowParams['descriptor']:
    #    if descriptor == 'brief' and bowParams['keypointDetector']!= 'star':
    #        print 'ERROR: BRIEF descriptor is only compatible with STAR keypoints!'
    #        exit(-1)
    

    # ###############################
    #             TRAINING
    # Functions below are located at: mcv_bow.py
    # ###############################    
    
    #    
    # 1. compute descriptors of 'nPt' points per image for vocabulary    
    #    
 
    if not os.path.isdir(descriptorDir): os.mkdir(descriptorDir)
    if not os.path.isdir(classifierDir): os.mkdir(classifierDir)
    if not os.path.isdir(trainDataDir):  os.mkdir(trainDataDir)
     
     
    ts_1 = time.time()
     
    if computeDescriptors: getDescriptor(trainFiles,descriptorDir,bowParams,showFig)

    te_1 = time.time()   
    
    # 2. compute vocabulary by clustering the descriptors: 
    #
    ts_2 = time.time()
    if computeVocabulary: getVocabulary(descriptorDir,'pkl', classifierDir,bowParams)
    te_2 = time.time()
    
    
    #
    # 3. words assignment to images
    #
    ts_3 = time.time()
    if computeAssignment: getAssignment(trainFiles,trainLabels, classifierDir, trainDataDir, bowParams,showFig)
    te_3 = time.time()
    #
    # 4. train SVM
    #
    ts_4 = time.time()
    if bowParams['classifier'] == 'svm' and train_svm: train_svm_bow(classifierDir, trainDataDir, bowParams)
    te_4 = time.time()
    
    print 'Compute descriptors time: ',te_1-ts_1
    print 'Compute vocabulary time: ', te_2-ts_2
    print 'Compute assignment time: ', te_3-ts_3
    print 'Compute classifier time: ', te_4-ts_4
    print 'Total time', te_4-ts_1
        
    
    



