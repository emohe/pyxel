""" 
    
    SIFT
    
    Master in Computer Vision - Barcelona
    
    Author: Francesco Ciompi, Ramon Baldrich, Jordi Gonzalez
         
"""

from PIL import Image
import os
from numpy import *
#from pylab import *
#import matplotlib
"""

    Based on:
    
    [Lowe04] Lowe, D. G., "Distinctive Image Features from Scale-Invariant Keypoints", International Journal of Computer Vision, 
    60, 2, pp. 91-110, 2004.
    
    [book] Programming Computer Vision with Python (http://programmingcomputervision.com/)     

"""


def process_image(imagename,resultname,params):
# """ Process an image and save the results in a file. """

    params_string = "--edge-thresh "+str(params['sift_edge-thresh'])+" --peak-thresh "+str(params['sift_peak-thresh'])

    import tempfile
    temp=tempfile.NamedTemporaryFile(suffix='.pgm',delete=False)
    temp.close()        
    
    if imagename[-3:] != 'pgm':
        # create a pgm file
        im = Image.open(imagename).convert('L')
        imagename = temp.name
        im.save(temp.name)
    
    # path to the SIFT code
#    cmmd = str("./sift_code/sift "+imagename+" --output="+resultname+" "+params_string)
    cmmd = str("./sift_code/sift "+imagename+" --output="+temp.name+".sift"+" "+params_string)
    os.system(cmmd)
    os.remove(temp.name)
    return temp.name+".sift"



def read_features_from_file(filename):
# Read features properties and return in matrix form

    f = loadtxt(filename)
    os.remove(filename)
    return f[:,:4],f[:,4:] # feature locations, descriptors


def write_features_to_file(filename,locs,desc):
# Save features location and descriptor to file
    
    savetxt(filename,hstack((locs,desc)))
    


def plot_features(im,locs,circle=False):
# Show image with features. input: im (image as array),
# locs (row, col, scale, orientation of each feature).

    def draw_circle(c,r):
        t = arange(0,1.01,.01)*2*pi
        x = r*cos(t) + c[0]
        y = r*sin(t) + c[1]
        plot(x,y,'b',linewidth=2)

    imshow(im)
    if circle:
        for p in locs:
            draw_circle(p[:2],p[2])
    else:
        plot(locs[:,0],locs[:,1],'ob')
    axis('off')


    
