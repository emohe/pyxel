""" 
    
    Library of tools for machine learning and data processing
    
     Master in Computer Vision - Barcelona
 
     Week 1 - Task 3: This code is already prepared to deal with HOG (mcv_descriptors.py), SIFT and SURF -> include SC and BRIEF
    
     Week 2 - Task 1: This code is already prepared to deal with 'keypointDetector' Harris (mcv_harris.py), surf and Sift (mcv_sift.py)
     
     Week 2 - Task 3: This code is already prepared to deal with GIST -> include CENTRIST
     
     Author: Francesco Ciompi, Ramon Baldrich, Jordi Gonzalez
         
"""

# Master in Computer Vision Libraries
import mcv_dsift as dsift            # dense SIFT [cite source]
import mcv_sift as sift                # alternative code from book
from mcv_descriptors import *
import mcv_harris as harris
#import mcv_shape_context as shape_context
#from SC import *

# External Libraries
import os                            # operating system
from numpy import *                    # numpy, for maths computations
from PIL import Image                 # python image library
import random as rnd                # random value generator
import cv2.cv as cv                            # opencv
import cv2                            # opencv2
#from pylab import *                    # matplotlib, for graphical plots
from sklearn.decomposition import PCA
import leargist
from ColorNaming import *
from skimage.feature import local_binary_pattern 
from mcv_centrist import *

#
#     get list of files in 'path' with extension 'ext'
#
def getFilelist(path,ext='',queue=''):
    # returns a list of filenames (absolute, relative) for all 'ext' images in a directory
    if ext != '':
        return [os.path.join(path,f) for f in os.listdir(path) if f.endswith(''+queue+'.'+ext+'')],  [f for f in os.listdir(path) if f.endswith(''+queue+'.'+ext+'')]    
    else:
        return [os.path.join(path,f) for f in os.listdir(path)]    


#
#    detect random points in the image domain
#
def detectPoints(imgSize,nPt,mode='rnd'):    
    """ compute the position of 'nPt' points in the image domain 'dataSize' with modality 'mode' 
    if 'dense' or a specific detector is declared, nPt is bypassed """
    
    if mode == 'rnd':
        return rnd.sample(range(0,imgSize[0]*imgSize[1]),  nPt);    
    else:
        print'Unkwnown Detection Mode!'
        return None

#
#     random subsample over the rows of data matrix 'data'
#
def randSubsample(data, nPt):
        
    print shape(data)[0] # = 502
    print nPt    
    if shape(data)[0] > nPt:
        idx = array(rnd.sample(range(0,shape(data)[0]), nPt));
        return data[idx]
    else:
        return data


#==============================================================================
# #
# #     Compute feature matrix per list of images
# #
# def getFeatList(imFilesPath,descriptor):
#     
#     if descriptor == 'hog':
#         FEAT = zeros((len(imFilesPath),468),float)
#         cont = 0
#         for filename in imFilesPath:
#             print filename
#             img = array(Image.open(filename),double)
#             H = hog(img)
#             print H
#             FEAT[cont,:] = H
#             cont+=1
#             print cont            
#            
#    return FEAT
#==============================================================================

    
#    
#     compute feature matrix per image
#
def getFeatImg(img,imgRGB,params,showFig=False,filename=''):
    """     Compute features corresponding to the selcted 'descriptor' for image 'img'
    The descriptor is applied to patches (patchSize) over a dense matrix (gridSpacing) 
    in case of SIFT and HOG, and to keypoints in all the others. It also implements the
    early fusion as a combination of descriptors """
        


    # ####################################    
    #     Descriptors positions    
    # ####################################
    
    # the patch is centered in the keypoint    

    #
    #    Harris keypoints    
    #
    img2 = img

    if params['keypointDetector'] == "FAST" or params['keypointDetector'] == "STAR" or params['keypointDetector'] == "SIFT" or params['keypointDetector'] == "SURF" or params['keypointDetector'] == "MSER" or params['keypointDetector'] == "HARRIS" or params['keypointDetector'] =='Dense' :
        detector = cv2.FeatureDetector_create(params['keypointDetector'])
        positions = None
        positions = detector.detect(img,None)
        
        POSITIONS=zeros((2,len(positions)))
        
        for k in range(0,len(positions)):
            POSITIONS[:,k]=positions[k].pt
            
        print shape(positions)
        
        if shape(positions)[0]<1:
            POSITIONS=None



    elif params['keypointDetector'] == 'dense':
        
        print 'Dense sampling'
        
        # dense sampling each in gridSpacing
        nr = int(shape(img2)[0]/params['gridSpacing'])
        nc = int(shape(img2)[1]/params['gridSpacing'])
        positions = zeros((2,nr*nc))
        keypoints = []
        k = 0; w = int(params['gridSpacing']/2)
        for r in range(w,params['gridSpacing']*nr,params['gridSpacing']):
            for c in range(w,params['gridSpacing']*nc,params['gridSpacing']):                
                positions[0,k] = r; positions[1,k] = c;
                kp = cv2.KeyPoint(float(r),float(c),float(params['patchSize']))
                keypoints.append(kp)
                k+=1
        POSITIONS = positions
    

    #    no keypoints: used in image retrieval and global descriptors    
    # 
    #elif params['keypointDetector'] == 'void' and params['descriptor'][0] == 'gist':
    
        # ####################################            
        #     GIST        
        # ####################################
            
        # it requires PIL image
     #   gist_vector = leargist.color_gist(Image.fromarray(img2))
      
      #  DESCRIPTORS = reshape(gist_vector,(1,params['nf_gist']))
      #  POSITIONS = []

   # elif params['keypointDetector'] == 'void' and params['descriptor'][0] == 'centrist':

    #    positions = zeros((2,1))
    #   k = 0
   #     for k in range(1): positions[0][k] = 1; positions[1][k] = 1;
   #     POSITIONS = positions
        
    # LOCAL DESCRIPTORS

    if POSITIONS == None:

        print 'Image skipped for lack of key-points'
        DESCRIPTORS = None

    else:

        if showFig:
            ion()        
            figure(1)
            imshow(img2,hold=False)
            gray()
            scatter(POSITIONS[1,:],POSITIONS[0,:])
            draw()    


    
        # ####################################    
        #     Compute image descriptor(s)    
        # ####################################
        #
        #     Include (optional) early fusion
        #
        # ####################################
            
        nf = 0
        for descriptor in params['descriptor']:
            if descriptor == 'SIFT': nf = nf + params['nf_sift']
            if descriptor == 'hog': nf = nf + params['nf_hog']
            if descriptor == 'SURF': nf = nf + params['nf_surf']
            if descriptor == 'sc': nf = nf + params['nf_sc']
            if descriptor == 'BRIEF': nf = nf + params['nf_brief']
            if descriptor == 'color_naming': nf = nf + params['nf_colornaming']
            if descriptor == 'oppsift': nf = nf + params['nf_oppsift']
            if descriptor == 'lbp': nf = nf + params['nf_lbp']
            if descriptor == 'centrist': nf = nf + params['nf_centrist']
        
        
        if params['keypointDetector'] != 'void': # if not image retrieval
            print "Nf:", nf
            DESCRIPTORS = zeros((shape(POSITIONS)[1],nf))


        descrPos = 0
        pos = 0

        for descriptor in params['descriptor']:

            if descriptor == "SIFT" or descriptor == "BRIEF" or descriptor == "SURF" :
                print descriptor

                if size(POSITIONS) > 0:
                    extractor = cv2.DescriptorExtractor_create(descriptor)
                    old_pos=shape(positions)[0]
                    positions, descriptors = extractor.compute(img, positions)
                    print shape(descriptors)
                    

                    #When using a keypoint detector that is not STAR, BRIEF descriptors have to be the first ones to be computed
                    #This is because the BRIEF  descriptor sometimes cannot be computed in all the positions, and returns less descriptors than positions
                    #When this happens, the initial shape of DESCRIPTORS needs to be changed according to the number of points where BRIEF can be computed.

                else:

                    descriptors = None
                    positions = None
                
                if positions is not None:

                    if shape(positions)[0] > 0:

                        if(shape(positions)[0]<old_pos):
                            POSITIONS=positions
                            print shape(positions)
                            DESCRIPTORS = zeros((shape(POSITIONS)[0],nf))
                            
                        DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                        descrPos = descrPos+shape(descriptors)[1]


        
            # ##############################################    
            #     Scale Invariant Feature Transform (SIFT)    
            # ##############################################

            
    
            elif descriptor =='oppsift':
                print 'OPPSIFT ('+str(params['nf_oppsift'])+' elems.)'
                
                extractor = cv2.DescriptorExtractor_create("OpponentSIFT")
                positions, descriptors = extractor.compute(imgRGB,positions)

                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]

            elif descriptor=='lbp':

                print 'LBP ('+str(params['nf_lbp'])+' elems.)'
                positions_lbp = zeros((2,shape(positions)[0]))

                for k in range(shape(positions)[0]):
                    positions_lbp[0,k] = positions[k].pt[1]; positions_lbp[1,k] = positions[k].pt[0];

                radius=3
                n_points = 8 * radius
                if size(positions) > 0:
                    nd = shape(positions_lbp)[1]
                    descriptors = zeros((nd,params['nf_lbp']))

                    for k in range(0,nd):
                        r = int(positions_lbp[0,k]); c = int(positions_lbp[1,k]);
                        patch = img2[r:r+params['patchSize'],c:c+params['patchSize']]
                        if size(patch) == params['patchSize']*params['patchSize']: # discard incomplete patches


                            lbp = local_binary_pattern(patch, n_points, radius, 'ror')
                            lbp_hist, _ = histogram(lbp.ravel(), bins=128)
                            descriptors[k,:] = lbp_hist
                

                    hstack((DESCRIPTORS,descriptors))


                else:

                    descriptors = None
                    positions = None

                # update descriptors
                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]


        
            # ########################################
            #     Histogram of Oriented Gradient (HOG)    
            # ########################################
            elif descriptor == 'hog':
                    
                print 'HOG ('+str(params['nf_hog'])+' elems.)'        
                positions_ = zeros((2,size(positions)))
                for k in range(len(positions)):
                    positions_[0,k] = positions[k].pt[1]; positions_[1,k] = positions[k].pt[0];

                if size(positions_) > 0:
                    nd = shape(positions_)[1]
                    descriptors = zeros((nd,params['nf_hog']))


                    for k in range(0,nd):
                        r = int(positions_[0,k]); c = int(positions_[1,k]);
                        patch = img2[r:r+params['patchSize'],c:c+params['patchSize']]
                        if size(patch) == params['patchSize']*params['patchSize']: # discard incomplete patches
                                      
                            # hog is found in mcv_descriptors.py
                            descriptors[k,:] = hog(patch).transpose()

                    hstack((DESCRIPTORS,descriptors))    


                else:
                
                    descriptors = None
                    positions = None
            
                # update descriptors
                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]


        
  
            elif descriptor == 'sc':
                pass

            elif descriptor == 'centrist':
            #It makes no sense to use centrist with early fusion, thus the descriptors are built again
                DESCRIPTORS = zeros((1,nf))
                hist, _ = centrist(img)
               
                for i in range(shape(hist)[0]):
                    DESCRIPTORS[0,i]=hist[i]
            elif descriptor == 'gist':
    
                gist_vector = leargist.color_gist(Image.fromarray(img2))
                print shape(gist_vector)
      
                DESCRIPTORS = reshape(gist_vector,(1,params['nf_gist']))
                    
            

            elif descriptor == 'color_naming':
                print 'Color naming ('+str(params['nf_colornaming'])+' elems.)'
                positions_ = zeros((shape(positions)[0],2))
          
                for k in range(shape(positions)[0]):
                    positions_[k,0] = positions[k].pt[0]; positions_[k,1] = positions[k].pt[1];
                    
                
               #I don't know what kind of vector positions_ needs to be created for this function to work:
                descriptors=ImColorNamingTSELabDescriptor(imgRGB,positions_,params['patchSize'])
                print descriptors
                print shape(descriptors)

                # update descriptors
                DESCRIPTORS[:,descrPos:descrPos+shape(descriptors)[1]] = descriptors
                descrPos = descrPos+shape(descriptors)[1]




        
        
    print '*****************************'
    print 'SHAPE OF FINAL DESCRIPTOR', shape(DESCRIPTORS)
   
    POSITIONS=positions
    return DESCRIPTORS,POSITIONS
#
#     normalize feature matrix
#
def normalizeFeat(x,mean_x=None,std_x=None):
    
    if mean_x == None and std_x is None:
        mean_x = x.mean(axis=0)
        std_x = x.std(axis=0)
        std_x[std_x==0] = 1
                
    return (x-tile(mean_x,(shape(x)[0],1)))/tile(std_x,(shape(x)[0],1)),mean_x,std_x
    
    
#    
#     accuracy of a confusion matrix    
#
def accuracy(confMat):
    
    return sum(diag(confMat))/sum(confMat.flatten())
    
    
#    
#     compute the row with minimum distance between vector 'v' and matrix 'm'
#
def yMinDist(v,m,metric):

    Dist = zeros((1,shape(m)[0]))
    
    for row in range(0,shape(m)[0]):
        
        if metric == 'euclidean':

            Dist[0,row] = sqrt(sum((m[row,:]-v)**2))
            
    return argmin(Dist[0,:])
    
        
#
#    Hamming distance
#
""" Calculate the Hamming distance between two given strings """
def hamming(a, b):
    
    return sum(logical_xor(a,b).astype(int))

    
    
#
#    show first 10 results of image-based query
#    
def showQueryResult(filename,trainDir,LIBRARY,idxs,method,relevance_feedback=False):
            
    #
    #    QUERY RESULTS VISUALIZATION (first 10 results)
    #                    
    fig = figure('IMAGE RETRIEVAL'); subplot(353); title('QUERY')
    img = array(Image.open(filename))
    imshow(img,cmap=gray)

    subplot(3,5,6)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[0]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[0]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
        
    subplot(3,5,7)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[1]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[1]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
        
    subplot(3,5,8)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[2]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[2]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
                
    subplot(3,5,9)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[3]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[3]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
                
    subplot(3,5,10)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[4]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[4]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
            
    subplot(3,5,11)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[5]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[5]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
            
    subplot(3,5,12)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[6]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[6]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
                
    subplot(3,5,13)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[7]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[7]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
            
    subplot(3,5,14)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[8]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[8]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
                
    subplot(3,5,15)
    try:
        img = array(Image.open(''+trainDir+'car/'+LIBRARY[idxs[9]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
    except:
        img = array(Image.open(''+trainDir+'dog/'+LIBRARY[idxs[9]].split('/')[-1][0:-4]+'JPEG')); imshow(img)
                
    
    if relevance_feedback:
                
        mouse = MouseMonitor()
        mouse.weights = []
        mouse.cont = 0
        connect('button_press_event', mouse.mycall)
        
        show()

        weights = mouse.weights
        
        del(mouse)    

        return weights

    else:
        show()


class MouseMonitor():

    event = None
    xdatalist = []
    ydatalist = []
    weights = []
    cont = 0

    def mycall(self, event):

        self.event = event
        self.xdatalist.append(event.x)
        self.ydatalist.append(event.y)
        self.cont+=1

        if event.button == 1:
            self.weights.append(1)
            print 'result '+str(self.cont)+'. RELEVANT'
        elif event.button == 3:
            self.weights.append(-1)
            print 'result '+str(self.cont)+'. NOT RELEVANT'


    
    
    
    
    
    
    
    
    

        
        
    
    
