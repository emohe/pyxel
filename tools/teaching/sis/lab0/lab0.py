# -*- coding: utf-8 -*-

#Tània Pinosa López i Xavier Giró-i-Nieto


#Importem llibreries necessàries
from pylab import *
from matplotlib import pyplot as plt
import numpy as np
import math as math
import cmath as math

# Tanca totes les figures
plt.close("all")

#Matrius i expressions
#----------------------------------------------
A=np.mat('3 5 7 9; 2 4 6 8; 1 0 1 0')#Creem la matriu base

print A[1,:] #Mostreu els elements de la fila 2

print A[:,2] #Mostreu els elements de la columna 3

A[2,3]=5 #Canvieu l'element A(3,4) per un 5

b=np.mat('1 1 1 1') #Creem matriu fila d'uns
A=np.concatenate((A,b)) #Afegiu una quarta fila d'uns.

B=A[0,:] #Deseu en un vector B la fila 1 de la matriu A

C=A[:,1] #Deseu en un vector C la columna 2 de la matriu A

B=5*B #Multipliqueu per 5 cada element de B

D=np.vdot(B,C) #Multipliqueu cada element de B per cada element de C (producte escalar)

z=1j; #constant complexe a phyton
x2=math.cos(3.5*math.pi)+z*math.sin(3.5*math.pi)#Calculeu les següents expresions
x2bis=math.exp(3.5*z*math.pi) #també

x3=math.sqrt(math.log10(25)) #Calculeu les següents expresions


#Gràfiques
#----------------------------------------------
x4=np.arange(-10,11) #vector n=-10:10
y=2*x4+1 #Calculeu els valors de la recta y=2x+1, per a valores enters de x entre -10 y 10

#Dibuixeu la gràfica de y en funció de x, utilitzant la funció plot
#Afegiu un títol ("Recta y=2x+1") i etiquetes pels eixos x ("X") i y ("Y")
fig1=plt.figure()
plt.plot(y)
fig1.suptitle('Recta 2x+1')
plt.xlabel('X')
plt.ylabel('Y')

#Dibuixeu la gràfica de y en función de x, utilitzant la funció stem
#Afegiu un títol ("Recta y=2x+1") i etiquetes pels eixos x ("X") i y ("Y")
fig2=plt.figure()
plt.stem(x4,y)
fig2.suptitle('Recta 2x+1')
plt.xlabel('X')
plt.ylabel('Y')

#show gràfiques
show(fig1)
show(fig2)