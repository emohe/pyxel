# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 1
import numpy as np
def grao(n):
    L=len(n) # Obtinc la longitud del array
    Y=np.ones(L) # Inicialitzo un array de 1 de longitud L
    i=0 # Inicialitzo el contador del bucle 
    while i<L: # Reccorro tot el array
        if n[i]<0: # Busco els números de n més petits que 0 per així poder generar un grao
            Y[i]=0 # Substitueixo els 1 en les posicions  per 0. (posicions en les quals n es més petit que 0 )
        i+=1 # Incremento el contador
    return Y #Retorno el array resultant(que formarà un graó unitari)
