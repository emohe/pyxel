# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 1

import numpy as np
import matplotlib.pyplot as plt
import grao as g

# Tanca totes les figures
plt.close("all")

n = np.array(range (-10, 11)) # Inicialitzo un array de rang -10 a 10
x1 = g.grao(n) # Crido a la funció grao
plt.figure() # Obro una figura
plt.stem(n,x1) # Pinto el gràfic
plt.title(u'Graó Unitari') # Títol de la gràfica
plt.xlabel(u'n') # Poso etiqueta al eix X
plt.ylabel(u'X1') # Poso etiqueta al eix Y
plt.show() # Mostro el gràfic

x2 = g.grao(n)-g.grao(n-7) # Crido a les dues funcions grao
plt.figure() # Obro una figura
plt.stem(n,x2) #Pinto el gràfic
plt.title(u'Pols Rectangular') # Títol de la gráfica
plt.xlabel(u'n') # Poso etiqueta al eix X
plt.ylabel(u'x2') # Poso etiqueta al eix Y
plt.show() # Mostro el gràfic