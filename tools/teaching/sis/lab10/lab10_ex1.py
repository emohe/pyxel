# -*- coding: utf-8 -*-
#Xavier Preixens
#Senyals i sistemes: pràctica 10

import numpy as np
import matplotlib.pyplot as plt
import scipy
import pols as p

# Tanca totes les figures
plt.close("all")

n=np.arange(0,64);
M=32;
x=p.pols(M,n);

plt.figure(1) 
plt.stem(n,x) 
plt.title('Pols 32 mostres') 
plt.xlabel('n')
plt.ylabel('X') 
plt.show() 


N=256;
n2=np.arange(0,256);
x2=scipy.fft(x,N);
plt.figure(2) 
plt.plot(abs(x2)) 
plt.title('FFT N=256') 
plt.xlabel('N')
plt.ylabel('FFT') 
plt.show() 


#Prova amb una N que no és potència de 2.

N=251;
n3=np.arange(0,251);
x3=scipy.fft(x,N);
plt.figure(3) 
plt.plot(abs(x3)) 
plt.title('FFT N=251') 
plt.xlabel('N')
plt.ylabel('FFT') 
plt.show()