# -*- coding: utf-8 -*-
# Xavier Preixens
# Senyals i sistemes: pràctica 10

from scipy.io.wavfile import read
import numpy as np

filename = "codificat.wav";
data=read(filename);
L=len(data);
n=np.arange(0,L);
M=np.cos(np.pi*n);
Z=data*M;
write("decodificat.wav",Z);