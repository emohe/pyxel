# -*- coding: utf-8 -*-
# Xavier Preixens
# Senyals i sistemes: pràctica 10

from scipy.io.wavfile import read
import numpy as np
import scipy
import promitjador as p
import matplotlib.pyplot as plt

filename = "interferencia.wav";
data=read(filename);

N=2048;
X=abs(scipy.fft(data,N));

# Tanca totes les figures
plt.close("all")

plt.figure(1) 
plt.stem(N,X)  
plt.show() 

s=p.promitjador(11,data);
S=abs(scipy.fft(s,2048));
plt.figure(2) 
plt.stem(2048,S)  
plt.show() 
