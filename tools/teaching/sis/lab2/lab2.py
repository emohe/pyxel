# -*- coding: utf-8 -*-

#Tània Pinosa López
#Grup 2

import numpy as np
import matplotlib.pyplot as plt
import grao as g
import signe as s
import pols as p
import delta as d
from pylab import *
import math as math
import cmath as math
import senyal1 as s1
import senyal2 as s2

# Tanca totes les figures
plt.close("all")

n = np.array(range (-10, 11)) # Inicialitzo un array de rang -10 a 10

#Senyal 1
x1=s.signe(n)
plt.figure(1) 
plt.stem(n,x1) 
plt.title('Funció signe') 
plt.xlabel('n')
plt.ylabel('X1') 
plt.show() 

#Senyal 2
M=4
x2=p.pols(M,n)
plt.figure(2) 
plt.stem(n,x2) 
plt.title('Funció pols') 
plt.xlabel('n') 
plt.ylabel('X2') 
plt.show() 

#Senyal 3
x3=p.pols2(M,n-2)
plt.figure(3) 
plt.stem(n.T,x3.T)
plt.title('Funció pols desplaçat') 
plt.xlabel('n') 
plt.ylabel('X3') 
plt.show() 

#Senyal 4
x4=d.delta(n)
plt.figure(4) 
plt.stem(n.T,x4.T) 
plt.title('Delta de Dirac') 
plt.xlabel('n') 
plt.ylabel('X4') 
plt.show() 

#Senyal 5
x5=d.delta(n)+d.delta(n-1)+d.delta(n-2)+d.delta(n-3)
plt.figure(5) 
plt.stem(n.T,x5.T)
plt.title('Pols rectangular') 
plt.xlabel('n') 
plt.ylabel('X5')
plt.show() 

#Senyal 6
x6=s1.senyal1(n)
plt.figure(6)
plt.stem(n.T,x6.T) 
plt.title('El meu senyal 1') 
plt.xlabel('n') 
plt.ylabel('X6') 
plt.show() 

#Senyal 7
x7=s2.senyal2(n)
print x7
