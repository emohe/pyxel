# -*- coding: utf-8 -*-

#Tània Pinosa López
#Grup 2

import numpy as np
import grao as g

def pols(M,n):
    
    y=g.grao(n)-g.grao(n-M) #combinacio de graos
    
    return y
    
def pols2(M,n):
    L=len(n) #longitud n
    y=np.zeros((1,L)) #vector longitud L ple de 0
    i = ((n>=0).nonzero()) #vector d'index de valors d'n iguals o més grans que zero
    y.put(i,1) #asignem a aquestes posicions un 1
    i=(n>=(M)).nonzero(); #vector d'index de valors d'n iguals o més grans que M
    y.put(i,0)#asignem a aquestes posicions un 0
    return y

    