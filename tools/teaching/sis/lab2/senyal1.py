# -*- coding: utf-8 -*-

#Tània Pinosa López
#Grup 2

import signe as s
import delta as d

def senyal1(n):
    #Combinacio senyals creats
    y=s.signe(n)+0.8*(d.delta(n-1))+0.6*(d.delta(n-2))+0.4*(d.delta(n-3))+0.2*(d.delta(n-4))-0.8*(d.delta(n+1))-0.6*(d.delta(n+2))-0.4*(d.delta(n+3))-0.2*(d.delta(n+4))
    return y