# -*- coding: utf-8 -*-

#Tània Pinosa López
#Grup 2

import numpy as np

def senyal2(n):
    
    y=np.sign(n)+np.sign(n+3)-np.sign(n+7)+np.sign(n-7)-np.sign(n-3) #combinacio signs python
    
    return y