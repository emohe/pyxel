# -*- coding: utf-8 -*-

#Tània Pinosa López
#Grup 2

import numpy as np
import grao as g

def signe(n):
    
    y = g.grao(n)-g.grao(-n) #combinacio de graos
    
    return y