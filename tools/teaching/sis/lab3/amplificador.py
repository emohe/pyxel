# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 3
import numpy as np
def amplificador(K,x):
    """"Funció que rep com a parametres un factor K i un array x. 
    Retorna la senyal amplificada."""
    return K*x 