# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 3
import numpy as np
import signals as s
import retardador1 as re1
import retardador as re
import reflexor as refle
import amplificador as am
import matplotlib.pyplot as plt

# Tanca totes les figures
plt.close("all")

n = np.arange(-10,11) # creo un array de interval -10 - 10
plt.figure("Retardador1")# creo la figura per la gràfica
plt.suptitle("Retardador1")# li poso un títol a la gràfica
plt.stem(n,re1.retardador1(s.grao(n))) # pinto la gràfica del retardador 1

plt.figure("Retardador")# creo la figura per la gràfica
plt.suptitle("Retardador")# creo la figura per la gràfica
plt.stem(n,re.retardador(3,s.grao(n))) # pinto la gràfica del retardador

plt.figure("Reflexor")# creo la figura per la gràfica
plt.suptitle("Reflexor")# creo la figura per la gràfica
plt.stem(n,refle.reflexor(s.grao(n))) # pinto la gràfica del reflexor


plt.figure("Amplificador")# creo la figura per la gràfica
plt.suptitle("Amplificador")# creo la figura per la gràfica
plt.stem(n,am.amplificador(2,s.grao(n)))# pinto la gràfica del amplificador

A=5 # Factor de amplifiació
B=2 # Factor de amplifiació
retard = 2 # Factor de retard
#Genero les dos senyals resultants després de pasar per dos sistemes
x5=am.amplificador(A,re.retardador(retard,s.trialo(n)))+am.amplificador(B,re.retardador(retard,s.desfado(n)))
x6=re.retardador(2,am.amplificador(A,s.trialo(n))+am.amplificador(B,s.desfado(n)))
#Pinto les dues gràfiques dels dos senyals resulants 
plt.figure("X5 i X6")
plt.subplot(1,2,1)
plt.title("X5")
plt.stem(n,x5,'green')
plt.subplot(1,2,2)
plt.title("X6")
plt.stem(n,x5,'red')

# Les dues gráfiques coincideixen, osigui es demostra la linealitat

#Genero les dos senyals resultants després de pasar per dos sistemes
x7=re.retardador(retard,am.amplificador(A,s.trialo(n)))
x8=am.amplificador(A,re.retardador(retard,s.trialo(n)))
#Pinto les dues gràfiques dels dos senyals resulants 
plt.figure("X7 i X8")
plt.subplot(1,2,1)
plt.title("X7")
plt.stem(n,x7,'green')
plt.subplot(1,2,2)
plt.title("X8")
plt.stem(n,x8,'red')

# Les dues gráfiques coincideixen, osigui es demostra la invariancia

plt.show() # mostro les gràfiques

