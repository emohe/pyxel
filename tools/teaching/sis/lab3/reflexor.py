# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 3
import numpy as np
def reflexor(x):
    """"Funció que rep com a parametres un array x. 
    Retorna la senyal reflexada"""
    L=len(x);
    Y=np.zeros(L)
    i=range(0,L)
    j=i.reverse()
    Y[i]=x[j]
    return Y