# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 3
import numpy as np
"""Funció que rep com a parametres un factor M i un array x. 
Retorna la senyal retardada."""
def retardador(M,x):
    L=len(x);# Guardo a L el tamany de de x
    i=M# inicialitzo el contador a M
    Y=np.zeros(L)# creo un array de zeros
    for i in range(i,L):
        Y[i]=x[i-M]# retardo les mostres
    return Y