# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 3
import numpy as np
def retardador1(x):
    """"Funció que rep com a parametres un array x. 
    Retorna la senyal retardada 1 mostra."""
    L=len(x); # Guardo a L el tamany de de x
    i=1 # inicialitzo el contador a 1
    Y=np.zeros(L)# creo un array de zeros
    for i in range(i,L): 
        Y[i]=x[i-1] # retardo les mostres
    return Y
    