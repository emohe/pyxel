# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 3
import numpy as np
def grao(n):
    """Funció que rep com a parametres un array n 
    i retorna la senyal graó en un array"""
    
    L=len(n) # Obtinc la longitud del array
    Y=np.ones(L) # Inicialitzo un array de 1 de longitud L
    i=(n<0)# Genero un vector amb les posicions dels números més grans que 0
    Y[i]=0# Canvio a les posicions anteriors els valors per 0
    return Y #Retorno el array resultant(que formarà un graó unitari)
  
def delta(n):
    """Funció que rep com a parametres un array n 
    i retorna la senyal delta en un array"""
    L=len(n)
    Y=np.zeros(L)
    Y[n==0] = 1
    return Y

def pols(M,n):
    """Funció que rep com a parametres un array n 
    i la duració del pols M. retorna la senyal pols rectangular en un array"""
    return  grao(n)-grao(n-M) # Restem a un graó un graó desplaçat M mostres per obtenir el pols

def signe(n):
    return  grao(n)-grao(-n)
    
def trialo(n):
    return grao(n-1)+grao(n-2)+grao(n-3)+grao(n-4)+grao(n-5)+grao(n-6)-grao(n+1)-grao(n+2)-grao(n+3)-grao(n+4)-grao(n+5)-grao(n+6)
    
def desfado(n):
    return signe(n) + pols(2,n-1) + pols(2,n-3) - signe(n-2)