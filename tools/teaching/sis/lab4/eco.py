# -*- coding: utf-8 -*-
import amplificador as am
import retardador as re
def eco(x,fs,D,K):
    #càlcul de les mostres necesaries per obtenir el eco
    M=int(D*fs) # transformo el resultat en int ja que en python si es fa 
    #una multiplicació amb un float encara que el resultat sigui enter la 
    #variable utiltizada serà un float
    return x+am.amplificador(K,re.retardador(M,x)) # genero el eco a través de un retardador i un amplificador