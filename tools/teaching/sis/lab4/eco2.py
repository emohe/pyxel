# -*- coding: utf-8 -*-
import numpy as np
import amplificador as am
def eco2 (x,fs,D,K):
    #càlcul de les mostres necesaries per obtenir el eco
    M=int(D*fs)# converteixo el resultat a enter
    z=np.zeros(M)# creo un array de zeros de tamany M
    v=np.concatenate([x,z])# concateno el senyal i la matriu de zeros
    vr=np.concatenate([z,x])# concateno la matriu de zeros i el senyal generan així el eco
    return v+am.amplificador(K,vr) # atenuo el eco i el sumo a la senyal
    