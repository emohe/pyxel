# -*- coding: utf-8 -*-
#Albert Mosella Montoro
#Grup 2
#Pràctica 4
import numpy as np
import eco2
import eco 
import matplotlib.pyplot as plt
from scipy.io import wavfile

# Tanca totes les figures
plt.close("all")


File1="veu.wav"# Ruta d'on es llegira el fitxer.(el path relatiu em dona problemes)
File2="eco.wav"# Ruta on es guardara eco.wav
File3="eco2.wav"# Ruta on es guardara eco2.wav
fs,x = wavfile.read(File1)
D=0.2# retard de 0.2 segons
K=0.4# factor de atenuació del eco
x1=eco.eco(x,fs,D,K)# generació del eco
# Representació gràfica del so original i el eco
plt.figure("Veu i Eco")
plt.subplot(1,2,1)
plt.title("Veu")
plt.plot(x,'green')
plt.subplot(1,2,2)
plt.title("Eco")
plt.plot(x1,'red')

x1=np.asarray(x1,dtype=np.int16)# és necesari perque el write funciona amb enters de 16 bits
wavfile.write(File2,fs,x1)# guardo el so modificat 

#11.Quina és la durada en segons del senyal llegit a disc ? I la durada del senyal amb eco
#generat ?

# Les dues tenen la mateixa duració un segon

#12.Canvieu el retard per un valor enter superior a la durada del senyal llegit a disc. Quin
#retard en mostres li esteu demanant al sistema retardador en aquest cas ? Per què es
#genera l'error ?

#La proba le feta amb un valor igual a 2, estic demanan un retard de 2*fs. Es genera l'error perque la durada
#del eco es més gran que la durada de la canço en si

D=5# retard de 5 segons

x2=eco2.eco2(x,fs,D,K) # genero el eco2

# Representació gràfica del so original i el eco2
plt.figure("Veu i Eco2")
plt.subplot(1,2,1)
plt.title("Veu")
plt.plot(x,'green')
plt.subplot(1,2,2)
plt.title("Eco2")
plt.plot(x2,'red')

plt.show() # mostro les gràfiques
x2=np.asarray(x2,dtype=np.int16) # és necesari perque el write funciona amb enters de 16 bits
wavfile.write(File3,fs,x2)# guardo el so modificat
