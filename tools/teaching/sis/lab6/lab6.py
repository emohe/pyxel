
'Marc Gallardo Ruiz'
'Grup 2 - Senyals i sistemas'

import numpy as np
import matplotlib.pyplot as plt
import math 
import delma2 as de2
import delma as de
import retardador as re

# Tanca totes les figures
plt.close("all")

n = np.arange(-64,65)
x1=np.cos((math.pi/8)*n)
plt.figure(1) 
plt.stem(n,x1) 
plt.title('cos(pi/8)') 
plt.xlabel('n= -64:64')
plt.ylabel('X1') 
plt.show() 


plt.figure(2) 
(x2,m) = de2.delma2(n,x1); 
plt.stem(m,x2);
plt.title('cos(pi/8)') 
plt.xlabel('n= -32:32')
plt.ylabel('X2') 
plt.show() 


plt.figure(3) 
(x3,m) = de2.delma2(m,x2); 
plt.stem(m,x3);
plt.title('cos(pi/8)') 
plt.xlabel('n= -16:16')
plt.ylabel('X3') 
plt.show()


plt.figure(4) 
(x4,m) = de2.delma2(m,x3); 
plt.stem(m,x4);
plt.title('cos(pi/8)') 
plt.xlabel('n=-8:8')
plt.ylabel('X4') 
plt.show()

plt.figure(5) 
(x5,m) = de2.delma2(m,x4); 
plt.stem(m,x5);
plt.title('cos(pi/8)') 
plt.xlabel('n=-4:4')
plt.ylabel('X5') 
plt.show()


plt.figure(6) 
x2br=re.retardador(1,x1);
(x2b,m1) = de2.delma2(n,x2br); 
plt.stem(m1,x2b);
plt.title('x2b[n]=x1[2n-1]') 
plt.xlabel('n')
plt.ylabel('X2b') 
plt.show()
'La diferencia amb x2[n] es que tens la mosta del mig dels cicles del cosinus repetida degut al desplassament'


plt.figure(7) 
(x7,m) = de.delma(n,x1,2); 
plt.stem(m,x7);
plt.title('cos(pi/8) amb la funcio delma') 
plt.xlabel('n=-32:32')
plt.ylabel('X6') 
plt.show()

