import numpy as np

def acumulador(n,x):
    L=len(x)
    Y=np.zeros(L)
    Y[1]=x[1]
    i=2
    for i in range(i,L):
        Y[i]=Y[i-1]+x[i]
    return Y