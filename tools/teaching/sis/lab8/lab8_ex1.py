import retardador as r
import numpy as np
import acumulador as a
import matplotlib.pyplot as plt
import grao as g
import promitjador as p

n=np.arange(-10,11)
L=len(n)
d=np.zeros(L)
d[10]=1

ha=r.retardador(1,d)
plt.figure(1) 
plt.stem(n,ha) 
plt.title('Retardador') 
plt.xlabel('n')
plt.ylabel('ha') 
plt.show() 

hb=a.acumulador(n,d);
plt.figure(2) 
plt.stem(n,hb) 
plt.title('Acumulador') 
plt.xlabel('n')
plt.ylabel('hb') 
plt.show() 

hc=p.promitjador(n,5,d);
plt.figure(3) 
plt.stem(n,hc) 
plt.title('Promitjador') 
plt.xlabel('n')
plt.ylabel('hc') 
plt.show() 