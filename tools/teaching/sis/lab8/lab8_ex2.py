import retardador as r
import numpy as np
import acumulador as a
import matplotlib.pyplot as plt

n=np.arange(-2,11)
m=np.arange(-2*2,(10*2)+1)
L=len(n)
d=np.zeros(L)
d[2]=1

x=2*(d)+4*(r.retardador(1,d))+6*(r.retardador(2,d))+8*(r.retardador(3,d))
plt.figure(1) 
plt.stem(n,x) 
plt.show() 

h=5*(d)+r.retardador(1,d)+3*(r.retardador(2,d))
plt.figure(2) 
plt.stem(n,h) 
plt.show() 

y=np.convolve(x,h)
plt.figure(3) 
plt.stem(m,y) 
plt.show() 
