# -*- coding: utf-8 -*-
import numpy as np
import grao as g
import pols as p
import matplotlib.pyplot as plt

M=10
n=np.arange(-M,M+1)
m=np.arange(-2*M,(M*2)+1)
y1=np.convolve(g.grao(n),g.grao(n));
plt.figure(1) 
plt.stem(m,y1) 
plt.title('Convolució graons') 
plt.xlabel('n')
plt.ylabel('y1') 
plt.show() 

y2=np.convolve(p.pols(4,n),p.pols(8,n));
plt.figure(2) 
plt.stem(m,y2) 
plt.title('Convolució polsos durada diferent') 
plt.xlabel('n')
plt.ylabel('y2') 
plt.show() 

y3=np.convolve(p.pols(12,n), p.pols(12,n));
plt.figure(3) 
plt.stem(m,y3) 
plt.title('Convolució polsos mateixa durada') 
plt.xlabel('n')
plt.ylabel('y3') 
plt.show() 