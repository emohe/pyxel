import grao as g
import retardador as r
import numpy as np
import promitjador as p
import acumulador as a
import matplotlib.pyplot as plt

M=10;
n=np.arange(-M,M+1)
m=np.arange(-2*M,(M*2)+1)
L=len(n)
d=np.zeros(L)
d[M]=1

x1 = np.convolve(g.grao(n),r.retardador(4,d));
plt.figure(1) 
plt.stem(m,x1) 
plt.show() 

x2 = np.convolve(g.grao(n),a. acumulador(n,d));
plt.figure(2) 
plt.stem(m,x2) 
plt.show() 

x3 = np.convolve(g.grao(n),p.promitjador(n,4,d));
plt.figure(3) 
plt.stem(m,x3) 
plt.show()