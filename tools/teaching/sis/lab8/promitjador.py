import numpy as np

def promitjador(n,L,x):
    D=len(x);
    Y=np.zeros(D)
    
    Y[1]=(x[1])/L
    i=2

    for i in range(i,L-1):
        Y[i]=Y[i-1]+(x[i])/L
        
    i=L
    for i in range(i,D):
        k=0
        j=i-L+1
        for j in range(j,i):
            k=x[j]+k;
        Y[i]=k/L
    return Y