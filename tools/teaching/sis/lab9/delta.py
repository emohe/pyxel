# -*- coding: utf-8 -*-

#Tània Pinosa López
#Grup 2

import numpy as np

def delta(n):
    
    L=len(n) #mida vector n
    Y=np.zeros((1,L)) #Vector de longitud L ple de 0
    i=((n==0).nonzero()) #Busquem posicio d'n on és el 0
    Y.put(i,1) #Canviem el valor de la posició a 1
    
    return Y
