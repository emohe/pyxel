#Tania Pinosa Lopez
#Grup 2

import numpy as np
import delta as d
import scipy.signal as lfil
import matplotlib.pyplot as plt

# Tanca totes les figures
plt.close("all")

n=np.arange(-20,21);       

A= [1]
B= [1,-0.77 ]
X= d.delta(n)

h=lfil.lfilter(A,B,X);

plt.figure(1) 
plt.stem(n,h.T) 
plt.show() 

