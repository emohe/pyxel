#Tania Pinosa Lopez
#Grup 2

import numpy as np
import delta as d
import scipy.signal as lfil
import matplotlib.pyplot as plt
from scipy import ndimage

# Tanca totes les figures
plt.close("all")

n=np.arange(-20,21);       

A= [1,-3,2]
B= [1]
X= d.delta(n)

h1=lfil.lfilter(A,B,X);
plt.figure(1) 
plt.stem(n,h1.T) 
plt.show() 

h2=lfil.lfilter(B,A,X);
plt.figure(2) 
plt.stem(n,h2.T) 
plt.show() 

m=np.arange(-20,21);

h=ndimage.convolve(h1,h2);
plt.figure(3) 
plt.stem(m,h.T) 
plt.show() 

# The convolution product is only given for points where the signals overlap completely
# so m=n instead of m=np.arange(-40,41);
