# -*- coding: utf-8 -*-
"""
Created on Wed Aug  6 16:46:48 2014

@author: xavi
"""

import os
import subprocess;
import time;
import sys;
import getpass


def run(tool_path, parameters, max_jobs = 10, out_file = sys.stdout, err_file = sys.stderr, wait=3, fast=False, mem=2000, run=True, print_command=False, wait_to_finish = False):
    # Run a given command to the cluster. It does not exit until executed
    
    done = False;
    
    # Get the name of the process
    # name = os.path.basename(tool_path); 

    # Count the amount of executions of the process (any user)
    # command = 'squeue -o %60j | grep ' + name + ' | wc -l';

    # Get the username
    name = getpass.getuser();

    # Count the amount of processes associated to the user
    command = 'squeue -o %60u | grep ' + name + ' | wc -l';
    
    parameter_string = " ".join([str(x) for x in parameters]);
    
    if (fast == False):
        srun_command = 'srun';
    else:
        srun_command = 'srun-fast';
        
    mem_string = "--mem=" + str(mem);
            
    if (wait_to_finish):
        async = "";
    else:
        async = "&";                
    
    command_to_run = srun_command + " " + mem_string + " " + tool_path + " " + parameter_string + async;        
    
    if (print_command):
        print command_to_run;
    
    while (done == False):
        proc_num_jobs = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE);
        num_jobs = int(proc_num_jobs.stdout.readline());
        
        if (num_jobs < max_jobs):
            done = True;
            
            
            if (run == True):
                subprocess.call(command_to_run, stdout=out_file, stderr=err_file, shell=True); #, stdout=devnull, stderr=devnull);
                time.sleep(0.1); # wait to create another process
        else:
            time.sleep(wait);
            
    return command_to_run;
    
    
def wait(tool_path):
    # Wait for the all the processes of a given name to finish
    
    # Get the name of the process
    # name = os.path.basename(tool_path); 

    # Count the amount of executions of the process (any user)
    # command = 'squeue -o %60j | grep ' + name + ' | wc -l';

    # Get the username
    name = getpass.getuser();

    # Count the amount of processes associated to the user
    command = 'squeue -o %60u | grep ' + name + ' | wc -l';
    
    terminated = False;
    while (not terminated):
        proc_num_jobs = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE);
        num_jobs = int(proc_num_jobs.stdout.readline());
        if (num_jobs != 0):
            time.sleep(3);
        else:
            terminated = True;

def buildPathCandidate( filename, pathDirCands ):	
		
	# Extract the basename from the filename
	baseName = os.path.basename( filename )
	rootName = os.path.splitext( baseName )[0]

	return os.path.join( pathDirCands, rootName + '.mat')

# Main
if __name__ == "__main__":

    # Path to the file containig the list of frames (one different for each user)  
    # pathFileDataset='/projects/retrieval/trecvid-ins14/imagesFirst10000shots.txt'
    # pathFileDataset='/projects/retrieval/trecvid-ins14/imagesFirst2000hots.txt'
    username = getpass.getuser();
    pathFileDataset='/projects/retrieval/trecvid-ins14/list_missing_' + username  + '.txt'

    # Path containing the generated object candidates
    pathDirCands='/projects/retrieval/trecvid-ins14/missing_candidates'    
            
    print 'Extracting object candidates from ' + pathFileDataset + "..."
                
    # Read the file containing the list of file paths
    fileDataset = open( pathFileDataset, 'r')
    
    # Read lines from the text file, stripping the end of line character
    fileNames = [line.strip() for line in fileDataset ]
    
    # Close file
    fileDataset.close()
        
    for filename in fileNames:
    # Run the extraction script for each image by providing parameters as
    # command line arguments

	# Check whether the object candidate has been already computed for the current file 
	pathFileCandidates = buildPathCandidate( filename, pathDirCands )
	if os.path.isfile(pathFileCandidates) == False:			

        	# Build a command for each visual vocabulary configuration that is to be generated
        	parameters = ['-nojvm -nodisplay -nosplash -r "mcg_trecvid_jobarrays(\''+ filename + '\')"']    
    
        	#print parameters
        
        	# Command: matlab -nodesktop -nosplash -r "mcg_trecvid_jobarrays('$INPUT_ITEM')"
        
        	# Run in the computation service
        	run('matlab', parameters ,mem=1000,max_jobs=20,print_command=True)

	else:
		print 'Object candidates already generated for ' + filename
       
    
    print "Extracting object candidates... done."
