#!/bin/sh
#SBATCH --mem-per-cpu=4G

# Example script to run job arrays on GPI system
#
# Detailed instructions for job arrays can be found at the GPI wiki
# https://imatge.upc.edu/trac/wiki/DevelopmentPlatform/ComputingService
#
# This file must be run with the following command:
# sbatch -—array=1-10 WorkerScript

# In order to find out the arrange of numerical IDs for the array, 
# you could count the amount of lines in the input file with the list
# of all files to be processed. You can do it with 
#’wc -l’ (Word Count on the amount of Lines=

# File containing the list of files to process
# You can generate it with the simple makeindex.sh provided or
# with the ‘dataset’ scripts you can find on Pyxel.

FILE_INDEX=/projects/retrieval/trecvid-ins14/list1000_$USER.txt

# Get one entry from the list of files to process
INPUT_ITEM=`sed -ne ${SLURM_ARRAY_TASK_ID}p < $FILE_INDEX`

# List the filename on the job array outputs, stored in the *.out files
echo $INPUT_ITEM

# MATLAB

# Check the following link to learn how to pass Matlab parameters:
# http://www.mathworks.com/matlabcentral/answers/97204-how-can-i-pass-input-parameters-when-running-matlab-in-batch-mode-in-windows

# Example running a Matlab script
matlab -nodesktop -nosplash -r "mcg_trecvid_jobarrays('$INPUT_ITEM')"
