function mcg_trecvid_jobarrays(imname)  
    
    % run('../install.m')
    run('/imatge/asalvador/workspace/MCG-PreTrained/install.m')
   
    savepath='/projects/retrieval/trecvid-ins14/missing_candidates/';
 
    I=imread(imname);
   
    [candidates_mcg, ~] = im2mcg(I,'fast');
   
    savename=strsplit(imname,'/'); 
    savename=savename{length(savename)};
    savename=strsplit(savename,'.');
    savename=savename{1};
    
    save(fullfile(savepath,savename),'candidates_mcg');