from functions import *
from sklearn.preprocessing import Normalizer

# The main objective is to have a list of shot, frame descriptor and query

#########################################################################
# PARAMETERS:
# Subset = True to select path to run experiments on subset features
#          False to select full dataset paths
# 
# Local = True to load local CCN features relative to the candidated
#         False to do the normal ranking based on global CNN features
#
# addCandidates = True to concatene the candides of one image frame
#                 False to add the CCN feats. as new frames
#########################################################################
subset = True
Local = True
addCandidates = False
#########################################################################

# Set the enviroment
if Local:
    str_local = '_local'
else:
    str_local = '_global'

if not subset:
    globalRanking = '/home/eva/CANDIDATES/rankingfile_full'
    path_Query_images = '/data/ins14/topics/tv14.example.images'
    path_global_descriptors = '/home/eva/workspace/videosINS14/descriptors/CNN_features'
    path_local_descriptors = '/home/eva/CANDIDATES/descriptors'
    path_global_Query_descriptors = '/home/amaia/work/output/query_features_CNN/'
    path_local_Query_descriptors = '/home/eva/CANDIDATES/descriptors_queries'
    pathOutput = '/home/eva/CANDIDATES/output'
else:
    path_Query_images = '/projects/retrieval/trecvid-ins14/GT_subset/QueryImages/'
    path_global_descriptors = '/projects/retrieval/trecvid-ins14/GT_subset/MyMeasurements/MCG_segms_20_SIFT_GRAY_mask/'
  #  path_local_descriptors= '/home/eva/workspace/caffe2/python/Candidates/square_p4/'
    path_global_Query_descriptors = '/projects/retrieval/trecvid-ins14/GT_subset/MyMeasurements/query_masks_SIFT_GRAY_mask/'
  #  path_local_Query_descriptors = '/home/eva/workspace/caffe2/python/Candidates/square_p4_queries/'
    pathOutput = os.path.join( os.getcwd(), 'Q3-T3')


if not os.path.isdir( pathOutput ):
    os.mkdir( pathOutput )

pathRankings = os.path.join( pathOutput, 'Rankings' )
if not os.path.isdir( pathRankings ):
    os.mkdir( pathRankings )

output_file = os.path.join( pathOutput,'AP.txt' )
output_file_ranking = os.path.join( pathOutput,'ranking_trecvFormat.txt' )

GT_file = 'ins.search.qrels.tv13'



#query, basename, feat = loadDescriptor( list_file_descriptors_queries[0] )

#nameShots, nameFrames, feats = loadDescriptor( list_file_descriptors[0] )


# BUILD DATASET
# LOAD QUERIES

pickleQueries = os.path.join( pathOutput, 'Q3.pickle')
pickleDataset = os.path.join( pathOutput, 'T3.pickle')


# Load queries
queries, frameQueries, featQueries = initDatabase( path_global_Query_descriptors, pickleQueries )

# Load dataset subset
shots, frames, feats = initDatabase( path_global_descriptors, pickleDataset )


# LOAD DESCRIPTORS
#nameShots, nameFrames, namesFeats = loadDescriptors( path_global_descriptors )


#########################################################################
#########################################################################
#                            MAIN
#########################################################################
#########################################################################


# 1 Normalize all the descriptors (list of descriptors)
feats, featQueries = Normalize_Descriptors( pathOutput, feats, featQueries )
   
    
# DataFrame to loop over queries
data_queries = pd.DataFrame( queries )
data_queries[ 'queries' ] = queries
data_queries[ 'frames' ] = frameQueries
data_queries[ 'feats' ] = featQueries

# Make the array feat descriptors for the full dataset

# List with Average Precision for each query     
ap_list = [] 

# Open file to sabe the AP/query and final mAP
fid = open( os.path.join( pathOutput, 'mAP.txt', 'wb' ) )

# Loop over the queries
for query in np.unique( queries ):
    # Select the query frame descriptors for the query
    QUERIES = np.array( data_queries[ 'feats' ][ data_queries[ 'queries' ] == query ] )

    # Compute distances (matrix where each col are the distances -in the same order- to the target frames)
    distances = 2*pairwise_distances(DESCRIPTORS, QUERIES, metric='cosine')

    # Select the min query distance for each frame
    distances = np.min( distances, axis = 1 )

    # Compute Ranking (list)
    ranking_info = computeRanking(distances,shots,frames,0,1000) # 'numQueries' set to 0 because they are not added to the DESCRIPTORS matrix, namesFrames or namesShots
    
    # Save Ranking
    saveRanking( pathOutput, query, ranking_info)

    # We can only compute mAP for the subset
    if subset: 
        # Compute AP
        ap = AveragePrecision('/home/amaia/work/ins.search.qrels.tv13', query, shots,1000)
        print '\t\t\t\t\t\t', query, ap
        ap_list.append( ap )
        
        # Write file Av Prec.
        fid.write( '%s\t%s\n' % (query, str(ap)) )
    

if subset: 
    print '\t\t\t\t\t\t',"============"
    print '\t\t\t\t\t\t',"mAP", np.mean(ap_list)
        
    fid.write( '============' )
    fid.write( "mAP %s" % str(np.mean(ap_list)))
    fid.close()






'''
# List with Average Precision for each query     
ap_list = [] 

# Output files for AP/query and final Ranking
if subset:
    fid = open( output_file, 'w' )
file_ranking = open( output_file_ranking, 'w' )

# For all the queries...
for query  in queriesID:
    
    # Define the list where we are going to store the descriptors
    # frames and shots ID
    shots = []
    frames = []
    descriptors = []
    d_queries = []


    # Load global and local descriptors for the quer
    g_query, l_query = getQueries( query )
    
    # Load global descriptros for the frames relevants to the query
    g_shot, g_frame, g_d = getGlobals( query )
    
    
    # Load loacl descriptors
    if Local:
        l_shot, l_frame, l_d = getLocals( query )
    
        # Add descriptors (local and global) to the final lists
        if addCandidates:
            
            #Extend list of name Shots
            shots.extend( g_shot )
            shots.extend( l_shot )
            
            # Extend list of name Images
            frames.extend( g_frame )
            frames.extend( l_frame )
            
            # Extend list desciptors
            descriptors.extend( g_d )
            descriptors.extend( l_d )

            d_queries.extend( g_query )
            d_queries.extend( l_query )
        else:
            #CONCATENE
            tmp_global = pd.DataFrame( g_shot )
            tmp_global.columns = ['shots']
            tmp_global['frames'] = g_frame
            tmp_global['descriptors'] = g_d
            
            print 'CONCATENATION======='
            #print '===================='
            #print 'Targets...'
            for (shot, frame, desc_local) in zip(l_shot, l_frame, l_d):
                desc_glob = np.array(tmp_global[tmp_global[ 'shots' ] == shot]['descriptors'] )
                
                des  = np.hstack( (desc_glob[0], np.array(desc_local))  )
                shots.append( shot )
                frames.append( frame )
                descriptors.append( np.hstack( (np.array(desc_glob[0]), np.array(desc_local))  ) )
            
          #  print '===================='
          #  print 'queries...'
            for n in range( len( g_query ) ):
                d_queries.append( np.hstack( (np.array(g_query[n]), np.array(l_query[n])  )  ) )
                
    else:
        # Only global descriptors
        shots.extend( g_shot )
        frames.extend( g_frame )
        descriptors.extend( g_d )
        d_queries.extend( g_query )    
          
    # Build the Matrix
    QUERIES = np.array( d_queries )
    DESCRIPTORS = np.array( descriptors )
        
    #Normalize Code
    nrm = Normalizer(copy=False)
    model_norm = nrm.fit( np.vstack((DESCRIPTORS,QUERIES)) )
    DESCRIPTORS=model_norm.transform(DESCRIPTORS)
    QUERIES=model_norm.transform(QUERIES)
    

    # Compute Distance
    distances = 2*pairwise_distances(DESCRIPTORS, QUERIES, metric='cosine')
    #distances = -np.dot(DESCRIPTORS, QUERIES, metric='cosine')
    
    
    # Dataframe with the distance where each col is the distance of one query-frame  
    data_ = pd.DataFrame( distances )
    labels =['G1', 'G2', 'G3', 'G4',
                    'L1', 'L2', 'L3', 'L4']
    
    # Labels to select Global or Local
    globalCols = labels[:4]
    localCols = labels[4:]
    
    if not Local or not addCandidates:
        # Redefine the labels to only the Global
        labels =globalCols
    
    # Add labels to the DataFrame
    data_.columns = labels
    
    # Add Shots and Frames identifier to the distance matrix
    data_['shots']  = pd.Series( shots, index=data_.index )
    data_['frames'] = pd.Series( frames, index=data_.index )
     
    # Compute the min distance for each shot of all the  
    distance = np.array( data_[ labels ].min( axis=1 ) )
    
    print 'computed distances',len( distance )
    # Compute Ranking
    ranking_info = computeRanking(distance,shots,frames,0,1000) # 'numQueries' set to 0 because they are not added to the DESCRIPTORS matrix, namesFrames or namesShots
    ranking_info = np.array(ranking_info)
    
    
    # Store ranking ingo=======
    shots=ranking_info[2,:]
    frames=ranking_info[1,:]
    distances=ranking_info[0,:]
    
    f= open(os.path.join( pathRankings ,query+'.bin'),'wb')
    pickle.dump(shots,f)
    pickle.dump(distances,f)
    pickle.dump(frames,f)
    f.close()
    # =========================
    
    # We can only compute mAP for the subset
    if subset: 
        # Compute AP
        ap = AveragePrecision('/home/amaia/work/ins.search.qrels.tv13', query, shots,1000)
        print '\t\t\t\t\t\t', query, ap
        ap_list.append( ap )
        
        # Write file Av Prec.
        fid.write( '%s\t%s\n' % (query, str(ap)) )
    
    # Write fileRanking
    rankingfile(file_ranking,query,shots,distances)

if subset: 
    print '\t\t\t\t\t\t',"============"
    print '\t\t\t\t\t\t',"mAP", np.mean(ap_list)
        
    fid.write( '============' )
    fid.write( "mAP %s" % str(np.mean(ap_list)))
    fid.close()
file_ranking.close()

# Distance Matrix
#pairwise_distances(X, Y, metric='cosine'
'''