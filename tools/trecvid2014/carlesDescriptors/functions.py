import sys
import os
import glob
import numpy as np
import pandas as pd
import pickle
import time
import scipy.io


from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import Normalizer

#########################################################################
#########################################################################
#                            FUNCTIONS
#########################################################################
#########################################################################

def loadDescriptor( matlab_file ):
    #Check if it is a query or a image
    basename = ''
    basename = os.path.splitext(os.path.basename( matlab_file ))[0]

    # Load mat
    mat = scipy.io.loadmat( matlab_file )
    dimFeat, numSamples =  np.shape( mat['D'] )
    
    # Change the order (samples, Dimfeats)
    feat = np.reshape( mat['D'], (numSamples, dimFeat) )

    if 'src' in basename:
        query = basename.split('.')[0]
        
        out1 = [query]
        out2 = [basename]
        out3 = [feat]
    
    else:
        shot = basename.split('_')[0]
        frame = basename.split('_')[0] + '_' + basename.split('_')[1]
        
        nameShots = []
        nameFrames = []
        feats = []
        for nCandidate in range( numSamples ):
            nameShots.append( shot )
            nameFrames.append( frame+'_'+str( nCandidate+1 ) )
            feats.append( feat[nCandidate, :] )
        
        out1 = nameShots
        out2 = nameFrames
        out3 = feats
    
    return out1, out2, out3
    
def loadDescriptors( path_to_descriptors ):
    list_1 = []
    list_2 = []
    list_3 = []
    listOfFilesPahts = glob.glob( path_to_descriptors+'*' )
    for (i , filename) in enumerate(listOfFilesPahts):
        t0 = time.time()
        out1, out2, out3 = loadDescriptor( filename )
        print 'Loading ', i+1 , ' of ', len( listOfFilesPahts ), ' time: ', time.time()-t0
        
        list_1.extend( out1 )
        list_2.extend( out2 )
        list_3.extend( out3 )
    return list_1, list_2, list_3
    
def initDatabase( path_to_load, pickleObject ):
    if not os.path.isfile(pickleObject ):
        print 'Loading Queries...'
        t0 = time.time()
        l1, l2, l3 = loadDescriptors( path_to_load )
        print 'Done! ', time.time()-t0
    
        print 'Saving pickle : ', pickleObject
        f = open( pickleObject, 'wb' ) 

        pickle.dump( l1, f )
        pickle.dump( l2, f )
        pickle.dump( l3, f )
        f.close()
    else:
        f = open( pickleObject, 'rb' ) 
        print 'Load pickle : ', pickleObject
        t0 = time.time()
        l1 = pickle.load( f )
        l2 = pickle.load( f )
        l3 = pickle.load( f )
        f.close()
        print 'Done! ', time.time()-t0
    
    return l1, l2, l3
    
def merdeLists(  list_1, list_2 ):
    a1, a2, a3 = list_1
    b1, b2, b3 = list_2
    
    tot1 = []
    tot2 = []
    tot3 = []
    
    tot1.extend( a1 )
    tot1.extend( b1 )

    tot2.extend( a2 )
    tot2.extend( b2 )
    
    tot3.extend( a3 )
    tot3.extend( b3 )
    
    return tot1, tot2, tot3
   
   
def Normalize_Descriptors( pathOutput, feats, featQueries ):
    
    # File with the model
    file_normalization = os.path.join( pathOutput, 'norma_model.bin')
    
    # Convert list into numpy arrays
    DESCRIPTORS = np.array( feats )
    QUERIES = np.array( featQueries )
    
    print 'Trying to read ', file_normalization
    
    # If the file exist load the model
    if  os.path.isfile( file_normalization ):
        print 'Loading pickle...'
        model_norm = pickle.load( open(file_normalization,'rb') )
    
    # If the file does not exit, then compte the model
    else:
        
        print 'Any model saved. Computing model...'
        
        nrm = Normalizer(copy=False)
        model_norm = nrm.fit( np.vstack((DESCRIPTORS,QUERIES)) )
        
        # Saving model
        pickle.dump( model_norm, open( file_normalization, 'wb' ) )

        
    # Normalize 
    DESCRIPTORS=model_norm.transform(DESCRIPTORS)
    QUERIES=model_norm.transform(QUERIES)
    
    
    # return list of descriptors normalized
    return DESCRIPTORS.tolist(), QUERIES.tolist()
                  
       
#    feat = np.reshape( mat['D'], ( 1, np.shape(mat['D'])[0] ) )
   # return feat

#def readRankingFile( globalRanking ):
#    ''' Function to read ranking file into pandas. 2 Columns: query, frame
#    '''
#    data_ranking = pd.read_csv( globalRanking, dtype='str', header = None, sep='\t')
#    data_ranking.columns = ['query', 'frame']
#    #print data_ranking.tail()
#        
#    return data_ranking
#
def computeRanking(distance,namesShots,namesFrames,numQueries,N):
   ''' Compute the ranking given a set of distances for each frame of each shot.
   'distance' : array with the pre computed distances
   'namesShots' : list of unique names of the shots of the database
   'namesFrames' : list of names of the frames to which the distances refer to
   '''

   namesFrames = np.array(namesFrames[0:np.shape(namesShots)[0]-numQueries])
   namesShots = np.array(namesShots[0:np.shape(namesShots)[0]-numQueries])
   distance = np.array(distance[0:np.shape(distance)[0]-numQueries])
   
   data = pd.DataFrame( { 'distance':distance,
                          'shot':namesShots,
                          'frames':namesFrames})
   
   t0 = time.time()

   # Sort the data acording to distances
   data =  data[ ~data['shot'].str.contains('shot0') ]
   t1 = time.time()

   #print "Delete Shot video 0:", t1-t0
   

   # Sort the data acording to distances
   data = data.sort( columns='distance', ascending = True )
   t2 = time.time()
   #print "Time to sort the whole thing:", t2-t1
   
   # Remove rows with duplicated shots,
   # keeping the rows that belongs to the first time
   # that the shot appeared in the sorted data
   data = data.loc[~data['shot'].duplicated()]
   
   
   resultPerShot = list((np.array(data['distance'][:N]),
                    np.array(data['frames'][:N]),
                    np.array(data['shot'][:N])))
   t3 = time.time()
   #print 'Time to remove all the duplicate shots (keeping the first)', t3-t2
   return resultPerShot
   
   
def saveRanking( pathOutput, query, ranking_info):
    # define filename
    filetosave = os.path.join( pathOutput, 'Rankings', query+'.bin')
    print 'Saving ranking... ', filetosave
    
    # convert the list to array
    ranking_info = np.array(ranking_info)

    # Store ranking ingo=======
    shots=ranking_info[2,:]
    frames=ranking_info[1,:]
    distances=ranking_info[0,:]
    
    # Write file
    f= open(filetosave,'wb')
    pickle.dump(shots,f)
    pickle.dump(distances,f)
    pickle.dump(frames,f)
    f.close()

#
def relnotrel( fileGT, id_q, rankingShots ):

    labelRankingShot = []
    t_shot = []
    a = np.loadtxt( fileGT, dtype='string' )
   
    # Extract shots for the query
    t_shot = a[ (a[:,0]==id_q) ]
    
    # Extract relevant shots for the query
    t_shot_rel = t_shot[ t_shot[:,3] == '1' ]
    t_shot_notrel = t_shot[ t_shot[:,3] == '0' ]

    
    # Total Number of relevant shots in the ground truth
    nRelTot = np.shape( t_shot_rel )[0]
    
  
    labelRankingShot = np.zeros((1, len(rankingShots)))
    
    i = 0
    for shotRanking in rankingShots:
        
        if shotRanking in t_shot_rel:
            labelRankingShot[0, i ] = 1

        i +=1   

    
    return labelRankingShot, nRelTot

def AveragePrecision( fileGT, id_q, rankingshots,N):
    
    (relist, nRelTot) = relnotrel( fileGT, id_q, rankingshots )
    
    map_ = 0 
    accu = 0
    numRel = 0
   
    for k in range(min(N,np.shape(relist)[1])):

        if relist[0,k] == 1:
            numRel = numRel + 1
            accu +=1
            map_ += float( accu )/ float(k+1)
   
    return (map_/nRelTot)
    
#def rankingfile(file_to_write,queryname,shots,distances):
#    distances = ( distances.astype(float) - min(distances.astype(float)) )/(max(distances.astype(float))-min(distances.astype(float)))
#    for n in range(np.shape(shots)[0]):
#	file_to_write.write(queryname+'\t') # query name
#	file_to_write.write('0\t') #
#	file_to_write.write(shots[n]+'\t')
#	file_to_write.write(str(n)+'\t')
#	file_to_write.write(str(1 - distances[n]) +'\t')
#	file_to_write.write('DCU\t')
#	file_to_write.write('\n')
#            
#
#
## Make a a list of the queries:
#data_ranking = readRankingFile( globalRanking )
#queryID = data_ranking['query']
#queriesID = np.unique( queryID )
#
## Make a list with the path to the query images 
#listQueryFiles = []
#for queryID in queriesID:
#    #print queryID, ' ', len(glob.glob(path_Query_images+'/'+queryID+'*src*'))
#    listQueryFiles.extend(glob.glob(path_Query_images+'/'+queryID+'*src*'))
#   # print queryID, ' NUM SHOTS: ', np.shape( data_ranking['query'] )[0]
#
#
##======================================
#
##LOAD GLOBAL DESCRIPTORS
#
#def getGlobals( query ):    
#    print 'Loading Global descriptors from disk...'
#    t0 = time.time()
#    count = 1
#    list_globalFeats = []
#    list_globalFeats_shots = []
#    list_globalFeats_frames = []
#
#    # load descriptors for the ranking image:
#    for frame in data_ranking[data_ranking['query']==query]['frame']:
#        shot = frame.split('/')[0]
#        frame = frame.split('/')[1]
#        if not subset:
#            path_frame = os.path.join( path_global_descriptors, shot,frame+'.obj' )
#        else:
#            path_frame = os.path.join( path_global_descriptors, shot,frame.split('.')[0]+'.obj' )
#
#        #print count, ' of ', len(data_ranking[data_ranking['query']==query]['frame'])
#        list_globalFeats.append( pickle.load( open( path_frame ,'rb') ) )
#        list_globalFeats_shots.append( shot )
#        list_globalFeats_frames.append( frame )
#        count +=1
#
#    # Once the list is generated save to disk
#    #  pickle.dump( list_globalFeats, open( 'list_globalFeats'+query+'.obj', 'wb' ) )
#    print 'LOADED', len(list_globalFeats), time.time()-t0, '\n'
#
#    return list_globalFeats_shots, list_globalFeats_frames, list_globalFeats
#    
#        
# 
##======================================
#
##LOAD LOCAL DESCRIPTORS
#
#def getLocals( query ):
#    fout = open( logFile , 'wa')
#    print 'Loading Local descriptors from disk...'
#    t0 = time.time()
#    
#    count = 1
#    list_localFeats = []
#    list_localFeats_shots = []
#    list_localFeats_frames = []
#
#    # load descriptors for the ranking image:
#    for frame in data_ranking[data_ranking['query']==query]['frame']:
#        shot = frame.split('/')[0]
#        frame = frame.split('/')[1]
#
#        path_frame = os.path.join( path_local_descriptors, shot,frame.split('.')[0]+'_*' )
#        
#        if len(glob.glob( path_frame )) !=20:
#            fout.write(query+'\t'+frame+'\n')
#
#        count_cand = 0
#        listCandidates = []
#        list_cand_shot = []
#        list_cand_frame = []
#        for candidate in glob.glob( path_frame ):
#            count_cand +=1
#            list_cand_shot.append( shot )
#            listCandidates.append( pickle.load( open(candidate,'rb') ) )
#            list_cand_frame.append( frame )
#                    
#        list_localFeats.extend( listCandidates )
#        list_localFeats_shots.extend( list_cand_shot )
#        list_localFeats_frames.extend( list_cand_frame )
#
#        #print 'loaded, ', len(glob.glob( path_frame ))
#        
#        #print count, ' of ', len(data_ranking[data_ranking['query']==query]['frame'])
#        count +=1
#
#    # Once the list is generated save to disk
#    #   pickle.dump( list_localFeats, open( 'list_localFeats'+query+'.obj', 'wb' ) )
#    #print 'SAVED'
#    fout.close()
#    print 'LOADED', len(list_localFeats), time.time()-t0, '\n'
#
#  
#    return list_localFeats_shots,list_localFeats_frames, list_localFeats
#        
#
##======================================
#
##LOAD QUERIES DESCRIPTORS
#
#def getQueries( query ):
#
#    list_globalFeats_queries = []
#    list_localFeats_queries = []
#    
#    pathGlobal = glob.glob( os.path.join(path_global_Query_descriptors, query+'*' ) )
#    pathLocal = glob.glob( os.path.join(path_local_Query_descriptors, query+'*' ) )
#    
#    for frame in range( np.shape(pathLocal)[0] ):
#        list_globalFeats_queries.append( pickle.load(open( pathGlobal[frame],'rb') ) )
#        list_localFeats_queries.append( pickle.load(open( pathLocal[frame],'rb') ) )
#    
#    
#    return list_globalFeats_queries, list_localFeats_queries