import numpy as np
caffe_root = '/usr/local/opt/caffe/'

# Make sure that caffe is on the python path:
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe

def loadNetCNN(deploy_file, model_file, mean_file):
	net = caffe.Classifier(deploy_file,
		               model_file, gpu=True)
	net.set_phase_test()
	net.set_mode_gpu()
	# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
	net.set_mean('data', mean_file)  # ImageNet mean
	net.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB
	net.set_input_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
	return net

def extract_CNN_feature( net, imagepath ):

	scores = net.predict([caffe.io.load_image(imagepath)])

	feat = net.blobs['fc7'].data
	feat = np.reshape(feat, (10,4096))

	meanfeat = np.average( feat, axis = 0 )

	return meanfeat

if __name__ == '__main__':

    mean_file = caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy'
    model_file = '/imatge/asalvador/caffe/models/mediaeval/mediaeval_iter_2000.caffemodel'
    deploy_file = '/imatge/asalvador/caffe/models/mediaeval/deploy.prototxt'
    image_path = '/imatge/asalvador/work/trecvid/queries/9069.1.src.bmp'

    net = loadNetCNN(deploy_file,model_file,mean_file)

    feat = extract_CNN_feature(net,image_path)

    print feat, np.shape(feat)