import numpy as np
import evaluation as eval
import ranking as rk

'''
The purpose of this file is to create two txt files for training and testing a CNN with data from TRECVID INS 2013. 
We get the relevant keyframes for each one of the queries and take half for testing and half for training.
'''

data_path = '/imatge/asalvador/work/trecvid/'
database = data_path + 'gt_imgs/'
queries = data_path + 'queries/'

ground_truth_file = data_path + 'ins.search.qrels.tv13'
descriptors_matrix = data_path + 'DESCRIPTORS_subset.bin'

if ___name___ == '__main__':

   queries_2013 = range(9069,9099)

   descriptors, shot_ids, keyframe_ids, n_queries = rk.LoadFeaturesGroundTruthSubset(descriptors_matrix,include_queries=True)
   
   label = 0
   
   for query in queries_2013:
       
       # 'labels' will contain the positions of the relevant images
       labels, num_relevant = eval.relnotrel(ground_truth_file, str(query), shot_ids)
       
       # TODO: add the 4 query images to training/testing data (2 and 2)
       
       for i in range(num_relevant):
       
           if i<num_relevant/2: # TODO: Store as training examples
           
           else: # TODO: Store as testing examples
       
       label += 1