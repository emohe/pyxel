# -*- coding: utf-8 -*-
import numpy as np
#import matplotlib.pyplot as plt
import os
import pickle
import timeit
import operator
import sys
import multiprocessing as mp
from sklearn.preprocessing import Normalizer
import time
from cropImages import *
import glob


# Make sure that caffe is on the python path:
#caffe_root = '/home/eva/workspace/caffe2/'  # this file is expected to be in {caffe_root}/examples
#import sys
#sys.path.insert(0, caffe_root + 'python')

#import caffe


def loadNetCNN():
	net = caffe.Classifier( caffe_root+ 'examples/imagenet/imagenet_deploy.prototxt',
		               caffe_root + 'examples/imagenet/caffe_reference_imagenet_model', gpu=True)
	net.set_phase_test()
	net.set_mode_gpu()
	# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
	net.set_mean('data', caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy')  # ImageNet mean
	net.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB
	net.set_input_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
	return net

#net = loadNetCNN()


def createDir( directory ):
	if not os.path.exists(directory):
		os.makedirs(directory)
		print 'Create dir: ', directory
	return directory


# CNN feature given an image path
def computeDLfeature( imagepath, filetosave, crop_flag=False, save_flag=True ):
	''' Compute the CNN features for an image
	arg: Path to the image
	return meanfeat (4096D)
	'''
	t0 =time.time()
	if crop_flag:
		fileMask = imagepath.split('src')[0]+'mask.bmp'
		
		crop = bbQuery( imagepath, fileMask )
		
		cv2.imwrite('tmpCrop.png',crop)

		scores = net.predict( [caffe.io.load_image('tmpCrop.png')] )
		
	else:
	        t0 = time.time()
		scores = net.predict([caffe.io.load_image(imagepath)])
		#print 'exact time CNN extraction: ', time.time()-t0

	feat = net.blobs['fc7'].data

	feat = np.reshape(feat, (10,4096))
	meanfeat = np.average( feat, axis = 0 )
        
	if save_flag:
		pickle.dump(meanfeat, open( filetosave, 'w' ))
		print 'saved - ', filetosave
        
        print imagepath, ' TIME:', time.time()-t0 
	return meanfeat



def getFeaturesCNN(allShotsPath, tmpPath ,runmultiprocess=False):
	'''getFeatures returns a matrix of all features from all images in the training set

	#Note: the double loop is because of the path structure of the dataset. allShotsPath contains a separate folder for every shot, each 		containing several frames.'''


	if runmultiprocess: 
		print 'MULTIPROCESS MODE'
		pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
		PoolResults = []

	start = timeit.default_timer()
	print 'Start timer...', start

	# Get the name of the shots
	for shot in os.listdir( allShotsPath ):
    
		#Define out dir for the features, organized by shots
		outDir = ''
		outDir = os.path.join( tmpPath, shot )
		createDir( outDir )

		# Get the name of the frames for one shot
		listFrameShot = os.listdir(os.path.join(allShotsPath, shot) )
    
    
		for frame in listFrameShot:
			
			imagePath = ''
			imagePath = os.path.join( allShotsPath, shot, frame )
			filetosave = os.path.join( outDir, frame.split('.bmp')[0]+'.obj' )

			if not os.path.exists( filetosave ):
				try: 

					#and get the descriptors for it
					if runmultiprocess: 
						pool.apply_async(computeDLfeature, args=( imagePath, filetosave ))

					else:    
						computeDLfeature( imagePath, filetosave )				


				except:
					fid = open( 'FeatureExtractionCNN_LOG.txt', 'a' )
					fid.write( imagePath +'\n' )
					fid.close()
	if runmultiprocess:
           	pool.close()
           	pool.join()

def wait():
    if raw_input("Press any key")  == 'q':
        sys.exit()



def loadFeatures( allShotsPath, tmpPath, runmultiprocess=False ):
    ''' Load features from disk. First path non relevant (only it is used to compare
        the total amount number of images and the features computed).
        It returns:
                Descriptors, nameFrames, name Shot. All of them arrays.
    '''
    
    if runmultiprocess: 
        print 'MULTIPROCESS MODE'
	pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
    


    descriptors = []
    namesFrames = []
    namesShots = []
    
    numDescriptors = 0
    
    
    for shot in os.listdir( tmpPath ):
        readDir = ''
        readDir = os.path.join( tmpPath, shot )
        
        # Get the name of the frames for one shot
        listFrameShot = os.listdir( os.path.join(tmpPath, shot) )
        
        if len(listFrameShot)>0:
            # Read all the frame descriptors from disk
            for frame in listFrameShot:
                imagePath = ''
                imagePath = os.path.join( tmpPath, shot, frame )
                
                # Define Output File
                filetoread = os.path.join(readDir, frame)            
                
                try:
                    
                    feat = pickle.load( open( filetoread, 'r' )) 
                   # print frame, ' loaded'
 	    	 
                    descriptors.append( feat )
                    namesFrames.append( frame.split('.')[0] )
                    namesShots.append( shot )
     		    print numDescriptors
		    numDescriptors = numDescriptors + 1
                except:
                    print frame, 'error'
    

    
    return descriptors, namesFrames, namesShots



if __name__ == '__main__':

    loadFeatures( '/home/eva/AllCandidates', '/home/eva/work/output/descriptors_candidates/CNN')
   # print glob.glob('/home/eva/work/output/descriptors_candidates/CNN/shot206_560/*')
    #a = pickle.load( open( '/home/eva/work/output/descriptors_candidates/CNN/shot206_560/shot206_560_1.obj', 'r' )) 
    #print np.unique(a)
