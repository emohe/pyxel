from bow_tools import *
from general_tools import *
from eval_tools import *
from sklearn.preprocessing import Normalizer
from product_quantization import *


#For multi-thread computing of the features
runmultiprocess=True


#Bool indicators - to specify which parts of the code should run

featureextraction=False
sampled=False
need_to_build_X=True
clustering=False
assignment=True # required for PQ od CNN
query=False
pq=False
computePQ = False
normalizeCNN = False

cropCNN = False
candidates = False


#========================= Step 0. Paths and parameters ========================

if candidates:
    LibraryDir='/home/eva/AllCandidates'
else:
    LibraryDir='/home/amaia/work/gt_imgs/'
    
queryDir='/home/amaia/work/queries'
fileGT='/home/amaia/work/ins.search.qrels.tv13'
outpath='/home/amaia/work/output'



# Parameters - features

feattype='BoW' # 'BoW' or 'CNN'

detector='SIFT'
descriptor='SIFT'
codesize=4096
nPts=200 # percentage of keypoint features used for clustering

#PQ parameters
n_splits=8
codesize_PQ= 256


if feattype=='CNN':
    from CNN_tools import *

#Create generic paths if unexistent)
createDir(outpath)
prep_path = createDir( os.path.join(outpath, 'pre-processing' ) )

if feattype=='BoW':       
    if pq: 
        if cropCNN:
        
            temp_path = createDir( os.path.join(outpath, 'temp_'+detector + '_' + descriptor +'_cropped_PQ_'+str(codesize_PQ)+'_'+str(n_splits ) ) )
        else:
            temp_path = createDir( os.path.join(outpath, 'temp_'+detector + '_' + descriptor +'PQ_'+str(codesize_PQ)+'_'+str(n_splits ) ) )

    else:
        if cropCNN:
            temp_path = createDir( os.path.join(outpath, 'temp_'+detector + '_' + descriptor + '_cropped') )
        else:
            temp_path = createDir( os.path.join(outpath, 'temp_'+detector + '_' + descriptor) )
else:
    
    if pq:
        if candidates:
            temp_path = createDir( os.path.join(outpath, 'temp_' + feattype + 'PQ_'+str(codesize_PQ)+'_'+str(n_splits) +'_candidates') )
        else:
            temp_path = createDir( os.path.join(outpath, 'temp_' + feattype + 'PQ_'+str(codesize_PQ)+'_'+str(n_splits)) )
    else:
        if candidates:
            temp_path = createDir( os.path.join(outpath, 'temp_' + feattype +'_candidates') )

        else:
            temp_path = createDir( os.path.join(outpath, 'temp_' + feattype ) )

cleanDir(temp_path) # clean temp path before the experiment


rankings_path = createDir( os.path.join(outpath, 'rankings' ) )
if feattype=='BoW':
    if pq:
        rankings_path = createDir( os.path.join(rankings_path, detector +'_'+descriptor+'_pq'+'_'+str(codesize_PQ)+'_'+str(n_splits) ))
    else:
        rankings_path = createDir( os.path.join(rankings_path, detector +'_'+descriptor ))
else:
    if pq:
        rankings_path = createDir( os.path.join(rankings_path, feattype+'_pq'+'_'+str(codesize_PQ)+'_'+str(n_splits) ))
    else:
        rankings_path = createDir( os.path.join(rankings_path, feattype ))



if feattype=='BoW': # run this part to store keypoint features and the codebook

    createDir(os.path.join(outpath,'kpfeats'))
    kpfeatpath=os.path.join(outpath,'kpfeats',detector + '_' + descriptor)
    clust_outfile=os.path.join(outpath,'Clustering'+detector + '_' + descriptor + '.km')
    createDir(kpfeatpath)
    
createDir(os.path.join(outpath,'descriptors'))

if feattype=='BoW': # set up the name of the folder where descriptors will be stored
  
    descriptorsdir=os.path.join(outpath,'descriptors',detector + '_' + descriptor)
else:
    if candidates:
        descriptorsdir=os.path.join(outpath,'descriptors', feattype+'_candidates')
    else:
        descriptorsdir=os.path.join(outpath,'descriptors',feattype)

createDir(descriptorsdir)


if pq:
  
    createDir(os.path.join(outpath,'pq'))
    if feattype=='BoW': # same for PQ
        pqdir = createDir(os.path.join(outpath,'pq',detector + '_' + descriptor))
    else:
        if candidates:
            pqdir = createDir(os.path.join(outpath,'pq',feattype, '_candidates'))
        else:
            pqdir = createDir(os.path.join(outpath,'pq',feattype))

     

#========================= Step 1. Features on image library ===================

# 1.1 Store features all together


if featureextraction:
    print "Extracting features..."
    ts=time.time()
    
    if feattype=='BoW':
        getFeatures(LibraryDir,detector,descriptor,kpfeatpath,runmultiprocess)
    else:
        getFeaturesCNN(LibraryDir, descriptorsdir ,runmultiprocess)
    te=time.time()

    print "Features stored. Elapsed time: ", te-ts
    
if clustering and feattype=='BoW':
   
    print "Loading data for clustering ..."
    if need_to_build_X:

        X = loadXforClustering(kpfeatpath,nPts)
        
        #print "X is being built and stored..." 
        #pickle.dump(X,open(os.path.join(outpath,'X_' + detector + '_' + descriptor+'.obj'),'w'))
    else:
        print "X is being loaded from disk..."
        X=pickle.load(open(os.path.join(outpath,'X_' + detector + '_' + descriptor+'.obj'),'r'))
 
    # 1.2 Clustering

    print "Clustering... this might take a while :)"

    getVocabulary(X,codesize,clust_outfile)
    

if assignment:

    # 1.3 Compute histogram of occurrences for each image and save them to disk
    if feattype=='BoW':
        print "Assigning..."
        getAssignments(LibraryDir,detector,descriptor,kpfeatpath,descriptorsdir,clust_outfile,codesize,runmultiprocess)
    
if pq and computePQ:
    print "PRODUCT QUANTIZATION"
    print "Loading descriptors from: ", descriptorsdir
    if feattype=='BoW':
        DESCRIPTORS , namesFrames, namesShots = loadDescriptors(descriptorsdir)
        DESCRIPTORS = np.resize( DESCRIPTORS, (np.shape(DESCRIPTORS)[0], np.shape(DESCRIPTORS)[2]))
    else:
        DESCRIPTORS , namesFrames, namesShots = loadFeatures( LibraryDir, descriptorsdir )     
    print "Done. Computing and storing pq vectors..."

    print np.shape(DESCRIPTORS)
    computeProductQuantization(DESCRIPTORS, namesFrames, namesShots, LibraryDir, descriptorsdir, codesize_PQ, n_splits , pqdir)

    
if query:

    #========================= Step 2. Query evaluation ============================

    # 2.1 Load all descriptor
    
    if pq:
        descriptorsdir =  os.path.join( pqdir, 'pqvectors_'+str(codesize_PQ)+'_'+str(n_splits))
    
    
    print "Loading descriptors..."
    if feattype=='BoW':
        DESCRIPTORS , namesFrames, namesShots = loadDescriptors(descriptorsdir)
        DESCRIPTORS = np.resize( DESCRIPTORS, (np.shape(DESCRIPTORS)[0], np.shape(DESCRIPTORS)[2]))
    else:
        DESCRIPTORS , namesFrames, namesShots = loadFeatures( LibraryDir, descriptorsdir ) 
        #DESCRIPTORS , namesFrames, namesShots = loadFeatures( LibraryDir, '/home/eva/output/CNN/features_dataset' ) 
        
        if normalizeCNN:
            # Normalize Features
            model_Norm = Normalizer().fit( DESCRIPTORS  )
            DESCRIPTORS = model_Norm.transform( DESCRIPTORS )
        
            filetosave = os.path.join( prep_path, feattype+'normalizer.obj')
            pickle.dump(model_Norm, open( filetosave, 'w' ))        
    
    namesShot_hash = []
    for shot in namesShots:
        namesShot_hash.append( hash(shot) )
    namesShot_hash = np.array( namesShot_hash )
            
    if feattype=='BoW':       
        if pq: 
            fid = open( os.path.join(outpath , detector + '_' + descriptor +'PQ_'+str(codesize_PQ)+'_'+str(n_splits)+ '_mAp.txt'), 'w' )
        else:
            fid = open( os.path.join(outpath , detector + '_' + descriptor + '_mAp.txt'), 'w' )
    else:
        
        if pq:
            if candidates:
                fid = open( os.path.join(outpath , feattype + 'PQ_'+str(codesize_PQ)+'_'+str(n_splits)+ '_candidates_mAp.txt'), 'w' )
            else:
                fid = open( os.path.join(outpath , feattype + 'PQ_'+str(codesize_PQ)+'_'+str(n_splits)+ '_mAp.txt'), 'w' )
        else:
            if candidates:
                fid = open( os.path.join(outpath , feattype +'_candidates_'+ '_mAp.txt'), 'w' ) #------> CUIDAO
            else:
                fid = open( os.path.join(outpath , feattype +'_mAp.txt'), 'w' ) #------> CUIDAO

    print "Done. Starting with queries..."

    # 2.2 Get rankings for all queries and evaluate it
    
    #queries = getFramesQueries(queryDir) # returns the absolute paths of all query images to use
    queries = getNFrames( queryDir, 10 )
    results=[]
    names=[]
    
    count =1
    for query in queries:
        
        name=query.split('.')[0]
        print 'Query %d of %d' %( count, len(queries) )
        if feattype=='BoW':
            
            feats = featQuery( os.path.join( queryDir, query ) ,name,detector,descriptor,clust_outfile,codesize,cropCNN)
        else:
            feats = computeDLfeature( os.path.join( queryDir, query ), '', crop_flag=cropCNN, save_flag=False ) # CNN feature!
            feats = np.resize( feats, (1, np.shape(feats)[0]) )

            if normalizeCNN:
                feats = model_Norm.transform( feats )
            
        
        if pq:
            #encode feats with the pq codebook
            filecodebook = 'pq_codebook_'+str(codesize_PQ)+'_'+str(n_splits)+'.pkl'
            
            f = open(os.path.join(pqdir, filecodebook), 'rb') # load it
            CODEBOOK = pickle.load(f)
            f.close()
            
            # Quantize the query vector
            feats = quantize_vector(feats, n_splits ,CODEBOOK, codesize_PQ)
            
     
        if feats is not None:
        
      
            if pq:
        
                ranking = pq_query(feats,CODEBOOK,os.path.join( pqdir, 'pqvectors_'+str(codesize_PQ)+'_'+str(n_splits)),n_splits,codesize_PQ)        
            else:
                ranking = computeRankingShots( feats, name, DESCRIPTORS, namesShots,namesShot_hash, namesFrames, '','l2',False)
   
            
            # Save provisional results for all frames
            filesave = open( os.path.join(temp_path, query.split('.')[0]+'_'+query.split('.')[1]+'.obj'),'w' )
            pickle.dump( ranking, filesave )
            filesave.close()
        
        count +=1
            
    print '\n'
    print 'Merging queries...'
    print '================='

    mergeQueries(temp_path, rankings_path)
    print 'Query \t AP'
    print '================='
    
    fid .write( 'Query \t AP\n' )
    fid .write('=================\n')
    
    for fileRanking in os.listdir( rankings_path ):
        filetoread = os.path.join( rankings_path,fileRanking)
        nameQuery = fileRanking.split('.')[0]
        r = np.array(pickle.load( open(filetoread, 'r' ) ))
        ap = AveragePrecision( fileGT, nameQuery, r[:,2] )

        print "%s \t %0.3f" % (nameQuery, ap )
        
        fid.write( nameQuery )
        fid .write( str(ap) )
        fid .write( '\n' )

        results.append( ap )
        names.append( nameQuery )
    
    print '================='
    print 'mAP \t %.03f' % (np.average( np.array( results ) ))
    
    fid .write( '=================\n')
    fid .write( 'mAP \t %.03f\n' % (np.average( np.array( results ) )))
    fid.close()  
