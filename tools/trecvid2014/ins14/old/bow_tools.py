import numpy as np
from general_tools import *
import os                           
from sklearn.preprocessing import Normalizer
import time 
import pickle
from scipy.cluster.vq import *
from sklearn.cluster import MiniBatchKMeans
import multiprocessing as mp
import sys

sys.path.insert(0,'/home/amaia/workspace/opencv-2.4.7/release/lib')
import cv2 


def computeFeatures(impath,detector,descriptor,savepath,imfile,savemode=False,cropped=False,sampled=False,nPts=200):
    #computeFeatures returns the descriptors for an image given a keypoint (SIFT,SURF,HARRIS,FREAK,ORB,MSER,Dense ...) detector and a descriptor type (SURF,SIFT,BRIEF,...). 
   
    img = cv2.imread(impath)
    
    if cropped:
        print "I am in cropping mode!"
        fileMask = impath.split('src')[0]+'mask.bmp'
		
        img = bbQuery( impath, fileMask )
    #Create detector object
    detector = cv2.FeatureDetector_create(detector)
    
    #sift = cv2.SIFT()
    #kp, descriptors = sift.detectAndCompute(gray,None)
    
    #Run the detector on the image
    keypoints = detector.detect(img,None)
    
    #Create the extractor object
    extractor = cv2.DescriptorExtractor_create(descriptor)
    
    #Compute features in keypoints
    keypoints, descriptors = extractor.compute(img, keypoints)
    
    if sampled:
        descriptors=randSubsample(descriptors, nPts)
        
    if descriptors is not None and savemode==True: #ignore it if no descriptors are returned
    
        pickle.dump(descriptors,open(os.path.join(savepath,imfile.split('.')[0]+'.obj'),'w'))
        print "Saved descriptor as: ", os.path.join(savepath,imfile.split('.')[0]+'.obj')
 
    return descriptors
    

def getFeatures(allShotsPath,detector,descriptor,savepath,runmultiprocess,sampled=False,nPts=200):
    #getFeatures returns a matrix of all features from all images in the training set

    #Note: the double loop is because of the path structure of the dataset. allShotsPath contains a separate folder for every shot, each containing several frames.

    if runmultiprocess: 
        pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)

    count=0
    
    shotdir=os.listdir(allShotsPath)
    
    
    #For every shot...
    for shot in shotdir:
        #...get the frames
        framelist=os.listdir(os.path.join(allShotsPath,shot))

	      #And for every frame...
        for imfile in framelist:
            count+=1
            
            #Read it
            impath=os.path.join(allShotsPath,shot,imfile)
          
            #and get the descriptors for it
            if runmultiprocess: 
                pool.apply_async(computeFeatures, args=(impath,detector,descriptor,savepath,imfile,True,False,sampled,nPts))
                
            else:    
                computeFeatures(impath,detector,descriptor,savepath,imfile,True,False,sampled,nPts)
                
    pool.close()
    pool.join()            
    print "Number of frames", count
    
    
      
def randDescriptor(featpath,feat,nPts):
    
    descriptors=pickle.load(open(os.path.join(featpath,feat),'r'))
    
    feats=randSubsample(descriptors,nPts)
    
    return feats
          
        
def loadXforClustering(featpath,nPts):

    idx=0

    for feat in os.listdir(featpath):
       
        descriptors=pickle.load(open(os.path.join(featpath,feat),'r'))
        descriptors=randSubsample(descriptors,nPts)
            
        if descriptors is not None:
            if idx==0:
            
                X=descriptors
                
            else:
                X=np.vstack((X,descriptors))
                
            idx += 1
            print idx
            
 
    print "Done. Ready for clustering."
    
    return X
    
def loadDescriptors(descriptorsPath):
    
    
    DESCRIPTORS=[]
    namesFrames=[]
    namesShots=[]
    
    descrDir=os.listdir(descriptorsPath)
    
    #For every shot folder containing descriptors...
    for fol in descrDir:
        #...get the list of their names.
        descrList=os.listdir(os.path.join(descriptorsPath,fol))
        
        
	      #And for each one of them...
        for frameDescr in descrList:
            
            #Load it
            feat = pickle.load(open(os.path.join(descriptorsPath,fol,frameDescr),'r'))
            
            DESCRIPTORS.append(feat)
            namesFrames.append(frameDescr.split('.')[0])
            namesShots.append(fol)
            
    return np.array(DESCRIPTORS),np.array(namesFrames),np.array(namesShots)
    
       
def getVocabulary(X,codesize,clustfile):

    print "Normalizing..."
    n = Normalizer()
    model_norm = n.fit(X)
    X = model_norm.transform(X)
    #X,meanX,stdX = normalizeFeat(X)
    
    print "Done normalizing!"

    print "Clustering..."
    
    ts=time.time()

    centroids,variance = kmeans(X,codesize,1,10-3)

    te=time.time()
    
    f = open(clustfile, 'wb');
    pickle.dump(centroids,f);
    pickle.dump(model_norm,f);
    f.close() 

    print "Codebook stored! Kmeans time: ", te-ts


def getAndStoreBoW(impath,detector,descriptor,imfile,outDir,model_norm,centroids,codesize):

    descriptors = computeFeatures(impath,detector,descriptor,'',imfile) 
    
    descriptors = model_norm.transform(descriptors)
    
    code,distance = vq(descriptors,centroids)
    hist = np.zeros((1,codesize))
            
    #Build histogram of occurrences
    for c in code:
        hist[0,c-1] = hist[0,c-1] + 1  
    feats = hist/(sum(hist)+0.0000000001) 
    f = open(os.path.join(outDir, imfile.split('.')[0]+'.obj'), 'wb');
	
    pickle.dump(feats,f)
    f.close()
    print "Saved", os.path.join(outDir, imfile.split('.')[0]+'.obj')
    
def getAssignments(allShotsPath,detector,descriptor,loadpath,savepath,centroidsfile,codesize,runmultiprocess):
  
    f = open(centroidsfile, 'r')
    centroids = pickle.load(f)
    model_norm = pickle.load(f)
    f.close()
    if runmultiprocess: 
        pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)

    #Re-compute features for all the frames
    for shot in os.listdir(allShotsPath):
        
        outDir=os.path.join(savepath,shot)
        createDir(outDir)
        
        for imfile in os.listdir(os.path.join(allShotsPath,shot)):
            print "Processing ", imfile
            if os.path.isfile(os.path.join(loadpath,imfile.split('.')[0]+'.obj')):
	        if runmultiprocess:
		    pool.apply_async(getAndStoreBoW, args=(os.path.join(allShotsPath,shot,imfile),detector,descriptor,imfile,outDir,model_norm,centroids,codesize))
		else:
	            getAndStoreBoW(os.path.join(allShotsPath,shot,imfile),detector,descriptor,imfile,outDir,model_norm,centroids,codesize)
    pool.close()            
    pool.join()

def featQuery(queryDir,queryfile,detector,descriptor,clusteringfile,codesize,cropped):
    
    f=open(clusteringfile,'r')
    centroids= pickle.load(f)
    model_norm= pickle.load(f)
   
    f.close()

    descriptors=computeFeatures(queryDir,detector,descriptor,'',queryfile,False,cropped)

    #Normalize the descriptors according to the mean and variance
   
    
    if descriptors is not None:
    
        descriptors = model_norm.transform(descriptors)

        #Get the code indicating where each feature falls
            
        code,distance=vq(descriptors,centroids)

        hist = np.zeros((1,codesize))
            
        #Build histogram of occurrences
        for c in code:
            hist[0,c-1] = hist[0,c-1] + 1
            
        #Normalize histogram          
        feats = hist/(sum(hist)+0.0000000001) 
    else:
        feats=None
    
    return feats
    

def chooseQueryfiles(querypath):

    allfiles=os.listdir(querypath)
    queries=list()
    
    for item in allfiles:
    
        query_partitioned = item.split('.')
        
        indic=query_partitioned[1]
        mask_or_src=query_partitioned[2]
        
        if (indic=='1' and mask_or_src=='src'): # PROVISIONAL! In the future we need to use all the query files
            queries.append(os.path.join(querypath,item))
            
    return queries
            
            
    
    
    
    
