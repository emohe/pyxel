import numpy as np
from multiprocessing import Process, cpu_count, Pool, Array, sharedctypes
import pickle
import time
import os
import ctypes as c
import warnings

# Global paths
which_data = 'full'
descriptors_file = '/home/amaia/work/output/DESCRIPTORS_'+which_data + '.bin'
distances_path = '/home/amaia/output/CNN_distances/'+which_data + '/'


def load_descriptors():
    
    f = file(descriptors_file,'rb')
    
    DESCRIPTORS = np.ctypeslib.as_ctypes(np.load(f))
    namesFrames = np.load(f)
    #namesShots = np.load(f)
    #numQueries = np.load(f)
   
    shared_array =sharedctypes.Array(DESCRIPTORS._type_,DESCRIPTORS,lock=False)
    
    f.close()

    return shared_array, namesFrames


def compute_and_store_distance(n,splitsize):

    with warnings.catch_warnings():
        warnings.simplefilter('ignore', RuntimeWarning)
        d = np.ctypeslib.as_array(D)
   
    distances = -np.dot(d,d[n:min(np.shape(d)[0],n+splitsize)].T)
    
    pickle.dump(distances,open(distances_path+ str(n)+'.bin','wb'))
    print "Stored for", n
    

def _init(arr):
    global D
    D = arr  
    
def compute_distances(runmultiprocess=True):
    splitsize = 10
    print "Loading descriptors..."
    DESCRIPTORS, namesFrames= load_descriptors()
    print "Done."
    
  
    range_ = range(0,np.shape(namesFrames)[0],splitsize)
    #print np.shape(range_)


    print "Creating pool"
    print cpu_count()
    pool = Pool(processes = cpu_count(), initializer = _init,initargs=(DESCRIPTORS,))
    print "Storing distances..."
    for n in range_:
        if runmultiprocess:
	    
	    pool.apply_async(compute_and_store_distance, args = (n,splitsize) )
        else:
  	    compute_and_store_distance(n,os.path.join(distances_path,namesFrames[n] + '.obj') ) 
	    
	
    pool.close()
    pool.join()
    print "Done"
    

if __name__ =='__main__':
    ts = time.time()
    compute_distances()
    print "Elapsed time:", time.time() - ts 
