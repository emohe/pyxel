import cv2
import numpy as np
#from matplotlib import pyplot as plt

import scipy.io
from CNN_tools import *
# Make sure that caffe is on the python path:
caffe_root = '/home/eva/workspace/caffe2/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')
#import caffe



fileQuery = '/home/amaia/work/queries/9070.1.src.bmp'
fileMask = '/home/amaia/work/queries/9070.1.mask.bmp'

def bbQuery( fileQuery, fileMask ):
    img = cv2.imread( fileQuery )
    #img = caffe.io.load_image( fileQuery )
    mask_ = cv2.imread( fileMask )
    #mask_ = caffe.io.load_image( fileMask )

    mask_ =mask_[:,:,0] 
    
    mask_[mask_<255]=0 

    y, x = np.where(mask_==255)
    
    x_min = np.min( x )
    y_min = np.min( y )
	
    x_max = np.max( x )
    y_max = np.max( y ) 

    crop = img[y_min:y_max, x_min:x_max,:]
    
    #cv2.rectangle(img, (x_min,y_min), (x_max, y_max), (0,255,0),3)
    return crop


def getCandidates_of_One_Image( nameQuery, imagePath, pathCandidates, outPath ):
    
    img = cv2.imread( imagePath )
    
    #mat = scipy.io.loadmat('/home/eva/MCG_trecvid_queries/9069.1.src.mat') 
    mat = scipy.io.loadmat(pathCandidates + nameQuery +'.mat') 
    allCandidatesImage = mat['candidates_mcg']
  
    mat_coordinates_candidates = allCandidatesImage['bboxes'][0][0]
    
    print 'Num candidats', np.shape(mat_coordinates_candidates)[0]
    numCandidate = 1
    
    for i in range( np.shape(mat_coordinates_candidates)[0] ):
        # Get coordinates
        y = mat_coordinates_candidates[i,0]
        x = mat_coordinates_candidates[i,1]
        y_max = mat_coordinates_candidates[i,2]
        x_max = mat_coordinates_candidates[i,3]
        
        
        img = cv2.imread(fileQuery)
        crop = img[y:y_max, x:x_max]
      #  plt.imshow( crop )
      #  plt.show()
        
        filetosave = outPath+'/'+nameQuery+'.'+str(numCandidate)+'.bmp'
        cv2.imwrite(  filetosave, crop )
        
        numCandidate +=1
        if numCandidate > 20:
            break 


def getCandidates(allShotsPath, outPath, pathCandidates, runmultiprocess):
	'''getFeatures returns a matrix of all features from all images in the training set

	#Note: the double loop is because of the path structure of the dataset. allShotsPath contains a separate folder for every shot, each 		containing several frames.'''


	if runmultiprocess: 
		print 'MULTIPROCESS MODE'
		pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
		PoolResults = []

	start = timeit.default_timer()
	print 'Start timer...', start

	# Get the name of the shots
	for shot in os.listdir( allShotsPath ):
    
		#Define out dir for the features, organized by shots
		outDir = ''
		outDir = os.path.join( outPath, shot )
		createDir( outDir )

		# Get the name of the frames for one shot
		listFrameShot = os.listdir(os.path.join(allShotsPath, shot) )
    
    
		for frame in listFrameShot:
			
			imagePath = ''
			imagePath = os.path.join( allShotsPath, shot, frame )
			filetosave = os.path.join( outDir, frame.split('.')[0]+'.bmp' )

			if not os.path.exists( filetosave ):
				try: 

					#and get the descriptors for it
					if runmultiprocess: 
						pool.apply_async(computeDLfeature, args=( imagePath, filetosave ))

					else:   
						t0=time.time()
						pathOut_candidates = createDir(os.path.join( outPath,shot ))
						getCandidates_of_One_Image( frame.split('.')[0], imagePath, pathCandidates, pathOut_candidates)
						print 'Elapsed time ',time.time()-t0
				


				except:
					fid = open( 'LogImageAllsubset.txt', 'a' )
					fid.write( imagePath +'\n' )
					fid.close()
	pool.close()
	pool.join()
    


if __name__ == '__main__':
   # getCandidates( '9070.1.src',fileQuery, ''  )
   #prova_Rectangle(fileQuery)
   #bbQuery( fileQuery, fileMask )
   allShotsPath = '/home/amaia/work/gt_imgs/'
   outPath = '/home/eva/AllCandidates/'
   pathCandidates = '/home/eva/MCG_trecvid/'
   getCandidates(allShotsPath, outPath, pathCandidates, False)
