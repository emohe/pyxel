import numpy as np
import operator
import time
import os
from sklearn.metrics.pairwise import pairwise_distances
import pickle
from general_tools import *
#import matplotlib.pyplot as plt


def plotHistograms(distances,relnotrel):
    
    relevant = distances[np.reshape(relnotrel==1,(np.shape(relnotrel==1)[1]))]
    nonrelevant = distances[np.reshape(relnotrel==0,(np.shape(relnotrel==0)[1]))]

    numexamples = min(np.shape(relevant)[0],np.shape(nonrelevant)[0])
    print numexamplespero
    #relevant = relevant[0:numexamples]
    #nonrelevant = nonrelevant[0:numexamples]

    nbins=100 
    
    relhist1=plt.hist(relevant.astype(float),nbins)
    plt.show()
    relhist2=plt.hist(nonrelevant.astype(float),nbins)
    plt.show()


def generateTrecvidfileResult( pathOutput_rankings, pathOutput_results ):
    fid = open( os.path.join(  pathOutput_results, 'final.result'), 'w' )

    
    for fileRanking in  os.listdir( pathOutput_rankings ):
        line = fileRanking.split('.')[0]  
        filetoread = os.path.join( pathOutput_rankings,fileRanking)
        nameQuery = fileRanking.split('.')[0]
        r = np.array(pickle.load( open(filetoread, 'r' ) ))
        for shot in r[:,2]:
            line += ' '+shot
        line += '\n'
        fid.write( line )
        line = ''
        
    fid.close()


def computeMAP(pathOutput_rankings, pathOutput_results ):
    results = [] 
    names = []
    
    fid = open( os.path.join(  pathOutput_results, 'mAP.txt'), 'w' )

    
    print 'Query \t AP'
    print '================='
    
    fid .write( 'Query \t AP\n' )
    fid .write('=================\n')

    
    
    for fileRanking in os.listdir( pathOutput_rankings ):
        filetoread = os.path.join( pathOutput_rankings,fileRanking)
        nameQuery = fileRanking.split('.')[0]
        r = np.array(pickle.load( open(filetoread, 'r' ) ))
        #ranking = r[:,2]
        
        ranking = r[:,2]
        ap = AveragePrecision( fileGT, nameQuery, ranking )
        line = "%s \t %0.3f" % (nameQuery, ap )
        print line
        fid .write( line )
        results.append( ap )
        names.append( nameQuery )
        ranking = []
    
    print '================='
    print 'mAP \t %.03f' % (np.average( np.array( results ) ))
    
    fid .write( '=================\n')
    fid .write( 'mAP \t %.03f\n' % (np.average( np.array( results ) )))
    fid.close()
    




def computeRankingShots( q_descriptor, q_namesFrame, descriptors_n, namesShots, namesShot_hash, namesFrames, pathOutput_rankings, distanceType = 'l2', save=True):
    
    distances = pairwise_distances( descriptors_n, q_descriptor, metric=distanceType )

    resultPerShot = []
    tmp_res = []
    tmp_names = []

    for shot in np.unique(namesShots):
        tmp_res = distances[ namesShot_hash==hash(shot) ]
        tmp_names = namesFrames[ namesShot_hash==hash(shot) ]
        
        ind_min = np.argmin( tmp_res )    
        resultPerShot.append( (tmp_res[ind_min], tmp_names[ind_min], shot) )
    
    
    res_final = sorted(resultPerShot, key=operator.itemgetter(0))


      # filesave
    if save:
        
        pathOutput_rankings = pathOutput_rankings+'_'+distanceType
        
        createDir( pathOutput_rankings )
        
        filesave = open( os.path.join(pathOutput_rankings, q_namesFrame+'.obj'),'w' )
        #print filesave
        pickle.dump( res_final, filesave )
        filesave.close()
        
        

    return res_final


def relnotrel( fileGT, id_q, rankingShots ):

    labelRankingShot = []
    t_shot = []
    a = np.loadtxt( fileGT, dtype='string' )
   
    # Extract shots for the query
    t_shot = a[ (a[:,0]==id_q) ]
    
    # Extract relevant shots for the query
    t_shot_rel = t_shot[ t_shot[:,3] == '1' ]
    t_shot_notrel = t_shot[ t_shot[:,3] == '0' ]

    
    # Total Number of relevant shots in the ground truth
    nRelTot = np.shape( t_shot_rel )[0]
    
  
    labelRankingShot = np.zeros((1, len(rankingShots)))
    
    i = 0
    for shotRanking in rankingShots:
        
        if shotRanking in t_shot_rel:
            labelRankingShot[0, i ] = 1

        i +=1   

    
    return labelRankingShot, nRelTot
        
    
def AveragePrecision( fileGT, id_q, rankingshots,N):
    
    (relist, nRelTot) = relnotrel( fileGT, id_q, rankingshots )
    
    map_ = 0 
    accu = 0
    numRel = 0
   
    for k in range(min(N,np.shape(relist)[1])):

        if relist[0,k] == 1:
            numRel = numRel + 1
            accu +=1
            map_ += float( accu )/ float(k+1)
   
    return (map_/nRelTot)
            
            
def mergeQueries( pathOutput_rankings, path_output_merged ):
    listFiles = os.listdir( pathOutput_rankings  )
    
    # Get ID queries in a list
    idQueries = []
    for  namefile in listFiles:
        idQueries.append( namefile.split('_')[0] )
    idQueries = np.unique( np.array( idQueries ) )
    
    count = 1
    for ID in idQueries:
   	tmpRes = []
   	print '%d of %d' % ( count, len(idQueries) )
   	for fileQuery in listFiles:
    
  		if fileQuery.split('_')[0] == ID:
 			filetoload = os.path.join( pathOutput_rankings, fileQuery )     
 			res_final = pickle.load( open( filetoload, 'r' )) 
 			r = np.array( res_final )
 			res_final = sorted(res_final, key=operator.itemgetter(2))
 			res_final = np.array( res_final )
 			tmpRes.append( res_final[:,0].astype(float) )

   	
   	tmpRes = np.array( tmpRes )

   	index = np.argsort( np.min( tmpRes, axis=0 ) )
   	res_final[:,0] = map(float,np.min( tmpRes, axis=0 ))
        res_final = res_final[index]
        
   	pickle.dump( res_final, open( os.path.join( path_output_merged, ID), 'w' ) )
   	count +=1 
