from functions_EvaluateRanking import *
from CNN_tools import createDir

######
# This script is to analyze the queries
#
# 1 - Load the database descriptors
# 2 - Normalize the descriptors
# 3 - Generate and Normalize the features for the queries
# 4 - Extract the relevant shots for each query
# 5 - Evaluate the results given a GT
# 
######

# Define the relevant paths
allShotsPath = '/home/eva/tmp/trecvid 2013/gt_imgs'
fileGT = '/home/eva/tmp/trecvid 2013/ins.search.qrels.tv13'

pathOutput_rankings = '/home/eva/output/CNN/pq/final_rankings/256_16'
pathOutput_results = '/home/eva/output/CNN/pq/final_results/256_16'



def generateTrecvidfileResult( pathOutput_rankings, pathOutput_results ):
    fid = open( os.path.join(  pathOutput_results, 'final.result'), 'w' )

    
    for fileRanking in  os.listdir( pathOutput_rankings ):
        line = fileRanking.split('.')[0]  
        filetoread = os.path.join( pathOutput_rankings,fileRanking)
        nameQuery = fileRanking.split('.')[0]
        r = np.array(pickle.load( open(filetoread, 'r' ) ))
        for shot in r[:,2]:
            line += ' '+shot
        line += '\n'
        fid.write( line )
        line = ''
        
    fid.close()


def computeMAP(pathOutput_rankings, pathOutput_results ):
    results = []
    names = []
    
    fid = open( os.path.join(  pathOutput_results, 'mAP.txt'), 'w' )

    
    print 'Query \t AP'
    print '================='
    
    fid .write( 'Query \t AP\n' )
    fid .write('=================\n')

    
    
    for fileRanking in os.listdir( pathOutput_rankings ):
        filetoread = os.path.join( pathOutput_rankings,fileRanking)
        nameQuery = fileRanking.split('.')[0]
        r = np.array(pickle.load( open(filetoread, 'r' ) ))
        #ranking = r[:,2]
        
        ranking = r[:,2]
        ap = AveragePrecision( fileGT, nameQuery, ranking )
        line = "%s \t %0.3f" % (nameQuery, ap )
        print line
        fid .write( line )
        results.append( ap )
        names.append( nameQuery )
        ranking = []
    
    print '================='
    print 'mAP \t %.03f' % (np.average( np.array( results ) ))
    
    fid .write( '=================\n')
    fid .write( 'mAP \t %.03f\n' % (np.average( np.array( results ) )))
    fid.close()
    
    
if __name__ == '__main__':
    createDir( pathOutput_results )
    computeMAP(pathOutput_rankings, pathOutput_results )
    generateTrecvidfileResult( pathOutput_rankings, pathOutput_results )
