import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
import timeit


def relnotrel( fileGT, id_q, rankingShots ):
	''' 
	Function to define if the elements of a ranking list are or not relevant.
	args:
		(fileGT, id_q (namequery), ranking shots)
	
	 - fileGT == string path to the file
	 - id_q == string with the query identifier
	 - rankingshots == list with the shot names (strings)
	'''

	labelRankingShot = []
	t_shot = []
	a = np.loadtxt( fileGT, dtype='string' )
	
	id_q = id_q.split('_')[0]
	
	# Extract shots for the query
	t_shot = a[ (a[:,0]==id_q) ]

	# Extract relevant shots for the query
	t_shot = t_shot[ t_shot[:,3] == '1' ]

	# Total Number of relevant shots in the ground truth
	nRelTot = np.shape( t_shot )[0]

	labelRankingShot = np.zeros((1, len(rankingShots)))

	i = 0
	for shotRanking in rankingShots:
		if shotRanking in t_shot:
		    labelRankingShot[0, i ] = 1
		i +=1   


	return labelRankingShot, nRelTot



def AveragePrecision( fileGT, id_q, rankingshots ):
	'''
	Function to compute the average precision of a ranking list
	args:
		(fileGT, id_q (namequery), ranking shots)

	 - fileGT == string path to the file
	 - id_q == string with the query identifier
	 - rankingshots == list with the shot names (strings)
	'''

	(relist, nRelTot) = relnotrel( fileGT, id_q, rankingshots )

	map_ = 0 
	accu = 0

	for k in range(np.shape(relist)[1]):

		if relist[0,k] == 1:
		    accu +=1
		    map_ += float(accu)/ float(k+1)

	return (map_/nRelTot)

