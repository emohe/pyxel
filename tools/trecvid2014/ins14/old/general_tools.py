import numpy as np
import os
import random as rnd
import pickle
import glob
import sys
sys.path.insert(0,'/home/amaia/workspace/opencv-2.4.7/release/lib')
import cv2 
def normalizeFeat(x,mean_x=None,std_x=None):
    x=np.array(x)
    
    if mean_x == None and std_x is None:
        mean_x = np.mean(x,axis=0)
        std_x = np.std(x,axis=0)
        std_x[std_x==0] = 1
        print " Obtaining mean and std from data..."
                
    return (x-np.tile(mean_x,(np.shape(x)[0],1)))/np.tile(std_x,(np.shape(x)[0],1)),mean_x,std_x


def createDir(directory):
    
    if not os.path.exists(directory):
        
        os.makedirs(directory)
        
        print 'Directory created', directory
        
    return directory


def cleanDir(directory):
    files = glob.glob(directory+'/*')
    for f in files:
        os.remove(f)


def randSubsample(data, nPt):
  
    if data is not None:    
        if np.shape(data)[0] > nPt:
        
            idx = np.array(rnd.sample(range(0,np.shape(data)[0]), nPt))
        
            return data[idx]
        
        else:
        
            return data
    else:
    
        return data
        

def getFramesQueries( pathQueries ):
    
    # List with all the files in the query directory
    l = np.array( os.listdir(pathQueries) )
    
    # Get ID queries in a list
    idQueries = []
    for  namefile in l:
        idQueries.append( namefile.split('.')[0] )
    idQueries = np.unique( np.array( idQueries ) )
    
    
    listQueries = []
    
    # For each query
    for IDquery in idQueries:
        # Get the files relevants in the directory
        for namefile in l:
            if namefile.split('.')[0] == IDquery and namefile.split('.')[2]=='src':
                listQueries.append( namefile )
    
    return listQueries
    

def getNFrames( pathQueries, n ):
    
    # List with all the files in the query directory
    l = np.array( os.listdir(pathQueries) )
    
    # Get ID queries in a list
    idQueries = []
    for  namefile in l:
        idQueries.append( namefile.split('.')[0] )
    idQueries = np.unique( np.array( idQueries ) )
    
    
    listQueries = []
    
    # For each query
    for IDquery in idQueries:
        # Get the files relevants in the directory
        for namefile in l:
            if  namefile.split('.')[1] in str(range(1,n+1)) and namefile.split('.')[0] == IDquery and namefile.split('.')[2]=='src':
                listQueries.append( namefile )
    
    return listQueries
    
    
def bbQuery( fileQuery, fileMask ):
    img = cv2.imread( fileQuery )
    #img = caffe.io.load_image( fileQuery )
    mask_ = cv2.imread( fileMask )
    #mask_ = caffe.io.load_image( fileMask )

    mask_ =mask_[:,:,0] 
    
    mask_[mask_<255]=0 

    y, x = np.where(mask_==255)
    
    x_min = np.min( x )
    y_min = np.min( y )
	
    x_max = np.max( x )
    y_max = np.max( y ) 

    crop = img[y_min:y_max, x_min:x_max,:]
    
    #cv2.rectangle(img, (x_min,y_min), (x_max, y_max), (0,255,0),3)
    return crop

def loadFeatures( allShotsPath, tmpPath, runmultiprocess=False ):
    ''' Load features from disk. First path non relevant (only it is used to compare
        the total amount number of images and the features computed).
        It returns:
                Descriptors, nameFrames, name Shot. All of them arrays.
    '''
    
    if runmultiprocess: 
        print 'MULTIPROCESS MODE'
	pool = mp.Pool(mp.cpu_count()- 1 if mp.cpu_count()>1 else 0)
    


    descriptors = []
    namesFrames = []
    namesShots = []
    
    numDescriptors = 0
    
    
    for shot in os.listdir( tmpPath ):
        readDir = ''
        readDir = os.path.join( tmpPath, shot )
        
        # Get the name of the frames for one shot
        listFrameShot = os.listdir( os.path.join(tmpPath, shot) )
        
        if len(listFrameShot)>0:
            # Read all the frame descriptors from disk
            for frame in listFrameShot:
                imagePath = ''
                imagePath = os.path.join( tmpPath, shot, frame )
                
                # Define Output File
                filetoread = os.path.join(readDir, frame)            
                
               
                    
                feat = pickle.load( open( filetoread, 'r' )) 
                   # print frame, ' loaded'
 	    	 
                descriptors.append( feat )
                namesFrames.append( frame.split('.')[0] )
                namesShots.append( shot )
     		print numDescriptors
		numDescriptors = numDescriptors + 1
              
    

    
    return descriptors, namesFrames, namesShots
