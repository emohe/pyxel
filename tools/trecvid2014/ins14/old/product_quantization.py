from numpy import *
from general_tools import *
from bow_tools import *
import os
import time
from scipy.cluster.vq import *    # for k-means
import pickle                    # save/load data files
#from PIL import Image     # python image library
import multiprocessing as mp
import operator

# Product quantization python path:
#pathEva = '/home/eva/python/scripts'  # this file is expected to be in {caffe_root}/examples
#import sys
#sys.path.insert(0, pathEva)
#from CNN_tools import *

# Notes:

# K was set to 20 for the clustering in product quantization in the CVC code.
# This was for a training set of two classes and 3500 images per class
# The number of splits was 16 (and used with sift vectors, so 16 splits of size 8.
# The size of the vector has to be multiple of the number of splits.
# For the CNN features we could do several things:
#       Do PCA first. This would allow us to have a smaller number of splits and smaller codesize.
#       If the size is 4096:
#               * 16 splits of 256
#               * 64 splits of 64
#               * ...

# the paper is here:
# https://lear.inrialpes.fr/pubs/2011/JDS11/jegou_searching_with_quantization.pdf

#Things they say in the paper
#   They use k=256 and m=8 for SIFT vectors



def computeProductQuantization(X, namesFrame, namesShots, library,descriptorspath,codesize,pq_m,savepath):

    createDir( savepath )
    nameFile = 'pq_codebook_'+str(codesize)+'_'+str(pq_m)+'.pkl'
    if os.path.isfile(os.path.join(savepath,nameFile)):
        
        f = open(os.path.join(savepath,nameFile), 'rb') # load it
        CODEBOOK = pickle.load(f)
        f.close()
    
    else:
        compute_codebook(X,pq_m,codesize,savepath) # pq_codebook.pkl is stored
        f = open(os.path.join(savepath,nameFile), 'rb') # load it
        CODEBOOK = pickle.load(f)
        f.close()

    #Path where the features are going to be saved
    dir_pq_features = createDir(os.path.join(savepath,'pqvectors'+'_'+str(codesize)+'_'+str(pq_m)))

    for shot in os.listdir(library):
        # List containing all the features of one shot
        frames_shot = os.listdir(os.path.join(library,shot))
        
        # Build the shot dir to store the descriptors
        dir_pq_features_shot = createDir(os.path.join(dir_pq_features, shot))

        for name in frames_shot:
            if os.path.isfile(os.path.join(descriptorspath,shot, name.split('.')[0] + '.obj')):

                print 'product quantization: '+name

                feats = pickle.load(open(os.path.join(descriptorspath,shot, name.split('.')[0] + '.obj'),'r'))
                qv = quantize_vector(feats,pq_m,CODEBOOK,codesize)
                f = open(os.path.join(dir_pq_features_shot,name.split('.')[0]+'.obj'), 'wb')
                pickle.dump(qv,f)
                f.close()

    print 'PQ vectors stored!'


def compute_codebook(X,m,k,Folder):
    """ compute the codebook 'C' for input data matrix 'X' and number of splits 'm'
    codebook is computed with k-means clustering, with parameter 'k' """
    X = array(X)
    D = shape(X)[1] # dimensionality of descriptor
    D_star = D/m    # size of subvectors
    print D
    print D_star
    if mod(D,m) != 0:
        print 'ERROR: descriptor size not an integer multiple of subvectors number!'
        exit(-1)

    CODEBOOK = zeros((k*m,D_star))

    for i in range(m):

        x = X[:,D_star*i:D_star*(i+1)] #Get portion of data

        print 'create codebook of '+str(k)+' words for '+str(i)+'/'+str(m)+' data size = '+str(shape(x))

        centroids,variance = kmeans2(x,k,minit='points')

        CODEBOOK[i*k:(i+1)*k] = centroids
        print 'stored '+str(k)+' centroids for subvector '+str(i)

    nameFile = 'pq_codebook_'+str(k)+'_'+str(m)+'.pkl'

    f = open(os.path.join(Folder,nameFile), 'wb')
    print 'Codebook saved in: ', os.path.join(Folder,nameFile)
    pickle.dump(CODEBOOK,f)
    f.close()


def quantize_vector(v,m,codebook,k):
    """
    vector 'v', split into 'm' parts
    """
    if len(np.shape(v)) == 1:
        v = np.reshape( v, (1,np.shape(v)[0]) )

    D = shape(v)[1]         # dimensionality of descriptor
    D_star = D/m    # size of subvectors

    CODE = zeros((m,1))
 
    for i in range(m):
        u = v[0,D_star*i:D_star*(i+1)]    # select the piece of the vector
        
        u = np.reshape( u , (1,np.shape( u )[0] ))
        
        C_i = codebook[i*k:(i+1)*k] # take the part of the codebook that we need to use
            
        code,distance = vq(u,C_i) # find the code for the piece of vector

        CODE[i] = code    #store it

    return CODE.astype(int) #return it as integer (not sure if we need this)

#
#    COMPUTE DISTANCE BETWEEN TWO VECTORS USING VECTOR QUANTIZATION
#
def pq_distance(x,y,dist_mode,codebook,m,k):

    # the sqrt is avoided for practical computation (as stated in [1])
    # since sqrt is a monotonically increasing function

    # yq is the already quantized vector, fetched from the database

    # vec_mode:
    #    'coded': x is full vector and y is the coded vector
    #    'full': x and y are full vectors

 #   if len(x) != len(y):
 #       print 'ERROR: the two vectors must be equal in size!'
 #       exit(-1)


    D = len(x)         # dimensionality of descriptor
    D_star = D/m    # size of subvectors

    # this is just to quantize the vector if we haven't done it before.
 #   if vec_mode != 'coded':
 #       code_y = quantize_vector(y,m,codebook,k)
  #  else:
    code_y = y

    #types of distances - in the master's code they were using 'sdc' by default, but in the paper they claim that adc is better
    if dist_mode == 'sdc':

        # symmetric distance computation, eq.(12) in [1]
        #code_x = quantize_vector(x,m,codebook,k)
        code_x = x

        d = 0.0
        for j in range(m):
            C_j = codebook[j*k:(j+1)*k]
            q_x = C_j[code_x[j]]
            q_y = C_j[code_y[j]]
            d+=sum((q_x-q_y)**2)

        return d

    elif dist_mode == 'adc':

        # asymmetric distance computation, eq.(13) in [1]
        d = 0.0
        for j in range(m):
            C_j = codebook[j*k:(j+1)*k]
            u_x = x[D_star*j:D_star*(j+1)]
            q_y = C_j[code_y[j]]
            d+=sum((u_x-q_y)**2)

        return d

#
#    compute query product-quantization
#

def pq_query(x,CODEBOOK,LIBRARY,n_splits,codesize):

    # compute query results

    shotlist=os.listdir(LIBRARY)
    res_final = []
    #For every shot...
    for shot in shotlist:
        #...get the frames
        pqDir=os.listdir(os.path.join(LIBRARY,shot))
                
        shotdist = zeros((1,len(pqDir)))
        cont=0
        #And for every frame...
        tmp_name = []
        for pqfile in pqDir:
            
            pDirShot = os.path.join(LIBRARY,shot)
            f = open(os.path.join(pDirShot,pqfile),'rb')
            qv = pickle.load(f)
            f.close()
           
          
            shotdist[0,cont] = pq_distance(x,qv,'sdc',CODEBOOK,n_splits,codesize)
                        
            tmp_name.append( pqfile )

            cont+=1
        
        if np.shape(shotdist)[1]==0:
            res_final.append( ( 100000000,'', shot  ) ) # assign a huge value to those shots for which we could not extract a descriptor
        else:
            in_min = np.argmin( shotdist )
            res_final.append( ( shotdist[0,in_min],tmp_name[in_min], shot  ) )
    
    res_final = sorted(res_final, key=operator.itemgetter(0))

    return res_final
