import os
import time
import random
from trecvid_ins_14_tools import *
import numpy as np


#path to full dataset CNN descriptors : /ichec/work/dccom033c/ins14/CNN/CNN_features
#path to subst CNN descriptors: /ichec/work/dccom033c/ins14/CNN/features_dataset/

# Set to True if running on the DCU server, set to False if on UPC one
cluster = False
which_data='subset'
maxNumFeats = 14000
random_ = False
rootCNN = False

if cluster:
    params = {    'feattype':'BoW' ,'keypointDetector':'SIFT', 'descriptor':'SIFT',\
                   'K': 4096, 'nPt': 200, \
                   'codesize_PQ':256, 'n_splits':8,\
                   'compute_descriptor':True , 'normalize_CNN':True , 'crop_flag':False ,\
                   'pathToTargetImages': '/html/path/where/target/images/are/stored', \
                   'pathToQDescriptors':'/ichec/work/dccom033c/ins14/CNN/query_features_CNN', \
                   'pathToTargetDescriptors': '/ichec/work/dccom033c/ins14/CNN/features_dataset/',\
               'pathToStoreDistances': '/ichec/work/dccom033c/ins14/output/distances/'+ which_data,\
                   'pathToQueryFiles': '/data/ins14/topics/tv14.example.images/',\
                   'fileGT':'/ichec/work/dccom033c/ins14/ins.search.qrels.tv13',\
               'file_with_data':'/ichec/work/dccom033c/ins14/output/TREC_info_'+ which_data+'.bin',\
               'pathRankings':'/ichec/work/dccom033c/ins14/output/rankings/'+ which_data,\
               'pq':True, 'n_splits':8,'codesize':256, 'pqpath_targets': '/home/blabla/targets', 'pqpath_queries':'/home/blabla/queries/',\
                   'distanceType':'cosine' }
# If running on the UPC server
else:

#path to full dataset CNN descriptors : /home/eva/workspace/videosINS14/descriptors/CNN_features
#path to subst CNN descriptors: /home/eva/output/CNN/features_dataset/

     params = {    'feattype':'CNN' ,'detector':'SIFT', 'descriptor':'SIFT',\
                   'K': 4096, 'nPt': 200, \
                   'compute_descriptor':True , 'normalize_CNN':True , 'crop_flag':False ,\
                   'pathToTargetImages': '/home/amaia/work/gt_imgs/', \
                   'pathToQDescriptors':'/home/amaia/work/output/query_features_CNN/', \
           'pathToQDescriptors_BoW':'/home/amaia/work/output/descriptors/SIFT_SIFT_queries/',\
                   'pathToTargetDescriptors': '/home/eva/workspace/videosINS14/descriptors/CNN_features',\
            'pathToTargetDescriptors_BoW': '/home/amaia/work/output/descriptors/SIFT_SIFT/',\
               'pathToStoreDistances': '/imatge/asalvador/work/trecvid/CNN_distances/'+ which_data,\
           'pathToStoreDistances_BoW':'/home/amaia/output/BoW_distances/' + which_data,\
                   'pathToQueryFiles': '/data/ins14/topics/tv14.example.images/',\
                   'fileGT':'/projects/retrieval/trecvid-ins14/new_qrels2.tv13',\
               'file_with_data':'/imatge/asalvador/work/trecvid/DESCRIPTORS_'+ which_data+'.bin',\
           'pathRankings':'/imatge/xgiro/work/trecvid/2014/rankings/',\
               'pq':False, 'n_splits':8,'codesize':2048, 'compute_pq':True, 'pqpath': '/home/amaia/output/pq/',\
               'pathToStoreDistances_pq':'/home/amaia/output/pq_distances/'+ which_data,\
                   'distanceType':'l2' }


def rerank_with_relevance_feedack( 	ranked_shots, 
					ranked_frames, 
					distances, 
					DESCRIPTORS, 
					frames_all, 
					K,
					ground_truth,
					query_id_str
 				):

    # Get the frames associated to the first K hits from the ranking
    top_K_frames = ranked_frames[1:K+1]

    # Define a function to get the indexes of the items in a list
    getIndexes = lambda list_all, list_items: [list_frames_all.index(i) for i in list_items]

    # Get the indexes that correspond to the top K frames
    list_frames_all = frames_all.tolist()
    indexes = getIndexes(list_frames_all, top_K_frames)
  
    # Get the descriptors associated to these frames as a numpy array
    features_ranked_frames = DESCRIPTORS[indexes, :]

    # Create a numpy array with the relevance feedback labels according
    # to the ground truth data
    top_K_shots = ranked_shots[1:K+1]
    labels_ranked_shots, nof_relevant_shots = build_label_list( ground_truth, query_id_str, top_K_shots )
    

    # Train a linear SVM with the relevance feedback
    clf = LinearSVC()
    clf.fit(features_ranked_frames, labels_ranked_shots)

    # Compute new scores for the whole collection of descriptors
    scores = clf.decision_function(DESCRIPTORS)

    # Transform the scores to distances
    distances = 1 - scores

     # Obtain the shot ranking based on the query image-vs-frames distances
    ranking_info = computeRanking(distances, namesShots, frames, numQueries, N)
    ranking_info = np.array(ranking_info)
  
    # Return three lists, one for each data field
    shots=ranking_info[2,:]
    frames=ranking_info[1,:]
    distances=ranking_info[0,:]

    # Return results as three independent lists
    return list(shots),list(frames),list(distances) 
    

def get_query_files( query_id_str, pathDirQueryFiles ):    
    ''' Build a list with all query images that define the specified query topic '''
    
    # Define an empty list to store the query files associated to the provided query ID
    queryfiles = []
    
    # For every file describing a query instance (from all query topics, mixed up)
    for q in namesQueries:
      
        # If the query ID is contained in the filename describing the current query image...
        if query_id_str in q:
    
            # Add the query instance to the list of frames that define the query topic
            queryfiles.append( os.path.join(pathDirQueryFiles, q +'.png') )
            
    return queryfiles

def run_query_topic(    query_id_str,
			pathDirQueryFiles,
                        namesQueries, 
                        DESCRIPTORS,
			frames_all,
                        namesShots,
                        numQueries,
                        N,
			ground_truth,
                        file_ranks_out,
			ap_list ):
    ''' Run only one query topic '''

    print "Running query " + query_id_str
       
    # If the current experiment is not random
    if not random_:

        # Get the image files that define the query topic        
        queryfiles = get_query_files( query_id_str, pathDirQueryFiles )
        
        # Save the time stamp before starting the search
        ts = time.time()

        # Run the actual search (core function)    
        ranked_shots, ranked_frames, distances = return_ranking_query(DESCRIPTORS,
                                                      queryfiles,
                                                      frames_all,
                                                      namesShots,
                                                      numQueries,
                                                      N)

    # If the current experiment must generate a random result (as baseline or sanity check)                                               
    else:
        ranked_shots = sorted(np.unique(namesShots[0:np.shape(namesShots)[0]-numQueries]), 
                               key=lambda k:random.random())

    # Rerank the results based on user feedback on positive and negative samples
    # on the first N considered results
    # shots,frames,distances = rerank_with_relevance_feedack( ranked_shots, ranked_frames,distances, DESCRIPTORS,namesFrames,N, ground_truth,query_id_str )

    # Print the elapsed time to run the search
    print "Elapsed time to return the ranking:", time.time() - ts

    # Write to disk the file with the ranking for the current query
    rankingfile( file_ranks_out, query_id_str, ranked_shots, np.array(distances))
       
    # Compute the average precision for the query topic (so slow !!)
    # ap = AveragePrecision(pathFileGT, query_id_str, shots,N)
    ap = 0

    # Add the average precision to the provided list of APs and print it on screen
    ap_list.append(ap)
    print query_id_str, ap

    print "Running query " + query_id_str + " AP = " + ap

    return ranked_shots, ranked_frames

def run_all_query_topics( 	pathDirQueryFiles,
                          	namesQueries, 
                          	DESCRIPTORS,
                                namesFrames,
                                namesShots,
                                numQueries,
                                N,
				ground_truth,
                                file_ranks_out,
				ap_list ):
    ''' Run all query topics '''

    # For each query topic
    for query_id in np.arange(9069,9099):

        # Process the current query topic
        ap = run_query_topic(   str(query_id),
				params['pathToQueryFiles'],
                                namesQueries, 
                                DESCRIPTORS,
                                namesFrames,
                                namesShots,
                                numQueries,
                                N,
				ground_truth,
                                file_ranks_out,
				ap_list )
        
if __name__ =='__main__':
    compute = True
    N = 1000
    createDir(params['pathRankings'])
    
    # Load precomputed data from disk
    print "Loading dataset”
    DESCRIPTORS, frames_all, namesShots, numQueries = init_database(False)
    print "Loading dataset done"

    # Open the file that will contain all the results
    path_file_rank_out = os.path.join(params['pathRankings'], 'rank_'+ which_data)
    file_ranks_out = open( path_file_rank_out,'w')

    namesQueries = frames_all[np.shape(frames_all)[0] - numQueries:np.shape(frames_all)[0]]

    # Init a sorted list that will store the average precision for each query topic
    ap_list=[]

    # Load the ground truth file in memory (slow)
    print "Loading ground truth"
    pathFileGT = params['fileGT']
    ground_truth = np.loadtxt( fileGT, dtype='string' )
    print "Loading ground truth done"

    # Run only one query topic (faster debugging)
    query_id_str = str(9069)
    
    ranked_shots, ranked_frames = run_query_topic(   	query_id_str,
						params['pathToQueryFiles'],
                       	 			namesQueries, 
                        			DESCRIPTORS,
						frames_all,
                        			namesShots,
                        			numQueries,
                        			N,
						ground_truth,
                        			file_ranks_out,
						ap_list )
    
    # Run all query topics to generate a list of the average precision for each topic
    #run_all_query_topics( 	params['pathToQueryFiles'],
    #                            namesQueries, 
    #                            DESCRIPTORS,
    #                            namesFrames,
    #                            namesShots,
    #                            numQueries,
    #                            N,
#				params['fileGT'],
#                                file_ranks_out,
#				ap_list )		

    file_ranks_out.close()
    print "============"
    print "mAP", np.mean(ap_list)

