from eval_tools import *
from trecvid_ins_14_tools import *

import sys
import os
import glob
import numpy as np
import pandas as pd
import pickle
import time

from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import Normalizer


##############################################################################
# PARAMETERS:
# Subset = True to select path to run experiments on subset features
#          False to select full dataset paths
# 
# Local = True to load local CCN features relative to the candidated
#         False to do the normal ranking based on global CNN features
#
# addCandidates = True to append CNN candides as additional frame descriptors
#                 False to do the concatenation of the global and local 
#                 features
#
# 
# FUNCTIONALITY:
# The script loads the necessary descriptors specified in the RankingFile
# containing the first N shots more relevant to each query.('globalRanking')  
##############################################################################
subset = True
Local = True
addCandidates = True
##############################################################################





# Set the enviroment
if Local:
    str_local = '_local'
else:
    str_local = '_global'

if not subset:
    globalRanking = '/home/eva/CANDIDATES/rankingfile_full'
    path_Query_images = '/data/ins14/topics/tv14.example.images'
    path_global_descriptors = '/home/eva/workspace/videosINS14/descriptors/CNN_features'
    path_local_descriptors = '/home/eva/CANDIDATES/descriptors'
    path_global_Query_descriptors = '/home/amaia/work/output/query_features_CNN/'
    path_local_Query_descriptors = '/home/eva/CANDIDATES/descriptors_queries'
    pathOutput = '/home/eva/CANDIDATES/output'
else:
    globalRanking = '/home/eva/CANDIDATES/rankingfile_subset'
    path_Query_images = '/data/ins14/topics/tv14.example.images'
    path_global_descriptors = '/home/eva/output/CNN/features_dataset/'
    path_local_descriptors= '/home/eva/workspace/caffe2/python/Candidates/square_p4/'
    path_global_Query_descriptors = '/home/amaia/work/output/query_features_CNN/'
    path_local_Query_descriptors = '/home/eva/workspace/caffe2/python/Candidates/square_p4_queries/'
    pathOutput = '/home/eva/CANDIDATES/output_subset'

logFile = os.path.join( pathOutput, 'missing_local.log' )

if addCandidates:
    pathRankings = os.path.join( pathOutput, 'rankings_addC'+str_local )
    output_file = os.path.join( pathOutput,'AP_addC'+str_local+'.txt' )
    output_file_ranking = os.path.join( pathOutput,'ranking_addC'+str_local+'.txt' )

else:
    pathRankings = os.path.join( pathOutput, 'rankings_concat'+str_local )
    output_file = os.path.join( pathOutput,'AP_concat'+str_local+'.txt' )
    output_file_ranking = os.path.join( pathOutput,'ranking_concat'+str_local+'.txt' )


if not os.path.isdir( pathRankings ):
    os.mkdir( pathRankings )
 
    
          
#########################################################################
#########################################################################
#                            FUNCTIONS
#########################################################################
#########################################################################

def readRankingFile( globalRanking ):
    ''' Function to read ranking file into pandas. 2 Columns: query, frame
    '''
    data_ranking = pd.read_csv( globalRanking, dtype='str', header = None, sep='\t')
    data_ranking.columns = ['query', 'frame']
    #print data_ranking.tail()
        
    return data_ranking


# Make a a list of the queries:
data_ranking = readRankingFile( globalRanking )
queryID = data_ranking['query']
queriesID = np.unique( queryID )

# Make a list with the path to the query images 
listQueryFiles = []
for queryID in queriesID:
    #print queryID, ' ', len(glob.glob(path_Query_images+'/'+queryID+'*src*'))
    listQueryFiles.extend(glob.glob(path_Query_images+'/'+queryID+'*src*'))
   # print queryID, ' NUM SHOTS: ', np.shape( data_ranking['query'] )[0]


#======================================

#LOAD GLOBAL DESCRIPTORS

def getGlobals( query ):    
    print 'Loading Global descriptors from disk...'
    t0 = time.time()
    count = 1
    list_globalFeats = []
    list_globalFeats_shots = []
    list_globalFeats_frames = []

    # load descriptors for the ranking image:
    for frame in data_ranking[data_ranking['query']==query]['frame']:
        shot = frame.split('/')[0]
        frame = frame.split('/')[1]
        if not subset:
            path_frame = os.path.join( path_global_descriptors, shot,frame+'.obj' )
        else:
            path_frame = os.path.join( path_global_descriptors, shot,frame.split('.')[0]+'.obj' )

        #print count, ' of ', len(data_ranking[data_ranking['query']==query]['frame'])
        list_globalFeats.append( pickle.load( open( path_frame ,'rb') ) )
        list_globalFeats_shots.append( shot )
        list_globalFeats_frames.append( frame )
        count +=1

    # Once the list is generated save to disk
    #  pickle.dump( list_globalFeats, open( 'list_globalFeats'+query+'.obj', 'wb' ) )
    print 'LOADED', len(list_globalFeats), time.time()-t0, '\n'

    return list_globalFeats_shots, list_globalFeats_frames, list_globalFeats
    
        
 
#======================================

#LOAD LOCAL DESCRIPTORS

def getLocals( query ):
    fout = open( logFile , 'wa')
    print 'Loading Local descriptors from disk...'
    t0 = time.time()
    
    count = 1
    list_localFeats = []
    list_localFeats_shots = []
    list_localFeats_frames = []

    # load descriptors for the ranking image:
    for frame in data_ranking[data_ranking['query']==query]['frame']:
        shot = frame.split('/')[0]
        frame = frame.split('/')[1]

        path_frame = os.path.join( path_local_descriptors, shot,frame.split('.')[0]+'_*' )
        
        if len(glob.glob( path_frame )) !=20:
            fout.write(query+'\t'+frame+'\n')

        count_cand = 0
        listCandidates = []
        list_cand_shot = []
        list_cand_frame = []
        for candidate in glob.glob( path_frame ):
            count_cand +=1
            list_cand_shot.append( shot )
            listCandidates.append( pickle.load( open(candidate,'rb') ) )
            list_cand_frame.append( frame )
                    
        list_localFeats.extend( listCandidates )
        list_localFeats_shots.extend( list_cand_shot )
        list_localFeats_frames.extend( list_cand_frame )

        #print 'loaded, ', len(glob.glob( path_frame ))
        
        print count, ' of ', len(data_ranking[data_ranking['query']==query]['frame'])
        count +=1

    # Once the list is generated save to disk
    #   pickle.dump( list_localFeats, open( 'list_localFeats'+query+'.obj', 'wb' ) )
    #print 'SAVED'
    fout.close()
    print 'LOADED', len(list_localFeats), time.time()-t0, '\n'

  
    return list_localFeats_shots,list_localFeats_frames, list_localFeats
        

#======================================

#LOAD QUERIES DESCRIPTORS

def getQueries( query ):

    list_globalFeats_queries = []
    list_localFeats_queries = []
    
    pathGlobal = glob.glob( os.path.join(path_global_Query_descriptors, query+'*' ) )
    pathLocal = glob.glob( os.path.join(path_local_Query_descriptors, query+'*' ) )
    
    for frame in range( np.shape(pathLocal)[0] ):
        list_globalFeats_queries.append( pickle.load(open( pathGlobal[frame],'rb') ) )
        list_localFeats_queries.append( pickle.load(open( pathLocal[frame],'rb') ) )
    
    
    return list_globalFeats_queries, list_localFeats_queries

   

#########################################################################
#########################################################################
#                            MAIN
#########################################################################
#########################################################################

# List with Average Precision for each query     
ap_list = [] 

# Output files for AP/query and final Ranking
fid = open( output_file, 'w' )
file_ranking = open( output_file_ranking, 'w' )

# For all the queries...
for query  in queriesID:
    
    # Define the list where we are going to store the descriptors
    # frames and shots ID
    shots = []
    frames = []
    descriptors = []
    d_queries = []


    # Load global and local descriptors for the quer
    g_query, l_query = getQueries( query )
    
    # Load global descriptros for the frames relevants to the query
    g_shot, g_frame, g_d = getGlobals( query )
    
    
    # Load loacl descriptors
    if Local:
        l_shot, l_frame, l_d = getLocals( query )
    
        # Add descriptors (local and global) to the final lists
        if addCandidates:
            
            #Extend list of name Shots
            shots.extend( g_shot )
            shots.extend( l_shot )
            
            # Extend list of name Images
            frames.extend( g_frame )
            frames.extend( l_frame )
            
            # Extend list desciptors
            descriptors.extend( g_d )
            descriptors.extend( l_d )

            d_queries.extend( g_query )
            d_queries.extend( l_query )
        else:
            #CONCATENE
            tmp_global = pd.DataFrame( g_shot )
            tmp_global.columns = ['shots']
            tmp_global['frames'] = g_frame
            tmp_global['descriptors'] = g_d
            
            #print 'CONCATENATION======='
            #print '===================='
            #print 'Targets...'
            for (shot, frame, desc_local) in zip(l_shot, l_frame, l_d):
                desc_glob = np.array(tmp_global[tmp_global[ 'shots' ] == shot]['descriptors'] )
                
                des  = np.hstack( (desc_glob[0], np.array(desc_local))  )
                shots.append( shot )
                frames.append( frame )
                descriptors.append( np.hstack( (np.array(desc_glob[0]), np.array(desc_local))  ) )
            
          #  print '===================='
          #  print 'queries...'
            for n in range( len( g_query ) ):
                d_queries.append( np.hstack( (np.array(g_query[n]), np.array(l_query[n])  )  ) )
                
    else:
        # Only global descriptors
        shots.extend( g_shot )
        frames.extend( g_frame )
        descriptors.extend( g_d )
        d_queries.extend( g_query )    
          
    # Build the Matrix
    QUERIES = np.array( d_queries )
    DESCRIPTORS = np.array( descriptors )
        
    #Normalize Code
    nrm = Normalizer(copy=False)
    model_norm = nrm.fit( np.vstack((DESCRIPTORS,QUERIES)) )
    DESCRIPTORS=model_norm.transform(DESCRIPTORS)
    QUERIES=model_norm.transform(QUERIES)
    

    # Compute Distance
    distances = 2*pairwise_distances(DESCRIPTORS, QUERIES, metric='cosine')
    #distances = -np.dot(DESCRIPTORS, QUERIES, metric='cosine')
    
    
    # Dataframe with the distance where each col is the distance of one query-frame  
    data_ = pd.DataFrame( distances )
    labels =['G1', 'G2', 'G3', 'G4',
                    'L1', 'L2', 'L3', 'L4']
    
    # Labels to select Global or Local
    globalCols = labels[:4]
    localCols = labels[4:]
    
    if not Local or not addCandidates:
        # Redefine the labels to only the Global
        labels =globalCols
    
    # Add labels to the DataFrame
    data_.columns = labels
    
    # Add Shots and Frames identifier to the distance matrix
    data_['shots']  = pd.Series( shots, index=data_.index )
    data_['frames'] = pd.Series( frames, index=data_.index )
     
    # Compute the min distance for each shot of all the               
    distance = np.array( data_[ labels ].min( axis=1 ) )

    # Compute Ranking
    ranking_info = computeRanking(distance,shots,frames,0,1000) # 'numQueries' set to 0 because they are not added to the DESCRIPTORS matrix, namesFrames or namesShots
    ranking_info = np.array(ranking_info)
    
    
    # Store ranking ingo=======
    shots=ranking_info[2,:]
    frames=ranking_info[1,:]
    distances=ranking_info[0,:]
    
    f= open(os.path.join( pathRankings ,queryID+'.bin'),'wb')
    pickle.dump(shots,f)
    pickle.dump(distances,f)
    pickle.dump(frames,f)
    f.close()
    # =========================
        
    # Compute AP
    ap = AveragePrecision('/home/amaia/work/ins.search.qrels.tv13', query, shots,1000)
    print '\t\t\t\t\t\t', query, ap
    ap_list.append( ap )
    
    # Write file Av Prec.
    fid.write( '%s\t%s\n' % (query, str(ap)) )
    
    # Write fileRanking
    rankingfile(file_ranking,query,shots,distances)


print '\t\t\t\t\t\t',"============"
print '\t\t\t\t\t\t',"mAP", np.mean(ap_list)
    
fid.write( '============' )
fid.write( "mAP %s" % str(np.mean(ap_list)))
fid.close()
file_ranking.close()

# Distance Matrix
#pairwise_distances(X, Y, metric='cosine'
