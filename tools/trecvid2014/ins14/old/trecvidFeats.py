# Make sure that caffe is on the python path:
caffe_root = '/home/eva/workspace/caffe2/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
import glob
import numpy as np
import os
import time

# Stuff for the Net
LibraryDir='/home/amaia/work/gt_imgs/'
MODEL_FILE = '/home/eva/workspace/caffe2/examples/imagenet/imagenet_deploy_TRECVID.prototxt'
PRETRAINED = '/home/eva/workspace/caffe2/examples/imagenet/caffe_reference_imagenet_model'
numCandidates = 256

# Load one image
dirShots = glob.glob( LibraryDir +'/*' )
imaPath = glob.glob(dirShots[0]+'/*.jpg')[0]
nameIma = os.path.basename(imaPath).split('.')[0]


#imacv = cv2.imread( imaPath )


# Load the net
net = caffe.Classifier(MODEL_FILE, PRETRAINED,
                       mean_file=caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy',
                       channel_swap=(2,1,0),
                       input_scale=255)
net.set_phase_test()
net.set_mode_gpu()


#prediction = net.predict( [caffe.io.load_image(imaPath)] )

t0 = time.time()
prediction = net.predictTRECVID( imaPath, numCandidates )
print 'DONE'
print 'prediction shape:', np.shape(prediction), time.time()-t0


# Need to do that if we load the image without opencv!
#ima = cv2.cvtColor(ima, cv2.COLOR_BGR2RGB)
#cv2.imwrite( nameIma+'_test.jpg', (ima[:]*255).astype('uint8') )