from general_tools import *
from eval_tools import *
from bow_tools import *
import pickle
import os
import time
import random
from CNN_tools import *
from scipy.spatial.distance import pdist, squareform
from product_quantization import *
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import Normalizer
from sklearn.linear_model import SGDClassifier
import numpy as np
from multiprocessing import Pool, cpu_count, Array
import sys
import pandas as pd


#path to full dataset CNN descriptors : /ichec/work/dccom033c/ins14/CNN/CNN_features
#path to subst CNN descriptors: /ichec/work/dccom033c/ins14/CNN/features_dataset/

cluster = False
which_data='subset'
maxNumFeats = 14000
random_ = False
rootCNN = False

if cluster:
    params = {    'feattype':'BoW' ,'keypointDetector':'SIFT', 'descriptor':'SIFT',\
                   'K': 4096, 'nPt': 200, \
                   'codesize_PQ':256, 'n_splits':8,\
                   'compute_descriptor':True , 'normalize_CNN':True , 'crop_flag':False ,\
                   'pathToTargetImages': '/html/path/where/target/images/are/stored', \
                   'pathToQDescriptors':'/ichec/work/dccom033c/ins14/CNN/query_features_CNN', \
                   'pathToTargetDescriptors': '/ichec/work/dccom033c/ins14/CNN/features_dataset/',\
	           'pathToStoreDistances': '/ichec/work/dccom033c/ins14/output/distances/'+ which_data,\
                   'pathToQueryFiles': '/data/ins14/topics/tv14.example.images/',\
                   'fileGT':'/ichec/work/dccom033c/ins14/ins.search.qrels.tv13',\
	           'file_with_data':'/ichec/work/dccom033c/ins14/output/TREC_info_'+ which_data+'.bin',\
	           'pathRankings':'/ichec/work/dccom033c/ins14/output/rankings/'+ which_data,\
	           'pq':True, 'n_splits':8,'codesize':256, 'pqpath_targets': '/home/blabla/targets', 'pqpath_queries':'/home/blabla/queries/',\
                   'distanceType':'cosine' }
else:

#path to full dataset CNN descriptors : /home/eva/workspace/videosINS14/descriptors/CNN_features
#path to subst CNN descriptors: /home/eva/output/CNN/features_dataset/

     params = {    'feattype':'CNN' ,'detector':'SIFT', 'descriptor':'SIFT',\
                   'K': 4096, 'nPt': 200, \
                   'compute_descriptor':True , 'normalize_CNN':True , 'crop_flag':False ,\
                   'pathToTargetImages': '/home/amaia/work/gt_imgs/', \
                   'pathToQDescriptors':'/home/amaia/work/output/query_features_CNN/', \
		   'pathToQDescriptors_BoW':'/home/amaia/work/output/descriptors/SIFT_SIFT_queries/',\
                   'pathToTargetDescriptors': '/home/eva/workspace/videosINS14/descriptors/CNN_features',\
 		   'pathToTargetDescriptors_BoW': '/home/amaia/work/output/descriptors/SIFT_SIFT/',\
	           'pathToStoreDistances': '/imatge/asalvador/work/trecvid/CNN_distances/'+ which_data,\
		   'pathToStoreDistances_BoW':'/home/amaia/output/BoW_distances/' + which_data,\
                   'pathToQueryFiles': '/projects/retrieval/trecvid-ins14/GT_subset/QueryImages',\
                   'fileGT':'/projects/retrieval/trecvid-ins14/new_qrels2.tv13',\
	           'file_with_data':'/imatge/asalvador/work/trecvid/DESCRIPTORS_'+ which_data+'.bin',\
		   'pathRankings':'/imatge/xgiro/work/trecvid/rankings/',\
	           'pq':False, 'n_splits':8,'codesize':2048, 'compute_pq':True, 'pqpath': '/home/amaia/output/pq/',\
	           'pathToStoreDistances_pq':'/home/amaia/output/pq_distances/'+ which_data,\
                   'distanceType':'l2' }

def svmscores(DESCRIPTORS,clf,savefile):
    #f = open(savefile,'wb')
    #pickle.dump( np.reshape(clf.decision_function(DESCRIPTORS),(np.shape(clf.decision_function(DESCRIPTORS))[0],1)),f)
    #f.close()
    return np.reshape(clf.decision_function(DESCRIPTORS),(np.shape(clf.decision_function(DESCRIPTORS))[0],1))
    


def rankingfile(file_to_write,queryname,shots,distances):
    # Generates the ranking file for evaluation with the trecvid tool
    
    distances = ( distances.astype(float) - min(distances.astype(float)) )/(max(distances.astype(float))-min(distances.astype(float)))
    for n in range(np.shape(shots)[0]):
	file_to_write.write(queryname+'\t') # query name
	file_to_write.write('0\t') #
	file_to_write.write(shots[n]+'\t')
	file_to_write.write(str(n)+'\t')
	file_to_write.write(str(1 - distances[n]) +'\t')
	file_to_write.write('DCU\t')
	file_to_write.write('\n')


def loadOneDescriptor(imagename):
    
    if 'shot' in imagename:
        descriptor = pickle.load(open(os.path.join(params['pathToTargetDescriptors'],imagename + '.obj'), 'r'))
    if 'src' in imagename:
        descriptor = pickle.load(open(os.path.join(params['pathToQDescriptors'],imagename + '.obj'), 'r'))

    return descriptor
    

def loadDescriptorsTargets(compute_pq=True):
    if params['pq'] and not compute_pq:
        path_to_read = params['pqpath'] + 'pqvectors'+'_'+str(params['codesize'])+'_'+str(params['n_splits'])
    else:
	if params['feattype']=='CNN':

            path_to_read = params['pathToTargetDescriptors']
	else:
	    path_to_read = params['pathToTargetDescriptors_BoW']

    DESCRIPTORS , namesFrames, namesShots = loadFeatures( params['pathToTargetImages'], path_to_read)
    print np.shape(DESCRIPTORS)
    return DESCRIPTORS, namesFrames, namesShots


def loadDescriptorsQueries(DESCRIPTORS , namesFrames, namesShots,compute_descriptors = False):
    numQueries = 0

    if compute_descriptors:
        query_descriptors()

    if params['pq']:
        codebook = pickle.load(open(params['pqpath']+'pq_codebook_'+str(params['codesize'])+'_'+str(params['n_splits'])+'.pkl','rb'))

    if params['feattype'] == 'CNN':
        path_to_descriptors = params['pathToQDescriptors']
    else:
	path_to_descriptors = params['pathToQDescriptors_BoW']

    for qfile in os.listdir(path_to_descriptors):  
        if params['pq']:
	    x = pickle.load(open(os.path.join(path_to_descriptors,qfile),'rb'))
	    
	    qv = quantize_vector(x,params['n_splits'],codebook,params['codesize'])
 	    DESCRIPTORS.append(qv)
	else:
            DESCRIPTORS.append(pickle.load(open(os.path.join(path_to_descriptors,qfile),'r')))

        qname= qfile.split('/')
        qname=qname[len(qname)-1]

        namesFrames.append(qname.split('.obj')[0])
        namesShots.append(qname.split('.obj')[0])
        numQueries = numQueries + 1
  
    return DESCRIPTORS, namesFrames, namesShots,numQueries


def computeRanking(distance,namesShots,namesFrames,numQueries,N):
   ''' Compute the ranking given a set of distances for each frame of each shot.
   'distance' : array with the pre computed distances
   'namesShots' : list of unique names of the shots of the database
   'namesFrames' : list of names of the frames to which the distances refer to
   '''

   namesFrames = np.array(namesFrames[0:np.shape(namesShots)[0]-numQueries])
   namesShots = np.array(namesShots[0:np.shape(namesShots)[0]-numQueries])
   distance = np.array(distance[0:np.shape(distance)[0]-numQueries])
   
   data = pd.DataFrame( { 'distance':distance,
                          'shot':namesShots,
                          'frames':namesFrames})
   
   t0 = time.time()

   # Sort the data acording to distances
   data =  data[ ~data['shot'].str.contains('shot0') ]
   t1 = time.time()

   #print "Delete Shot video 0:", t1-t0
   

   # Sort the data acording to distances
   data = data.sort( columns='distance', ascending = True )
   t2 = time.time()
   #print "Time to sort the whole thing:", t2-t1
   
   # Remove rows with duplicated shots,
   # keeping the rows that belongs to the first time
   # that the shot appeared in the sorted data
   data = data.loc[~data['shot'].duplicated()]
   
   
   resultPerShot = list((np.array(data['distance'][:N]),
                    np.array(data['frames'][:N]),
                    np.array(data['shot'][:N])))
   t3 = time.time()
   #print 'Time to remove all the duplicate shots (keeping the first)', t3-t2
   return resultPerShot


def computeDistances(d,qname):
    ''' Computes the distances between the target descriptors and the query'''

    if not os.path.isfile(os.path.join(params['pathToStoreDistances'],qname+'.obj')):

        distance = -np.dot(DESCRIPTORS, np.reshape(d,(np.shape(d)[1])) )
        pickle.dump(distance,open(os.path.join(params['pathToStoreDistances'],qname+'.obj'),'wb'))
    else:
    
        distance = pickle.load(open(os.path.join(params['pathToStoreDistances'],qname+'.obj'),'rb'))

    return distance

    
def loadDistancesQuery(qname):

    for f in os.listdir(params['pathToStoreDistances']):
        if qname in f:
            distance = pickle.load(open(os.path.join(params['pathToStoreDistances'],f+'.obj'),'rb'))

        distances.append(distance)

    return distances
    

def return_ranking_query(DESCRIPTORS,queryfiles,namesFrames,namesShots,numQueries,N):
    ''' Generates the ranking using the images in 'queryfiles' as query images'''
    qdistances=[]
    
    for qfile in queryfiles: # For all the images in the query files list...
        qname= qfile.split('/')
        qname=qname[len(qname)-1]
        qname = qname[0:-4] #remove extension
       
        if params['pq']:
	    distance = pickle.load(open(os.path.join(params['pathToStoreDistances_pq'],qname+'.obj'),'rb'))
        else:
	    if params['feattype'] == 'CNN':
  	        distance = computeDistances(DESCRIPTORS[namesFrames == qname],qname)
      
	    else:
		distance = pickle.load(open(os.path.join(params['pathToStoreDistances_BoW'],qname+'.obj'),'rb'))

	if np.shape(namesFrames)[0]>maxNumFeats:
            distance = np.reshape(distance,np.shape(namesFrames)[0])
	
	
	qdistances.append(distance)
   
  
    #Take the minimum distance for each frame between all the query images    
    # distance = np.amin(np.array(qdistances),axis=0)

    #Take the minimum distance for each frame between all the query images    
    distance = np.average(np.array(qdistances),axis=0)

    #Obtain the ranking
   
    ranking_info = computeRanking(distance,namesShots,namesFrames,numQueries,N)
    ranking_info = np.array(ranking_info)
  
    shots=ranking_info[2,:]
    frames=ranking_info[1,:]
    distances=ranking_info[0,:]
   
    return list(shots),list(frames),list(distances)


def query_descriptors():

    if params['feattype'] == 'CNN':

        path_to_descriptors = createDir(params['pathToQDescriptors'])
    else:

	path_to_descriptors = createDir(params['pathToQDescriptors_BoW'])
	clust_outfile=os.path.join('/home/amaia/work/output/','Clustering'+params['detector'] + '_' + params['descriptor'] + '.km')


    for qfile in os.listdir(params['pathToQueryFiles']): # For all the images in the query files list...

        qname= qfile.split('/')
        qname=qname[len(qname)-1]
        if qname.split('.')[2] == 'src' and int(qname.split('.')[0])<9099:
  	    
           
            # Obtain descriptor and save it
 	    if params['feattype'] == 'CNN':

		file_to_save = os.path.join(params['pathToQDescriptors'],qname.split('.png')[0] + '.obj')
                descriptors = computeDLfeature( os.path.join(params['pathToQueryFiles'],qfile), file_to_save, params['crop_flag'],True)
	    else:
	        file_to_save = os.path.join(params['pathToQDescriptors_BoW'],qname.split('.png')[0] + '.obj')
		feats = featQuery( os.path.join( params['pathToQueryFiles'], qfile ) ,qname ,params['detector'],params['descriptor'],clust_outfile,params['K'],False)
		pickle.dump(feats,open(file_to_save,'wb'))
		
    
    
def build_descriptors_matrix(need_to_load=True):

    #query_descriptors()
  

    if need_to_load:
        print "Loading for targets..."
    
        if params['pq'] and params['compute_pq']:
	    DESCRIPTORS, namesFrames, namesShots = loadDescriptorsTargets()
            computeProductQuantization(DESCRIPTORS, namesFrames, namesShots, params['pathToTargetImages'], params['pathToTargetDescriptors'], params['codesize'], params['n_splits'] , params['pqpath'])
            DESCRIPTORS, namesFrames, namesShots = loadDescriptorsTargets(False)
        if params['pq'] and not params['compute_pq']:
            DESCRIPTORS, namesFrames, namesShots = loadDescriptorsTargets(False)
        if not params['pq']:
            DESCRIPTORS, namesFrames, namesShots = loadDescriptorsTargets()
        
        
        print "Done. Loading query descriptors..."
        DESCRIPTORS, namesFrames, namesShots, numQueries= loadDescriptorsQueries(DESCRIPTORS,namesFrames,namesShots)
    
        DESCRIPTORS = np.array( DESCRIPTORS)
    
        if rootCNN:
            DESCRIPTORS = np.sqrt(DESCRIPTORS)

        namesFrames = np.array( namesFrames )
        namesShots = np.array( namesShots )
        print "Done."

	if params['normalize_CNN'] and not params['pq']: #we normalize only if it's not PQ

            print "Normalizing..."
        
	    nrm = Normalizer(copy=False)
	    model_norm = nrm.fit(DESCRIPTORS)

 	    if np.shape(DESCRIPTORS)[0]>maxNumFeats:
 
	        for n in range(np.shape(DESCRIPTORS)[0]):
		 
	            DESCRIPTORS[n,:] = model_norm.transform(np.reshape(DESCRIPTORS[n,:],(1,np.shape(DESCRIPTORS[n,:])[0])))
	        
            else:
	        DESCRIPTORS = model_norm.transform(DESCRIPTORS)
	
            print "Done!"

        f = file(params['file_with_data'],'wb')
        np.save(f,DESCRIPTORS)
        np.save(f,namesFrames)
        np.save(f,namesShots)
	np.save(f,numQueries)
        f.close()
    else:
	print "Loading from big file..."
	f = file(params['file_with_data'],'rb')
        DESCRIPTORS = np.load(f)
        namesFrames = np.load(f)
        namesShots = np.load(f)
	numQueries = np.load(f)
        f.close()

    
    return np.array(DESCRIPTORS), namesFrames, namesShots, numQueries


def init_database(compute_flag=True):
    global DESCRIPTORS
    if compute_flag:

        print "1. Loading descriptors ..."
        ts = time.time()
        DESCRIPTORS, namesFrames, namesShots, numQueries = build_descriptors_matrix(False)
        te=time.time()
        print "Done. Elapsed time: ", te - ts , "seconds."

  
        print "2. Computing pairwise distances..."
	if params['feattype'] == 'CNN':
            distances_path = createDir(params['pathToStoreDistances'])
        else:
	    distances_path = params['pathToStoreDistances_BoW']
	    DESCRIPTORS = np.reshape(DESCRIPTORS, (np.shape(DESCRIPTORS)[0],np.shape(DESCRIPTORS)[2]))

        createDir(distances_path)
        if not params['pq']:

	    range_=range(np.shape(DESCRIPTORS)[0]-numQueries,np.shape(DESCRIPTORS)[0])
	   
	    pool = Pool(cpu_count())
            for n in range_:
	     
	        pool.apply_async(computeDistances, args = (n,os.path.join(distances_path,namesFrames[n] + '.obj') ) )

	    pool.close()
	    pool.join()
         

        else: # pq distance

            range_=range(np.shape(DESCRIPTORS)[0]-numQueries,np.shape(DESCRIPTORS)[0])
            codebook = pickle.load(open(params['pqpath']+'pq_codebook_'+str(params['codesize'])+'_'+str(params['n_splits'])+'.pkl','r'))
  	    
    	    createDir(params['pathToStoreDistances_pq'])
            for n in range_: # for all queries
   		print namesFrames[n]
		distance = []
                for nn in range(np.shape(DESCRIPTORS)[0]): #compute distance to all elements in the database
		    #np.reshape(DESCRIPTORS[n,:],(1,np.shape(DESCRIPTORS)[1]))
		    
		    distance.append(pq_distance(DESCRIPTORS[n,:],DESCRIPTORS[nn,:],'sdc',codebook,params['n_splits'],params['codesize']))
		distance = np.array(distance)
		print "Stored distance for", namesFrames[n]
	        pickle.dump(distance,open(os.path.join(params['pathToStoreDistances_pq'],namesFrames[n] + '.obj'),'wb'))
		

    else:

	ts = time.time()
        DESCRIPTORS, namesFrames, namesShots, numQueries = build_descriptors_matrix(False)
        te=time.time()

    
    return DESCRIPTORS, namesFrames, namesShots, numQueries

def saveshots(names,file_to_save):
    
    for item in names:
        file_to_save.writelines(str(item) + '\n')
    file_to_save.close()
    
    

if __name__ =='__main__':
    compute = True
    N = 1000
    createDir(params['pathRankings'])
    DESCRIPTORS, namesFrames, namesShots, numQueries = init_database(False)

    file_to_write_ranking = open(os.path.join(params['pathRankings'], 'rankingfile_'+which_data),'w')
    if random_:
        shots = sorted(np.unique(namesShots[0:np.shape(namesShots)[0]-numQueries]), key=lambda k:random.random())
    
    namesQueries = namesFrames[np.shape(namesFrames)[0]-numQueries:np.shape(namesFrames)[0]]
    print "Data is ready now. Obtaining rankings..."
    
    ap_list=[]
    
    for query in np.arange(9069,9099):
	queryfiles = []
	
        
        for q in namesQueries:
	    if str(query) in q:
		
                queryfiles.append(os.path.join(params['pathToQueryFiles'],q+'.png'))
        
        queryname = str(query)
       
      
	if not random_:
	       
	    ts = time.time()
            shots,frames,distances = return_ranking_query(DESCRIPTORS,queryfiles,namesFrames,namesShots,numQueries,N)
	    print "Elapsed time to return the ranking:", time.time() - ts
	    rankingfile(file_to_write_ranking,queryname,shots,np.array(distances))
       
        
        ap = AveragePrecision(params['fileGT'], queryname, shots,N)
	ap_list.append(ap)
	#print frames[np.where(shots =='shot5_1469')]
	
        #plotHistograms(distances,relist)
	print queryname, ap

    file_to_write_ranking.close()
    print "============"
    print "mAP", np.mean(ap_list)
  
      
    
    
    
    