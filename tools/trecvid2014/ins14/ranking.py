import numpy as np
import evaluation as eval

def LoadFeaturesGroundTruthSubset(filename,include_queries=False):

    '''Loads and returns features, keyframe names shot names for the ground truth subset (queries included in the end). Also returns the number of queries

    Takes:
    filename - the binary file to read the data
    include_queries - False (default) excludes the query data True( returns everything)

    '''

    with open(filename, 'rb') as f:
        descriptors = np.load(f)
        keyframe_ids = np.load(f)
        shot_ids = np.load(f)
        n_queries = np.load(f)


    if include_queries:
        return descriptors, shot_ids, keyframe_ids, n_queries

    else:
        return descriptors[0:len(shot_ids) - n_queries], shot_ids[0:len(shot_ids)- n_queries], keyframe_ids[0:len(shot_ids)- n_queries], n_queries


def unique_shots(ranking):
    # given a shot list, ensures that there are no repetitions. takes each name in the list and adds it to a second list if it's not already there. This way each shot only appears once.
    unique_shots = []

    for shot in ranking:
        if shot not in unique_shots:
            unique_shots.append(shot)

    return unique_shots

if __name__ == '__main__':

    data_path = '/imatge/asalvador/work/trecvid/'

    ground_truth_file = data_path + 'ins.search.qrels.tv13'
    descriptors_matrix = data_path + 'DESCRIPTORS_subset.bin'

    queries_2013 = range(9069,9099)

    descriptors, shot_ids, keyframe_ids, n_queries = LoadFeaturesGroundTruthSubset(descriptors_matrix,include_queries=True)

    print 'Loaded', np.shape(descriptors), 'descriptors.'
    print 'Number of queries: ', n_queries

    # This will be used to find and select the query descriptors
    keyframe_ids = list(keyframe_ids)
    
    # Remove query names from this list, since it will be sorted according to the distances and evaluated
    shot_ids = shot_ids[0:len(shot_ids)- n_queries]
    print "Average Precision / query"

    mAP = 0
    for query in queries_2013:

        # Initialize array of distances
        distance = np.zeros((np.shape(descriptors)[0] - n_queries))

        # Every query has 4 examples
        for num in range(4):

            # Build the string name
            sample_query_name = str(query) + '.' + str(num+1) + '.src'

            # Extract the feature
            sample_feature = descriptors[ keyframe_ids.index(sample_query_name), :] # Extract the sample query descriptor

            # Add up to distance array
            distance += -np.dot(descriptors[0:len(keyframe_ids) - n_queries], sample_feature)

        # Compute average distance
        distance = distance / 4


        # Sort the shots according to their average distance to the query
        ranking = shot_ids[np.argsort(distance)]

        # Remove duplicates (each shot contains several keyframes!)
        ranking = unique_shots(ranking)

        # Evaluation:

        labels, num_relevant = eval.relnotrel(ground_truth_file, str(query), ranking)

        # Note: If you want to compute the AP of the first N elements, just cut 'labels')

        ap = eval.AveragePrecision(np.squeeze(labels),num_relevant)
        mAP += ap
        print "Query", query, ':', ap

    print '============'
    print "Mean Average Precision:", mAP/len(queries_2013)






